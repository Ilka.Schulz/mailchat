FROM debian:11
LABEL maintanier "mailchat@schulz.com.de"
LABEL description "a chat application that communicates over email"
RUN apt-get update && \
    apt-get install -y libcurl4 libqt5widgets5 libqt5webkit5 libboost-iostreams1.74.0 gpg && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
COPY mailchat /usr/bin/mailchat
VOLUME ["$HOME/.mailchat"]
CMD ["/usr/bin/mailchat"]
