/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Contact.hpp"
#include "Account.hpp"
#include <filesystem>
#include <iostream>
#include "src/util/Properties.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/TristateBool.hpp"


const String Contact::propertyKeyId        = "Contact::id";
const String Contact::propertyKeyName      = "Contact::name";
const String Contact::propertyKeyEmail     = "Contact::email";
const String Contact::propertyKeyConfirmed = "Contact::confirmed";
std::vector<Contact*> Contact::allContacts;

Contact::Contact(ContactId _id, Account *_account, Properties properties)
        :  id(_id!=0 ? _id : generateNewId()),  account(nullptr)  {
    // set data from properties
    setProperties(properties);
    // set account via function (that informs the Account object as well)
    setAccount(_account);
    // register
    allContacts.push_back(this);

    // no need to inform observers. The owning account is informed via the setAccount() call above.
}
Contact::Contact(ContactId _id, Account *_account, String name, String email, TristateBool confirmed)
        : Contact(_id, _account, Properties({
                                                {Contact::propertyKeyName, name},
                                                {Contact::propertyKeyEmail, email},
                                                {Contact::propertyKeyConfirmed, TristateBool(confirmed).toString()}
                                            }))
{
}
Contact::~Contact()  {
    for(ContactObserver *observer : observers)
        observer->contactEvent_destroyedEvent(this);
    if(account!=nullptr)
        account->removeContact(this);
    auto itr = std::find(allContacts.begin(), allContacts.end(), this);
    if( itr != allContacts.end() )
        allContacts.erase(itr);
}

/// @todo make IDs account-dependant
Contact::ContactId Contact::generateNewId()  {
    int max=0;
    for(Contact *contact : allContacts)  ///< @todo sort @c Contact::allContacts
        if(contact->getId() > max)
            max = contact->getId();
    return max+1;
}

void Contact::setProperties(Properties properties)  {
    if(properties.has(propertyKeyId))  /// @todo does this if make sense?
        id = properties.get(propertyKeyId).toInt();
    setName(properties.get(propertyKeyName));
    setEmail(properties.get(propertyKeyEmail));
    setConfirmed(TristateBool(properties.get(propertyKeyConfirmed)));
}
Properties Contact::getProperties()  {
    Properties properties;
    properties.set(propertyKeyId,        std::to_string(id));
    properties.set(propertyKeyName,      getName());
    properties.set(propertyKeyEmail,     getEmail());
    properties.set(propertyKeyConfirmed, TristateBool(isConfirmed()).toString());
    return properties;
}


std::vector<Contact*> Contact::getAllContacts()  {
    return allContacts;
}

Contact::ContactId Contact::getId()  {
    return id;
}
Contact *Contact::getById(ContactId id)  {
    for(Contact *contact : allContacts)  /// @todo sort @c Contact::allContacts by id
        if(contact->getId() == id)
            return contact;
    throw Exception("could not find contact object with id "+std::to_string(id));
}

String Contact::getName()  {
    return name;
}
String Contact::getEmail()  {
    return email;
}
TristateBool Contact::isConfirmed()  {
    return confirmed;
}
Account *Contact::getAccount(){
    return account;
}

void Contact::setName(String name)  {
    this->name = name;
    for(ContactObserver *observer : observers)
        observer->contactEvent_nameChangedEvent(this);
}
void Contact::setEmail(String email)  {
    this->email = email;
    for(ContactObserver *observer : observers)
        observer->contactEvent_emailChangedEvent(this);
}
void Contact::setConfirmed(TristateBool confirmed)  {
    this->confirmed = confirmed;
    for(ContactObserver *observer : observers)
        observer->contactEvent_confirmedChangedEvent(this);
}
void Contact::setAccount(Account *newAccount)  {
    // unregister from old account
    if(account != nullptr)
        account->removeContact(this);
    // register new account
    if(newAccount != nullptr)
        newAccount->addContact(this);
    // update reference
    account = newAccount;
}

Contact *Contact::getEmailContact(Account *account, std::vector<AddressInfo> addressInfo)  {
    if(addressInfo.empty())
        throw Exception("address line does not have any addresses");

    /// @todo allow multiple addresses in @c Contact object
    if(addressInfo.size() > 1)
        throw Exception("cannot create Contact object for multiple addresses");

    // find existing contact
    Contact *contact = nullptr;
    for(Contact *c : account->getContacts())  {
        if(c->getEmail() == addressInfo[0].email)  {  ///< @todo allow multiple addresses in @c Contact object
            contact = c;
            break;
        }
    }
    if(contact == nullptr)  {
        // create new contact
        contact = new Contact(0, account, addressInfo[0].name, addressInfo[0].email, TristateBool::VAL_UNKNOWN);
    }
    return contact;
}
