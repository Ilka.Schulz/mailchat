/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Application.hpp"
#include <unistd.h>
#include <chrono>
#include "src/settings.hpp"
#include <iostream>
#include "src/util/exceptions/Exception.hpp"

#include "src/util/exceptions/ConnectionTestFailedException.hpp"

Application *Application::instance=nullptr;

Application::Application()
        : exitScheduled(false)  {
}

Application *Application::getInstance()  {
    if(instance == nullptr)
        instance = new Application();
    return instance;
}

void Application::run()  {
    while( ! exitScheduled )
        refresh();
}

void Application::exit()  {
    exitScheduled = true;
}

void Application::addHandler(void (*handler)())  {
    handlers.push_back(handler);
}

void Application::refresh(bool handleExceptions)  {
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    for(auto handler : handlers)  {
        if( ! handleExceptions )
            handler();
        else  {
            try {
                handler();
            }
            /// @todo add specific handlers for specific exeptions: e.g. try to reconnect if connections fails
            catch (ConnectionTestFailedException &e)  {
                std::cerr << "ConnectionTestFailedException flew into event loop: " << e.what() << std::endl;
            }
            catch (Exception &e) {
                std::cerr << "GenericException flow into event loop: " << e.what() << std::endl;
            }
            catch (std::exception &e) {
                std::cerr << "std::exception flew into event loop: " << e.what() << std::endl;
            }
        }
    }
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    unsigned long duration = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

    /// @todo move the usleep out of this method: This method is called by blocking code and maybe the code is blocking
    /// because it is ressource-intensive and not because it is waiting for network or so. In that case, the calling code
    /// really needs the CPU time for better things than sleeping.
    if(duration < Settings::desiredApplicationRunInterval)
        usleep( Settings::desiredApplicationRunInterval - duration );
}
