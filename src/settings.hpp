/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/util/String.hpp"
#include "src/util/Version.hpp"

namespace Settings  {
    // ---------- name and version ----------

    const String   applicationName = "Mailchat";  ///< the application's display name
    extern Version applicationVersion;  ///< application version

    /// information about the build containing CPU architecture or OS
    /// @todo this is hardcoded
    const String   buildSpecification = "amd64";

    const bool     debug = true;  ///< @todo set this during build

    const Version  gpgMinimumVersion("2.1.0");

    const String   gpgDownloadUrl =
        #if defined(_WIN32)
            "https://www.gpg4win.org/download.html";
        #else
            "https://www.gnupg.org/download/";
        #endif


    // ---------- network ----------

    /// the user agent tag to set if no other, more specific user agent settings applies
    const String   generalUserAgent = applicationName+" "+applicationVersion.getString();
    /// user agent tag for the imap client
    const String   imapUserAgent = generalUserAgent;
    /// user agent tag for the smtp client
    const String   smtpUserAgent = generalUserAgent;
    /// user agent tag for the HTTPS client
    const String   httpsUserAgent = generalUserAgent;

    /// TCP port used for IMAP
    /// @todo allow individual values for each @c ImapConnection
    const unsigned int  imapPort = 993;
    /// number of @c ImapFetchJob objects running concurrently
    /// @details This does not affect other @c CurlJob instances that are also related to IMAP, e.g. jobs created by
    /// @c ImapConnection
    const unsigned int  concurrentImapFetchJobs = 5;
    /// whether IMAP-related @c CurlJob objects shall print debug information to the console
    const unsigned long imapCurlVerbose = 0L;

    /// TCP port used for SMTP
    /// @todo allow individual values for each @c SmtpConnection
    const unsigned int  smtpPort = 587;
    /// whether SMTP-related @c CurlJob objects shall print debug information to the console
    const unsigned long smtpCurlVerbose = 0L;

    /// whether HTTPs-related @c CurlJob objects shall print debug information to the console
    const unsigned long httpsCurlVerbose = 0L;

    /// interval in microseconds between two Application::run calls
    /// @details Intervals may actually be longer if on Application::run call blocks that long.
    const unsigned long desiredApplicationRunInterval = 10000L;

    const String pgpKeyServer = "keys.openpgp.org";


    // ---------- file system ----------

    /// if true, the application shall write no data to disk. default: false
    extern bool         dryRun;

    /// @returns directory where to store application data, e.g. "/home/user/.mailchat/" under Qubes OS
    const String getBaseDirectory();

    /// character used to separate directories in a path (e.g. '/' or '\\')
    const char directorySeparator =
    #if defined(_WIN32)
        '\\';
    #else
        '/';
    #endif

    /// directory for storing @c Account objects and their associated data
    const String   diskAccountsDirectory = getBaseDirectory()+"accounts"+directorySeparator;
    /// directory for storing @c Message objects
    const String   diskMessagesDirectory = getBaseDirectory()+"messages"+directorySeparator;

}
