/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "base64.hpp"
#include "src/util/exceptions/macros.hpp"
#include <sstream>

const char *base64encodebuf::index_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

base64encodebuf::base64encodebuf(std::streambuf *sbuf)
        : sbuf_(sbuf)  {
}
/// @todo support more than 3 characters
void base64encodebuf::encode(char *src, std::streamsize n)  {
    if(n<=2)  // did not get third character
        src[2] = '\0';
    if(n<=1) // did not get second character
        src[1] = '\0';
    buffer_[0] = index_table[ (src[0] & 0b11111100)>>2 ];
    buffer_[1] = index_table[ ((src[0] & 0b00000011 )<<4) + ((src[1] & 0b11110000)>>4) ];
    buffer_[2] = index_table[ ((src[1] & 0b00001111)<<2) + ((src[2] & 0b11000000)>>6) ];
    buffer_[3] = index_table[ (src[2] & 0b00111111) ];
    if(n<=2)
        buffer_[3] = '=';
    if(n<=1)
        buffer_[2] = '=';
}
int base64encodebuf::underflow()  {
    if(this->gptr() == this->egptr())  {
        int n = sbuf_->sgetn(read_buffer_, 3);
        if(n!=0)  {
            encode(read_buffer_, n);
            this->setg(buffer_, buffer_, buffer_+4);  // only chaning gptr
            return std::char_traits<char>::to_int_type(*this->gptr());
        }
    }
    return this->gptr() == this->egptr()
             ? std::char_traits<char>::eof()
             : std::char_traits<char>::to_int_type(*this->gptr());
}
int base64encodebuf::overflow(int c)  {
    if(this->pptr() == this->epptr())
        sync();
    *this->pptr() = c;
    this->pbump(1);
    return c;
}
int base64encodebuf::sync()  {
    if(this->pptr() != nullptr)  {
        encode(this->pbase(), this->pptr()-this->pbase());
        sbuf_->sputn(buffer_, 4);
    }
    this->setp(write_buffer_, write_buffer_+3);
    return 0;
}



const std::string base64decodebuf::index_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

base64decodebuf::base64decodebuf(std::streambuf *sbuf)
    : sbuf_(sbuf)  {
}
int base64decodebuf::decode(char *src, std::streamsize n)  {
    /// @todo exceptions seem to get handled...
    if(n!=4)
        ERROR(std::logic_error,"unpadded base64 string: >"+std::to_string(n)+"<");

    int bytes_read=3;
    if(src[3]=='=')
        bytes_read--;
    if(src[2]=='=')
        bytes_read--;

    for(int i=0; i<n; i++)  {
        if(src[i]=='=')
            src[i] = 0;
        else  {
            size_t pos = std::string(index_table).find(src[i]);
            if(pos==std::string::npos)
                ERROR(std::logic_error,"invalid character in base64 string");
            src[i] = pos;
        }
    }
    buffer_[0] = ((src[0] & 0b111111)<<2) + ((src[1] & 0b110000)>>4);
    buffer_[1] = ((src[1] & 0b001111)<<4) + ((src[2] & 0b111100)>>2);
    buffer_[2] = ((src[2] & 0b000011)<<6) + ((src[3] & 0b111111));

    return bytes_read;
}
int base64decodebuf::underflow()  {
    if(gptr() == egptr())  {
        int n = sbuf_->sgetn(read_buffer_, 4);
        if(n!=0)  {
            std::streamsize bytes_read = decode(read_buffer_, n);
            setg(buffer_, buffer_, buffer_+bytes_read);
            return std::char_traits<char>::to_int_type(*gptr());
        }
    }
    return this->gptr() == this->egptr()
             ? std::char_traits<char>::eof()
             : std::char_traits<char>::to_int_type(*this->gptr());
}
int base64decodebuf::overflow(int c)  {
    if(this->pptr() == this->epptr())
        sync();
    *this->pptr() = c;
    this->pbump(1);
    return c;
}
int base64decodebuf::sync()  {
    if(this->pptr() != nullptr)  {
        std::streamsize n =this->pptr()-this->pbase();
        if(n==0)
            return 0;
        int bytes_read = decode(this->pbase(), n);
        sbuf_->sputn(buffer_, bytes_read);
    }
    this->setp(write_buffer_, write_buffer_+4);
    return 0;
}

#include <iostream>

std::string base64encode(const std::vector<char> &data)  {
    std::ostringstream ss;
        base64encodebuf b64buf(ss.rdbuf());
            std::ostream os(&b64buf);
                os.write(&data.front(), data.size());
        b64buf.sync();
    ss.flush();
    return ss.str();
}

std::vector<char> base64decode(const std::string &data, bool ignore_garbage)  {
    std::string retval;
    std::ostringstream ss;
        base64decodebuf b64buf(ss.rdbuf());
            std::ostream os(&b64buf);
                if(!ignore_garbage)
                    os.write(data.c_str(), data.size());
                else
                    for(const char &c : data)
                        if(c == '=' || base64decodebuf::index_table.find(c) != std::string::npos)
                            os.write(&c,1);
        b64buf.sync();
    ss.flush();
    retval = ss.str(); // std::ostringstream.str() does not return a reference to always the same instance...
    return std::vector<char>(retval.begin(), retval.end());
}


// tests
#include <iostream>
#undef NDEBUG
#include <assert.h>
int src_streams_base64(int,char**)  {
    const std::string plain = "Hello World";
    const std::string encoded = "SGVsbG8gV29ybGQ=";
    const std::vector<char> plain_vector(plain.begin(), plain.end());

    assert(base64encode(plain_vector)  == encoded);
    assert(base64decode(encoded) == plain_vector);

    /// @todo test ignore_garbage argument

    return 0;
}
