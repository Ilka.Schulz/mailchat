/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <streambuf>
#include <string>
#include <vector>

/** @brief a @c streambuf that encodes the data with base64
 */
class base64encodebuf  :  public std::streambuf  {
    private:
        /// string of 64 different "text" characters used for encoding
        static const char *index_table;

        /// the @c std::streambuf to read unencoded data from / write encoded data to
        std::streambuf *sbuf_;

        /// buffer of unencoded data read from @c base64encodebuf::sbuf_
        char read_buffer_[3];

        /// buffer of unencoded data to write (in encoded form) to @c base64encodebuf::sbuf_
        char write_buffer_[3];

        /// buffer of encoded data availabe to be read or flushed (see @c std::streambuf)
        char buffer_[4];


    public:
        /** @brief constructor
         *  @param sbuf  initializes @c base64encodebuf::sbuf_
         */
        base64encodebuf(std::streambuf *sbuf);


    private:
        /** @brief encodes from @c base64encodebuf::src into base64encodebuf::buffer_
         *  @param src  the unencoded data to encode
         *  @param n    the number of unencoded bytes to encode (must be in the range [1,3])
         */
        void encode(char *src, std::streamsize n);

    public:
        /** @brief implements @c std::streambuf::underflow
         *  @return see base class
         */
        virtual int underflow()  override;

        /** @brief implements @c std::streambuf::overflow
         *  @param c see base class
         *  @return see base class
         */
        virtual int overflow(int c)  override;

        /** @brief implements @c std::streambuf::sync
         *  @return see base class
         */
        virtual int sync() override;
};



/** @brief a @c streambuf that decodes base64-encoded data
 */
class base64decodebuf  :  public std::streambuf  {
    private:
        /// string of 64 different "text" characters used for the previous encoding
        static const std::string index_table;

        /// the @c std::streambuf to read encoded data from / write decoded data to
        std::streambuf *sbuf_;

        /// buffer of encoded data read from @c base64encodebuf::sbuf_
        char read_buffer_[4];

        /// buffer of encoded data to write (in decoded form) to @c base64encodebuf::sbuf_
        char write_buffer_[4];

        /// buffer of decoded data availabe to be read or flushed (see @c std::streambuf)
        char buffer_[3];

    public:
        /** @brief constructor
         *  @param sbuf  initializes @c base64decodebuf::sbuf_
         */
        base64decodebuf(std::streambuf *sbuf);

    private:
        /** @brief decodes from @c base64decodebuf::src into base64decodebuf::buffer_
         *  @param src  the encoded data to decode
         *  @param n    the number of encoded bytes to decode (must be in the range [4])
         *  @returns the number of decoded bytes
         */
        int decode(char *src, std::streamsize n);

    public:
        /** @brief implements @c std::streambuf::underflow
         *  @return see base class
         */
        virtual int underflow()  override;

        /** @brief implements @c std::streambuf::overflow
         *  @param c see base class
         *  @return see base class
         */
        virtual int overflow(int c)  override;

        /** @brief implements @c std::streambuf::sync
         *  @return see base class
         */
        virtual int sync()  override;

    friend std::vector<char> base64decode(const std::string &, bool);  // needs to access index_table
};

std::string base64encode(const std::vector<char> &data);
/// @todo decide whether to make ignore_garbage a property of the streambuf
std::vector<char> base64decode(const std::string &data, bool ignore_garbage=false);
