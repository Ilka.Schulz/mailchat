/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "quotedprintable.hpp"
#include <stdexcept>
#include "src/util/tictoc.hpp"

/// @todo maybe put this algorithm into its own library
std::vector<char> quotedprintabledecode(const String &encoded)  {
    tictoc timer = tictoc::construct(String(__PRETTY_FUNCTION__)+" ("+std::to_string(encoded.size())+" bytes)");
    std::vector<char> retval;
    std::vector<String> lines;
    {
        tictoc timer = tictoc::construct("splitIntoLines");
        lines = encoded.splitIntoLines(PLAIN);
    }
    for(const String &line : lines)  {
        // empty line => this is a regular line break (no soft line break)
        if(line.empty())  {
            retval.push_back('\n');
            continue;
        }
        // split
        std::vector<String> parts = line.split('=', PLAIN);

        // detect soft line break
        bool softLineBreak=false;
        if(parts.back().empty())  {  // line ends with '=' => so called 'soft line break'
            softLineBreak = true;
            parts.pop_back();
        }
        // should not be possible...
        if(parts.empty())  {
            /// @todo issue warning
            continue;
        }

        // process first part (does NOT begin with an escaped 8bit char)
        for(const char &c : parts.front())
            retval.push_back(c);
        parts.erase(parts.begin());

        // process further parts (begin with an escaped 8bit char each)
        for(const String &part : parts)  {
            // decode 8bit character
            if(part.size() < 2)
                throw ParseException("quoted-printable: encoded character is shorter than two hex digits");
            const String literals = "0123456789ABCDEF";
            size_t first_idx  = literals.find(part[0], CASE_INSENSITIVE),
                   second_idx = literals.find(part[1], CASE_INSENSITIVE);
            if(first_idx == std::string::npos  ||  second_idx == std::string::npos)
                throw ParseException(std::string(__PRETTY_FUNCTION__)+": illegal hex digits: >"+part[0]+"< and >"+part[1]+"<");
            retval.push_back((first_idx<<4) + second_idx);
            // add all other characters
            for(size_t i=2; i<part.size(); i++)
                retval.push_back(part[i]);
        }
    }
    return retval;
}


// tests
#include <iostream>
#include "src/util/misc.hpp"
#undef NDEBUG
#include <assert.h>
int src_streams_quotedprintable(int, char**)  {
    std::map<std::string,std::string> pairs  {
        {"Hello World!", "Hello=20World=21"},
        {"Hello\nWorld", "Hello=0AWorld"},
        {"Hello\nWorld", "Hello\nWorld"},
        {"Hello World" , "Hello=20=\nWorld"},
        {"Hello\nWorld", "Hello=0aWorld"}
    };
    size_t idx=0;
    for(const auto &pair : pairs)  {
        std::vector<char> decoded = quotedprintabledecode(pair.second);
        if(decoded != stringToVector(pair.first))  {
            std::cerr << "bad decoding of pair "<<idx<<": should be >"<<pair.first<<"<"<<std::endl;
            return 1;
        }
        idx++;
    }
    return 0;
}
