/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "Emoji.hpp"
#include "submodules/emojicpp/emoji.h"

template<typename T, typename U>
std::unordered_map<T,U> mapToUnorderedMap(std::map<T,U> src)  {
    std::unordered_map<T,U> ret;
    for(auto itr = src.begin(); itr != src.end(); itr++)
        ret[itr->first] = itr->second;
    return ret;
}
const std::unordered_map<std::string, std::string> Emoji::emojis = mapToUnorderedMap(emojicpp::EMOJIS);
const std::map<std::string, std::string> Emoji::ordered_emojis = emojicpp::EMOJIS;

std::string Emoji::convertEmoji(std::string raw)  {
    const auto itr = emojis.find(raw);
    if(itr == emojis.end())
        return raw;  /// @todo or should this throw an exception?
    return itr->second;
}
/// @todo compare performance with the algorithm in emojicpp namespace and maybe submit PR to project on GitHub
std::string Emoji::convertTextWithEmojis(std::string raw)  {
    size_t idx;  // position of the currently viewed colon
    size_t idx_end;  // position of the next colon
    for(idx=raw.find(':');  idx != std::string::npos;  idx = idx_end)  {
        idx_end = raw.find(':',idx+1);
        if(idx_end == std::string::npos)
            return raw;  // no next colon --> there cannot be any further emojis
        size_t len = idx_end-idx+1;  // length of the emoji
        if(len>=50)
            continue;  // there are no emojis longer than 50 spaces, thus safe performance
        std::string emoji = raw.substr(idx, len);
        const auto itr = emojis.find(emoji);
        if(itr == emojis.end())
            continue;  // not a valid emoji
        raw.replace(idx,len,itr->second);
    }
    return raw;
}
std::unordered_map<std::string,std::string> Emoji::getSuggestions(std::string search, size_t limit)  {
    /// @todo make more performant by making use of the ordered map container
    std::unordered_map<std::string,std::string> ret;
    // constraints
    if(search.empty())
        return emojis;
    // search
    for(const auto &pair : emojis)  {
        if(pair.first.find(search) != std::string::npos)  {
            ret.insert(pair);
            if(ret.size() == limit)
                break;
        }
    }
    return ret;
}
