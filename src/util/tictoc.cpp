/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "tictoc.hpp"
#include <stack>
#include <chrono>
#include <iostream>
#include <assert.h>
#include "src/settings.hpp"

size_t tictoc::n_existing = 0;

tictoc::tictoc(String _description, bool _enclose)
        : tic_point(std::chrono::high_resolution_clock::now()),
          description(_description),
          indentation(n_existing),
          enclose(_enclose)  {
    if( ! Settings::debug )
        return ;
    if(enclose)  {
        for(size_t i=0; i<indentation; i++)
            std::cerr<<"    ";
        std::cerr << "tic"
                  << (description.empty() ? "" : " ("+description+")") << std::endl;
    }
    n_existing++;
}
tictoc::~tictoc()  {
    if( ! Settings::debug )
        return ;
    unsigned long microseconds =
        std::chrono::duration_cast<std::chrono::microseconds> (
                    std::chrono::high_resolution_clock::now() - tic_point
                    ).count();
    for(size_t i=0; i<indentation; i++)
        std::cerr<<"    ";
    //std::cerr.imbue(std::locale(std::cerr.getloc(), new std::numpunct<char>()));
    std::cerr << (enclose ? "toc" : "tictoc")
              << (description.empty() ? "" : " ("+description+")")
              << " : " << microseconds << " µs" << std::endl;
    n_existing--;
}

tictoc tictoc::construct(String description, bool enclose)  {
    return tictoc(description, enclose);
}
