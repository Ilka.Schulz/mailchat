/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <string>
#include <vector>
#include <iostream>

enum ParseFlags  {
    // search and comparison functions will not care about case
    // applies to compare,equals,has,beginsWith,endsWith,find,rfind,findFirst,findLast (list may be incomplete)
    CASE_INSENSITIVE = 0x1,

    /// @todo respect quotes
    /*
    // split functions will never split a quoted string,
    // e.g. split("\"abc def\" ghi", ' ') will return {"\"abc def\"", "ghi"}
    // applies to split,splitIntoLines (may be incomplete)
    SINGLE_QUOTED_IS_LITERAL = 0x10,
    DOUBLE_QUOTED_IS_LITERAL = 0x20,
    QUOTED_IS_LITERAL        = SINGLE_QUOTED_IS_LITERAL | DOUBLE_QUOTED_IS_LITERAL,*/

    // split functions will ignore empty parts
    // applies to split,splitIntoLines (list may be incomplete)
    SKIP_EMPTY_FIRST          = 0x100,
    SKIP_EMPTY_LAST           = 0x200,
    SKIP_EMPTY_FIRST_AND_LAST = SKIP_EMPTY_FIRST | SKIP_EMPTY_LAST,
    SKIP_ALL_EMPTY            = 0x400,

    PLAIN = 0,
};

class String  {
    private:
        std::string content;

    public:
        // ---------- types ----------
        typedef std::string::reference              reference;
        typedef std::string::const_reference        const_reference;
        typedef std::string::pointer                pointer;
        typedef std::string::const_pointer          const_pointer;
        typedef std::string::iterator               iterator;
        typedef std::string::const_iterator         const_iterator;
        typedef std::string::reverse_iterator       reverse_iterator;
        typedef std::string::const_reverse_iterator const_reverse_iterator;
        static constexpr auto npos = std::string::npos;

    public:
        // ---------- constructors ----------
        String() = default;
        String(const String &src);
        String(const std::string &src);
        String(const char *src);
        String(const char *src, size_t len);
        String(const char src);
        String(const int src);

    public:
        // ---------- reimplemented ----------
        bool empty()  const;
        size_t size()  const;
        const_reference front()  const noexcept;
        const_reference back()  const noexcept;
        const_iterator  begin()  const noexcept;
        iterator        begin()  noexcept;
        const_iterator  end()  const noexcept;
        iterator        end()  noexcept;
        reference       at(size_t i);
        const_reference at(size_t i)  const;
        reference       operator[](size_t i);
        const_reference operator[](size_t i) const;
        const String    operator+ (const String &other)  const;
        String         &operator+=(const String &other);
        bool            operator< (const String &other)  const;
        bool            operator> (const String &other)  const;

        // no operator==, use equals() instead!
        bool            operator==(const String &other)  const  { return content==other.toStdString(); }

        String          substr(size_t idx, size_t len=npos)  const;
        String         &erase(size_t __pos = 0, size_t __n = npos);
        iterator        erase(const_iterator __position);
        iterator        erase(const_iterator __first, const_iterator __last);

        // ---------- comparison ----------
        int  compare(const String &other, const ParseFlags &flags)  const;
        bool equals(const String &other, const ParseFlags &flags)  const;
        bool unequal(const String &other, const ParseFlags &flags)  const;
        bool has(const String &substring, const ParseFlags &flags)  const;
        bool beginsWith(const String &substring, const ParseFlags &flags)  const;
        bool beginsWith(const std::vector<String> &substrings, const ParseFlags &flags)  const;
        bool endsWith(const String &substring, const ParseFlags &flags)  const;
        bool endsWith(const std::vector<String> &substrings, const ParseFlags &flags)  const;

        // ---------- search ----------
        size_t find(const String &needle, const ParseFlags &flags)  const;
        size_t find(const String &needle, size_t startIdx, const ParseFlags &flags)  const;
        size_t rfind(const String &needle, const ParseFlags &flags)  const;
        size_t rfind(const String &needle, size_t startIdx, const ParseFlags &flags)  const;

        size_t findFirst(const std::vector<String> &needles, const ParseFlags &flags)  const;  ///< @returns position of the first needle found in @c this
        size_t findLast(const std::vector<String> &needles, const ParseFlags &flags)  const;   /// @returns position of the last needle found in @c this

        // ---------- replace ----------
        void replace(size_t pos, size_t n, String replacement);
        void replaceAll(String needle, String replacement, const ParseFlags &flags);  ///< @todo support flags

        // ---------- split ----------
        std::vector<String> split(String delimiter, const ParseFlags &flags, size_t nMaxParts=0)  const;  ///< @todo support flags
        std::vector<String> splitIntoLines(const ParseFlags &flags, size_t nMaxParts=0)  const;  ///< @todo support flags

        // ---------- transform ----------
        operator       std::string&();//  { return content; }
        operator const std::string&()  const;//  { return content; }
              std::string &toStdString();
        const std::string &toStdString()  const;
        const char *c_str()  const;
        String toLower()  const;
        String toUpper()  const;
        String preview(const size_t max_len, const String &placeholder="...")  const;
        int toInt()  const;

        // ---------- trim ----------
        /// removes whitespace
        bool trim(const ParseFlags &flags=PLAIN);
        /// @brief deletes an entire dust particle (not individual characters)
        /// @return whether the string was altered
        bool trim(String dust, const ParseFlags &flags);
        /// @brief deletes several entire dust particles
        /// @return whether the string was altered
        bool trim(std::vector<String> dust, const ParseFlags &flags);

        bool trimQuotes();
        bool trimMatchingQuotes();  /// @todo use this short-hand function everywhere

        bool trimAtStart(String dust, const ParseFlags &flags);
        bool trimAtStart(std::vector<String> dust, const ParseFlags &flags);

        bool trimAtEnd(String dust, const ParseFlags &flags);
        bool trimAtEnd(std::vector<String> dust, const ParseFlags &flags);
};

// ---------- operators ----------
String operator+(const char *a, const String &b);
std::basic_ostream<char> &operator<<(std::basic_ostream<char> &stream, const String &string);

/// @brief defines @c std::hash for @c String
/// @details This is needed for @c std::map, etc.
template<>
struct std::hash<String>  {
    std::size_t operator()(String const& s) const noexcept  {
        return std::hash<std::string>{}(s.toStdString());
    }
};

#include "src/util/exceptions/ParseException.hpp"  // include here so that calling code already has ParseException in its scope
