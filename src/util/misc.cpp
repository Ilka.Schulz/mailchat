/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "misc.hpp"
#include <fstream>
#include <sstream>
#include "src/util/exceptions/Exception.hpp"
#include <cassert>


std::vector<AddressInfo> addressLineToInfo(String line)  {
    /// @todo clean this up
    std::vector<AddressInfo> infos;
    for(String address : line.split(',',PLAIN))  {  /// @todo respect quotes!
        AddressInfo info;
        address.trim();
        std::size_t pos = address.find("<",PLAIN);  /// @todo respect quotes!
        if(pos != String::npos)  {
            info.name = address.substr(0,pos);
            info.name.trim();
            info.email = address.substr(pos+1, address.size()-pos-2);
        }
        else  {
            info.name = "";
            info.email = address;
        }
        infos.push_back(info);
    }
    return infos;
}

String readFromTextFile(String fn)  {
    std::stringstream s;
    std::ifstream f(fn.c_str(), std::ios::binary);
        assert(f);  ///< @todo delete this object AND remove it from index AND remove it ImapUid
        while(f)  {   /// @todo is this efficient?
            char c;
            f.read(&c, 1);
            if(f)
                s.write(&c, 1);
        }
    f.close();
    String ret = s.str();
    return ret;
}
void writeToTextFile(String fn, String content)  {
    // DO NOT respect Settings::dryRun, see function description
    std::ofstream f(fn.c_str());
        assert(f);
        f.write(content.c_str(), content.size());
    f.close();
}

String base_name(const std::filesystem::__cxx11::path &path)  {
    String s = std::filesystem::path(path).filename().string();
    size_t pos = s.find(".",PLAIN);
    if(pos == String::npos)
        return s;
    return s.substr(0, pos);
}

std::vector<char> stringToVector(const String &str)  {
    return std::vector<char>(str.begin(), str.end());
}
String vectorToString(const std::vector<char> &vec)  {
    return String(vec.data(), vec.size());
}
