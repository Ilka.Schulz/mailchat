/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "disk.hpp"

#include <filesystem>
#include <fstream>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include "src/streams/base64.hpp"
#include "src/util/Properties.hpp"

#include "src/util/exceptions/Exception.hpp"
#include "src/util/misc.hpp"

#include "src/Account.hpp"
#include "src/Contact.hpp"
#include "src/connections/Connection.hpp"
#include "src/connections/EmailConnection.hpp"
#include "src/connections/GenericConnection.hpp"

#include "src/messages/EmailMessage.hpp"

#include "src/settings.hpp"

const String Disk::propertyKeyConnections = "connections";
const String Disk::propertyKeyContacts    = "contacts";


void Disk::saveAllDataToDisk()  {
    for(Account *account : Account::getAllAccounts())
       saveAccountToDisk(account);
}
void Disk::saveAccountToDisk(Account *account)  {
    if(Settings::dryRun)
        throw std::logic_error(String(__PRETTY_FUNCTION__)+" should not get called while Settings::dryRun is on.");
    std::ofstream f_raw((Settings::diskAccountsDirectory+account->getId()+".gz").c_str(), std::ios::binary);
        boost::iostreams::filtering_ostreambuf gzipbuf;
        gzipbuf.push(boost::iostreams::gzip_compressor(boost::iostreams::gzip::default_compression));
        gzipbuf.push(f_raw);
            std::ostream f(&gzipbuf);
                // account
                writePropertiesToStream(f, account->getProperties());

                // connections
                f << propertyKeyConnections << "=";
                for(Connection *connection : Connection::getAllConnections())
                    if(connection->getAccount() == account)  {
                        base64encodebuf b64buf(f.rdbuf());
                            std::ostream os(&b64buf);
                                writePropertiesToStream(os, connection->getProperties());
                        b64buf.sync();
                        f << ";";
                    }
                f.write("\n", 1);

                // contacts
                f << propertyKeyContacts << "=";
                for(Contact *contact : Contact::getAllContacts())  {
                    if(contact->getAccount() == account)  {
                        base64encodebuf b64buf(f.rdbuf());
                            std::ostream os(&b64buf);
                                writePropertiesToStream(os, contact->getProperties());
                        b64buf.sync();
                        f << ";";
                    }
                }
                f.write("\n",1);
            f.flush();
        boost::iostreams::close(gzipbuf);
    f_raw.close();
}
void Disk::loadAllFromDisk()  {
    // accounts
    for( auto entry : std::filesystem::directory_iterator(Settings::diskAccountsDirectory.toStdString()))
        if(entry.path().filename().string().substr(0,1) != ".")  {  // ignore hidden files
            Account *account = loadAccountFromDisk(entry);
            std::filesystem::create_directories( (Settings::diskMessagesDirectory+account->getId()).toStdString() );
        }

    // messages
    for(auto entry : std::filesystem::directory_iterator(Settings::diskMessagesDirectory.toStdString()))  {
        Account *account = Account::getById(base_name(entry.path()));  /// @todo handle expetion: account may not exist anymore but messages may still be cached in the directory
        String fn = entry.path().string()+"/index";
        if( ! std::filesystem::exists(fn.toStdString()) )
            continue;
        std::ifstream f;
        //f.exceptions(std::ios::failbit);
        f.open(fn.c_str(), std::ios::binary);  ///< @todo handle exception
            while(f)  {
                // see Message::addToIndex for writing correspondant
                Message::MessageId id;
                f.read((char*)&id, sizeof(id));
                Message::MessageFlags flags;
                f.read((char*)&flags, sizeof(flags));
                time_t timeStamp;
                f.read((char*)&timeStamp, sizeof(timeStamp));
                Contact::ContactId contactId;
                f.read((char*)&contactId, sizeof(contactId));
                if(!f)
                    continue;
                /// @todo read type
                new EmailMessage(id, flags | Message::FLAG_SHALLOW, "", Contact::getById(contactId), timeStamp, "");
            }
        f.close();
    }
    /*std::filesystem::create_directories(Settings::diskMessagesDirectory);
    for(auto entry : std::filesystem::directory_iterator(Settings::diskAccountsDirectory))  {
        Account *account = Account::getById(base_name(entry.path()));
        for(auto fileEntry : std::filesystem::directory_iterator( Settings::diskMessagesDirectory+account->getId() ))
            if(entry.path().extension() == ".raw_full")
                EmailMessage::createFromRawFullMessage(readFromTextFile(entry.path()), account);
    }*/
}
Account *Disk::loadAccountFromDisk(std::filesystem::directory_entry &entry)  {
    String id = std::string(entry.path().filename());
    size_t dotpos = id.find(".",PLAIN);
    if(dotpos != String::npos)
        id = id.substr(0, dotpos);
    String path = entry.path().string();

    // read properties
    std::ifstream f_raw(path.c_str());
        boost::iostreams::filtering_istreambuf gzipbuf;
        gzipbuf.push(boost::iostreams::gzip_decompressor());
        gzipbuf.push(f_raw);
            std::istream f(&gzipbuf);
                Properties properties = readPropertiesFromStream(f);
        boost::iostreams::close(gzipbuf);
    f_raw.close();

    // health checks
    /// @todo check if PGP key exists
    if( ! properties.has(Account::propertyKeyEmail) )
        throw Exception("invalid file "+path+": does not contain email key");
    if( ! properties.has(Account::propertyKeyName)  )
        throw Exception("invalid file "+path+": does not contain name key");
    if( ! properties.has(propertyKeyConnections)    )
        throw Exception("invalid file "+path+": does not contain connections key");
    if( ! properties.has(propertyKeyContacts)       )
        throw Exception("invalid file "+path+": does not contain contacts key");
    // account
    Account *account = new Account(id, properties, false);
    // connections
    {
        for(const String &encodedConnectionProperties : properties.get(propertyKeyConnections).split(';',SKIP_EMPTY_LAST))  {
            std::stringstream encodedConnectionPropertiesStream(encodedConnectionProperties);
                base64decodebuf b64buf(encodedConnectionPropertiesStream.rdbuf());
                    std::istream is(&b64buf);
                        Properties properties = readPropertiesFromStream(is);
                        new GenericConnection(account, properties);
        }
    }
    // contacts
    {
        for(const String &encodedContactProperties : properties.get(propertyKeyContacts).split(';',SKIP_EMPTY_LAST))  {
            std::stringstream encodedContactPropertiesStream(encodedContactProperties);
                base64decodebuf b64buf(encodedContactPropertiesStream.rdbuf());
                    std::istream is(&b64buf);
                        new Contact(0, account, readPropertiesFromStream(is));
        }
    }
    return account;
}
