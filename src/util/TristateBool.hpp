/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/util/String.hpp"

class TristateBool;

/** @brief an alternative to @c bool that allows a third "unknown" option
 */
class TristateBool  {
    public:
        /// enumeration of the allowed values
        enum TristateBoolValue  { VAL_FALSE=0, VAL_TRUE=1, VAL_UNKNOWN=2 };

    private:
        /// the value of the object
        TristateBoolValue value;

    public:
        /// default contructor – sets value to @c TristateBool::TristateBoolValue::VAL_UNKNOWN
        TristateBool();

        /** @brief constructor
         *  @param source  the value to copy
         */
        TristateBool(bool source);

        /** @brief copy constructor
         *  @param source  the source to copy
         */
        TristateBool(const TristateBool &source);

        /** @brief constructor
         *  @param _value  the value of the constructed object
         */
        TristateBool(TristateBool::TristateBoolValue _value);

        /** @brief constructor
         *  @param _value  string representing a @c TristateBool value, see @c TristateBool::toString
         */
        TristateBool(String _value);

    public:
        /// @returns true if not equal to @c TristateBool::TristateBoolValue::VAL_UNKNOWN, false otherwise
        bool isKnown();
        /// @returns true if equal to @c TristateBool::TristateBoolValue::VAL_TRUE, false otherwise
        operator bool();
        /// @brief assignment operator  @param source  source to copy  @returns new value
        TristateBool &operator=(TristateBool source);
        /// @brief assignment operator  @param source  source to copy  @returns new value
        TristateBool &operator=(TristateBoolValue source);
        /// @brief assignment operator  @param source  source to copy  @returns new value
        TristateBool &operator=(bool source);

        bool operator==(TristateBool b);
        bool operator==(TristateBoolValue b);

        /// @returns string representation of this object
        String toString();
        /// @brief assignment operator
        /// @param str  string representing a @c TristateBool value, see @c TristateBool::toString
        /// @returns new value
        TristateBool &operator=(String str);
};
