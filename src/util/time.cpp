/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "time.hpp"


String Time::timeStampToText(const time_t t, const String &format)  {
    if(t == 0)
        return "unknown timestamp";
    tm *time_info = localtime(&t);
    char c_time_string[200];
    strftime(c_time_string, sizeof(c_time_string), format.c_str(), time_info);
    //delete time_info; that object is managed by the ctime library
    return c_time_string;
}
time_t Time::textToTimeStamp(const String &s, const String &format)  {
    // On the first call, the global `timezone` varialbe is not initialized yet (sic!). It gets initialized with mktime().
    static bool first_call=true;
    if(first_call)  {
        tm time_info;
        mktime(&time_info);
    }
    tm time_info{0,0,0,0,0,0,0,0,0,0,0};  // v.i.
    if(strptime(s.c_str(), format.c_str(), &time_info) == nullptr)
        return -1;  /// @todo maybe throw exception instead?
    long gmtoff = time_info.tm_gmtoff;
    time_info.tm_isdst=0;  // This is never initialized and may confuse mktime.

    // stupid mktime uses the global timezone instead of
    // tm::gtmoff. see https://www.gnu.org/software/libc/manual/html_node/Broken_002ddown-Time.html and
    // https://stackoverflow.com/questions/28991427/why-is-mktime-unsetting-gmtoff-how-to-make-mktime-use-the-gmtoff-field
    time_t retval = mktime(&time_info) - gmtoff - timezone;
    return retval;
}


// tests
#include <iostream>
#include <map>
#include "src/settings.hpp"
#undef NDEBUG
#include <assert.h>
int src_util_time(int,char**)  {
    Settings::dryRun=true;  // not really needed here but safe anyways
    const std::map<String, time_t> pairs  {
        {"Sun, 16 Jan 2022 20:18:06 +0100", 1642364286-3600},
        {"Sun, 16 Jan 2022 20:18:06 +0000", 1642364286},
        {"Sun, 16 Jan 2022 20:18:06"      , -1},
        {"foo",-1}
    };
    const size_t repetitions=100;  // Unfortunately, that stupid mktime is not deterministic, so the tests have to be
                                   // repeated a couple times.
    for(size_t i=0; i<repetitions; i++)  {
        for(const auto &pair : pairs)  {
            const time_t converted = Time::textToTimeStamp(pair.first, Time::emailDateFormat);
            const time_t &correct = pair.second;
            if(converted != correct)
                std::cerr << "converted: "<<converted<<", correct: "<<correct<<std::endl;
            assert(converted == correct);
        }
    }
    return 0;
}
