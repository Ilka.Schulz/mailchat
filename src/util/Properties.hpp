/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/util/String.hpp"
#include <map>


/** @brief a map of key-value pairs used for saving objects to disk or reading them from disk
 *  @details Use the @c Properties::set and @c Properties::get methods to access the elements safely.
 *  @todo consider passing @c properties as reference in each class that uses them
 *  @todo increase performance?
 */
class Properties  :  private std::map<String, String>  {
    public:
        Properties();
        Properties(Properties &src);
        Properties(std::initializer_list<std::pair<const String,String> > list);
        ~Properties();
    public:
        void set(String key, String value);
        void update(String key, String value);
        void remove(String key);
        String get(String key);
        bool has(String key);
        bool empty();

        std::map<String,String>::iterator begin();
        std::map<String,String>::const_iterator end();
};

/** @brief reads @c ::Properties list from input stream (in the form of "key=value" per line)
 *  @param stream  the input stream to read from
 *  @returns properties list read from  stream
 */
Properties readPropertiesFromStream(std::istream &stream);

/** @brief writes @c ::Properties list to output stream (in the form of "key=value" per line)
 *  @param stream      the output stream to write to
 *  @param properties  the properties list to write to stream
 */
void writePropertiesToStream(std::ostream &stream, Properties properties);

/** @brief merge two @c ::Properties lists
 *  @details See @c std::map::merge for more information.
 *  @param a  one @c ::Properties list
 *  @param b  another @c ::Properties list
 *  @param allow_duplicate  if true, the value to a duplicate key will be taken from @c b, otherwise throw an exception
 *  @returns a @c ::Properties list holding the items of both original lists
 */
Properties mergeProperties(Properties a, Properties b, bool allow_duplicate=false);
