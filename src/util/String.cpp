/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "String.hpp"
#include <algorithm>
#include <map>
#include "src/util/exceptions/macros.hpp"
#include "src/util/tictoc.hpp"
// ---------- constructors ----------
String::String(const String &src)
    : content(src.toStdString())  {
}
String::String(const std::string &src)
    : content(src)  {
}
String::String(const char *src)
    : content(src)  {
}
String::String(const char *src, size_t len)
    : content(src,len)  {
}
String::String(const char src)
    : content(&src,1)  {
}
String::String(const int src)
    : content((const char*)&src,1)  {
}

// ---------- reimplemented ----------
bool String::empty()  const  {
    return content.empty();
}
size_t String::size()  const  {
    return content.size();
}
std::string::const_reference String::front()  const noexcept  {
    return content.front();
}
std::string::const_reference String::back()  const noexcept  {
    return content.back();
}
std::string::const_iterator String::begin()  const noexcept  {
    return content.begin();
}
std::string::iterator String::begin()  noexcept  {
    return content.begin();
}
std::string::const_iterator String::end()  const noexcept  {
    return content.end();
}
std::string::iterator String::end()  noexcept  {
    return content.end();
}
std::string::reference String::at(size_t i)  {
    return content.at(i);
}
std::string::const_reference String::at(size_t i)  const  {
    return content.at(i);
}
std::string::reference String::operator[](size_t i)  {
    //return std::string::operator[](i);  this does not check the range!
    return content.at(i);
}
std::string::const_reference String::operator[](size_t i)  const  {
    //return std::string::operator[](i);  this does not check the range!
    return content.at(i);
}
const String String::operator+(const String &other)  const  {
    return content+other.toStdString();
}
String &String::operator+=(const String &other)  {
    content += other.toStdString();
    return *this;
}
bool String::operator<(const String &other)  const  {
    return content < other.toStdString();
}
bool String::operator>(const String &other)  const  {
    return content > other.toStdString();
}
String String::substr(size_t idx, size_t len)  const  {
    return content.substr(idx, len);
}
String &String::erase(size_t __pos, size_t __n)  {
    content.erase(__pos, __n);
    return *this;
}
String::iterator String::erase(const_iterator __position)  {
    return content.erase(__position);
}
String::iterator String::erase(const_iterator __first, const_iterator __last)  {
    return content.erase(__first, __last);
}

// ---------- comparison ----------
int String::compare(const String &other, const ParseFlags &flags) const  {
    const size_t max = std::min(size(), other.size());
    for(size_t i=0; i<max; i++)
        if(flags & CASE_INSENSITIVE ?
           std::tolower(at(i)) != std::tolower(other.at(i)) :
                        at(i)  !=              other.at(i)
        )
            return int(at(i)) - int(other.at(i));
    if(size() < other.size())
        return -1;
    if(size() > other.size())
        return +1;
    return 0;
}
bool String::equals(const String &other, const ParseFlags &flags) const  {
    return compare(other, flags) == 0;
}
bool String::unequal(const String &other, const ParseFlags &flags) const  {
    return ! equals(other,flags);
}
bool String::has(const String &substring, const ParseFlags &flags)  const  {
    return find(substring,0,flags) != std::string::npos;
}
bool String::beginsWith(const String &substring, const ParseFlags &flags)  const  {
    if(substring.size() > size())
        return false;
    for(size_t i=0; i<substring.size(); i++)
        if(flags & CASE_INSENSITIVE ?
           std::tolower(at(i)) != std::tolower(substring.at(i)) :
                        at(i)  !=              substring.at(i)
        )
            return false;
    return true;
}
bool String::beginsWith(const std::vector<String> &substrings, const ParseFlags &flags)  const  {
    for(String substring : substrings)
        if(beginsWith(substring,flags))
            return true;
    return false;
}
bool String::endsWith(const String &substring, const ParseFlags &flags)  const  {
    if(substring.size() > size())
        return false;
    for(size_t i=0; i<substring.size(); i++)
        if(flags & CASE_INSENSITIVE ?
           std::tolower(at(size()-1-i)) != std::tolower(substring.at(substring.size()-1-i)) :
                        at(size()-1-i)  !=              substring.at(substring.size()-1-i)
        )
            return false;
    return true;
}
bool String::endsWith(const std::vector<String> &substrings, const ParseFlags &flags)  const  {
    for(String substring : substrings)
        if(endsWith(substring,flags))
            return true;
    return false;
}

// ---------- search ----------
size_t String::find(const String &needle, const ParseFlags &flags)  const  {
    return find(needle, 0, flags);
}
size_t String::find(const String &needle, size_t startIdx, const ParseFlags &flags)  const  {
    if(startIdx+needle.size() > size() )
        return std::string::npos;
    for(size_t i=startIdx; i<=size()-needle.size(); i++)  {
        bool match=true;
        for(size_t j=0; j<needle.size()&&match; j++)
            if( flags&CASE_INSENSITIVE ?
                std::tolower(at(i+j)) != std::tolower(needle.at(j)) :
                at(i+j) != needle.at(j)
            )
                match = false;
        if(match)
            return i;
    }
    return std::string::npos;
}
size_t String::rfind(const String &needle, const ParseFlags &flags)  const  {
    return rfind(needle, size()-1, flags);
}
size_t String::rfind(const String &needle, size_t startIdx, const ParseFlags &flags)  const  {
    if(needle.size() > startIdx+1)
        return std::string::npos;
    if(needle.empty())  /// @todo is this really correct?
        return startIdx+1;
    for(size_t i=std::min(startIdx,size()-needle.size()); i!=std::string::npos; i--)  {
        bool match=true;
        for(size_t j=0; j<needle.size()&&match; j++)
            if( flags&CASE_INSENSITIVE ?
                std::tolower(at(i+j)) != std::tolower(needle.at(j)) :
                at(i+j) != needle.at(j)
            )
                match = false;
        if(match)
            return i;
    }
    return std::string::npos;
}
size_t String::findFirst(const std::vector<String> &needles, const ParseFlags &flags)  const  {
    size_t retval = std::string::npos;
    for(const String &needle : needles)  {
        size_t pos = find(needle, flags);
        if(  pos != std::string::npos  &&  ( pos < retval || retval==std::string::npos)  )
            retval = pos;
    }
    return retval;
}
size_t String::findLast(const std::vector<String> &needles, const ParseFlags &flags)  const  {
    size_t retval = std::string::npos;
    for(const String &needle : needles)  {
        size_t pos = rfind(needle, flags);
        if(  pos != std::string::npos  &&  ( pos > retval || retval==std::string::npos)  )
            retval = pos;
    }
    return retval;
}

// ---------- replace ----------
void String::replace(size_t pos, size_t n, String replacement)  {
    content.replace(pos, n, replacement);
}
void String::replaceAll(String needle, String replacement, const ParseFlags &flags)  {
    if(needle.empty())
        throw std::logic_error("String::replaceAll requires non-empty needle");
    while(true)  {
        size_t pos = find(needle,flags);
        if(pos == std::string::npos)
            return;
        replace(pos, needle.size(), replacement);
    }
}

// ---------- split ----------
std::vector<String> String::split(String delimiter, const ParseFlags &flags, size_t nMaxParts) const  {
    std::vector<String> retval;
    if(delimiter.empty() || delimiter.size() > size()  || nMaxParts==1)
        retval.push_back(*this);
    else  {
        size_t last_pos = 0;
        size_t pos = find(delimiter,flags);
        while( pos != std::string::npos )  {
            retval.push_back(substr(last_pos, pos-last_pos));
            last_pos = pos + delimiter.size();
            if(last_pos < size())
                pos = find(delimiter, last_pos, flags);
            else
                pos = std::string::npos;
            if(nMaxParts!=0 && retval.size() == nMaxParts-1)
                break;
        }
        if(last_pos <= size())
            retval.push_back(substr(last_pos));
        if(retval.empty())
            retval.push_back("");
    }

    if(flags & SKIP_EMPTY_FIRST  && ! retval.empty() && retval.front().empty())
        retval.erase(retval.begin());
    if(flags & SKIP_EMPTY_LAST   && ! retval.empty() && retval.back().empty())
        retval.pop_back();
    if(flags & SKIP_ALL_EMPTY)
        for(size_t i=0; i<retval.size(); i++)
            if(retval.at(i).empty())  {
                retval.erase(retval.begin()+i);
                i--;
            }

    return retval;
}
std::vector<String> String::splitIntoLines(const ParseFlags &flags, size_t nMaxParts) const  {
    std::vector<String> retval =split("\n",flags,nMaxParts);
    for(size_t idx=0; idx<retval.size()-1; idx++)
        retval[idx].trimAtEnd('\r',flags);  /// @todo maybe only remove ONE '\r'?

    /// @todo properly impelement SKIP_* flags
    return retval;
}

// ---------- transform ----------
String::operator std::string&()  {
    return content;
}
String::operator const std::string&()  const  {
    return content;
}
std::string &String::toStdString()  {
    return *this;
}
const std::string &String::toStdString() const  {
    return *this;
}
const char *String::c_str() const  {
    return content.c_str();
}
String String::toLower() const  {
    String retval = *this;
    std::transform(retval.begin(), retval.end(), retval.begin(), ::tolower);
    return retval;
}
String String::toUpper() const  {
    String retval = *this;
    std::transform(retval.begin(), retval.end(), retval.begin(), ::toupper);
    return retval;
}
String String::preview(const size_t max_len, const String &placeholder) const  {
    if(placeholder.size() > max_len)
        ERROR(std::logic_error, "placeholder string is longer than max_len ("+std::to_string(max_len)+")");
    if(size() <= max_len)
        return *this;
    return substr(0, max_len-placeholder.size()) + placeholder;
}
int String::toInt() const  {
    /// @todo better alternative to stoi
    try  {
    return stoi(content);
    } catch(...)  {
        ERROR(ParseException,"cannot convert "+*this+" to integer");
    }
}

// ---------- trim ----------
bool String::trim(const ParseFlags &flags)  {
    static const std::vector<String> whitespace = {' ','\t','\r','\n'};
    return trimAtStart(whitespace,flags) | trimAtEnd(whitespace,flags);
}
bool String::trim(String dust, const ParseFlags &flags)  {
    return trimAtStart(dust,flags) | trimAtEnd(dust,flags);
}
bool String::trim(std::vector<String> dust, const ParseFlags &flags)  {
    bool sth_done = false;
    bool sth_done_in_this_loop=true;
    while(sth_done_in_this_loop)  {
        sth_done_in_this_loop = false;
        for(String s : dust)
            if(trim(s,flags))  {
                sth_done = true;
                sth_done_in_this_loop = true;
            }
    }
    return sth_done;
}
bool String::trimQuotes()  {
    return trim({'\'','\"'}, PLAIN);
}
bool String::trimMatchingQuotes()  {
    /// @todo add '<'-'>' pair
    bool sth_done=false;
    bool sth_done_in_this_iteration;
    do  {
        sth_done_in_this_iteration=false;
        for(const std::pair<char,char> pair : std::map<char,char>({
                                                      {'\'','\''},
                                                      {'\"','\"'},
                                                      {'<' ,'>' }
                                                  }))  {
            if(size()>=2 && front()==pair.first && back()==pair.second)  {
                erase(begin());
                erase(end()-1);
                sth_done=true;
                sth_done_in_this_iteration=true;
            }
        }
    }
    while(sth_done_in_this_iteration);
    return sth_done;
}

bool String::trimAtStart(String dust, const ParseFlags &flags)  {
    if(dust.empty())
        throw std::logic_error("trying to trim empty dust");
    bool sth_done=false;
    while( ! empty() && find(dust,flags)==0)  {
        erase(0,dust.size());
        sth_done = true;
    }
    return sth_done;
}
bool String::trimAtStart(std::vector<String> dust, const ParseFlags &flags)  {
    bool sth_done = false;
    bool sth_done_in_this_loop=true;
    while(sth_done_in_this_loop)  {
        sth_done_in_this_loop = false;
        for(String s: dust)
            if(trimAtStart(s,flags))  {
                sth_done = true;
                sth_done_in_this_loop = true;
            }
    }
    return sth_done;
}
bool String::trimAtEnd(String dust, const ParseFlags &flags)  {
    if(dust.empty())
        throw std::logic_error("trying to trim empty dust");
    bool sth_done=false;
    while( ! empty() &&  size()>=dust.size() && rfind(dust,flags) == size()-dust.size() )  {
        erase(size()-dust.size(), dust.size());
        sth_done = true;
    }
    return sth_done;
}
bool String::trimAtEnd(std::vector<String> dust, const ParseFlags &flags)  {
    bool sth_done = false;
    bool sth_done_in_this_loop=true;
    while(sth_done_in_this_loop)  {
        sth_done_in_this_loop = false;
        for(String s: dust)
            if(trimAtEnd(s,flags))  {
                sth_done = true;
                sth_done_in_this_loop = true;
            }
    }
    return sth_done;
}


// ---------- operators ----------
String operator+(const char *a, const String &b)  {
    return String(a) + b;
}
std::basic_ostream<char> &operator<<(std::basic_ostream<char> &stream, const String &string)  {
    return stream << string.toStdString();
}


// ---------- tests ----------
#include <iostream>
#include "src/settings.hpp"
#undef NDEBUG
#include <assert.h>
/// @todo test toInt, replace, operators
int src_util_String(int, char**)  {
    Settings::dryRun = true;  // not really needed here but safe anyways
    const String abcdef = "abcdef";
    const String ffb = "foo foo bar";
    const String digits = "0123456789";
    String tmp;  // may have any value at any time

    // empty
    assert( ! abcdef.empty() );
    assert(String("").empty());

    // size
    assert(abcdef.size() == 6);
    assert(String("").size() == 0);

    // front
    assert(abcdef.front() == 'a');
    /// @todo test empty string
    /// @todo test manipulation

    // back
    assert(abcdef.back() == 'f');
    /// @todo test empty string
    /// @todo test manipulation

    // compare
    assert( ffb.compare("foo eoo bar",PLAIN           ) > 0 );
    assert( ffb.compare("foo goo bar",PLAIN           ) < 0 );
    assert( ffb.compare(ffb          ,PLAIN           ) == 0);
    assert( ffb.compare("foo Eoo bar",CASE_INSENSITIVE) > 0 );
    assert( ffb.compare("foo Goo bar",CASE_INSENSITIVE) > 0 );
    assert( ffb.compare("FOO fOo Bar",CASE_INSENSITIVE) == 0);

    // equals
    assert(   ffb.equals("foo foo bar",PLAIN           ) );
    assert( !  ffb.equals("foo"       ,PLAIN           ) );
    assert( ! ffb.equals("foo bar bar",PLAIN           ) );
    assert(   ffb.equals("FOO foo Bar",CASE_INSENSITIVE) );
    assert( ! ffb.equals("Foo Bar BAR",CASE_INSENSITIVE) );
    assert( ! ffb.equals("FOO"        ,CASE_INSENSITIVE) );

    // has (String)
    assert(   abcdef.has("ab"    , PLAIN           ) );
    assert(   abcdef.has("f"     , PLAIN           ) );
    assert(   abcdef.has("abcdef", PLAIN           ) );
    assert(   abcdef.has(""      , PLAIN           ) );
    assert( ! abcdef.has("A"     , PLAIN           ) );
    assert(   abcdef.has("BcD"   , CASE_INSENSITIVE) );
    assert( ! abcdef.has("BcF"   , CASE_INSENSITIVE) );

    // beginsWith (String)
    assert(   abcdef.beginsWith("a"     , PLAIN           ) );
    assert(   abcdef.beginsWith("abcdef", PLAIN           ) );
    assert(   abcdef.beginsWith(""      , PLAIN           ) );
    assert( ! abcdef.beginsWith("A"     , PLAIN           ) );
    assert(   abcdef.beginsWith("AbC"   , CASE_INSENSITIVE) );
    assert( ! abcdef.beginsWith("bC"    , CASE_INSENSITIVE) );

    // beginsWith (std::vector<String>)
    assert(   abcdef.beginsWith({"a","X"}, PLAIN           ) );
    assert( ! abcdef.beginsWith({"A","X"}, PLAIN           ) );
    assert(   abcdef.beginsWith({"A","X"}, CASE_INSENSITIVE) );
    assert( ! abcdef.beginsWith({"B","X"}, CASE_INSENSITIVE) );

    // endsWith (String)
    assert(   abcdef.endsWith("f"     , PLAIN           ) );
    assert(   abcdef.endsWith("abcdef", PLAIN           ) );
    assert(   abcdef.endsWith(""      , PLAIN           ) );
    assert( ! abcdef.endsWith("F"     , PLAIN           ) );
    assert(   abcdef.endsWith("DeF"   , CASE_INSENSITIVE) );
    assert( ! abcdef.endsWith("De"    , CASE_INSENSITIVE) );

    // endsWith (std::vector<String>)
    assert(   abcdef.endsWith({"f","X"}, PLAIN           ) );
    assert( ! abcdef.endsWith({"F","X"}, PLAIN           ) );
    assert(   abcdef.endsWith({"F","X"}, CASE_INSENSITIVE) );
    assert( ! abcdef.endsWith({"E","X"}, CASE_INSENSITIVE) );

    // find
    assert(abcdef.find("c"  , PLAIN           ) == 2);
    assert(abcdef.find("C"  , PLAIN           ) == std::string::npos);
    assert(abcdef.find("cde", PLAIN           ) == 2);
    assert(abcdef.find("CdE", CASE_INSENSITIVE) == 2);

    // rfind
    assert(abcdef.rfind("c"  , PLAIN           ) == 2);
    assert(abcdef.rfind("C"  , PLAIN           ) == std::string::npos);
    assert(abcdef.rfind("cde", PLAIN           ) == 2);
    assert(abcdef.rfind("CdE", CASE_INSENSITIVE) == 2);

    // findFirst (std::vector<String>)
    assert(abcdef.findFirst({"b","c"}, PLAIN           ) == abcdef.find("b",PLAIN)  );
    assert(abcdef.findFirst({"b",""} , PLAIN           ) == 0                       );
    assert(abcdef.findFirst({"b","X"}, PLAIN           ) == abcdef.find("b",PLAIN)  );
    assert(abcdef.findFirst({"A","B"}, PLAIN           ) == std::string::npos       );
    assert(abcdef.findFirst({"A","b"}, CASE_INSENSITIVE) == 0                       );

    // findLast (std::vector<String>)
    assert(abcdef.findLast({"b","c"}, PLAIN           ) == abcdef.find("c",PLAIN) );
    assert(abcdef.findLast({"b",""} , PLAIN           ) == abcdef.size()          );
    assert(abcdef.findLast({"b","X"}, PLAIN           ) == abcdef.find("b",PLAIN) );
    assert(abcdef.findLast({"A","B"}, PLAIN           ) == std::string::npos      );
    assert(abcdef.findLast({"F","a"}, CASE_INSENSITIVE) == 5                      );

    // replaceAll (String,String)
    tmp = ffb;
        tmp.replaceAll("foo","abc",PLAIN);
        assert(tmp.equals("abc abc bar",PLAIN));
    tmp = ffb;  {
        bool thrown=false;
        try {
            tmp.replaceAll("", "abc",PLAIN);
        }  catch(std::logic_error&)  {
            thrown = true;
        }
        if(!thrown)  {
            std::cerr << "replaceAll did not throw an exception despite empty needle";
            return 1;
        }
    }
    tmp = ffb;
        tmp.replaceAll(ffb,"",PLAIN);
        assert(tmp.empty());
    tmp = ffb;
        tmp.replaceAll("FoO","abc", CASE_INSENSITIVE);
        assert(tmp.equals("abc abc bar",PLAIN));

    // split (String)
    assert(ffb.split(" "  ,PLAIN           ) == std::vector<String>({"foo","foo","bar"}));
    assert(ffb.split("foo",PLAIN           ) == std::vector<String>({""," "," bar"}));
    assert(ffb.split(ffb  ,PLAIN           ) == std::vector<String>({"",""}));
    assert(ffb.split("Foo",CASE_INSENSITIVE) == std::vector<String>({""," "," bar"}));

    // split (String, int)
    assert(ffb.split(" "  ,PLAIN           ,4) == std::vector<String>({"foo","foo","bar"}));
    assert(ffb.split(" "  ,PLAIN           ,3) == std::vector<String>({"foo","foo","bar"}));
    assert(ffb.split(" "  ,PLAIN           ,2) == std::vector<String>({"foo","foo bar"}));
    assert(ffb.split(" "  ,PLAIN           ,1) == std::vector<String>({ffb}));
    assert(ffb.split(" "  ,PLAIN           ,0) == std::vector<String>({"foo","foo","bar"}));
    assert(String("foo  bar").split(" ",PLAIN) == std::vector<String>({"foo","","bar"}));
    assert(ffb.split("FOO",CASE_INSENSITIVE,2) == std::vector<String>({""," foo bar"}));
    assert(ffb.split("bar",SKIP_EMPTY_LAST   ) == std::vector<String>({"foo foo "}));
    assert(ffb.split("foo",SKIP_EMPTY_FIRST  ) == std::vector<String>({" "," bar"}));
    assert(String(" a b c d ").split(" ", SKIP_EMPTY_FIRST_AND_LAST) == std::vector<String>({"a","b","c","d"}));
    assert(String("foo  bar").split(" ",SKIP_ALL_EMPTY) == std::vector<String>({"foo","bar"}));
    assert(String("").split(" ",SKIP_EMPTY_LAST).empty());
    assert(String("").split(" ",PLAIN) == std::vector<String>({""}));

    // splitIntoLines ()
    assert(String("a\nb\r\nc\n").splitIntoLines(PLAIN           ) == std::vector<String>({"a","b","c",""}));
    assert(String("a\rb\n"     ).splitIntoLines(PLAIN           ) == std::vector<String>({"a\rb",""}));
    assert(String("A\nb"       ).splitIntoLines(CASE_INSENSITIVE) == std::vector<String>({"A","b"}));

    // toLower ()
    assert(String("AbCd \n").toLower().equals("abcd \n",PLAIN));

    // toUpper ()
    assert(String("AbCd \n").toUpper().equals("ABCD \n",PLAIN));

    // preview
    {
        bool thrown=false;
        try {
            digits.preview(2,"...");
        }
        catch(std::logic_error &e)  {
            thrown = true;
        }
        assert(thrown == true);
    }
    for(ParseFlags flags : {PLAIN, CASE_INSENSITIVE})  {
        assert(digits.preview(3,"...") .equals("..."      ,flags));
        assert(digits.preview(9,"...") .equals("012345...",flags));
        assert(digits.preview(5,"...") .equals("01..."    ,flags));
        assert(digits.preview(10,"...").equals(digits     ,flags));
        assert(digits.preview(20,"...").equals(digits     ,flags));
    }
    /// @todo for all trim methods: also pass a job with nothing to do

    // trim ()
    tmp = " \t\nfoo \t\r\n";
        assert(tmp.trim());
        assert(tmp.equals("foo",PLAIN));
    tmp = "f b";
        assert( ! tmp.trim() );
        assert(tmp.equals("f b",PLAIN));
    tmp = "F b";
        assert( ! tmp.trim() );
        assert(tmp.equals("F b",PLAIN));

    // trim (String)
    tmp = "foofobarfoo";
        assert(tmp.trim("foo",PLAIN));
        assert(tmp.equals("fobar",PLAIN));
    tmp = "foobarfoo";
        assert( ! tmp.trim("bar",PLAIN) );
        assert(tmp.equals("foobarfoo",PLAIN));
    tmp = "fOofobarFOO";
        assert(tmp.trim("foo",CASE_INSENSITIVE));
        assert(tmp.equals("fobar",PLAIN));
    tmp = "foobarfoo";
        assert( ! tmp.trim("BAR",CASE_INSENSITIVE) );
        assert(tmp.equals("foobarfoo",PLAIN));

    // trim (std::vector<String>)
    tmp = "foobarfbfoobar";
        assert(tmp.trim({"foo","bar"},PLAIN));
        assert(tmp.equals("fb",PLAIN));
    tmp = "_foobar_";
        assert( ! tmp.trim({"foo","bar"},PLAIN));
        assert(tmp.equals("_foobar_",PLAIN));
    tmp = "FoobarfbfoobAr";
        assert(tmp.trim({"foO","Bar"},CASE_INSENSITIVE));
        assert(tmp.equals("fb",PLAIN));
    tmp = "_foobar_";
        assert( ! tmp.trim({"foo","bar"},CASE_INSENSITIVE));
        assert(tmp.equals("_foobar_",PLAIN));

    // trimQuotes ()
    tmp = "\"\'foo\"\'";
        assert(tmp.trimQuotes());
        assert(tmp.equals("foo",PLAIN));
    tmp = "a\'\"b";
        assert( ! tmp.trimQuotes() );
        assert(tmp.equals("a\'\"b",PLAIN));

    // trimMatchingQuotes ()
    tmp = "\'\"abc\"\'";
        assert(tmp.trimMatchingQuotes());
        assert(tmp.equals("abc",PLAIN));
    tmp = "\'abc\"";
        assert( ! tmp.trimMatchingQuotes() );
        assert(tmp.equals("\'abc\"",PLAIN));
    tmp = "abc \"def\"";
        assert( ! tmp.trimMatchingQuotes() );
        assert(tmp.equals("abc \"def\"",PLAIN));

    // trimAtStart (String)
    tmp = "foofoofobarfoo";
        assert(tmp.trimAtStart("foo",PLAIN));
        assert(tmp.equals("fobarfoo",PLAIN));
    tmp = "fofoo";
        assert( ! tmp.trimAtStart("foo",PLAIN) );
        assert(tmp.equals("fofoo",PLAIN));
    tmp = "FoofoofobarfOo";
        assert(tmp.trimAtStart("fOo",CASE_INSENSITIVE));
        assert(tmp.equals("fobarfOo",PLAIN));
    tmp = "foFoo";
        assert( ! tmp.trimAtStart("fOo",CASE_INSENSITIVE) );
        assert(tmp.equals("foFoo",PLAIN));

    // trimAtStart (std::vector<String>)
    tmp = "foobarfb_foobar";
        assert(tmp.trimAtStart({"foo","bar"},PLAIN));
        assert(tmp.equals("fb_foobar",PLAIN));
    tmp = "foobar";
        assert( ! tmp.trimAtStart({"oo","bar"},PLAIN));
        assert(tmp.equals("foobar",PLAIN));
    tmp = "FoobArfb_foobar";
        assert(tmp.trimAtStart({"fOo","Bar"},CASE_INSENSITIVE));
        assert(tmp.equals("fb_foobar",PLAIN));
    tmp = "foobar";
        assert( ! tmp.trimAtStart({"oo","bar"},CASE_INSENSITIVE));
        assert(tmp.equals("foobar",PLAIN));

    // trimAtEnd (String)
    tmp = "foobarfofoofoo";
        assert(tmp.trimAtEnd("foo",PLAIN));
        assert(tmp.equals("foobarfo",PLAIN));
    tmp = "foofo";
        assert( ! tmp.trimAtEnd("foo",PLAIN) );
        assert(tmp.equals("foofo",PLAIN));
    tmp = "foobarfofoOfOo";
        assert(tmp.trimAtEnd("Foo",CASE_INSENSITIVE));
        assert(tmp.equals("foobarfo",PLAIN));
    tmp = "foofo";
        assert( ! tmp.trimAtEnd("foo",CASE_INSENSITIVE) );
        assert(tmp.equals("foofo",PLAIN));

    // trimAtEnd (std::vector<String>)
    tmp = "foobar_fbfoobar";
        assert(tmp.trimAtEnd({"foo","bar"},PLAIN));
        assert(tmp.equals("foobar_fb",PLAIN));
    tmp = "foobar";
        assert( ! tmp.trimAtEnd({"foo","ba"},PLAIN));
        assert(tmp.equals("foobar",PLAIN));
    tmp = "foobar_fbfOoBar";
        assert(tmp.trimAtEnd({"Foo","baR"},CASE_INSENSITIVE));
        assert(tmp.equals("foobar_fb",PLAIN));
    tmp = "foobar";
        assert( ! tmp.trimAtEnd({"foo","ba"},CASE_INSENSITIVE));
        assert(tmp.equals("foobar",PLAIN));

    return 0;
}
