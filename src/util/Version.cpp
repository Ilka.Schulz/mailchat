/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "Version.hpp"
#include "src/util/exceptions/macros.hpp"

Version::Version(const String &_string)
        : string(_string)  {
    std::vector<String> strParts = string.split('-',SKIP_EMPTY_LAST,2)[0]  // ignore everything after '-'
                                         .split('.',SKIP_ALL_EMPTY);       // split at '.'
    if(strParts.empty())
        ERROR(std::runtime_error, "empty version");
    for(const String &str : strParts)  {
        parts.push_back(str.toInt());
        if(parts.back() < 0)
            ERROR(std::runtime_error, "negative version part");
    }
}
Version::Version(const std::vector<int> &_parts)
        : parts(_parts)  {
    if(parts.empty())
        ERROR(std::runtime_error, "empty version");
    for(int part : parts)  {
        if(part < 0)
            ERROR(std::runtime_error, "negative version part");
        string += std::to_string(part) + ".";
    }
    string.erase(string.size()-1,1);
}
Version::Version(const int &mainVersion)
        : Version(std::vector<int>{mainVersion})  {
}

const String &Version::getString() const  {
    return string;
}
const std::vector<int> &Version::getParts() const  {
    return parts;
}

bool Version::operator==(const Version &other) const  {
    for(size_t i=0; i < std::min(this->parts.size(),other.parts.size()); i++)
        if(this->parts.at(i) != other.parts.at(i))
            return false;
    for(size_t i=std::min(this->parts.size(),other.parts.size());
        i < std::max(this->parts.size(),other.parts.size());
        i++)  {
        if(this->parts.size() > i)
            if(this->parts.at(i) != 0)
                return false;
        if(other.parts.size() > i)
            if(other.parts.at(i) != 0)
                return false;
    }
    return true;
}
bool Version::operator!=(const Version &other) const  {
    return ! (*this==other);
}
bool Version::operator<(const Version &other) const  {
    // common parts
    for(size_t i=0; i < std::min(this->parts.size(),other.parts.size()); i++)
        if(this->parts.at(i) != other.parts.at(i))
            return this->parts.at(i) < other.parts.at(i);
    // if one has more parts than the other
    if(this->parts.size() != other.parts.size())  {
        for(size_t i = std::min(this->parts.size(),other.parts.size());
            i < std::max(this->parts.size(),other.parts.size());
            i++)  {
            if(this->parts.size() > i)  {
                if(this->parts.at(i) != 0)
                    return false;
            }
            if(other.parts.size() > i)  {
                if(other.parts.at(i) != 0)
                    return true;
            }
        }
    }
    // they are equal
    return false;
}
bool Version::operator>(const Version &other) const  {
    // common parts
    for(size_t i=0; i < std::min(this->parts.size(),other.parts.size()); i++)
        if(this->parts.at(i) != other.parts.at(i))
            return this->parts.at(i) > other.parts.at(i);
    // if one has more parts than the other
    if(this->parts.size() != other.parts.size())  {
        for(size_t i = std::min(this->parts.size(),other.parts.size());
            i < std::max(this->parts.size(),other.parts.size());
            i++)  {
            if(this->parts.size() > i)  {
                if(this->parts.at(i) != 0)
                    return true;
            }
            if(other.parts.size() > i)  {
                if(other.parts.at(i) != 0)
                    return false;
            }
        }
    }
    // they are equal
    return false;
}
bool Version::operator<=(const Version &other) const  {
    return *this==other || *this<other;
}
bool Version::operator>=(const Version &other) const  {
    return *this==other || *this>other;
}

// test
#include <iostream>
#undef NDEBUG
#include <assert.h>
int src_util_Version(int, char**)  {
    // resources
    const Version v1    ("1");
    const Version v1_0  ("1.0");
    const Version v1_1  ("1.1");
    const Version v1_0_0("1.0.0");


    // string constructor
    assert((Version("1"    ).parts == std::vector<int>{1}));
    assert((Version("1.0"  ).parts == std::vector<int>{1,0}));
    assert((Version("1.1"  ).parts == std::vector<int>{1,1}));
    assert((Version("1.0.0").parts == std::vector<int>{1,0,0}));

    // int constructor
    assert((Version( 1     ).parts == std::vector<int>{1}));
    assert((Version({1    }).parts == std::vector<int>{1}));
    assert((Version({1,0  }).parts == std::vector<int>{1,0}));
    assert((Version({1,1  }).parts == std::vector<int>{1,1}));
    assert((Version({1,0,0}).parts == std::vector<int>{1,0,0}));
    assert(Version( 1   ).getString() == "1"  );
    assert(Version({1,0}).getString() == "1.0");

    // == , !=
    assert(v1 == v1);
    assert(v1 == v1_0);
    assert(v1 == v1_0_0);
    assert(v1 != v1_1);

    // <
    assert(v1 < v1_1);
    assert( ! (v1 < v1_0) );

    // >
    assert(v1_1 > v1);
    assert( ! (v1 > v1_0) );

    // <=
    assert(v1 <= v1_0);
    assert(v1 <= v1_1);
    assert( ! (v1_1 <= v1) );

    // >=
    assert(v1_1 >= v1);
    assert(v1_0 >= v1);
    assert( ! (v1 >= v1_1) );

    return 0;
}
