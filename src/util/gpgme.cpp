/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "gpgme.hpp"
#include <gpgme.h>
#include <string>
#include <vector>
#include <deque>
#include "src/util/exceptions/Exception.hpp"
#include "src/util/exceptions/macros.hpp"
#include "src/settings.hpp"
#include <algorithm>
#include <assert.h>

using namespace Gpgme;

#ifndef ASSERT_GPGME_NO_ERROR
    /** @brief throws an @c Exception if the specified GPGme result code is not healthy
     *  @param call  an expression that yields a GPGme result code
     *  @param msg   the error message to pass to @c Exception::Exception if the result code is not healthy
     */
    #define ASSERT_GPGME_NO_ERROR(call, msg)  { \
        gpgme_error_t error_code = call; \
        if( error_code != GPG_ERR_NO_ERROR ) \
            throw Exception(String(__PRETTY_FUNCTION__)+": "+String(msg)+String(": error code ")+String(std::to_string(error_code))); \
    }
#endif


const std::map<gpgme_validity_t,String> Gpgme::validityNames  {
    {gpgme_validity_t::GPGME_VALIDITY_UNKNOWN  , "unknown"    },
    {gpgme_validity_t::GPGME_VALIDITY_UNDEFINED, "undefined"  },
    {gpgme_validity_t::GPGME_VALIDITY_NEVER    , "not trusted"},
    {gpgme_validity_t::GPGME_VALIDITY_MARGINAL , "marginally" },
    {gpgme_validity_t::GPGME_VALIDITY_FULL     , "fully"      },
    {gpgme_validity_t::GPGME_VALIDITY_ULTIMATE , "ultimately" }
};

Context::Context()  {
    ASSERT_GPGME_NO_ERROR(gpgme_new(&ctx), "could not create gpgme context");
}
Context::~Context()  {
    gpgme_release(ctx);
}
Context::operator gpgme_ctx_t()  {
    return ctx;
}

Key::Key()
        : key(nullptr)  {
}
Key::Key(gpgme_key_t _key)
        :  key(_key)  {
}
Key::Key(const Key &other) noexcept
        :  key(nullptr)  {
    if(other.key == nullptr)
        return;
    Key temp = getByFingerprint(other.getFingerprint());
    assert(temp == other);
    key = temp.key;
    temp.key = nullptr;
}
Key::Key(Key &&other) noexcept
        :  key(other.key)  {
    other.key = nullptr;
}
Key::~Key()  {
    if(key != nullptr)
        gpgme_key_release(key);
}
/*Key::operator _gpgme_key *()  {
    return key;
}*/
/// @todo this operator moves instead of copying
Key &Key::operator=(const Key &_other)  {
    if(key != nullptr)
        gpgme_key_release(key);
    Key temp = getByFingerprint(_other.getFingerprint());
    key = temp.key;
    temp.key = nullptr;
    return *this;
}
bool Key::operator==(const Key &other)  const  {
    return getFingerprint().equals(other.getFingerprint(),CASE_INSENSITIVE);
}

void Key::refresh()  {
    String fingerprint = getFingerprint();
    gpgme_key_release(key);
    key = nullptr;
    *this = Key::getByFingerprint(fingerprint);
}

bool Key::isRevoked() const  {
    return key->revoked;
}
bool Key::isExpired() const  {
    return key->expired;
}
time_t Key::getExpiry() const  {
    time_t retval = 0;
    for(gpgme_subkey_t subkey = key->subkeys; subkey!=nullptr; subkey = subkey->next)  {
        if(subkey->expires != 0  &&
          (subkey->expires < retval || retval==0))
            retval = subkey->expires;
    }
    return retval;
}
bool Key::isDisabled() const  {
    return key->disabled;
}
bool Key::isInvalid() const  {
    return key->invalid;
}
bool Key::canEncrypt() const  {
    return key->can_encrypt;
}
bool Key::canSign() const  {
    return key->can_sign;
}
bool Key::canCertify() const  {
    return key->can_certify;
}
bool Key::isSecret() const  {
    return key->secret;
}
bool Key::canAuthenticate() const  {
    return key->can_authenticate;
}
bool Key::isQualified() const  {
    return key->is_qualified;
}
uint Key::getOrigin() const  {
    return key->origin;
}
gpgme_protocol_t Key::getProtocol() const  {
    return key->protocol;
}
String Key::getIssuerSerial() const  {
    return key->issuer_serial;
}
String Key::getIssuerName() const  {
    return key->issuer_name;
}
String Key::getIssuerChainId() const  {
    return key->chain_id;
}
gpgme_validity_t Key::getOwnerTrust() const  {
    return key->owner_trust;
}
gpgme_user_id_t Key::getUserIds() const  {
    return key->uids;
}
String Key::getFingerprint() const  {
    return key->fpr;
}
ulong Key::getLastUpdate() const  {
    return key->last_update;
}

bool Key::isHealthy() const  {
    return  key!=nullptr &&
            ! isRevoked()&&
            ! isExpired() &&
            ! isDisabled() &&
            ! isInvalid() &&
            canEncrypt() &&
            canSign();
}

String Key::toString() const  {
    return String("\"") + getUserIds()->name + " <" + getUserIds()->email + ">\"\n" +
           getFingerprint();
}




Key Key::getByFingerprint(const String &fingerprint)  {
    Context context;
    Key retval(nullptr);
    if(gpgme_get_key(context, fingerprint.c_str(), &retval.key, true) != GPG_ERR_NO_ERROR)  // get private key
        if(gpgme_get_key(context, fingerprint.c_str(), &retval.key, false) != GPG_ERR_NO_ERROR)  // get public key
            throw GpgmeKeyNotFoundException();
    return retval;
}
std::vector<Key> Key::getByEmail(const String &email)  {
    std::vector<Key> retval;
    std::vector<Key> keys = getAllKeys(email);
    for(const Key &key : keys)
        if(email.equals(key.getUserIds()->email,CASE_INSENSITIVE))
            retval.push_back(key);
    if(retval.empty())
        throw GpgmeKeyNotFoundException();
    return retval;
}
std::vector<Key> Key::getAllKeys(const String &filter)  {
    Context context;
    std::vector<Key> keys;
        ASSERT_GPGME_NO_ERROR(gpgme_op_keylist_start(context,
                                                     filter.empty() ? nullptr : filter.c_str(),
                                                     false), "could not retreive key list");
        while(true)  {
            gpgme_key_t key;
            gpgme_error_t error = gpgme_op_keylist_next(context, &key);
            if(error == GPG_ERR_EOF)
                break;
            else if(error!=GPG_ERR_NO_ERROR)
                break;  /// @todo I get strange error codes with ASSERT_GPGME_NO_ERROR(error,"could not retrieve key from key list");
            //keys.push_back(key);
            keys.push_back(getByFingerprint(key->fpr));
            gpgme_key_release(key);
        }
        ASSERT_GPGME_NO_ERROR(gpgme_op_keylist_end(context), "cannot end key list retrieval operation");
        gpgme_keylist_result_t result = gpgme_op_keylist_result(context);
        if(result->truncated)
            // This is true if the crypto backend had to truncate the result, and less than the desired keys could be listed.
            ; /// @todo react
    return keys;
}
Key Key::create(String userid, String algorithm, ulong expires, const Key &certKey, uint flags)  {
    Context context;
    if( gpgme_op_createkey(context,
                           userid.c_str(),
                           algorithm.c_str(),
                           0,
                           expires,
                           certKey.key,
                           flags
                          ) != GPG_ERR_NO_ERROR )
        throw GpgmeCreatingKeyFailedException();
    return Key::getByFingerprint(gpgme_op_genkey_result(context)->fpr);
}
std::vector<Key> Key::import(String raw)  {
    Context context;
    // convert String to gpgme_data_t
    gpgme_data_t data=nullptr;
    if(gpgme_data_new_from_mem(&data,raw.c_str(),raw.size(),true) != GPG_ERR_NO_ERROR)
        throw GpgmeImportingKeyFailedException("cannot create gpgme_data_t object");
    gpgme_data_type_t type = gpgme_data_identify(data, 0);
    if(type != gpgme_data_type_t::GPGME_DATA_TYPE_PGP_KEY)  {
        gpgme_data_release(data);
        throw GpgmeImportingKeyFailedException("provided data do not represent a PGP key");
    }
    // import
    gpgme_error_t error = gpgme_op_import(context, data);
    gpgme_data_release(data);
    if(error != GPG_ERR_NO_ERROR)
        throw GpgmeImportingKeyFailedException("undefined error during import");
    // fetch
    gpgme_import_result_t result = gpgme_op_import_result(context);
    if(result->not_imported != 0)
        throw GpgmeImportingKeyFailedException(std::to_string(result->not_imported)+" key(s) not imported");
    if(result->imported != 1)
        throw GpgmeImportingKeyFailedException("number of keys imported is not equal to one");
    std::vector<Key> retval;
    for(gpgme_import_status_t status = result->imports; status != nullptr; status = status->next)
        if(status->result == GPG_ERR_NO_ERROR)
            retval.push_back(getByFingerprint(status->fpr));
    if(retval.size()!=1)
        throw GpgmeImportingKeyFailedException("number of keys to return is not equal to one");
    return retval;
}
Key Key::nullKey()  {
    return Key((gpgme_key_t)nullptr);
}


void sign(gpgme_key_t signer, gpgme_key_t signee)  {
    Context context;
    gpgme_signers_clear(context);

    gpgme_error_t result = gpgme_signers_add(context, signer);
    if(result != GPG_ERR_NO_ERROR)
        throw GpgmeSigningKeyFailedException(signer, signee);

    result = gpgme_op_keysign(context, signee, nullptr, 0, 0);  /// @todo this supports extra options
    if(result != GPG_ERR_NO_ERROR)
        throw GpgmeSigningKeyFailedException(signer, signee);
}

class gpgme_disable_opaque_t  {
    private:
        bool disabled;
        bool done;
    public:
        gpgme_disable_opaque_t(bool _disabled)
            : disabled(_disabled), done(false)  {
        }
        bool getDisabled()  const  {
            return disabled;
        }
        bool isDone()  const  {
            return done;
        }
        void markDone()  {
            done = true;
        }
};

gpgme_error_t gpgme_disable_callback(void *void_opaque, const char *keyword, const char *args, int fd)  {
    if(Settings::debug)
        std::cerr << __PRETTY_FUNCTION__ << std::endl
                  << "keyword: >" << keyword << "<" << std::endl
                  << "args:    >" << args    << "<" << std::endl
                  << "fd:       " << fd             << std::endl;

    gpgme_disable_opaque_t *opaque = ((gpgme_disable_opaque_t*)void_opaque);

    String response;
    if(String(keyword).equals("KEY_CONSIDERED",CASE_INSENSITIVE))
        response = "";
    else if(String(keyword).equals("GET_LINE",CASE_INSENSITIVE))  {
        if(String(args).equals("keyedit.prompt",CASE_INSENSITIVE))  {
            if(opaque->isDone())
                response = "quit";
            else  {
                response =  opaque->getDisabled() ? "disable" : "enable";
                opaque->markDone();
            }
        }
        else
            ERROR(std::runtime_error," unknown gpgme args: >"+String(args)+"<");
    }
    else if(String(keyword).equals("GOT_IT",CASE_INSENSITIVE))
        response = "";
    else if(String(keyword).empty())
        response = "";
    else
        ERROR(std::runtime_error, "unknown gpgme keyword: >"+String(keyword)+"<");
    if(fd >=0)  {  // response required
        if( ! response.empty() )  {
            std::cerr << "responding >"<<response<<"<"<<std::endl;
            gpgme_io_writen(fd, response.c_str(), response.size());
            gpgme_io_writen(fd, "\n", 1);
        }
    }
    return 0;
}
void Key::setDisabled(bool disabled)  {
    Context context;
    gpgme_data_t data=nullptr;
    gpgme_disable_opaque_t opaque(disabled);
    ASSERT_GPGME_NO_ERROR(gpgme_data_new(&data), "cannot create gpgme_data_t");
    gpgme_error_t error = gpgme_op_interact(context, key, 0, gpgme_disable_callback, (void*)&opaque, data);
    gpgme_data_release(data);
    if(error != GPG_ERR_NO_ERROR)
        throw GpgmeTrustingKeyFailedException(key);
    refresh();
}
class gpgme_trust_opaque_t  {
    private:
        gpgme_validity_t validity;  ///< the validity to set in the trust-edit job
        bool done=false;
    public:
        gpgme_trust_opaque_t(const gpgme_validity_t &_validity)
            : validity(_validity), done(false)  {
        }
        gpgme_validity_t getValidity()  const  {
            return validity;
        }
        bool isDone()  const  {
            return done;
        }

        void markDone()  {
            done = true;
        }
};
gpgme_error_t gpgme_trust_callback(void *void_opaque, const char *keyword, const char *args, int fd)  {
    if(Settings::debug)
        std::cerr << __PRETTY_FUNCTION__ << std::endl
                  << "keyword: >" << keyword << "<" << std::endl
                  << "args:    >" << args    << "<" << std::endl
                  << "fd:       " << fd             << std::endl;

    gpgme_trust_opaque_t *opaque = ((gpgme_trust_opaque_t*)void_opaque);

    String response;
    if(String(keyword).equals("KEY_CONSIDERED",CASE_INSENSITIVE))
        response = "";
    else if(String(keyword).equals("GET_LINE",CASE_INSENSITIVE))  {
        if(opaque->isDone())
            response = "quit";
        else if(String(args).equals("keyedit.prompt",CASE_INSENSITIVE))
            response = "trust";
        else if(String(args).equals("edit_ownertrust.value",CASE_INSENSITIVE))  {
            response = std::to_string(opaque->getValidity());
            opaque->markDone();
        }
        else
            ERROR(std::runtime_error, "unknown gpgme args: >"+String(args)+"<");
    }
    else if(String(keyword).equals("GET_BOOL",CASE_INSENSITIVE))  {
        if(String(args).equals("edit_ownertrust.set_ultimate.okay",CASE_INSENSITIVE))
            response = "y";
        else
            ERROR(std::runtime_error," unknown gpgme args: >"+String(args)+"<");
    }
    else if(String(keyword).equals("GOT_IT",CASE_INSENSITIVE))
        response = "";
    else if(String(keyword).empty())
        response = "";
    /// @todo handle keyword "KEYEXPIRED"
    else
        ERROR(std::runtime_error, "unknown gpgme keyword: >"+String(keyword)+"<");
    if(fd >=0)  {  // response required
        if( ! response.empty() )  {
            std::cerr << "responding >"<<response<<"<"<<std::endl;
            gpgme_io_writen(fd, response.c_str(), response.size());
            gpgme_io_writen(fd, "\n", 1);
        }
    }
    return 0;
}
void Key::trust(gpgme_validity_t validity)  {
    static const std::vector<gpgme_validity_t>  allowed_validities{
        GPGME_VALIDITY_NEVER    ,
        GPGME_VALIDITY_MARGINAL ,
        GPGME_VALIDITY_FULL     ,
        GPGME_VALIDITY_ULTIMATE
    };
    if(std::find(allowed_validities.begin(), allowed_validities.end(), validity) == allowed_validities.end())
        ERROR(std::runtime_error, "validity not supported in GPGme: >"+std::to_string(validity)+"<");

    Context context;
    gpgme_data_t data=nullptr;
    gpgme_trust_opaque_t opaque(validity);
    ASSERT_GPGME_NO_ERROR(gpgme_data_new(&data), "cannot create gpgme_data_t");
    gpgme_error_t error = gpgme_op_interact(context, key, 0, gpgme_trust_callback, (void*)&opaque, data);
    gpgme_data_release(data);
    if(error != GPG_ERR_NO_ERROR)
        throw GpgmeTrustingKeyFailedException(key);
    refresh();
}
/*void revoke(gpgme_key_t signer, gpgme_key_t signee)  {
    Context context;
    // requires gpgme 1.14.1 or newer
    /// @todo does not work properly yet, maybe it is a bug in the calling code...
    gpg_error_t result = gpgme_op_revsig(context, signee, signer, nullptr, 0);
    if(result != GPG_ERR_NO_ERROR)
        throw GpgmeRevokingKeyFailedException(signer, signee);
}*/
void Key::changePassphrase()  {
    Context context;
    gpg_error_t result = gpgme_op_passwd(context, key, 0);
    if(result != GPG_ERR_NO_ERROR)
        throw GpgmeChangingPassphraseFailedException(key);
}
class gpgme_revoke_opaque_t  {
    private:
        gpgme_revocation_reason_t reason;
        std::deque<String> text_lines;
        bool done;
    public:
        gpgme_revoke_opaque_t(gpgme_revocation_reason_t _reason, const String &_text)
                : reason(_reason), done(false)  {
            std::vector<String> lines = _text.splitIntoLines(SKIP_EMPTY_FIRST_AND_LAST);
            std::copy(lines.begin(), lines.end(), std::inserter(text_lines, text_lines.end()));
            assert(lines.size() == text_lines.size());
        }
        gpgme_revocation_reason_t getReason()  const  {
            return reason;
        }
        /*const std::vector<String> &getTextLines()  const  {
            return text_lines;
        }*/
        const String getNextTextLine()  {
            if(text_lines.empty())
                return "";
            String retval = text_lines.front();
            text_lines.pop_front();
            return retval;
        }
        bool isDone()  const  {
            return done;
        }
        void markDone()  {
            done = true;
        }
};
gpgme_error_t gpgme_revoke_callback(void *void_opaque, const char *keyword, const char *args, int fd)  {
    if(Settings::debug)
        std::cerr << __PRETTY_FUNCTION__ << std::endl
                  << "keyword: >" << keyword << "<" << std::endl
                  << "args:    >" << args    << "<" << std::endl
                  << "fd:       " << fd             << std::endl;

    gpgme_revoke_opaque_t *opaque = ((gpgme_revoke_opaque_t*)void_opaque);

    std::optional<String> response;
    if(String(keyword).equals("KEY_CONSIDERED",CASE_INSENSITIVE))
        ;
    else if(String(keyword).equals("GET_LINE",CASE_INSENSITIVE))  {
        if(opaque->isDone())
            response = "quit";
        else if(String(args).equals("keyedit.prompt",CASE_INSENSITIVE))
            response = "revkey";
        else if(String(args).equals("ask_revocation_reason.code",CASE_INSENSITIVE))
            response = std::to_string((int)opaque->getReason());
        else if(String(args).equals("ask_revocation_reason.text",CASE_INSENSITIVE))
            response = opaque->getNextTextLine();
        else
            ERROR(std::runtime_error, "unknown gpgme args: >"+String(args)+"<");
    }
    else if(String(keyword).equals("GET_BOOL",CASE_INSENSITIVE))  {
        if(String(args).equals("keyedit.revoke.subkey.okay",CASE_INSENSITIVE))
            response = "y";
        else if(String(args).equals("ask_revocation_reason.okay",CASE_INSENSITIVE))  {
            response = "y";
            opaque->markDone();
        }
        else if(String(args).equals("keyedit.save.okay",CASE_INSENSITIVE))
            response = "y";
        else
            ERROR(std::runtime_error, "unknown gpgme args: >"+String(args)+"<");
    }
    else if(String(keyword).equals("GOT_IT",CASE_INSENSITIVE))
        ;
    else if(String(keyword).empty())
        ;
    else
        ERROR(std::runtime_error, "unknown gpgme keyword: >"+String(keyword)+"<");
    if(fd >=0)  {  // response required
        if( response.has_value() )  {
            std::cerr << "responding >"<<response.value()<<"<"<<std::endl;
            gpgme_io_writen(fd, response.value().c_str(), response.value().size());
            gpgme_io_writen(fd, "\n", 1);
        }
        else
            ERROR(std::runtime_error, "gpgme requests answer, but not answer available");  /// @todo add this to other callback functions
    }
    return 0;
}
void Key::revoke(gpgme_revocation_reason_t reason, String text)  {
    Context context;
    gpgme_data_t data=nullptr;
    gpgme_revoke_opaque_t opaque(reason,text);
    ASSERT_GPGME_NO_ERROR(gpgme_data_new(&data), "cannot create gpgme_data_t");
    gpgme_error_t error = gpgme_op_interact(context, key, 0, gpgme_revoke_callback, (void*)&opaque, data);
    gpgme_data_release(data);
    if(error != GPG_ERR_NO_ERROR)
        throw GpgmeRevokingKeyFailedException(*this);
    refresh();
}
void Key::delete_(bool askForConfirmation)  {
    Context context;
    gpgme_error_t result = gpgme_op_delete_ext(context, key,
                                               GPGME_DELETE_ALLOW_SECRET | (askForConfirmation?0:GPGME_DELETE_FORCE) );
    if(result != GPG_ERR_NO_ERROR)
        throw GpgmeDeletingKeyFailedException(key);
}

GpgmeKeyNotFoundException::GpgmeKeyNotFoundException()
    : Exception("key not found")  {
}
GpgmeCreatingKeyFailedException::GpgmeCreatingKeyFailedException()
    : Exception("creating key failed")  {
}
GpgmeImportingKeyFailedException::GpgmeImportingKeyFailedException(const String &reason)
    : Exception("importing key failed: "+reason)  {
}
GpgmeSigningKeyFailedException::GpgmeSigningKeyFailedException(gpgme_key_t signer, gpgme_key_t signee)
    : Exception("signing key "+std::string(signee->fpr)+" with "+std::string(signer->fpr)+" failed")  {
}
GpgmeTrustingKeyFailedException::GpgmeTrustingKeyFailedException(gpgme_key_t key)
    : Exception("trusting key with fingerprint "+std::string(key->fpr)+" failed")  {
}
GpgmeDisablingKeyFailedException::GpgmeDisablingKeyFailedException(gpgme_key_t key)
    : Exception("disabling key with fingerprint "+std::string(key->fpr)+" failed")  {
}
GpgmeRevokingKeyFailedException::GpgmeRevokingKeyFailedException(const Key &key)
    : Exception("revoking key with fingerprint "+std::string(key.getFingerprint())+" failed")  {
}
GpgmeChangingPassphraseFailedException::GpgmeChangingPassphraseFailedException(gpgme_key_t key)
    : Exception("changing passphrase of key with fingerprint "+std::string(key->fpr)+" failed")  {
}
GpgmeDeletingKeyFailedException::GpgmeDeletingKeyFailedException(gpgme_key_t key)
    : Exception("deleting key with fingerprint "+std::string(key->fpr)+" failed")  {
}

// tests
#include <iostream>
#undef NDEBUG
#include <assert.h>
int src_util_gpgme(int, char**)  {
    // init
    gpgme_set_global_flag("debug","1");
    gpgme_check_version(nullptr);

    using namespace Gpgme;

    // create keypair
    /// @todo define function
    Key key = Key::create("Alice <alice@example.com>",
                          "default",
                          60,  // expiration in seconds
                          nullptr,
                          GPGME_CREATE_FORCE | GPGME_CREATE_NOPASSWD
              );
    assert(String(key.getUserIds()->name).equals("Alice",PLAIN));
    assert(String(key.getUserIds()->email).equals("alice@example.com",PLAIN));


    /// @todo test getByFingerprint
    /// @todo test getByEmail
    /// @todo test getAllKeys
    /// @todo test import
    /// @todo test nullKey
    /// @todo test sign()

    // change validity
    for(gpgme_validity_t validity : {
                                        //GPGME_VALIDITY_UNKNOWN  ,
                                        //GPGME_VALIDITY_UNDEFINED,
                                        GPGME_VALIDITY_NEVER    ,
                                        GPGME_VALIDITY_MARGINAL ,
                                        GPGME_VALIDITY_FULL     ,
                                        GPGME_VALIDITY_ULTIMATE
                                    } )  {
        key.trust(validity);
        assert(key.getOwnerTrust() == validity);
        key.refresh();
        assert(key.getOwnerTrust() == validity);
    }

    // disable
    key.setDisabled(true);
    assert(key.isDisabled());
    key.refresh();
    assert(key.isDisabled());

    key.setDisabled(false);
    assert( ! key.isDisabled() );
    key.refresh();
    assert( ! key.isDisabled() );

    // revoke
    key.revoke(gpgme_revocation_reason_t::NO_LONGER_USED, "some message...");
    assert(key.isRevoked());
    key.refresh();
    assert(key.isRevoked());
    /// @todo assert gpgme_revocation_reason_t?


    /// @todo test changePassphrase()

    // delete key
    {
        String fingerprint = key.getFingerprint();
        key.delete_(false);
        try {
            Key::getByFingerprint(fingerprint);
            std::cerr << "deleted key still exists"<<std::endl;
            return 1;
        }
        catch(...)  { }
    }

    return 0;
}
