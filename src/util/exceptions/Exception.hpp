/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <exception>
#include "src/util/String.hpp"

class Exception;

/// @todo this class is way to simple – derive more
/// @todo rename to UnspecifiedException
class Exception  :  public std::exception  {
    private:
        /// error message to be returned by @c Exception::what
        String message;

    public:
        /// constructor
        /// @param _message  initializes @c Exception::message
        Exception(String _message);
        /// @returns  @c Exception::message
        virtual const char *what()  const noexcept override;
};

class NotYetImplementedException  :  public std::exception  {
    private:
        String functionName;
        size_t lineNumber;
        String fullErrorMessage;
    public:
        NotYetImplementedException(String _functionName, size_t _lineNumer);
        virtual const char *what()  const noexcept override;
};
#define NOT_YET_IMPLEMENTED()  throw NotYetImplementedException(__PRETTY_FUNCTION__, __LINE__)


class ShouldBeUnreachableException  :  public std::exception  {
    private:
        String functionName;
        String reason;
        String fullErrorMessage;
    public:
        ShouldBeUnreachableException(String _functionName, String _reason);
        virtual const char *what()  const noexcept override;
};
#define SHOULD_BE_UNREACHABLE(reason)  throw ShouldBeUnreachableException(__PRETTY_FUNCTION__, reason)
