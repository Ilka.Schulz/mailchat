/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Exception.hpp"

Exception::Exception(String _message)
        : message(_message)  {
}

const char *Exception::what()  const noexcept  {
    return message.c_str();
}


NotYetImplementedException::NotYetImplementedException(String _functionName, size_t _lineNumer)
        : functionName(_functionName), lineNumber(_lineNumer), fullErrorMessage("This function is not yet (fully) implemented: "+functionName+": line "+std::to_string(lineNumber))  {
}
const char *NotYetImplementedException::what() const noexcept  {
    return fullErrorMessage.c_str();
}


ShouldBeUnreachableException::ShouldBeUnreachableException(String _functionName, String _reason)
        : functionName(_functionName), reason(_reason), fullErrorMessage(
          "This code in function "+functionName+" should be unreachable"+String(reason.empty()?"":"(reason: "+reason+")")
                                                            )  {
}
const char *ShouldBeUnreachableException::what() const noexcept  {
    return fullErrorMessage.c_str();
}
