/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "SmtpSendCurlJobFailedException.hpp"
#include "src/connections/SendMessageSmtpConnectionCurlJob.hpp"
#include "src/connections/SmtpConnection.hpp"
#include "src/Account.hpp"


SmtpSendCurlJobFailedException::SmtpSendCurlJobFailedException(SendMessageSmtpConnectionCurlJob *_job)
        : job(_job)  {
    what_str = "could not send message via SMTP (" + job->getConnection()->getAccount()->getNameAndEmail() + ")";
}
const char *SmtpSendCurlJobFailedException::what() const noexcept  {
    return what_str.c_str();
}
