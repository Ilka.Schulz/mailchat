/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file
/// @todo use this class everywhere where applicable

#pragma once

#include <exception>
#include "src/util/String.hpp"

class ParseException  :  public std::exception  {
    private:
        String message;
    public:
        ParseException(const String &_message);  /// @todo maybe allow to pass the parsed string and the location of error
        virtual const char *what()  const noexcept override;
};
