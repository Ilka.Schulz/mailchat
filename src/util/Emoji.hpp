/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <string>  /// @todo use @c String
#include <unordered_map>
#include <map>
#include <vector>

namespace Emoji {
    /// unordered map between raw and Unicode emojis
    const extern std::unordered_map<std::string, std::string> emojis;
    /// ordered map between raw and Unicode emojis
    const extern std::map<std::string,std::string> ordered_emojis;

    /** @brief converts a raw string, e.g. ":smile:" to an Unicode emoji
     *  @param raw  string containing a raw emoji, e.g. ":smile:" and nothing else
     *  @returns string containing an Unicode emoji and nothing else
     */
    std::string convertEmoji(std::string raw);

    /** @brief converts a full text containing raw emojis to Unicode emojis preserving other text elements
     *  @details see Emoji::convertEmoji for examples
     *  @param raw a full text
     *  @returns full text with raw emojis replaced by Unicode emojis
     */
    std::string convertTextWithEmojis(std::string raw);

    /** @brief provides a list of suggestions for valid raw emojis
     *  @param search   a partial raw emoji
     *  @param limit    limit to the number of elements returned (if zero: no limit)
     *  @returns a map of possible completions (raw->unicode emojis)
     */
    std::unordered_map<std::string, std::string> getSuggestions(std::string search, size_t limit=0);
}
