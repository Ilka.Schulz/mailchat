/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/TristateBool.hpp"
#include "src/util/exceptions/Exception.hpp"


TristateBool::TristateBool()
        : value(VAL_UNKNOWN)  {
}
TristateBool::TristateBool(bool source)
        : value( source ? VAL_TRUE : VAL_FALSE )  {
}
TristateBool::TristateBool(const TristateBool &source)
        : value(source.value)  {
}
TristateBool::TristateBool(TristateBool::TristateBoolValue _value)
        : value(_value)  {
}
TristateBool::TristateBool(String _value)  {
    *this = _value;
}

bool TristateBool::isKnown()  {
    return value != VAL_UNKNOWN;
}
TristateBool::operator bool()  {
    return value==VAL_TRUE;
}
TristateBool &TristateBool::operator=(TristateBool source)  {
    value = source.value;
    return *this;
}
TristateBool &TristateBool::operator=(TristateBool::TristateBoolValue source)  {
    value = source;
    return *this;
}
TristateBool &TristateBool::operator=(bool source)  {
    if(source)
        value = VAL_TRUE;
    else
        value = VAL_FALSE;
    return *this;
}

bool TristateBool::operator==(TristateBool b)  {
    return value == b.value;
}
bool TristateBool::operator==(TristateBoolValue b)  {
    return value == b;
}

String TristateBool::toString()  {
    if(value == VAL_FALSE)
        return "false";
    else if(value == VAL_TRUE)
        return "true";
    else if(value == VAL_UNKNOWN)
        return "unknown";
    else
        throw Exception("unknown TristateBool state");
}
TristateBool &TristateBool::operator=(String str)  {
    if(str == "false")
        value = VAL_FALSE;
    else if(str == "true")
        value = VAL_TRUE;
    else if(str == "unknown")
        value = VAL_UNKNOWN;
    else
        throw Exception("unknown TristateBoot string representation: >"+str+"<");
    return *this;
}
