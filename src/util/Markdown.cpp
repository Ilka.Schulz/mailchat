/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "Markdown.hpp"
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <iostream>
#include <sstream>
#include "src/util/exceptions/Exception.hpp"


namespace Markdown {
    /// font sizes (in em) of the different level of markdown captions
    extern std::vector<double> caption_font_sizes;
    extern const std::unordered_map<std::string, std::string> font_markers;
    enum ListOpener  { sorted, unsorted, invalid };

    ListOpener getLineListOpener(std::string line);
    std::string parseList(std::string paragraph);
    std::string parseMarkdownParagraphToMarkup(std::string paragraph);
}


std::vector<double> Markdown::caption_font_sizes{1.75, 1.5, 1.3, 1.2, 1, 0.95};
const std::unordered_map<std::string, std::string> Markdown::font_markers  {
    {"**", "strong"},
    {"__", "strong"},
    {"*" , "em"},
    {"_" , "em"},
    {"~~", "font style='text-decoration: line-through;'"}
};

Markdown::ListOpener Markdown::getLineListOpener(std::string line)  {
    // unsorted openers
    if(line.rfind("- ",0)==0 || line.rfind("* ",0)==0)
        return unsorted;
    // sorted openers
    size_t point_pos = line.find(". ");
    if(point_pos != std::string::npos)  {
        for(size_t i=0; i<point_pos; i++)  {
            if( ! std::isdigit(line[i]) )
                return invalid;
        }
        return sorted;
    }
    return invalid;
}

int getIndentationLevel(std::string line)  {
    if(line.empty())
        return 0;
    if(line[0]==' ' || line[0]=='\t')  {
        size_t first_non_white = line.find_first_not_of(" \t");
        std::string indentation_string = line.substr(0, first_non_white);
        line.erase(0, first_non_white);
        int tab_count=0, space_count=0;
        for(size_t i=0; i<indentation_string.size(); i++)
            if(indentation_string[i] == ' ')
                space_count++;
            else if(indentation_string[i] == '\t')
                tab_count++;
        return tab_count*2 + space_count;
    }
    return 0;
}
void removeIndentation(std::string &line, int level)  {
    // remove whitespace according to current indentation level
    int levels_removed=0;
    while(levels_removed<level)  {
        if(line.empty())
            throw Exception("could not remove "+std::to_string(level)+" indentation levels");
        if(line[0]==' ')  {
            levels_removed++;
            line.erase(0,1);
        }
        else if(line[0]=='\t')  {
            levels_removed+=2;
            line.erase(0,1);
        }
        else
            throw Exception("could not remove "+std::to_string(level)+" indentation levels");
    }
}
std::string Markdown::parseList(std::string paragraph)  {
    std::vector<int> indentation_levels;
    std::vector<ListOpener> openers;
    std::string ret=paragraph;  // return value

    size_t first_line_break_pos = paragraph.find('\n');
    if(first_line_break_pos != std::string::npos)  {
        // get root object
        std::string first_line = paragraph.substr(0, first_line_break_pos);
        indentation_levels.push_back(getIndentationLevel(first_line));
        removeIndentation(first_line, indentation_levels[0]);
        openers.push_back(getLineListOpener(paragraph));
        if(openers[0] != invalid)  {
            // sort lines into valid items (one items may consist of more than one line)
            std::vector<std::string> items;
            std::stringstream ss(paragraph);
            std::string line;

            ret.clear();
            ret += std::string(openers.back()==sorted?"<ol>":"<ul>") + "\n";

            #define FLUSH_ITEMS() { \
                for(std::string &item : items)  { \
                    boost::trim(item); \
                    ret += "<li>"+item+"</li>\n"; \
                } \
                items.clear(); \
            }

            while(std::getline(ss,line))  {
                if(line.empty())
                    throw Exception("invalid markdown list item: empty line");

                int level = getIndentationLevel(line);

                // manage indentation
                if(level >= indentation_levels.back()+2)  {
                    std::string line_copy = line;  // use copy to identify list opener
                    removeIndentation(line_copy, level);
                    ListOpener opener = getLineListOpener(line_copy);
                    if(opener != invalid)  {
                        FLUSH_ITEMS();
                        indentation_levels.push_back(level);
                        openers.push_back(opener);
                        ret += std::string(openers.back()==sorted?"<ol>":"<ul>") + "\n";
                    }
                }
                else if(level <= indentation_levels.back()-2)  {
                    FLUSH_ITEMS();
                    indentation_levels.pop_back();
                    ret += std::string(openers.back()==sorted?"</ol>":"</ul>") + "\n";
                    openers.pop_back();
                }

                // process line (item)
                removeIndentation(line, indentation_levels.back());

                // identify opener type
                ListOpener opener = getLineListOpener(line);

                // add item to list
                if(opener != invalid)  {
                    // remove opener
                    size_t space_pos = line.find(' ');
                    if(space_pos == std::string::npos)
                        throw Exception("invalid markdown list: item has no space after opener");
                    line.erase(0,space_pos+1);
                    boost::trim(line);
                    // add item
                    items.push_back(line);
                }
                else  {  // this line does not start with a valid opener, so it is some addition to the previous item
                    boost::trim(line);
                    items[items.size()-1] += " "+line;
                }
            }
            // all lines processed, now close the remaining markup tags
            FLUSH_ITEMS();
            while( ! indentation_levels.empty() )  {
                indentation_levels.pop_back();
                ret += std::string(openers.back()==sorted?"</ol>":"</ul>") + "\n";
                openers.pop_back();
            }
        }
    }
    return ret;
}

std::string Markdown::parseMarkdownParagraphToMarkup(std::string paragraph)  {
    // constraints
    if(paragraph.empty())
        return paragraph;
    // identify links
    size_t idx = paragraph.find('[');  // index of opening bracket
    while(idx != std::string::npos)  {
        size_t idx_closing = paragraph.find(']',idx+1);
        if(idx_closing != std::string::npos)  {
            std::string text = paragraph.substr(idx+1, idx_closing-idx-1);
            size_t url_idx = paragraph.find('(', idx_closing+1);
            size_t other_than_url_idx = paragraph.find_first_not_of("( ", idx_closing+1);
            if(url_idx != std::string::npos &&
                (other_than_url_idx==std::string::npos || url_idx<other_than_url_idx))  {
                size_t url_idx_closing = paragraph.find(')',url_idx+1);
                if(url_idx_closing != std::string::npos)  {
                    std::string url = paragraph.substr(url_idx+1, url_idx_closing-url_idx-1);
                    std::string replacement;  // the markup string to replace the markdown with
                    if(idx != 0 && paragraph[idx-1]=='!')  {
                        // this is a picture
                        idx--;
                        replacement = "<img src='"+url+"' alt='"+text+"'>";
                    }
                    else  {
                        // this is a hyperlink
                        replacement = "<a href='"+url+"'>"+text+"</a>";
                    }
                    paragraph.replace(idx, url_idx_closing-idx+1, replacement);
                }
            }
        }
        idx = paragraph.find('[', idx+1);
    }
    // parse emphasis
    for(size_t idx=0; idx<paragraph.size() && idx!=std::string::npos; idx++)  {
        for(const auto &pair : font_markers)  {  // check different marker types
            if(paragraph.rfind(pair.first,idx) == idx)  {  // current idx is at a opening marker
                if(paragraph.size() > idx+pair.first.size() && ! std::isspace(paragraph[idx+pair.first.size()])
                        && paragraph[idx+pair.first.size()] != pair.first[0])  {  // no whitespace after opening marker allowed
                    size_t idx_closing = paragraph.find(pair.first, idx+pair.first.size());
                    if(idx_closing != std::string::npos)  {  // marker needs to be closed somewhere
                        if( ! std::isspace(paragraph[idx_closing-1]) )  {  // no whitespace before closing marker allowed
                            std::string tag_name = pair.second;  // the bare name of the markup tag whitout markup attributes
                            size_t whitespace_pos = tag_name.find(' ');
                            if(whitespace_pos != std::string::npos)
                                tag_name = tag_name.substr(0,whitespace_pos);
                            // replace markers with markup tags
                            paragraph.replace(idx_closing, pair.first.size(), "</"+tag_name+">");
                            paragraph.replace(idx, pair.first.size(), "<"+pair.second+">");
                            idx = idx_closing + pair.first.size()-1;
                            break;
                        }
                    }
                }
            }
        }
    }
    // parse lists
    paragraph = parseList(paragraph);
    // parse quotes
    if(paragraph[0] == '>')  {
        const std::string tag="div";
        const std::string style="border-left:3px solid #e0e0ff; padding-left:10px;";  /// @todo magic number
        int level=1;  // level of indentation
        std::string ret = "<"+tag+" style='"+style+";'>";
            // iterate over lines
            std::stringstream ss(paragraph);
            std::string line;
            while(std::getline(ss, line))  {
                // process line start (level of indentation and spaces)
                int idx_content_starting=0;  // the first character in the line that is part of the content and no '>' or ' '
                int line_level=0;  // level of the current line's level
                while(line[idx_content_starting]=='>' || line[idx_content_starting]==' ')  {
                    if(line[idx_content_starting] == '>')
                        line_level++;
                    idx_content_starting++;
                }
                line.erase(0,idx_content_starting);
                if(line_level==0)
                    line_level++;
                // process level
                while(level<line_level)  {
                    // increase indentation
                    while( ! ret.empty() && ret.back()=='\n')
                        ret.pop_back();
                    level++;
                    ret += "<"+tag+" style='"+style+";'>\n";
                }
                while(level>line_level)  {
                    // decrease indentation
                    while( ! ret.empty() && ret.back()=='\n')
                        ret.pop_back();
                    ret += "</"+tag+">\n";
                    level--;
                }
                ret += line+"<br>\n";
            }
        ret += "</"+tag+">";
        paragraph = ret;
    }
    return paragraph;
}

class MarkdownSection  :  public std::string  {
    public:
        enum Type  { MARKDOWN, CODE, CODEBLOCK };
    private:
        Type type;
        bool started_in_new_line;  ///< whether this section started at the beginning of a new line
    public:
        MarkdownSection(std::string _s, Type _type, bool _started_in_new_line);
        operator Type &();
        std::string typeName();
        bool isStartedInNewLine();
        void setStartedInNewLine(bool val);
};
MarkdownSection::MarkdownSection(std::string _s, Type _type, bool _started_in_new_line)
    : std::string(_s), type(_type), started_in_new_line(_started_in_new_line)  {
}
MarkdownSection::operator MarkdownSection::Type &()  {
    return type;
}
std::string MarkdownSection::typeName()  {
    if(type==MARKDOWN)
        return "markdown";
    if(type==CODE)
        return "code";
    if(type==CODEBLOCK)
        return "codeblock";
    throw Exception("unknown markdown section type");
}
bool MarkdownSection::isStartedInNewLine()  {
    return started_in_new_line;
}
void MarkdownSection::setStartedInNewLine(bool val)  {
    started_in_new_line = val;
}


std::string Markdown::parseMarkdownToMarkup(std::string markdown)  {
    // empty markdown results in empty markup...
    if(markdown.empty())
        return markdown;

    // disect code blocks from markdown blocks
    std::vector<MarkdownSection> sections;  // Each section is either a code block or markdown. Code Blocks will not be parsed.
    size_t last_idx=0;  // the position after the end of the last processed section
    MarkdownSection::Type current_section_type = MarkdownSection::MARKDOWN;
    bool current_section_started_with_new_line=true;
    for(size_t i=0; i<markdown.size() && i!=std::string::npos; i++)  {
        if(markdown[i] == '`')  {
            // count backticks
            /// @todo Can code block starters NOT be escaped by double backslashs?
            int num_backticks=1;
            if(i<markdown.size()-1 && markdown[i+1] == '`')
                num_backticks++;
            if(num_backticks==2 && i<markdown.size()-2 && markdown[i+2] == '`')
                num_backticks++;

            if(num_backticks == 1)  {
                if(current_section_type == MarkdownSection::MARKDOWN)  {  // starting single backtick
                    sections.push_back(MarkdownSection(markdown.substr(last_idx,i-last_idx), current_section_type, current_section_started_with_new_line ));
                    current_section_type = MarkdownSection::CODE;
                    current_section_started_with_new_line = i==0 || markdown[i-1]=='\n';
                    last_idx = i+1;
                }
                else if(current_section_type==MarkdownSection::CODE)  {  // closing single backtick
                    sections.push_back(MarkdownSection(markdown.substr(last_idx,i-last_idx), current_section_type, current_section_started_with_new_line ));
                    current_section_type = MarkdownSection::MARKDOWN;
                    current_section_started_with_new_line = i==0 || markdown[i-1]=='\n';
                    last_idx = i+1;
                }
                else if(current_section_type==MarkdownSection::CODEBLOCK)  {
                    // ignore...
                }
                else
                    throw Exception("unhandled markdown parsing event: single backtick in unexpected environment");
            }
            else if(num_backticks==2)  {  // two stray backticks
                // ignore and advance index so that the next backtick is not parsed as a single backtick
                i++;
            }
            else if(num_backticks == 3)  {
                if(current_section_type == MarkdownSection::MARKDOWN)  {  // starting triple backticks
                    sections.push_back(MarkdownSection(markdown.substr(last_idx,i-last_idx), current_section_type, current_section_started_with_new_line ));
                    current_section_type = MarkdownSection::CODEBLOCK;
                    current_section_started_with_new_line = i==0 || markdown[i-1]=='\n';
                    last_idx = i+3;
                    i+=2;
                }
                else if(current_section_type == MarkdownSection::CODE)  {  // closing single backtick plus two stray backticks
                    sections.push_back(MarkdownSection(markdown.substr(last_idx,i-last_idx), current_section_type, current_section_started_with_new_line ));
                    current_section_type = MarkdownSection::MARKDOWN;
                    current_section_started_with_new_line = i==0 || markdown[i-1]=='\n';
                    last_idx = i+1;
                }
                else if(current_section_type == MarkdownSection::CODEBLOCK)  {  // closing tripe backticks
                    sections.push_back(MarkdownSection(markdown.substr(last_idx,i-last_idx), current_section_type, current_section_started_with_new_line ));
                    current_section_type = MarkdownSection::MARKDOWN;
                    current_section_started_with_new_line = i==0 || markdown[i-1]=='\n';
                    last_idx = i+3;
                    i+=2;
                }
                else
                    throw Exception("unhandled markdown parsing event: single backtick in unexpected environment");
            }
        }
        if(markdown[i]=='\n')  {
            if(i<markdown.size()-1 && markdown[i+1]=='\n')  {
                // flush previous section
                sections.push_back(MarkdownSection(markdown.substr(last_idx,i-last_idx), current_section_type, current_section_started_with_new_line ));
                // skip all the new lines
                while(i<markdown.size() && markdown[i]=='\n')
                    i++;
                i--;
                // next section will be markdown
                current_section_type = MarkdownSection::MARKDOWN;
                current_section_started_with_new_line = true;
                last_idx = i+1;
            }
        }
    }
    sections.push_back(MarkdownSection( markdown.substr(last_idx), current_section_type, last_idx==0||markdown[last_idx-1]=='\n' ));

    // sections are NOT trimmed because they may include double line breaks (paragraph endings)

    // parse code blocks and markdown setions
    std::string ret;  // return value
    bool par_opened=false;  // whether a <p> tag has been opened but no closed yet
    for(size_t section_idx=0; section_idx<sections.size(); section_idx++)  {
        MarkdownSection s = sections[section_idx];
        if(s.empty())
            continue;
            if(s == MarkdownSection::MARKDOWN)  {
                // close paragraph if one is currently opened
                if(par_opened && s.isStartedInNewLine())  {
                    ret+="</p>";
                    par_opened=false;
                }
                // parse markdown
                std::string markdown_paragraph="";  // the paragraph that is currently being constructed
                #define FLUSH_MARKDOWN_PARAGRAPH()  { \
                    boost::trim(markdown_paragraph); \
                    /*if( ! markdown_paragraph.empty() )*/ \
                        if( !par_opened && s.isStartedInNewLine()) { \
                            ret += "<p>"; \
                            par_opened=true; \
                        } \
                        ret += parseMarkdownParagraphToMarkup(markdown_paragraph); \
                        if(par_opened)  { \
                            ret += "</p>"; \
                            par_opened = false; \
                        } \
                        ret += "\n"; \
                        par_opened=false; \
                    markdown_paragraph.clear(); \
                }
                // parse markdown  line by line
                /// @todo handle stupid carriage returns
                for(size_t idx=0; idx<s.size() && idx!=std::string::npos;)  {
                    // fetch next line
                    size_t next_linebreak_idx = s.find('\n', idx+1);
                    std::string markdown_line;
                    if(next_linebreak_idx == std::string::npos)
                        markdown_line = s.substr(idx, std::string::npos);
                    else
                        markdown_line = s.substr(idx, next_linebreak_idx-idx);
                    idx = next_linebreak_idx;

                    // trim linebreaks but not spaces (spaces are needed to identify list indentation
                    while( ! markdown_line.empty() && markdown_line[0]=='\n')
                        markdown_line.erase(0,1);
                    while( ! markdown_line.empty() && markdown_line[markdown_line.size()-1]=='\n')
                        markdown_line.erase(markdown_line.size()-1,1);

                    /*// parse paragraph endings (double clear line)
                    if(markdown_line.empty())  {
                        if( ! markdown_paragraph.empty() )  {
                            FLUSH_MARKDOWN_PARAGRAPH();
                        }
                    } else*/
                    // parse captions
                    if( ! markdown.empty() && markdown_line[0] == '#')  {
                        // first, flush current paragraph
                        if( ! markdown_paragraph.empty() )
                            FLUSH_MARKDOWN_PARAGRAPH();
                        // now, parse the caption
                        int level = markdown_line.find_first_not_of('#');  // the caption level
                        markdown_line.erase(0,level);
                        boost::trim(markdown_line);
                        ret += "<h"+std::to_string(level)+">"  +
                                   parseMarkdownParagraphToMarkup(markdown_line) +
                               "</h"+std::to_string(level)+">" +
                               "\n";
                    }
                    else
                        markdown_paragraph += markdown_line+"\n";
                }
                // process opened but not yet closed markdown paragraph
                boost::trim(markdown_paragraph);
                if( ! markdown_paragraph.empty() )  {
                    if(s.isStartedInNewLine() && !par_opened)  {  // start a new paragraph if this section starts in a new line and now paragraph is currently opened
                        ret += "<p>";
                        par_opened = true;
                    }
                    // no closing </p> tag if the paragraph has not ended yet
                    ret += parseMarkdownParagraphToMarkup(markdown_paragraph);
                }
                #undef FLUSH_MARKDOWN_PARAGRAPH
        }
        else  {  // type is CODE or CODEBLOCK
                // maybe close paragraph if one is currently opened
                if(s == MarkdownSection::CODEBLOCK)  {
                    if(s.isStartedInNewLine())  {
                        if(par_opened)  {
                            ret+="</p>";
                            par_opened=false;  // it is clean to set it to false at this point but the variable will
                            (void)par_opened;  // likely not be read until a few lines below
                        }
                        ret+="<p>";
                        par_opened=true;
                    }
                }
                // open code markup tag
                std::string css = "";
                if(s == MarkdownSection::CODEBLOCK)
                    /// @todo magic number
                    ret += "<pre style='"+css+"; border:1px solid #D8D8D8; padding-left:5px'>";
                else  {
                    ret += "<code style='"+css+"; background-color:#E8E8E8'>";
                }
                // read language tag
                std::string language="";
                if( s == MarkdownSection::CODEBLOCK )  {
                    size_t first_line_break = s.find('\n');
                    if(first_line_break != std::string::npos)  {
                        language = s.substr(0, first_line_break);
                        s.erase(0, first_line_break+1);
                    }
                }
                // parse code
                /// @todo syntax highlighting
                //ret += "language: >"+language+"<\n";
                std::stringstream ss(s);
                std::string line;
                bool first_line=true;
                while(std::getline(ss, line))  {
                    if(first_line)
                        first_line = false;
                    else
                       ret += "\n";
                    ret += line;
                }
                // close code markup tag
                if(s == MarkdownSection::CODEBLOCK)  {
                    ret += "</pre>";
                    if(s.isStartedInNewLine())  {
                        ret += "</p>";
                        par_opened = false;
                        if(section_idx < sections.size()-1)  // next section can be regarded as "started in new line"
                            sections[section_idx+1].setStartedInNewLine(true);
                    }
                }
                else
                    ret += "</code>";
        }
    }
    if(par_opened)  {
        ret+="</p>";
        par_opened = false;  // it is clean to set it to false at this point but the variable will likely not be read
        (void)par_opened;    // until the function returns
    }
    return ret;
}
