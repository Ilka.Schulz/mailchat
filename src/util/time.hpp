/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/util/String.hpp"

namespace Time  {
    /// string format of time stamps used in emails (RFC 822 sec. 5.1) (see @c ::timeStampToText)
    /// @todo There are various formats, see https://jrklein.com/2014/11/18/proper-mail-date-header-formatting-rfc-4021-rfc-2822-rfc-822-and-analysis-of-132k-date-headers/
    const String emailDateFormat = "%a, %d %b %Y %H:%M:%S %z";
    const String pgpExpirationDateFormat = "%b %d %Y";  /// @todo localize

    /// string format of time stamps used in the GUI (see @c ::timeStampToText)
    const String widgetDateFormat = "%b %d %H:%M";

    /// string format of time stamps written to disk
    const String fileDateFormat = emailDateFormat;

    /** @brief converts a time stamp into a (formatted) string
     *  @param t       time stamp
     *  @param format  string format, e.g. @c ::emailDateFormat or @c ::widgetDateFormat
     *  @returns f     ormatted string showing the time stamp
     */
    String timeStampToText(const time_t t, const String &format);

    /** @brief converts string in the specified format into a time stamp
     *  @param s       string representing a time stamp
     *  @param format  some format, e.g. @c emailDateFormat
     *  @returns       the time stamp
     *  @todo handle invalid strings
     */
    time_t textToTimeStamp(const String &s, const String &format);
}
