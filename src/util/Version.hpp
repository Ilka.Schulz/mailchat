/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/util/String.hpp"
#include <vector>

class Version  {
    private:
        String string;
        std::vector<int> parts;

    public:
        Version(const String &_string);
        Version(const std::vector<int> &_parts);
        Version(const int &mainVersion);

    public:
        const String &getString()  const;
        const std::vector<int> &getParts()  const;

        bool operator==(const Version &other)  const;
        bool operator!=(const Version &other)  const;
        bool operator<(const Version &other)   const;
        bool operator>(const Version &other)   const;
        bool operator<=(const Version &other)  const;
        bool operator>=(const Version &other)  const;

    friend int src_util_Version(int,char**);  // tests
};
