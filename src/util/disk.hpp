/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <filesystem>
#include "src/util/String.hpp"

class Disk;
class Account;

/** @brief reads data from disk or writes it to disk
 *  @details A file is created for each @c Account object, so each @c Account object can be saved / loaded individually.
 *  The functions of this class will not just save / load the data of the @c Account object but also associated data,
 *  e.g. @c Connection objects and @c Contact objects.
 */
class Disk {
        static const String propertyKeyConnections;  ///< property key for list of @c Connection objects read from account file
        static const String propertyKeyContacts;     ///< property key for list of @c Contact objects read from account file

    private:
        /// saves all saveable data to disk
        static void saveAllDataToDisk();

        /// saves a specific @c Account and its associated objects to disk
        /// @param account  the @c Account to save
        static void saveAccountToDisk(Account *account);

        /// loads all loadable data from disk
        static void loadAllFromDisk();

        /// loads a specific @c Account and its associated objects to disk
        /// @param entry  the file holding the @c Account data
        /// @returns the newly loaded @c Account object
        static Account *loadAccountFromDisk(std::filesystem::directory_entry &entry);

    /// @c ::main(int,char**) will need to load data at startup end save data on shutdown
    friend int main(int argc, char **argv);
};
