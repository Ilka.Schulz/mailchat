/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <vector>
#include <map>
#include "src/util/String.hpp"
#include <iostream>
#include <filesystem>


/// information parsed from a string like "Max Mustermann <max@mustermann.de>"
struct AddressInfo  {
    /// address name, e.g. "Max Mustermann"
    String name;
    /// address email, e.g. "max@mustermann.de"
    String email;
};

/** @brief converts an address line to an @c AddressInfo
 *  @param line  a string in the form of "name <name@host.tld>"
 *  @returns @c AddressInfo holding the name and email extracted from the specified address line
 */
std::vector<AddressInfo> addressLineToInfo(String line);

/** @brief reads from text file
 *  @details This functions behavior is undefined if the file contains non-ASCII characters because its reads the file
 *  character by character.
 *  @param fn  the path to the file to read
 *  @returns  the content of the text file
 */
String readFromTextFile(String fn);

/** @brief writes to text file
 *  @details This function will work even if @c Settings::dryRun is true because it is only a helper function and will
 *  not write to disk on its own.
 *  @param fn  the path to the file to write to
 *  @param content  the content to write to the file
 */
void writeToTextFile(String fn, String content);

/** @brief returns the base name of a absolute or relative path
 *  @param path  the path the to file
 *  @returns the file's base name
 */
String base_name(const std::filesystem::path &path);


std::vector<char> stringToVector(const String &str);
String vectorToString(const std::vector<char> &vec);
