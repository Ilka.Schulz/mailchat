/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "Properties.hpp"
#include <sstream>
#include "src/util/exceptions/KeyAlreadyExistingException.hpp"
#include "src/util/exceptions/MissingPropertyException.hpp"

Properties::Properties() :
    std::map<String, String>()  {
}
Properties::Properties(Properties &src) :
    std::map<String, String>(src)  {
}
Properties::Properties(std::initializer_list<std::pair<const String,String> > list) :
    std::map<String, String>(list)  {
}
Properties::~Properties()  {
}
void Properties::set(String key, String value)  {
    if(find(key) != end())
        throw KeyAlreadyExistingException(key);
    update(key, value);
}
void Properties::update(String key, String value)  {
    (*this)[key] = value;

}
void Properties::remove(String key)  {
    auto itr = find(key);
    if(itr == end())
        throw MissingPropertyException(key);
    erase(itr);
}
String Properties::get(String key)  {
    auto itr = find(key);
    if(itr == end())
        throw MissingPropertyException(key);
    return itr->second;
}
bool Properties::has(String key)  {
    return find(key)!=end();
}
bool Properties::empty()  {
    return std::map<String,String>::empty();
}

std::map<String,String>::iterator Properties::begin()  {
    return std::map<String,String>::begin();
}
std::map<String,String>::const_iterator Properties::end()  {
    return std::map<String,String>::end();
}


/// @todo use @c String
Properties readPropertiesFromStream(std::istream &stream)  {
    Properties properties;
    std::string line;
    while(std::getline(stream, line))  {
        std::istringstream lineStream(line);
        std::string key;
        if(std::getline(lineStream,key,'='))  {
            if(key.empty() || key[0]=='#')
                continue ;
            std::string value;
            std::getline(lineStream,value);
            properties.set(key, value);
        }
    }
    return properties;
}
void writePropertiesToStream(std::ostream &stream, Properties properties)  {
    for( auto itr : properties )  {
        String line = (itr.first+"="+itr.second+"\n").c_str();
        stream.write(line.c_str(), line.size());
    }
}

Properties mergeProperties(Properties a, Properties b, bool allow_dupllicate)  {
    for( auto itr : b )
        if(allow_dupllicate)
            a.update(itr.first, itr.second);
        else
            a.set(itr.first, itr.second);
    return a;
}
