/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/util/String.hpp"
#include "gpgme.h"
#include "src/util/exceptions/Exception.hpp"
#include <map>

namespace Gpgme  {

    extern const std::map<gpgme_validity_t,String> validityNames;


    enum gpgme_revocation_reason_t  {
        NO_REASON      = 0,
        COMPROMISED    = 1,
        SUPERSEEDED    = 2,
        NO_LONGER_USED = 3
    };

    class Context  {
        private:
            gpgme_ctx_t ctx;
        public:
            Context();
            ~Context();
            operator gpgme_ctx_t();
    };

    class Key  {
        private:
            mutable gpgme_key_t key;
        public:
            // constructors
            Key();
            Key(gpgme_key_t key);
            Key(const Key &other) noexcept;
            Key(Key &&other) noexcept;

            // pseudo-constructors
            static Key getByFingerprint(const String &fingerprint);
            static std::vector<Key> getByEmail(const String &email);
            static std::vector<Key> getAllKeys(const String &filter="");
            static Key create(String userid,
                              String algorithm,
                              ulong expires,
                              const Key &certKey,
                              uint flags=GPGME_CREATE_FORCE);
            static std::vector<Key> import(String raw);
            static Key nullKey();

            // destructor
            ~Key();

            // operators
            //operator gpgme_key_t();
            Key &operator=(const Key &_other);
            bool operator==(const Key &other)  const ;

            void refresh();

            // get properties
            bool isRevoked()        const;
            bool isExpired()        const;
            time_t getExpiry()        const;
            bool isDisabled()       const;
            bool isInvalid()        const;
            bool canEncrypt()       const;
            bool canSign()          const;
            bool canCertify()       const;
            bool isSecret()         const;
            bool canAuthenticate()  const;
            bool isQualified()      const;
            uint getOrigin()        const;
            gpgme_protocol_t getProtocol()  const;
            String getIssuerSerial()   const;
            String getIssuerName()     const;
            String getIssuerChainId()  const;
            gpgme_validity_t getOwnerTrust()  const;
            /// @todo subkeys
            gpgme_user_id_t getUserIds()  const;
            String getFingerprint()  const;
            ulong getLastUpdate()  const;

            bool isHealthy()  const;
            String toString()  const;

            // operations
            void setDisabled(bool disabled);
            void trust(gpgme_validity_t validity=gpgme_validity_t::GPGME_VALIDITY_FULL);
            void changePassphrase();
            void revoke(gpgme_revocation_reason_t reason, String text);
            void delete_(bool askForConfirmation=true);

    };
    void sign(gpgme_key_t signer, gpgme_key_t signee);
    /// @todo change expiration ?

    /// @todo save constructor parameters in all below classes
    class GpgmeKeyNotFoundException : public Exception  {
        public:
            GpgmeKeyNotFoundException();
    };

    class GpgmeCreatingKeyFailedException : public Exception  {
        public:
            GpgmeCreatingKeyFailedException();
    };
    class GpgmeImportingKeyFailedException  :  public Exception  {
        public:
            GpgmeImportingKeyFailedException(const String &reason);
    };
    class GpgmeSigningKeyFailedException : public Exception  {
        public:
            GpgmeSigningKeyFailedException(gpgme_key_t signer, gpgme_key_t signee);
    };
    class GpgmeTrustingKeyFailedException : public Exception  {
        public:
            GpgmeTrustingKeyFailedException(gpgme_key_t key);
    };
    class GpgmeDisablingKeyFailedException : public Exception  {
        public:
            GpgmeDisablingKeyFailedException(gpgme_key_t key);
    };
    class GpgmeRevokingKeyFailedException : public Exception  {
        public:
            GpgmeRevokingKeyFailedException(const Key &key);
    };
    class GpgmeDeletingKeyFailedException  :  public Exception  {
        public:
            GpgmeDeletingKeyFailedException(gpgme_key_t key);
    };
    class GpgmeChangingPassphraseFailedException  : public Exception  {
        public:
            GpgmeChangingPassphraseFailedException(gpgme_key_t key);
    };
}
