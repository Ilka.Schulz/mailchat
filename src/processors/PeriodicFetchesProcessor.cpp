/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "PeriodicFetchesProcessor.hpp"
#include "src/Account.hpp"
#include "src/connections/Connection.hpp"
#include <chrono>
#include <optional>

#include <iostream>

std::optional<
    std::chrono::time_point<
        std::chrono::system_clock
    >
> lastPeriodicFetch = {} ;

void Processors::processPeriodicFetches()  {
    if( ! lastPeriodicFetch.has_value()  ||
            std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::system_clock::now() - lastPeriodicFetch.value()
            ).count() >= 5*60 )  {  /// @todo allow different periods in settings – preferably per @c Account
        // mark time point
        lastPeriodicFetch = std::chrono::system_clock::now();
        // create fetch jobs (non-blocking)
        std::cout << "fetching mails... "<< lastPeriodicFetch.has_value() <<std::endl;
        for(Account *account : Account::getAllAccounts())
            for(Connection *connection : account->getConnections())
                connection->fetchIncomingMessages(false);
    }
}
