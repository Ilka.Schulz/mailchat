/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "GpgEngineProcessorObserver.hpp"
#include "src/util/exceptions/macros.hpp"
#include <algorithm>
#include <assert.h>

std::vector<GpgEngineProcessorObserver*> GpgEngineProcessorObserver::allGpgEngineProcessorObservers;

GpgEngineProcessorObserver::GpgEngineProcessorObserver()  {
    allGpgEngineProcessorObservers.push_back(this);
}
GpgEngineProcessorObserver::~GpgEngineProcessorObserver()  {
    auto itr = std::find(allGpgEngineProcessorObservers.begin(), allGpgEngineProcessorObservers.end(), this);
    assert(itr != allGpgEngineProcessorObservers.end());
    allGpgEngineProcessorObservers.erase(itr);
}

const std::vector<GpgEngineProcessorObserver*> &GpgEngineProcessorObserver::getAllGpgEngineProcessorObservers()  {
    return allGpgEngineProcessorObservers;
}


// do nothing by default
void GpgEngineProcessorObserver::gpgEngineProcessorEvent_keyAdded(Gpgme::Key &)  { }
void GpgEngineProcessorObserver::gpgEngineProcessorEvent_keyRemoved(const Gpgme::Key &)  { }
void GpgEngineProcessorObserver::gpgEngineProcessorEvent_keyExpired(Gpgme::Key &)  { }
void GpgEngineProcessorObserver::gpgEngineProcessorEvent_keyWillExpireSoon(Gpgme::Key &)  { }
