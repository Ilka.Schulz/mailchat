/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "GpgEngineProcessor.hpp"
#include "GpgEngineProcessorObserver.hpp"
#include "src/util/gpgme.hpp"  /// @todo that stuff should be implemented in an asynchronous fashion
#include <chrono>
#include <optional>
#include "src/util/String.hpp"
#include <algorithm>

std::optional<
    std::chrono::time_point<
        std::chrono::system_clock
    >
> lastPeriodicCheck = {} ;

std::vector<Gpgme::Key> keysExpiringSoon;
std::vector<Gpgme::Key> keysExpired;


void Processors::processGpgEngine()  {
    if( ! lastPeriodicCheck.has_value()  ||
            std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::system_clock::now() - lastPeriodicCheck.value()
            ).count() >= 1 )  {
        // mark time point
        lastPeriodicCheck = std::chrono::system_clock::now();

        // check changes
        static std::vector<Gpgme::Key> oldKeys;
        std::vector<Gpgme::Key> keys = Gpgme::Key::getAllKeys();
        if(keys!=oldKeys)
        {
            // check for new keys)
            for(Gpgme::Key &key : keys)  {
                if(std::find(oldKeys.begin(), oldKeys.end(), key) == oldKeys.end())
                    for(GpgEngineProcessorObserver *observer :
                            GpgEngineProcessorObserver::getAllGpgEngineProcessorObservers())  {
                        observer->gpgEngineProcessorEvent_keyAdded(key);
                    }
            }
            // check for removed keys
            for(const Gpgme::Key &oldKey : oldKeys)  {
                if(std::find(keys.begin(), keys.end(), oldKey) == keys.end())
                    for(GpgEngineProcessorObserver *observer :
                            GpgEngineProcessorObserver::getAllGpgEngineProcessorObservers())  {
                        observer->gpgEngineProcessorEvent_keyRemoved(oldKey);
                    }
            }

            oldKeys = keys;
        }
        // check expiration
        for(Gpgme::Key &key : keys)  {
            time_t expires = key.getExpiry();
            // key expired
            if(expires!=0  &&
               expires < time(nullptr)  &&
               std::find(keysExpired.begin(), keysExpired.end(), key)==keysExpired.end())
            {
                for(GpgEngineProcessorObserver *observer :
                        GpgEngineProcessorObserver::getAllGpgEngineProcessorObservers())
                    observer->gpgEngineProcessorEvent_keyExpired(key);
                keysExpired.push_back(key);
            }
            // key will expire
            else if(expires != 0  &&
                    expires > time(nullptr)  &&
                    expires < time(nullptr)+30*86400  &&
                    std::find(keysExpiringSoon.begin(),keysExpiringSoon.end(),key)==keysExpiringSoon.end())
            {
                for(GpgEngineProcessorObserver *observer :
                        GpgEngineProcessorObserver::getAllGpgEngineProcessorObservers())
                    observer->gpgEngineProcessorEvent_keyWillExpireSoon(key);
                keysExpiringSoon.push_back(key);
            }
        }
    }
}
