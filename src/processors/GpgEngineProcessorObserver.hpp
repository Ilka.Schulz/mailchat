/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <vector>
#include "src/util/gpgme.hpp"

namespace Processors  {
    void processGpgEngine();
}

class GpgEngineProcessorObserver  {
    private:
        static std::vector<GpgEngineProcessorObserver*> allGpgEngineProcessorObservers;
    public:
        GpgEngineProcessorObserver();
        ~GpgEngineProcessorObserver();

    public:
        static const std::vector<GpgEngineProcessorObserver*> &getAllGpgEngineProcessorObservers();

    protected:
        virtual void gpgEngineProcessorEvent_keyAdded(Gpgme::Key &key);
        virtual void gpgEngineProcessorEvent_keyRemoved(const Gpgme::Key &key);
        virtual void gpgEngineProcessorEvent_keyExpired(Gpgme::Key &key);
        virtual void gpgEngineProcessorEvent_keyWillExpireSoon(Gpgme::Key &key);

    friend void Processors::processGpgEngine();
};
