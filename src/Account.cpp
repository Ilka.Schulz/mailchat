/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Account.hpp"
#include <algorithm>
#include "src/util/Properties.hpp"
#include <filesystem>
#include <iostream>
#include "src/util/exceptions/Exception.hpp"
#include "src/util/exceptions/MissingPropertyException.hpp"
#include "src/connections/Connection.hpp"
#include "src/AccountObserver.hpp"
#include "src/Contact.hpp"

const String Account::propertyKeyName              = "Account::name";
const String Account::propertyKeyEmail             = "Account::email";
const String Account::propertyKeyPgpKeyFingerprint = "Account::pgpKeyFingerprint";
const String Account::propertyKeySignature         = "Account::signature";
std::vector<Account*> Account::allAccounts;


Account::Account(bool _shadow)
        : Account(generateNewId(), Properties(), _shadow)  {
}
Account::Account(String _id, Properties properties, bool _shadow)
        : id(_id==0?generateNewId():_id), shadow(_shadow)  {
    if( ! properties.empty() )
        setProperties(properties);
    if( ! shadow )  {
        allAccounts.push_back(this);
        // inform ALL observers because they did not have a chance yet to register with this object
        for(AccountObserver *observer : AccountObserver::getAllAccountObservers())
            observer->accountEvent_constructed(this);
    }
}
Account::Account(const String &_id, const String &_name, const String &_email, const String &_pgpKeyFingerprint,
                 const String &_signature, const bool _shadow)
    : Account(_id, Properties{
                                {propertyKeyName,_name},
                                {propertyKeyEmail, _email},
                                {propertyKeyPgpKeyFingerprint, _pgpKeyFingerprint},
                                {propertyKeySignature, _signature}
                             }, _shadow)  {
}
Account::~Account()  {
    // delete all contacts
    while( ! contacts.empty() )
        delete contacts[0];
    contacts.clear();
    // inform observer
    if( ! shadow )  {
        auto itr = std::find(allAccounts.begin(), allAccounts.end(), this);
        if(itr != allAccounts.end())
            allAccounts.erase(itr);
        for(AccountObserver *observer : observers)
            observer->accountEvent_destructed(this);
    }
}

String Account::generateNewId()  {
    bool duplicate=false;
    String idCandidate="";
    do  {
        idCandidate = std::to_string(rand());
        for(Account *account : allAccounts)
            if(account->getId().equals(idCandidate,PLAIN))
                duplicate = true;
    }
    while(duplicate);
    return idCandidate;
}

std::vector<Account*> Account::getAllAccounts()  {
    return allAccounts;
}
Account *Account::getById(String id)  {
    for( Account *account : allAccounts )
        if(account->getId().equals(id,PLAIN))
            return account;
    throw Exception("invalid account id: "+id);
}

void Account::setProperties(Properties properties)  {
    setEmail            (properties.get(propertyKeyEmail));
    setName             (properties.get(propertyKeyName));
    setPgpKeyFingerprint(properties.get(propertyKeyPgpKeyFingerprint));
    setSignature        (properties.get(propertyKeySignature));
}
Properties Account::getProperties()  {
    if(shadow)
        throw Exception("cannot save Account object with shadow==true");
    Properties properties;
    properties.set(propertyKeyName,              getName());
    properties.set(propertyKeyEmail,             getEmail());
    properties.set(propertyKeyPgpKeyFingerprint, getPgpKeyFingerprint());
    properties.set(propertyKeySignature,         getSignature());
    return properties;
}

String Account::getId()  const  {
    return id;
}
String Account::getName()  const  {
    return name;
}
String Account::getNameAndEmail()  const  {
    return name+" <"+email+">";
}
String Account::getEmail()  const  {
    return email;
}
String Account::getPgpKeyFingerprint()  const  {
    return pgpKeyFingerprint;
}
String Account::getSignature()  const  {
    return signature;
}
const std::vector<Contact*> &Account::getContacts()  const  {
    return contacts;
}
std::vector<Connection*> Account::getConnections()  const  {
    std::vector<Connection*> connections;
    for(Connection *connection : Connection::getAllConnections())
        if(connection->getAccount() == this)
            connections.push_back(connection);
    return connections;
}

void Account::setName(String name)  {
    this->name = name;
    for(AccountObserver *observer : observers)
        observer->accountEvent_nameChanged(this);
}
void Account::setEmail(String email)  {
    this->email = email;
    for(AccountObserver *observer : observers)
        observer->accountEvent_emailChanged(this);
}
void Account::setPgpKeyFingerprint(String pgpKeyFingerprint)  {
    this->pgpKeyFingerprint = pgpKeyFingerprint;
    for(AccountObserver *observer : observers)
        observer->accountEvent_pgpKeyFingerprintChanged(this);
}
void Account::setSignature(String signature)  {
    this->signature = signature;
    for(AccountObserver *observer : observers)
        observer->accountEvent_signatureChanged(this);
}
void Account::addContact(Contact *contact)  {
    auto itr = std::find(contacts.begin(), contacts.end(), contact);
    if(itr == contacts.end())  {  // avoid duplicates
        contacts.push_back(contact);
        // inform observers
        for(AccountObserver *observer : observers)
            observer->accountEvent_contactAdded(this, contact);
    }
}
void Account::removeContact(Contact *contact)  {
    auto itr = std::find(contacts.begin(), contacts.end(), contact);
    if(itr != contacts.end())
        contacts.erase(itr);
    // inform observers
    for(AccountObserver *observer : observers)
        observer->accountEvent_contactRemoved(this, contact);
}
