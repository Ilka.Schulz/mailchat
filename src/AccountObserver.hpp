/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>

class AccountObserver;
class Account;
class Contact;

/** @brief observer of @c Account
 *  @details design pattern @c Observer
 */
class AccountObserver  {
    private:
        static std::vector<AccountObserver*> allAccountObservers;
    public:
        AccountObserver();
        ~AccountObserver();
        static std::vector<AccountObserver*> getAllAccountObservers();
    protected:
        virtual void accountEvent_constructed(Account *account);
        virtual void accountEvent_destructed(Account *account);

        /** @brief the observed @c Account object's name has changed, e.g. by user input
         *  @param account  the observed @c Account object
         */
        virtual void accountEvent_nameChanged(Account *account);

        virtual void accountEvent_emailChanged(Account *account);

        virtual void accountEvent_pgpKeyFingerprintChanged(Account *account);

        virtual void accountEvent_signatureChanged(Account *account);

        /** @brief a @c Contact object has been added to the observed @c Account object's contact list
         *  @param account  the observed @c Account object
         *  @param contact  the added @c Contact object
         */
        virtual void accountEvent_contactAdded(Account *account, Contact *contact);

        /** @brief a @c Contact object has been removed from the observed @c Account object's contact list
         *  @param account  the observed @c Account object
         *  @param contact  the removed @c Contact object
         */
        virtual void accountEvent_contactRemoved(Account *account, Contact *contact);

    /// @c Account is the observed class so it needs to call the protected event handlers
    friend Account;
};
