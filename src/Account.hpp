/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <vector>

class Account;
class Contact;
class Connection;
class Disk;

#include "src/patterns/Observable.hpp"
#include "src/AccountObserver.hpp"
#include "src/util/Properties.hpp"


/** @brief a user account in the application, often times also exactly one account at an email service provider
 */
/// @todo make contact observer in order to remove contact on deletion event
class Account  :  public Observable<AccountObserver>  {
        static const String propertyKeyName;               ///< property key for @c Account::name
        static const String propertyKeyEmail;              ///< property key for @c Account::email
        static const String propertyKeyPgpKeyFingerprint;  ///< property key for @c Account::pgpKeyFingerprint
        static const String propertyKeySignature;          ///< property key for @c Account::signature

    private:
        /// static register of all existing objects
        static std::vector<Account*> allAccounts;

        /// unique id within all @c Account objects (also used to determine file name when saving object to disk)
        /// @todo should this not be an integer?
        String id;

        /** @brief if true: object will not be shown in GUI, not be registered in allAccounts and not be saved to disk.
         *  @details Useful for temporary objects for background proceses like connection tests, etc.
         */
        bool shadow;

        /// displayed account name, e.g. "Alice" or "Bob"
        String name;

        /// email address of the account
        String email;

        /// @brief fingerprint of the PGP keypair used by this account
        /// @details The keypair must be available int the GnuPG keyring
        String pgpKeyFingerprint;

        /// @brief signature added to every composed message
        String signature;

        /// list of all contacts owned by this @c Account object
        /// @todo do not use pointers?
        std::vector<Contact*> contacts;

    public:
        /** @brief constructs a new @c Account object with new ID an no properties set yet
         *  @param _shadow  passed to @c Account::Account(String, Properties, bool)
         */
        Account(bool _shadow=false);

        /** @brief constructs an @c Account object with specified ID from specified properties
         * @param _id         the ID of the constructed object – this must be generated with @c generateId()
         * @param properties  properties to set
         * @param _shadow     if true: object is not registered in @c allAccounts
         */
        Account(String _id, Properties properties, bool _shadow=false);
        Account(const String &_id, const String &_name, const String &_email, const String &_pgpKeyFingerprint,
                const String &_signature="", const bool _shadow=false);
        ~Account();

    protected:
        /// generates a new unique ID   @returns the newly generated ID
        static String generateNewId();

        /** @brief changes this object according to specified properties
         *  @param properties  the properties to set
         *  @param allowShallow  whether some properties (all except email) are allowed to be empty
         *  @details allowed properties: \n
         *      - "Account::name"  : the @c Account::name field \n
         *      - "Account::email" : the @c Account::email field \n
         *      - "Account::pgpKeyFingerprint" : the @c Account::pgpKeyFingerprint field \n
         *      - "Account::signature" : the @c Account::signature field \n
         */
        void setProperties(Properties properties);

        /** @returns complete list of properties of this object (see @c Account::setProperties for valid keys)
         */
        Properties getProperties();

    public:
        /// @returns @c Account::allAccounts
        static std::vector<Account*> getAllAccounts();

        /** @brief fetched an existing @c Account object with the specified ID
         *  @details This function throws an exception if no @c Account object with the ID exists.
         *  @param id  the ID to search an @c Account object for
         *  @returns the @c Account with the specified id
         */
        static Account *getById(String id);

        /// @returns @c Account::id
        String getId()  const;
        /// @returns @c Account::name
        String getName() const;
        String getNameAndEmail()  const;
        /// @returns @c Account::email
        String getEmail()  const;
        /// @returns @c Account::pgpKeyFingerprint
        String getPgpKeyFingerprint()  const;
        /// @returns @c Account::signature
        String getSignature()  const;
        /// @returns @c Account::contacts
        const std::vector<Contact*> &getContacts()  const;
        /// @returns a list of @c Connections objects owned by this object
        std::vector<Connection*> getConnections() const;

        /// sets @c Account::name  @param name  the name to set
        void setName(String name);
        /// sets @c Account::email  @param email  the email to set
        void setEmail(String email);
        /// sets @c Account::pgpKeyFingerprint  @param pgpKeyFingerprint  the pgpKeyFingerprint to set
        void setPgpKeyFingerprint(String pgpKeyFingerprint);
        /// sets @c Account::signature  @param signature  the signature to set
        void setSignature(String signature);

    private:
        /// @todo addContact and removeContact are BS because this should be a ContactObserver... – also removed AccountObserver::contact_added/removed
        /// adds a @c Contact object to @c Account::contacts   @param contact  the contact to add
        void addContact(Contact *contact);
        /** @brief removes a @c Contact object from @c Account::contacts
         *  @details This function does nothing if the specified @c Contact object does not exists in
         *      @c Account::contacts
         *  @param contact  the contact to remove
         */
        void removeContact(Contact *contact);

    /// @c Disk reads and writes @c Account objects from and to disk.
    friend Disk;
    friend Contact;  // needs to call addContact and removeContact
};
