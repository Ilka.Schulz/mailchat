/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include <iostream>
#include "src/ui/MainWindow.hpp"
#include "src/Account.hpp"
#include "src/Contact.hpp"
#include <filesystem>
#include <gpgme.h>
#include <QFontDatabase>
#include "src/ui/WindowNewAccount.hpp"
#include "Application.hpp"
#include "src/util/disk.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/exceptions/macros.hpp"
#include "src/util/Version.hpp"

#include "src/connections/SmtpConnection.hpp"
#include "src/connections/ImapConnection.hpp"
#include "src/messages/Message.hpp"

#include "src/connections/ImapFetchJob.hpp"

#include "src/processors/GpgEngineProcessor.hpp"
#include "src/processors/PeriodicFetchesProcessor.hpp"
#include "src/processors/UiProcessor.hpp"
#include <QApplication>  // the qtApplication object is instantiated in main() but declared in the above included file

#include "src/settings.hpp"
#include "src/resources.hpp"
#include <QByteArray>

#include "src/ui/WindowDocumentViewer.hpp"


extern QIcon applicationIcon;
extern QFont emojiFont;

#include <QMessageBox>


/** @brief main function
 *  @details just read the code...
 *  @param argc
 *  @param argv
 *  @return zero if existing gracefully, any other value otherwise
 */
int main(int argc, char **argv)
{
    // read version
    try  {
        Settings::applicationVersion = Version("APPLICATION_VERSION_NUMBER");
    }
    catch(...)  {
        std::cerr << "warning: could not get appliction version"<<std::endl;
    }

    // init Qt
    qtApplication = new QApplication(argc, argv);
    qtApplication->setDesktopFileName("Mailchat");

    // font
    emojiFont = QFont("NotoColorEmoji");
    /// @todo check if font was loaded successfully

    // icon
    QPixmap iconPixmap;
    if( ! iconPixmap.loadFromData((unsigned char*)&_binary_res_icon_icon256_png_start,
                                  &_binary_res_icon_icon256_png_end - &_binary_res_icon_icon256_png_start) )
        throw Exception("could not load application icon");
    applicationIcon.addPixmap(iconPixmap);

    // init random
    srand(time(NULL));

    // init GnuPG
    {
        // init
        const char *gpgmeVersion = gpgme_check_version(nullptr);

        /// @todo set gpg executable file

        // get GnuPG version
        gpgme_engine_info_t engine_info;
        if( gpgme_get_engine_info(&engine_info) != GPG_ERR_NO_ERROR )
            ERROR(std::runtime_error, "can not get gpgme engine info");
        while(engine_info!=nullptr  &&  engine_info->protocol != gpgme_protocol_t::GPGME_PROTOCOL_OpenPGP)
            engine_info = engine_info->next;
        if(engine_info == nullptr)  {
            // no OpenPGP support available
            ERROR(std::runtime_error, "GnuPG installation does not support OpenPGP");
        }
        if(engine_info->file_name == nullptr)  {
            // engine file name not available
            ERROR(std::runtime_error, "GnuPG installation has no executable for OpenPGP protocol");
        }
        if(engine_info->version == nullptr)  {
            // engine not installed properly
            ERROR(std::runtime_error, "GnuPG is not installed properly");
        }
        if(engine_info->req_version != nullptr)  {
            // minimum required version not installed (required by GPGme library)
            if(Version(engine_info->version) < Version(engine_info->req_version))
                ERROR(std::runtime_error, String("GPGme requires minimum version ")+engine_info->req_version+
                                                 ", but installed version is "+engine_info->version);
        }

        /// @todo handle GPG_ERR_INV_ENGINE

        // minimum version required by application
        if(Version(engine_info->version) < Settings::gpgMinimumVersion)  {
            QApplication app(argc,argv);
            QMessageBox box;
            box.setText(QObject::tr("Please install GnuPG version") + " " +
                        QString::fromStdString(Settings::gpgMinimumVersion.getString()) +
                        QObject::tr(" or newer.") + " " +
                        QObject::tr("Currently installed is version:") + " " +
                        QString::fromLatin1(engine_info->version) + " " +
                        QObject::tr("You can download a newer version from:") + " " +
                        QString::fromStdString(Settings::gpgDownloadUrl)
                       );
            box.setIcon(QMessageBox::Icon::Critical);
            box.exec();
            return 1;
        }

        // check OpenPGP protocol
        gpgme_error_t openPGPCheck = gpgme_engine_check_version(gpgme_protocol_t::GPGME_PROTOCOL_OpenPGP);
        if(openPGPCheck != GPG_ERR_NO_ERROR)  {
            QApplication app(argc,argv);
            QMessageBox box;
            box.setText(QObject::tr("The installed GnuPG executable does not support OpenPGP protocol.")+" "+
                        QObject::tr("You can download a newer version from:")+" "+
                        QString::fromStdString(Settings::gpgDownloadUrl)
                       );
            box.setIcon(QMessageBox::Icon::Critical);
            box.exec();
            return 1;
        }
    }

    // read from disk
    std::filesystem::create_directories(Settings::diskAccountsDirectory.toStdString());
    std::filesystem::create_directories(Settings::diskMessagesDirectory.toStdString());
    Disk::loadAllFromDisk();

    // init GUI
    new MainWindow();

    /// @todo special dialogs
    if(Account::getAllAccounts().empty())  {  /// @todo hide main window during this
        new WindowNewAccount(true);
    }

    // run application
    Application::getInstance()->addHandler(Processors::processGpgEngine);
    Application::getInstance()->addHandler(Processors::processUi);
    if( ! Settings::debug )
        Application::getInstance()->addHandler(Processors::processPeriodicFetches);
    Application::getInstance()->run();

    /// @todo free curl multi handles

    // save to disk
    Disk::saveAllDataToDisk();

    return 0;
}
