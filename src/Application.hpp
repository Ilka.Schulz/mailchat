/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>

class Application;


/** @brief singleton class that repeatedly calls various event processing handlers
 *  @details Design pattern @c Singleton
 */
class Application  {
    private:
        /// whether the application should exit on the next execution of the main loop body
        bool exitScheduled;
        /// all handlers that shall be called repeatedly
        std::vector<void(*)()> handlers;
        /// singleton object
        static Application *instance;

    private:
        /// constructs object but does not enter the main loop (call Application::run() for that)
        Application();
        /// gets called on exit
        ~Application();

    public:
        /// @returns the singleton instance @c Application::instance
        static Application *getInstance();

        /// starts a blocking infite loop that can only be exited by a call to Application::exit()
        void run();

        /// sets exitScheduled to true an thus lets the application exit soon
        void exit();

        /// registers a calback function that shall be called repreatedly during the object main loop
        /// @param handler the handler function pointer to add
        void addHandler(void (*handler)());

        /// call each handler once. Blocking code should call this repeatedly.
        /// @param handleExceptions If true, the method will try its best to handle exceptions. Functions that implement blocking behavior and call this function in order to not let the application freeze, should set this option to false in order to propagate exceptions back to their own exception handlers.
        void refresh(bool handleExceptions=true);
};
