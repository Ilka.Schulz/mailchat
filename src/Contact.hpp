/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include "src/util/Properties.hpp"

class Contact;
class Account;
class Disk;

#include "src/ui/ContactWidget.hpp"

#include "src/patterns/Observable.hpp"
#include "ContactObserver.hpp"
#include "src/util/misc.hpp"
#include "src/util/TristateBool.hpp"
#include "src/util/String.hpp"


/** @brief contact information
 */
class Contact  :  public Observable<ContactObserver>  {
    public:
        /** @brief unique id to identify every @c Contact object
         *  @details Those 32 bits are actually needed because every combination of contacts and even newsletter
         *  authors, etc. will be a contact object.
         *  @todo use unique IDs within an @c Account object's "namespace"
         */
        typedef uint32_t ContactId;

    private:
        static const String propertyKeyId;     ///< property key for @c Contact::id
        static const String propertyKeyName;   ///< property key for @c Contact::name
        static const String propertyKeyEmail;  ///< property key for @c Contact::email
        static const String propertyKeyConfirmed;  ///< property key for @c Contact::confirmed

    private:
        /// static register for all existing @c Contact objects
        static std::vector<Contact*> allContacts;
        /// unique id to identify every @c Contact object
        ContactId id;
        /// contact name, random string, e.g. "Alice"
        String name;
        /// contact email, e.g. "alice@example.org"
        String email;
        /// whether the user manually confirmed to chat with this contact
        /// if unknown: this object has been created automatically (e.g. because the IMAP account has some correspondence
        /// with the user.
        TristateBool confirmed;
        /// @c Account that owns this object
        Account *account;

    public:
        /** @brief constructor
         *  @param _id         initializes @c Contact::id  (can be zero so that the constructor chooses a new unique id)
         *  @param _account    the @c Account object that owns this object
         *  @param properties  the list of @c Properties that initialize this object (see @c Contact::setProperties)
         */
        Contact(ContactId _id, Account *_account, Properties properties);
        Contact(ContactId _id, Account *_account, String name="", String email="", TristateBool confirmed=true);
        virtual ~Contact();

    private:
        /** @brief generates a new unique id
         *  @todo use unique IDs within an @c Account object's "namespace"
         *  @returns a new unique id
         */
        static ContactId generateNewId();

        /** @brief adjusts this object to have the specified properties
         *  @param properties to set
         *  @details see @c Contact::propertyKey* members for valid keys
         */
        void setProperties(Properties properties);

        /// @returns complete list of properties of this object (see @c Contact::setProperties for valid keys)
        Properties getProperties();

    public:
        /// @returns @c Contact::allContacts
        static std::vector<Contact*> getAllContacts();
        /// @returns @c Contact::id
        ContactId getId();
        /// @returns the @c Contact object with the specified id  @param id  the the id of the @c Contact object to search
        static Contact *getById(ContactId id);
        /// @returns @c Contact::name
        String getName();
        /// @returns @c Contact::email
        String getEmail();
        /// @returns @c Contact::confirmed
        TristateBool isConfirmed();
        /// @returns @c Contact::account
        Account *getAccount();
        /// sets @c Contact::name  @param name  the name to set
        void setName(String name);
        /// sets @c Contact::email  @param email  the email to set
        void setEmail(String email);
        /// sets @c Contact::confirmed  @param confirmed  the new confirmation status to set
        void setConfirmed(TristateBool confirmed);
        /// sets @c Contact::account  @param newAccount  the new account to own this object
        void setAccount(Account *newAccount);

    public:
        /** @brief returns a @c Contact object meeting the specified addressInfo
         *  @details This function searches through the existing @c Contact objects and returns the one whose contact
         *  email / addressInfo meets the specified addressInfo. If no such @c Contact object is found, a new one is
         *  created and returned. The newly created object will be unconfirmed.
         *  @todo see #20: derive subclass @c EmailContact and move this function there
         *  @param account      the @c Account object that the @c Contact object must be associated with
         *  @param addressInfo  the email / @c std::vector<AddressInfo> that the @c Contact object must have
         *  @returns the found or newly created @c Contact object
         */
        static Contact *getEmailContact(Account *account, std::vector<AddressInfo> addressInfo);

    friend Disk;  ///< needs to load from and save data to disk
};
