/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

class ContactObserver;
class Contact;

/** @brief observer of @c Contact
 *  @details design pattern @c Observer
 */
class ContactObserver  {
    protected:
        /** @brief the observerd @c Contact object's destructor has been called but has not yet finished
         *  @param contact  the observed @c Contact object
         */
        virtual void contactEvent_destroyedEvent(Contact *contact);

        /** @brief the observed @c Contact object's name has been changed by the user (or by the application)
         *  @param contact  the observed @c Contact object
         */
        virtual void contactEvent_nameChangedEvent(Contact *contact);

        /** @brief the observed @c Contact object's email has been changed by the user (or by the application)
         *  @param contact  the observed @c Contact object
         */
        virtual void contactEvent_emailChangedEvent(Contact *contact);

        /** @brief the observed @c Contact object's confirmation status has changed, e.g. because the user confirmed it
         *  @param contact  the observed @c Contact object
         */
        virtual void contactEvent_confirmedChangedEvent(Contact *contact);

    /// @c Contact is the observed class so it needs to call the protected event handlers
    friend Contact;
};
