/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AccountObserver.hpp"
#include <algorithm>

std::vector<AccountObserver*> AccountObserver::allAccountObservers;

AccountObserver::AccountObserver()  {
    allAccountObservers.push_back(this);
}
AccountObserver::~AccountObserver()  {
    const auto itr = std::find(allAccountObservers.begin(), allAccountObservers.end(), this);
    if(itr != allAccountObservers.end())
        allAccountObservers.erase(itr);
}

std::vector<AccountObserver*> AccountObserver::getAllAccountObservers()  {
    return allAccountObservers;
}

// do nothing by default
void AccountObserver::accountEvent_constructed(Account *account)  { }
void AccountObserver::accountEvent_destructed(Account *account)  { }
void AccountObserver::accountEvent_nameChanged(Account *account)  { }
void AccountObserver::accountEvent_emailChanged(Account *account)  { }
void AccountObserver::accountEvent_pgpKeyFingerprintChanged(Account *account)  { }
void AccountObserver::accountEvent_signatureChanged(Account *account)  { }
void AccountObserver::accountEvent_contactAdded(Account *account, Contact *contact)  { }
void AccountObserver::accountEvent_contactRemoved(Account *account, Contact *contact)  { }
