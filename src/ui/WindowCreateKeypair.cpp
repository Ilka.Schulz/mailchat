/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/ui/WindowCreateKeypair.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/Properties.hpp"
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <gpgme.h>
#include <chrono>
#include "src/util/gpgme.hpp"
#include <QMessageBox>
#include "src/ui/AccountSelectorWidget.hpp"
#include "src/Account.hpp"

WindowCreateKeypair::WindowCreateKeypair(Account *_account)
        : WindowGenericDialog("Create Keypair", 400,300)  {
    // properties grid
    propertiesGrid = new QGridLayout(this);
    propertiesGrid->layout()->setSpacing(4);  /// @todo magic number
    getContainer()->setLayout(propertiesGrid);
    propertiesGrid->setAlignment(Qt::AlignTop);
        // account
            // label
            accountLabel = new QLabel(tr("Account:"), this);
            propertiesGrid->addWidget(accountLabel, 0,0);
            // input
            accountSelectorWidget = new AccountSelectorWidget(this);
            if(_account == nullptr && ! Account::getAllAccounts().empty())
                accountSelectorWidget->selectAccount(Account::getAllAccounts()[0]);
            else
                accountSelectorWidget->selectAccount(_account);
            accountSelectorWidget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
            propertiesGrid->addWidget(accountSelectorWidget, 0,1);
            // switch to manual label
            accountSwitchToManualLabel = new QLabel("<a href='switch'>"+tr("switch to manual input mode")+"</a>");
            propertiesGrid->addWidget(accountSwitchToManualLabel, 1,1);
            QObject::connect(accountSwitchToManualLabel, &QLabel::linkActivated, this,
                             &WindowCreateKeypair::accountSwitchToManualLabelLinkActivatedEvent);
        // expiry
            // label
            expiryLabel = new QLabel(this);
            expiryLabel->setText(tr("expires:"));
            propertiesGrid->addWidget(expiryLabel, 2,0, 1,1);
            // input box
                expiryInputBox = new QBoxLayout(QBoxLayout::LeftToRight);
                expiryInputBox->setSpacing(4);  /// @todo magic number
                propertiesGrid->addLayout(expiryInputBox, 2,1, 1,1);
                    // number spinner
                    expiryNumberInput = new QSpinBox(this);
                    expiryNumberInput->setSingleStep(1);
                    expiryNumberInput->setMinimum(1);
                    expiryNumberInput->setMaximum(1000);
                    expiryInputBox->addWidget(expiryNumberInput);
                    // unit combobox
                    expiryUnitInput = new QComboBox(this);
                    expiryUnitInput->addItem("years" , "y");
                    expiryUnitInput->addItem("months", "m");
                    expiryUnitInput->addItem("days"  , "d");
                    expiryUnitInput->setCurrentIndex(0);
                    expiryInputBox->addWidget(expiryUnitInput);
    /// @todo define tab order
    /*setTabOrder(nameInput, emailInput);
    setTabOrder(emailInput, expiryNumberInput);
    setTabOrder(expiryNumberInput, expiryUnitInput);*/
    show();
    accountSelectorWidget->setFocus();
}

WindowCreateKeypair::WindowCreateKeypair(String name, String email)
    :  WindowCreateKeypair(nullptr) {
    accountSwitchToManualLabelLinkActivatedEvent("");
    nameInput->setText(QString::fromStdString(name));
    emailInput->setText(QString::fromStdString(email));
}

void WindowCreateKeypair::accountSwitchToManualLabelLinkActivatedEvent(const QString &link)  {
    Account *account = nullptr;  // the previously selected Account
    if(accountSelectorWidget != nullptr)
        account = accountSelectorWidget->getSelectedAccount();

    // delete old widgets
    delete accountLabel;
    accountLabel = nullptr;
    delete accountSelectorWidget;
    accountSelectorWidget = nullptr;
    delete accountSwitchToManualLabel;
    accountSwitchToManualLabel = nullptr;

    // name
        // label
        nameLabel = new QLabel(this);
        nameLabel->setText(tr("Your name:"));
        propertiesGrid->addWidget(nameLabel, 0,0, 1,1);
        // input
        nameInput = new QLineEdit(this);
        if(account != nullptr)
            nameInput->setText(QString::fromStdString(account->getName()));
        propertiesGrid->addWidget(nameInput, 0,1, 1,1);
    // email
        // label
        emailLabel = new QLabel(this);
        emailLabel->setText(tr("Email address:"));
        propertiesGrid->addWidget(emailLabel, 1,0, 1,1);
        // input
        emailInput = new QLineEdit(this);
        if(account != nullptr)
            emailInput->setText(QString::fromStdString(account->getEmail()));
        propertiesGrid->addWidget(emailInput, 1,1, 1,1);
}

void WindowCreateKeypair::okButtonClickedEvent()  {
    // gather data
    Account *account=nullptr;
    String name;
    String email;
    if(accountSelectorWidget != nullptr)  {
        account = accountSelectorWidget->getSelectedAccount();
        if(account == nullptr)  {
            QMessageBox box;
            box.setIcon(QMessageBox::Critical);
            box.setText(tr("You need to select the account for which to create the keypair"));
            box.exec();
            return ;
        }
        name = account->getName();
        email = account->getEmail();
    }
    else  {
        name = nameInput->text().toStdString();
        email = emailInput->text().toStdString();
    }
    String expiryNumberString = expiryNumberInput->text().toStdString();
    int expiryNumber;
    try  {
        expiryNumber = boost::lexical_cast<int>(expiryNumberString);
    }
    catch(boost::bad_lexical_cast &)  {
        QMessageBox box;
        box.setIcon(QMessageBox::Critical);
        box.setText(tr("Please specifiy a valid number for the expiration."));
        box.exec();
        return ;
    }
    String expiryUnit = expiryUnitInput->currentData().toString().toStdString();
    int expirySeconds = 0;
    if(expiryUnit == "d")  /// @todo implement this properly
        expirySeconds = expiryNumber*86400;
    else if(expiryUnit == "m")
        expirySeconds = expiryNumber*30.5*86400;
    else if(expiryUnit == "y")
        expirySeconds = expiryNumber*365.25*86400;
    else
        throw Exception("unknown expiry unit: "+expiryUnit);

    // constraints
    if(name.empty())
        ; /// @todo ask user if they are sure
    if(email.empty())  {  /// @todo sanity-check email
        QMessageBox box;
        box.setIcon(QMessageBox::Critical);
        box.setText(tr("Please specify a valid email address."));
        box.exec();
        return ;
    }

    // generate keypair
    Gpgme::Key key = Gpgme::Key::create(
            name+" <"+email+">",
            "default", // @todo give user option
            expirySeconds,
            nullptr,
            GPGME_CREATE_FORCE
        );

    QMessageBox box;
    box.setIcon(QMessageBox::Information);
    box.setText(tr("You sucessfully created a new keypair. Its fingerprint is ") +
                   QString::fromStdString(key.getFingerprint()) +
                   ".");

    // close window
    delete this;
}
