/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "WindowKeyManagement.hpp"
#include <QBoxLayout>
#include <QLineEdit>
#include <QTreeWidget>
#include <QHeaderView>
#include <QToolButton>
#include <QMenu>
#include <gpgme.h>
#include "src/util/gpgme.hpp"
#include "src/util/exceptions/Exception.hpp"
#include <QMessageBox>
#include "src/util/time.hpp"

#include "src/ui/WindowCreateKeypair.hpp"
#include "src/ui/WindowChangeGpgKeyTrust.hpp"

WindowKeyManagement::WindowKeyManagement()
    : WindowGenericDialog("Key Management")  {
    verticalBox = new QBoxLayout(QBoxLayout::TopToBottom, this);
    setMinimumSize(600,400);  /// @todo magic numbers
    getContainer()->setLayout(verticalBox);
        // header box
        headerBox = new QBoxLayout(QBoxLayout::LeftToRight, this);
        verticalBox->addLayout(headerBox);
            // search input
            searchEdit = new QLineEdit(this);
            searchEdit->setPlaceholderText(tr("search..."));
            /// @todo set icon for searchEdit
            searchEdit->setClearButtonEnabled(true);
            QObject::connect(searchEdit, &QLineEdit::textChanged, this, &WindowKeyManagement::searchEditTextChangedEvent);
            headerBox->addWidget(searchEdit);
            // menu button
            menuButton = new QToolButton(this);
            menuButton->setText("\u2630");  /// @todo add this to emoji list
            menuButton->setPopupMode(QToolButton::InstantPopup);
            headerBox->addWidget(menuButton);
                // menu
                QMenu *menu = new QMenu(this);
                    QMenu *menuAddKey = new QMenu(tr("&Add Key"), this);
                    menu->addMenu(menuAddKey);
                        /*QAction *menuAddKeyFromFile = new QAction(tr("From &File"), this);
                        menuAddKey->addAction(menuAddKeyFromFile);
                        QAction *menuAddKeyFromClipboard = new QAction(tr("From &Clipbloard"), this);
                        menuAddKey->addAction(menuAddKeyFromClipboard);
                        QAction *menuAddKeyFromUrl = new QAction(tr("From &URL"), this);
                        menuAddKey->addAction(menuAddKeyFromUrl);
                        QAction *menuAddKeyFromKeyserver = new QAction(tr("From &Keyserver"), this);
                        menuAddKey->addAction(menuAddKeyFromKeyserver);*/
                        QAction *menuAddKeyGenerateNew = new QAction(tr("&Generate new one"), this);
                        menuAddKey->addAction(menuAddKeyGenerateNew);
                        QObject::connect(menuAddKeyGenerateNew, &QAction::triggered, this,
                                         &WindowKeyManagement::menuAddKeyGenerateNewTriggeredEvent);
                        /*QAction *menuAddKeySearchForAllContacts = new QAction(tr("&Search for all contacts on keyserver"), this);
                        menuAddKey->addAction(menuAddKeySearchForAllContacts);
                    QMenu *menuExportKey = new QMenu(tr("&Export Key"), this);
                    menu->addMenu(menuExportKey);
                        QAction *menuExportKeyAllOwnToKeyserver = new QAction(tr("All own to &Keyserver"), this);
                        menuExportKey->addAction(menuExportKeyAllOwnToKeyserver);
                        QAction *menuExportKeyAllOwnToFile = new QAction(tr("All own to &File"), this);
                        menuExportKey->addAction(menuExportKeyAllOwnToFile);
                        QAction *menuExportKeyRevocationCertificat = new QAction(tr("&Revocation certificate to file"), this);
                        menuExportKey->addAction(menuExportKeyRevocationCertificat);*/
                    //QMenu *menuView = new QMenu(tr("&View"), this);

                    menu->addSeparator();

                    QAction *menuRefresh = new QAction(tr("&Rescan Keyring"), this);
                    menu->addAction(menuRefresh);
                    connect(menuRefresh, &QAction::triggered, this,
                            &WindowKeyManagement::menuRefreshTriggeredEvent);

                    //menu->addMenu(menuView);
                menuButton->setMenu(menu);
        // tree widget
        treeWidget = new QTreeWidget(this);
        treeWidget->setColumnCount(4);  /// @todo magic number
        treeWidget->setHeaderLabels(QStringList{tr("Name"), tr("Key ID"), tr("Trust"), tr("Expires")});
        treeWidget->header()->setSectionResizeMode(QHeaderView::Stretch);
        treeWidget->setRootIsDecorated(false);
        treeWidget->setSelectionMode(QTreeWidget::SelectionMode::ExtendedSelection);
        treeWidget->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
        QObject::connect(treeWidget, &QTreeWidget::customContextMenuRequested, this,
                         &WindowKeyManagement::treeWidgetCustomContextMenuRequestedEvent);
        verticalBox->addWidget(treeWidget);
    // scan key ring once
    scanKeyRing();

    show();
    searchEdit->setFocus();
    /// @todo hide OK and Cancel button
}

void WindowKeyManagement::searchEditTextChangedEvent(const QString &text)  {
    (void)text;
    scanKeyRing();
}

void WindowKeyManagement::scanKeyRing()  {
    std::string search = searchEdit->text().toStdString();
    std::vector<Gpgme::Key> keys = Gpgme::Key::getAllKeys(search);
    // delete list box entries
    treeWidget->clear();
    // show key fingerprint candidates
    if( ! keys.empty() )  {
        for(const Gpgme::Key &key : keys )  {
            // list box row
            QTreeWidgetItem *item = new QTreeWidgetItem(treeWidget);
            /// @todo this decision to disable some keys is copy-pasted code
            bool disabled = ! key.isHealthy();
            //item->setDisabled(disabled);
            for(int column=0; column<treeWidget->columnCount(); column++)  {
                QFont font = item->font(column);
                if(key.isSecret())
                    font.setBold(true);
                if(disabled)
                    font.setStrikeOut(true);
                item->setFont(column,font);
            }
            gpgme_user_id_t user_id = key.getUserIds();
            item->setText(0, QString::fromStdString(  /// @todo magic number
                              std::string(user_id->name) + " <" + std::string(user_id->email) + ">"
                         ) );
            item->setText(1, QString::fromStdString(  /// @todo magic number
                              std::string(key.getFingerprint())
                         ) );
            item->setText(2, QString::fromStdString(  /// @todo magic number
                              Gpgme::validityNames.find(key.getOwnerTrust())->second
                         ) );
            item->setText(3, QString::fromStdString(  /// @todo magic number
                              Time::timeStampToText(key.getExpiry(),Time::widgetDateFormat)
                         ) );
        }
    }
}

void WindowKeyManagement::menuAddKeyGenerateNewTriggeredEvent(bool checked)  {
    (new WindowCreateKeypair())->exec();
}

void WindowKeyManagement::treeWidgetCustomContextMenuRequestedEvent(const QPoint &pos)  {
    QList<QTreeWidgetItem*> selectedItems =  treeWidget->selectedItems();
    if(selectedItems.empty())
        return ;
    QMenu *menu = new QMenu(this);
        /*QMenu *menuExport = new QMenu(tr("&Export"), menu);
        menu->addMenu(menuExport);
            QMenu *menuExportPubOnly = new QMenu(tr("&Public key only"), menuExport);
            menuExport->addMenu(menuExportPubOnly);
                QAction *menuExportPubOnlyToFile = new QAction(tr("To &File"), menuExportPubOnly);
                menuExportPubOnly->addAction(menuExportPubOnlyToFile);
                QAction *menuExportPubOnlyToClipboard = new QAction(tr("To &Clipboard"), menuExportPubOnly);
                menuExportPubOnly->addAction(menuExportPubOnlyToClipboard);
                QAction *menuExportPubOnlyToKeyserver = new QAction(tr("To &Keyserver"), menuExportPubOnly);
                menuExportPubOnly->addAction(menuExportPubOnlyToKeyserver);
            QMenu *menuExportPubAndSecret = new QMenu(tr("Public and &secret key"), menuExport);
            menuExport->addMenu(menuExportPubAndSecret);
                QAction *menuExportPubAndSecretToFile = new QAction(tr("To &File"), menuExportPubAndSecret);
                menuExportPubAndSecret->addAction(menuExportPubAndSecretToFile);
                QAction *menuExportPubAndSecretToClipboard = new QAction(tr("To &Clipboard"), menuExportPubAndSecret);
                menuExportPubAndSecret->addAction(menuExportPubAndSecretToClipboard);
        menu->addSeparator();*/
        QMenu  *menuTrust = new QMenu(tr("&Trust"), this);
        menu->addMenu(menuTrust);
            QAction *menuTrustTrust = new QAction(tr("&Trust"), this);
            menuTrustTrust->setEnabled(true);  /// @todo
            menuTrust->addAction(menuTrustTrust);
            QObject::connect(menuTrustTrust, &QAction::triggered, this,
                             &WindowKeyManagement::contextMenuTrustTrustTriggeredEvent);

            QAction *menuTrustUntrust = new QAction(tr("&Untrust"), this);
            menuTrustUntrust->setEnabled(true);  /// @todo
            menuTrust->addAction(menuTrustUntrust);
            QObject::connect(menuTrustUntrust, &QAction::triggered, this,
                             &WindowKeyManagement::contextMenuTrustUntrustTriggeredEvent);

            QAction *menuTrustCustom = new QAction(tr("&Custom"), this);
            menuTrust->addAction(menuTrustCustom);
            QObject::connect(menuTrustCustom, &QAction::triggered, this,
                             &WindowKeyManagement::contextMenuTrustCustomTriggeredEvent);
        /*QAction *menuSign = new QAction(tr("&Sign"), this);
        menu->addAction(menuSign);*/

        // disable
        {
            if(selectedItems.size()>1)  {
                QAction *menuDisable = new QAction(tr("&Disable"), this);
                menu->addAction(menuDisable);
                connect(menuDisable, &QAction::triggered, this, &WindowKeyManagement::contextMenuDisableTriggeredEvent);

                QAction *menuEnable = new QAction(tr("&Enable"), this);
                menu->addAction(menuEnable);
                connect(menuEnable, &QAction::triggered, this, &WindowKeyManagement::contextMenuEnableTriggeredEvent);
            }
            else  {
                Gpgme::Key key = Gpgme::Key::getByFingerprint(selectedItems.at(0)->text(1).toStdString());  /// @todo magic number
                QAction *menuToggleDisabled = new QAction(key.isDisabled()?"&Enable":"&Disable");
                menu->addAction(menuToggleDisabled);
                connect(menuToggleDisabled, &QAction::triggered, this,
                        key.isDisabled() ?
                            &WindowKeyManagement::contextMenuEnableTriggeredEvent :
                            &WindowKeyManagement::contextMenuDisableTriggeredEvent
                       );
            }
        }

        // revoke
        {
            /// @todo only secret keys can be revoked
            bool not_yet_revoked=false;
            for(QTreeWidgetItem *item : selectedItems)  {
                Gpgme::Key key = Gpgme::Key::getByFingerprint(item->text(1).toStdString());  /// @todo magic number
                if( ! key.isRevoked() )
                    not_yet_revoked = true;
            }
            QAction *menuRevoke = new QAction((not_yet_revoked ? tr("&Revoke") : tr("Already revoked...")),
                                              this
                                              );
            menuRevoke->setEnabled(not_yet_revoked);
            menu->addAction(menuRevoke);
            connect(menuRevoke, &QAction::triggered, this, &WindowKeyManagement::contextMenuRevokeTriggeredEvent);
        }

        QAction *menuDelete = new QAction(tr("&Delete"), this);
        menu->addAction(menuDelete);
        QObject::connect(menuDelete, &QAction::triggered, this,
                         &WindowKeyManagement::contextMenuDeleteTriggeredEvent);

        menu->addSeparator();

        /*QAction *menuModifyExpiration = new QAction(tr("&Modify Expiration"), this);
        menu->addAction(menuModifyExpiration);*/

        QAction *menuChangePassphrase = new QAction(tr("&Change Passphrase"), this);
        menu->addAction(menuChangePassphrase);
        connect(menuChangePassphrase, &QAction::triggered, this,
                &WindowKeyManagement::contextMenuChangePassphraseTriggeredEvent);

        //menu->addSeparator();

        /*QAction *menuProperties = new QAction(tr("&Properties"), this);
        menu->addAction(menuProperties);*/


    menu->popup(treeWidget->mapToGlobal(pos));
}

void WindowKeyManagement::contextMenuTrustTrustTriggeredEvent(bool checked)  {
    (void)checked;
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        std::string fingerprint = item->text(1).toStdString();  /// @todo magic number
        Gpgme::Key key = Gpgme::Key::getByFingerprint(fingerprint);
        try {
            key.trust(gpgme_validity_t::GPGME_VALIDITY_FULL);
        }  catch (Gpgme::GpgmeTrustingKeyFailedException &e) {
            QMessageBox box;
            box.setText(tr("trusting the key with the fingerprint ") +
                        QString::fromStdString(key.getFingerprint()) +
                        tr("failed"));
            box.setIcon(QMessageBox::Critical);
            box.exec();
        }
    }
    scanKeyRing();
}
void WindowKeyManagement::contextMenuTrustUntrustTriggeredEvent(bool checked)  {
    (void)checked;
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        std::string fingerprint = item->text(1).toStdString();  /// @todo magic number
        Gpgme::Key key = Gpgme::Key::getByFingerprint(fingerprint);
        try {
            key.trust(gpgme_validity_t::GPGME_VALIDITY_NEVER);
        }  catch (Gpgme::GpgmeTrustingKeyFailedException &e) {
            QMessageBox box;
            box.setText(tr("untrusting the key with the fingerprint ") +
                        QString::fromStdString(key.getFingerprint()) +
                        tr("failed"));
            box.setIcon(QMessageBox::Critical);
            box.exec();
        }
    }
    scanKeyRing();
}
void WindowKeyManagement::contextMenuTrustCustomTriggeredEvent(bool checked)  {
    (void)checked;
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        std::string fingerprint = item->text(1).toStdString();  /// @todo magic number
        Gpgme::Key key = Gpgme::Key::getByFingerprint(fingerprint);
        WindowChangeGpgKeyTrust *window = new WindowChangeGpgKeyTrust(key);
        connect(window, &QDialog::accepted, this, [this]()  {
            scanKeyRing();
        } );
    }
}
// Sign
void WindowKeyManagement::contextMenuDisableTriggeredEvent(bool checked)  {
    (void) checked;
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        std::string fingerprint = item->text(1).toStdString(); /// @todo magic number
        Gpgme::Context context;
        Gpgme::Key key = Gpgme::Key::getByFingerprint(fingerprint);
        try {
            key.setDisabled(true);
        }  catch (Gpgme::GpgmeDisablingKeyFailedException &e) {
            QMessageBox box;
            box.setText(tr("disabling the key with the fingerprint ") +
                        QString::fromStdString(key.getFingerprint()) +
                        tr(" failed"));
            box.setIcon(QMessageBox::Critical);
            box.exec();
        }
    }
    scanKeyRing();
}
void WindowKeyManagement::contextMenuEnableTriggeredEvent(bool checked)  {
    (void) checked;
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        std::string fingerprint = item->text(1).toStdString(); /// @todo magic number
        Gpgme::Context context;
        Gpgme::Key key = Gpgme::Key::getByFingerprint(fingerprint);
        try {
            key.setDisabled(false);
        }  catch (Gpgme::GpgmeDisablingKeyFailedException &e) {
            QMessageBox box;
            box.setText(tr("enabling the key with the fingerprint ") +
                        QString::fromStdString(key.getFingerprint()) +
                        tr(" failed"));
            box.setIcon(QMessageBox::Critical);
            box.exec();
        }
    }
    scanKeyRing();
}
void WindowKeyManagement::contextMenuRevokeTriggeredEvent(bool checked)  {
    (void)checked;
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        std::string fingerprint = item->text(1).toStdString(); /// @todo magic number
        Gpgme::Context context;
        Gpgme::Key key = Gpgme::Key::getByFingerprint(fingerprint);
        if( ! key.isRevoked() )  {

            // ask user if they are sure – this can not be reversed!
            QMessageBox box;
            box.setText(tr("Are you sure you want to revoke this key?")+
                        tr("This can not be undone!")+
                        "\n\n"+
                        QString::fromStdString(key.toString())
                        );
            box.setIcon(QMessageBox::Warning);
            box.addButton(QMessageBox::StandardButton::Yes);
            box.addButton(QMessageBox::StandardButton::Cancel);
            /// @todo add buttons YesToAll and CancelAll
            if(box.exec() == QDialog::Accepted)  {
                try {

                    key.revoke(Gpgme::gpgme_revocation_reason_t::NO_REASON, "no reason given");  /// @todo let user decide
                }  catch (Gpgme::GpgmeRevokingKeyFailedException &e) {
                    QMessageBox box;
                    box.setText(tr("revoking the key with the fingerprint ") +
                                QString::fromStdString(key.getFingerprint()) +
                                tr(" failed"));
                    box.setIcon(QMessageBox::Critical);
                    box.exec();
                }
                /// @todo ask user if they want to upload the revocation certificate to keyserver
            }
        }
    }
    scanKeyRing();
}
void WindowKeyManagement::contextMenuDeleteTriggeredEvent(bool checked)  {
    (void)checked;
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        std::string fingerprint = item->text(1).toStdString();  /// @todo magic number
        Gpgme::Context context;
        Gpgme::Key key = Gpgme::Key::getByFingerprint(fingerprint);
        try {
            key.delete_();  /// @todo allow to delete private keys as well
        }  catch (Gpgme::GpgmeDeletingKeyFailedException &e) {
            QMessageBox box;
            box.setText(tr("deleting the key with the finterprint ") +
                        QString::fromStdString(key.getFingerprint()) +
                        tr(" failed"));
            box.setIcon(QMessageBox::Critical);
            box.exec();
        }
    }
    scanKeyRing();
}
void WindowKeyManagement::contextMenuChangePassphraseTriggeredEvent(bool checked)  {
    (void)checked;
    for(QTreeWidgetItem *item : treeWidget->selectedItems())  {
        std::string fingerprint = item->text(1).toStdString();  /// @todo magic number
        Gpgme::Context context;
        Gpgme::Key key = Gpgme::Key::getByFingerprint(fingerprint);
        try {
            key.changePassphrase();  /// @todo allow to delete private keys as well
        }  catch (Gpgme::GpgmeChangingPassphraseFailedException &e) {
            QMessageBox box;
            box.setText(tr("changing the passphrase of the key with the finterprint ") +
                        QString::fromStdString(key.getFingerprint()) +
                        tr(" failed"));
            box.setIcon(QMessageBox::Critical);
            box.exec();
        }
    }
    scanKeyRing();
}
void WindowKeyManagement::menuRefreshTriggeredEvent(bool)  {
    scanKeyRing();
}

void WindowKeyManagement::gpgEngineProcessorEvent_keyAdded(Gpgme::Key &key)  {
    /// @todo implement more efficiently
    scanKeyRing();
}
void WindowKeyManagement::gpgEngineProcessorEvent_keyRemoved(const Gpgme::Key &key)  {
    scanKeyRing();
}
