/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QWidget>
#include <QBoxLayout>

class ChatWidgetAllowingEmpty;
class ChatWidget;
class Contact;

#include "ChatWidgetPlaceHolder.hpp"

class ChatWidgetAllowingEmpty  :  public QWidget  {
    private:
        /// the contact currently shown in the @c chatWidget or nullptr if currently showing placeholder
        Contact *contact;

        /// container holding either the chat widget or placeholder
        QBoxLayout *container;
            /// the @c ChatWidget object that can show a chat history and compose buttons, etc.
            /// This object is dynamically constructed with the appropriate @c Contact as constructor paramter, so the
            /// pointer may actually be nullptr.
            ChatWidget *chatWidget;
            /// @brief the @c ChatWidgetPlaceHolder that shows a nice screen as long as no contact is selected to show a
            /// @c ChatWidget for
            ChatWidgetPlaceHolder placeholder;

    public:
        /** @brief constructor
         *  @param _container  the container to which to add this object to
         *  @param _contact    the @c Contact to show the chat with (can be nullptr to show placeholder)
         */
        ChatWidgetAllowingEmpty(QWidget *_parent, Contact *_contact);
        ~ChatWidgetAllowingEmpty();

        /** @brief changes the @c Contact with which the chat shall be shown
         *  @param contact  the @c Contact to show the chat with (can be nullptr to show placeholder)
         */
        void setContact(Contact *contact);

        Contact *getContact()  const;
};
