/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ChatWidgetAllowingEmpty.hpp"
#include "ChatWidgetPlaceHolder.hpp"
#include "ChatWidget.hpp"

ChatWidgetAllowingEmpty::ChatWidgetAllowingEmpty(QWidget *_parent, Contact *_contact)
        : QWidget(_parent), contact(nullptr), chatWidget(nullptr), placeholder(this)  {
    container = new QBoxLayout(QBoxLayout::TopToBottom, this);
    container->setContentsMargins(0,0,0,0);
    setLayout(container);
        container->addWidget(&placeholder);
    setContact(_contact);
}
ChatWidgetAllowingEmpty::~ChatWidgetAllowingEmpty()  {
    if(chatWidget != nullptr)
        delete chatWidget;
}

void ChatWidgetAllowingEmpty::setContact(Contact *contact)  {
    if(contact==nullptr)  {
        // show placeholder
        if(chatWidget!=nullptr)  {
            delete chatWidget;
            chatWidget = nullptr;
        }
        placeholder.setVisible(true);
    }
    else  {
        // show chat widget
        placeholder.setVisible(false);
        if(chatWidget!=nullptr)
            delete chatWidget;
        chatWidget = new ChatWidget(this,contact);
        container->addWidget(chatWidget);
    }
    this->contact = contact;
}

Contact *ChatWidgetAllowingEmpty::getContact() const  {
    return contact;
}
