/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/ui/WindowNewAccount.hpp"
#include <iostream>
#include <gpgme.h>
#include "src/util/exceptions/Exception.hpp"
#include <vector>
#include "src/Account.hpp"
#include "src/util/Properties.hpp"
#include "src/ui/WindowCreateKeypair.hpp"
#include "src/connections/EmailConnection.hpp"
#include "src/Application.hpp"
#include "src/util/gpgme.hpp"
#include <QMessageBox>
#include <src/util/exceptions/ConnectionTestFailedException.hpp>

WindowNewAccount::WindowNewAccount(bool _required)
        : WindowGenericDialog("New Account", 600,400), required(_required)  {
    // properties grid
    propertiesGrid = new QGridLayout(this);
    propertiesGrid->layout()->setSpacing(4);  /// @todo magic number
    getContainer()->setLayout(propertiesGrid);
        // name
            // label
            nameLabel = new QLabel(this);
            nameLabel->setText(tr("Your name:"));
            propertiesGrid->addWidget(nameLabel, 0,0, 1,1);
            // input
            nameInput = new QLineEdit(this);
            propertiesGrid->addWidget(nameInput, 0,1, 1,1);
        // email
            // label
            emailLabel = new QLabel(this);
            emailLabel->setText(tr("Email address:"));
            propertiesGrid->addWidget(emailLabel, 1,0, 1,1);
            // input
            emailInput = new QLineEdit(this);
            propertiesGrid->addWidget(emailInput, 1,1, 1,1);
            connect(emailInput, &QLineEdit::textEdited, this, &WindowNewAccount::emailInputChangedEvent);
        // password
            // label
            passwordLabel = new QLabel(this);
            passwordLabel->setText(tr("Password:"));
            propertiesGrid->addWidget(passwordLabel, 2,0, 1,1);
            // input
            passwordInput = new QLineEdit(this);
            passwordInput->setEchoMode(QLineEdit::Password);
            propertiesGrid->addWidget(passwordInput, 2,1, 1,1);
        // PGP key fingerprint
            // label
            pgpKeyFingerprintLabel = new QLabel(this);
            pgpKeyFingerprintLabel->setText(tr("PGP key:"));
            propertiesGrid->addWidget(pgpKeyFingerprintLabel, 3,0, 1,1);
            // list widget
            pgpKeyFingerprintTreeWidget = new QTreeWidget(this);
            pgpKeyFingerprintTreeWidget->setHeaderHidden(true);
            pgpKeyFingerprintTreeWidget->setRootIsDecorated(false);
            pgpKeyFingerprintTreeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
            propertiesGrid->addWidget(pgpKeyFingerprintTreeWidget, 3,1, 1,1);
            // creation label
            pgpKeyCreateLabel = new QLabel(this);
            pgpKeyCreateLabel->setText(tr(
                    "<span color=\"black\">"
                    "<a href='create'>Click here to create a keypair.</a>"
                    "</span>"
            ));
            propertiesGrid->addWidget(pgpKeyCreateLabel, 4,1, 1,1);
            QObject::connect(pgpKeyCreateLabel, &QLabel::linkActivated, this, &WindowNewAccount::keyCreateLabelClickedEvent);

    setTabOrder(nameInput, emailInput);
    setTabOrder(emailInput, passwordInput);
    setTabOrder(passwordInput, pgpKeyFingerprintTreeWidget);
    show();
    nameInput->setFocus();
}
WindowNewAccount::~WindowNewAccount()  {
    if(required)
        Application::getInstance()->exit();
}

void WindowNewAccount::emailInputChangedEvent()  {
    String email = emailInput->text().toStdString();
    std::vector<Gpgme::Key> keys;

    if(email.size()>=3)  // don't spam the key search function with super-short inputs for `email`
        keys = Gpgme::Key::getAllKeys(email);

    // delete list box entries
    pgpKeyFingerprintTreeWidget->clear();
    // show key fingerprint candidates
    if( ! keys.empty() )  {
        for(const Gpgme::Key &key : keys)  {
            // list box row
            QTreeWidgetItem *item = new QTreeWidgetItem(pgpKeyFingerprintTreeWidget);
            item->setDisabled( ! key.isHealthy() );
            item->setText(0, QString::fromStdString(key.getFingerprint()));
        }
    }
}

void WindowNewAccount::keyCreateLabelClickedEvent()  {
    new WindowCreateKeypair(
            nameInput->text().toStdString(),
            emailInput->text().toStdString()
    );
}

void WindowNewAccount::okButtonClickedEvent()  {
    // fetch PGP fingerprint
    if(pgpKeyFingerprintTreeWidget->selectedItems().empty())  {
        QMessageBox box;
        box.setText(tr("Please select a PGP keypair or create one."));
        box.setIcon(QMessageBox::Critical);
        box.exec();
        return ;
    }
    if(pgpKeyFingerprintTreeWidget->selectedItems().count() > 1)
        throw Exception(tr("more than one fingerprint selected").toStdString());
    String fingerprint = pgpKeyFingerprintTreeWidget->selectedItems().at(0)->text(0).toStdString();

    // fetch server settings from GUI and do sanity checks
    String email = emailInput->text().toStdString();
    std::size_t atSignPosition = email.find("@",PLAIN);
    if(atSignPosition == String::npos)  {
        QMessageBox box;
        box.setIcon(QMessageBox::Critical);
        box.setText(tr("You seem to have specified an invalid email address (no @ sign)"));
        box.exec();
        return ;
    }

    String smtpUser = email.substr(0, atSignPosition);
    if(smtpUser.empty())  {
        QMessageBox box;
        box.setIcon(QMessageBox::Critical);
        box.setText(tr("You seem to have specified an invalid email address (empty user name)"));
        box.exec();
        return ;
    }

    String smtpHost = email.substr(atSignPosition+1, String::npos);
    if(smtpHost.empty())  {
        QMessageBox box;
        box.setIcon(QMessageBox::Critical);
        box.setText(tr("You seem to have specified an invalid email address (empty host name)"));
        box.exec();
        return ;
    }

    String smtpPassword = passwordInput->text().toStdString();
    if(smtpPassword.empty())  {
        QMessageBox box;
        box.setIcon(QMessageBox::Critical);
        box.setText(tr("Please specify a password."));
        box.exec();
        return ;
    }

    // test connection
    /// @todo disable OK button for that time (and maybe also Cancle button)
    {
        Account shadowAccount(true);
        shadowAccount.setEmail(email);

        EmailConnection connection(&shadowAccount, true);
        connection.setHost(smtpHost);
        connection.setUsername(smtpUser);
        connection.setPassword(smtpPassword);

        try  {
            connection.test(true);
        }
        catch(ConnectionTestFailedException &e)  {
            QMessageBox box;
            box.setIcon(QMessageBox::Critical);
            box.setText(tr("Could not connect to server. Please check the settings for typos.\n\n")
                            + QString::fromStdString(String(e.what()))
                        );
            box.exec();
            return ;
        }
    }


    // create Account object
    Account *account = new Account();
    account->setName(nameInput->text().toStdString());
    account->setEmail(emailInput->text().toStdString());
    account->setPgpKeyFingerprint(fingerprint);
    /// @todo set signature

    // create Connection object
    EmailConnection *connection = new EmailConnection(account);
    connection->setHost(smtpHost);
    connection->setUsername(smtpUser);
    connection->setPassword(smtpPassword);

    // close window
    required = false;  // this class has succeeded in its job (to create an account) so it can be regarded as "not required anymore"
    delete this;
}

void WindowNewAccount::gpgEngineProcessorEvent_keyAdded(Gpgme::Key &key)  {
    /// @todo implement more efficiently
    emailInputChangedEvent();
}
void WindowNewAccount::gpgEngineProcessorEvent_keyRemoved(const Gpgme::Key &key)  {
    emailInputChangedEvent();
}
