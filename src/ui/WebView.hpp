/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QWidget>
#include "src/util/String.hpp"

class QBoxLayout;
class QLabel;
class QPushButton;
class QWebView;

class QNetworkRequest;
class RestrictiveNetworkAccessManager;

class Label;
class ResizingWebView;

class WebView  :  public QWidget  {
    public:
        enum WebDocumentType  { PLAIN_TEXT, HTML, MARKDOWN };
        enum NetworkAccessRestriction  { ALLOW, DENY, DEFAULT };

    private:
        bool loading=true;
        QBoxLayout *rootContainer;
            QWidget *networkAccessRestrictedBanner;
                QBoxLayout *networkAccessRestrictedBannerLayout;
                    Label *networkAccessRestrictedBannerLabel;
                    QBoxLayout *networkAccessRestrictedBannerButtonsLayout;
                        QPushButton *networkAccessRestrictedBannerAllowButton;
                        QPushButton *networkAccessRestrictedBannerDenyButton;
            ResizingWebView *qwebView;

        String content;
        NetworkAccessRestriction networkAccessRestriction;

    public:
        WebView(String content,
                WebDocumentType contentType,
                QWidget *parent=nullptr,
                bool removeMargins=true);
        virtual ~WebView();

    protected:
        // event handlers
        void networkAccessRestrictedBannerAllowButtonClicked(bool checked);
        void networkAccessRestrictedBannerDenyButtonClicked(bool checked);
        void loadFinished(bool ok);

        // callbacks for children
        bool decideOverRequest(const QNetworkRequest &request);
        void contentsSizeChanged(const QSize &s);

    friend RestrictiveNetworkAccessManager;  // is allowed to call decideOverRequest()
};
