/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "WindowChangeGpgKeyTrust.hpp"

#include <QFormLayout>
#include <QLabel>
#include <QComboBox>
#include "src/util/gpgme.hpp"

WindowChangeGpgKeyTrust::WindowChangeGpgKeyTrust(const Gpgme::Key &key)
        : WindowGenericDialog(tr("change key trust level").toStdString())  {
    formLayout = new QFormLayout(this);
    getContainer()->setLayout(formLayout);

        // key
        keyLabel = new QLabel(QString::fromStdString(key.getFingerprint()), this);
        formLayout->addRow(tr("Key"), keyLabel);

        // current trust level
        currentTrustLabel = new QLabel( QString::fromStdString(Gpgme::validityNames.find(key.getOwnerTrust())->second),
                                        this);
        formLayout->addRow(tr("Trust"), currentTrustLabel);

        // trust input
        trustInput = new QComboBox(this);
        for(const auto &pair : Gpgme::validityNames)
            if(pair.first != gpgme_validity_t::GPGME_VALIDITY_UNKNOWN &&
               pair.first != gpgme_validity_t::GPGME_VALIDITY_UNDEFINED)
                trustInput->addItem(tr(pair.second.c_str()), QVariant((int)pair.first));
        trustInput->setCurrentIndex(trustInput->findData(key.getOwnerTrust()));
        formLayout->addRow(tr("New trust"), trustInput);

        /// @todo add help button

    /// @todo tab order
    show();
    trustInput->setFocus();
}

void WindowChangeGpgKeyTrust::okButtonClickedEvent()  {
    Gpgme::Key key = Gpgme::Key::getByFingerprint(keyLabel->text().toStdString());
    key.trust( (gpgme_validity_t)trustInput->currentData().toInt() );
    WindowGenericDialog::okButtonClickedEvent();
}
