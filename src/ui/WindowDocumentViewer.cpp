/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "WindowDocumentViewer.hpp"
#include "src/util/Markdown.hpp"


/// @todo make this window class a singleton (one object per content)
WindowDocumentViewer::WindowDocumentViewer(String text, WebView::WebDocumentType documentType, String windowTitle)
    :  QDialog(nullptr)  {
    // window
    resize(600,400);
    setContentsMargins(0,0,0,0);
    setWindowTitle(QString::fromStdString(windowTitle));
    setAttribute(Qt::WA_DeleteOnClose,true);
    /// @todo delete on close?
        // box
        box = new QBoxLayout(QBoxLayout::TopToBottom);
        box->setContentsMargins(0,0,0,0);
        setLayout(box);
            // web View
            webView = new WebView(text, documentType, this, false);
            //webView->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
            box->addWidget(webView);
    resize(600,400);
    show();
    webView->setFocus();
}

void WindowDocumentViewer::linkActivated(const QString &link)  {
    /// @todo make it possible to open links and to jump to anchors
}
