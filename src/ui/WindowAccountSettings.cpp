/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "WindowAccountSettings.hpp"
#include "src/Account.hpp"
#include "src/ui/ComposeEdit.hpp"

WindowAccountSettings::WindowAccountSettings(Account *_account)
    : WindowGenericDialog("Account settings for "+_account->getName()),
      account(_account)  {
    setMinimumSize(400,300);
    propertiesGrid = new QGridLayout(this);
    getContainer()->setLayout(propertiesGrid);
        // name
        nameLabel = new QLabel(this);
        nameLabel->setText(tr("Name:"));
        propertiesGrid->addWidget(nameLabel,0,0);
        nameEdit = new QLineEdit(this);
        nameEdit->setPlaceholderText("your email...");
        nameEdit->setText(QString::fromStdString(account->getName()));
        propertiesGrid->addWidget(nameEdit,0,1);
        // email
        emailLabel = new QLabel(this);
        emailLabel->setText(tr("Email:"));
        propertiesGrid->addWidget(emailLabel, 1,0);
        emailEdit = new QLineEdit(this);
        emailEdit->setPlaceholderText("your email...");
        emailEdit->setText(QString::fromStdString(account->getEmail()));
        propertiesGrid->addWidget(emailEdit, 1,1);
        // signature
        signatureLabel = new QLabel(this);
        signatureLabel->setText(tr("Signature:"));
        propertiesGrid->addWidget(signatureLabel, 2,0);
        signatureEdit = new ComposeEdit(this,false);
        signatureEdit->setPlaceholderText(tr("your signature..."));
        signatureEdit->setPlainText(QString::fromStdString(account->getSignature()));
        propertiesGrid->addWidget(signatureEdit, 2,1);

    setTabOrder(nameEdit,emailEdit);
    setTabOrder(emailEdit, signatureEdit);
    show();
    nameEdit->setFocus();
}
WindowAccountSettings::~WindowAccountSettings()  {
}

void WindowAccountSettings::cancelButtonClickedEvent()  {
    reject();
}
void WindowAccountSettings::okButtonClickedEvent()  {
    account->setName(nameEdit->text().toStdString());
    account->setEmail(emailEdit->text().toStdString());
    account->setSignature(signatureEdit->toPlainText().toStdString());
    accept();
}
