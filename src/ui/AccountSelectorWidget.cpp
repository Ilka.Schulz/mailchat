/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AccountSelectorWidget.hpp"
#include <boost/lexical_cast.hpp>
#include "src/Account.hpp"
#include <iostream>
#include "src/util/exceptions/Exception.hpp"
#include "src/settings.hpp"

Q_DECLARE_METATYPE(Account*);  /// @todo is this really a good location for the macro call?

AccountSelectorWidget::AccountSelectorWidget(QWidget *_parent)
        : QComboBox(_parent)  {
    // trigger event handlers once
    for(Account *account : Account::getAllAccounts())
        AccountSelectorWidget::accountEvent_constructed(account);

    // connect observer event handlers
    QObject::connect(this, &QComboBox::currentTextChanged, this, [this](const QString&)  {
        for(AccountSelectorWidgetObserver *observer : observers)
            observer->accountSelectorWidgetEvent_accountSelected(this, getSelectedAccount());
    });
}
AccountSelectorWidget::~AccountSelectorWidget()  {
    // unregister from observed accounts
    for(Account *account : Account::getAllAccounts())
        account->removeObserver(this);
}
QString AccountSelectorWidget::accountToString(const Account &account)  {
    return QString::fromStdString(account.getNameAndEmail());
}

// update functions
void AccountSelectorWidget::accountEvent_constructed(Account *account)  {
    // add item to QComboBox
    QVariant userData;
    userData.setValue(account);
    addItem(accountToString(*account),
            userData);
    // observer account
    account->addObserver(this);
}
void AccountSelectorWidget::accountEvent_destructed(Account *account)  {
    // remove item from QComboBox
    QVariant userData;
    userData.setValue(account);
    int idx = findData(userData);
    if(idx == -1)
        return;  // not shown...
    removeItem(idx);
    // unregister as observer
    account->removeObserver(this);
}
void AccountSelectorWidget::accountEvent_nameChanged(Account *account)  {
    // update text in QComboBox item
    QVariant userData;
    userData.setValue(account);
    int idx = findData(userData);
    if(idx == -1)
        return;  // not shown...
    setItemText(idx, accountToString(*account));
}
void AccountSelectorWidget::accountEvent_emailChanged(Account *account)  {
    // update text in QComboBox item
    accountEvent_nameChanged(account);
}
void AccountSelectorWidget::accountEvent_pgpKeyFingerprintChanged(Account *account)  {
    // update text in QComboBox item
    accountEvent_nameChanged(account);
}

Account *AccountSelectorWidget::getSelectedAccount()  {
    QVariant userData = currentData();
    if(userData.type() == QVariant::Invalid)
        return nullptr;
    Account *account = userData.value<Account*>();
    assert(account != nullptr);
    return account;
}
void AccountSelectorWidget::selectAccount(Account *account)  {
    if(account == nullptr)
        setCurrentIndex(-1);
    QVariant userData;
    userData.setValue(account);
    int idx = findData(userData);
    assert(idx != -1);
    setCurrentIndex(idx);
}


// tests
#include <iostream>
#include <QApplication>
#undef NDEBUG
#include <assert.h>
int src_ui_AccountSelectorWidget(int argc, char** argv)  {
    Settings::dryRun = true;

    // manage creation and deletion of objects
    {
        Account *a1 = new Account(0, "a1", "a1@example.com", "FPR", "", false);

        // create test object
        QApplication app(argc, argv);
        AccountSelectorWidget widget(nullptr);
        assert(widget.count() == 1);
        assert(widget.getSelectedAccount() == a1);

        // new account
        Account *a2 = new Account(0, "a2", "a2@example.com", "FPR", "", false);
        assert(widget.count() == 2);
        assert(widget.getSelectedAccount() == a1);

        // select account
        widget.selectAccount(a2);
        assert(widget.getSelectedAccount() == a2);
        assert(widget.currentIndex() == 1);

        // delete non-selected account
        delete a1;
        assert(widget.count() == 1);
        assert(widget.getSelectedAccount() == a2);

        // delete selected account
        delete a2;
        assert(widget.count() == 0);
        assert(widget.getSelectedAccount() == nullptr);
    }

    // react to changes
    {
        Account a1(0, "a1", "a1@example.com", "FPR", "", false);
        Account a2(0, "a2", "a2@example.com", "FPR", "", false);

        QApplication app(argc, argv);
        AccountSelectorWidget widget(nullptr);
        assert(widget.getSelectedAccount() == &a1);

        // static text label
        assert(widget.currentText() == AccountSelectorWidget::accountToString(a1));

        // change account name
        a1.setName("Alice");
        assert(widget.currentText() == AccountSelectorWidget::accountToString(a1));

        // change email
        a1.setEmail("alice@example.com");
        assert(widget.currentText() == AccountSelectorWidget::accountToString(a1));
    }

    return 0;
}
