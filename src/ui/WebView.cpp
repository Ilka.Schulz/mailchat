/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "WebView.hpp"
#include <QWidget>
#include <QStyle>
#include <QBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QtWebKitWidgets/QWebView>
#include <QtWebKit/QWebSettings>
#include <QtWebKitWidgets/QWebFrame>
#include <QtNetwork/QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>
#include "src/util/Markdown.hpp"
#include "src/util/exceptions/Exception.hpp"
#include <assert.h>
#include "src/streams/base64.hpp"

#include <iostream>


class EmptyNetworkReply  :  public  QNetworkReply  {
    private:
        //static const String reply = "403 Forbidden";
        QByteArray content;
        size_t offset=0;

    public:
        EmptyNetworkReply(QObject *parent=nullptr)
                : QNetworkReply(parent)  {
            setAttribute(QNetworkRequest::Attribute::HttpStatusCodeAttribute, 403);
            setAttribute(QNetworkRequest::Attribute::HttpReasonPhraseAttribute, "Forbidden");
            setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, "text/html");
            //open(ReadOnly | Unbuffered);
        }
        void setContent(const QByteArray &content)  {
            this->content = content;
            offset = 0;
            open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::OpenModeFlag::Unbuffered);
            setHeader(QNetworkRequest::KnownHeaders::ContentLengthHeader, this->content.size());
            QTimer::singleShot(0, this, SIGNAL(readyRead()));
            QTimer::singleShot(0, this, SIGNAL(finished()));
        }

        // implement pure virtual methdos of QNetworkReply
        virtual void abort()  override  {
        }
        // implement pure virtual methods of QIODevice
        virtual bool isSequential()  const override  {
            return true;
        }
        virtual qint64 bytesAvailable()  const override  {
            return content.size() - offset - QIODevice::bytesAvailable();
        }
        virtual qint64 readData(char *data, qint64 maxlen)  override  {
            if (offset >= content.size())
                return -1;
            size_t count = std::min<size_t>(maxlen, content.size() - offset);
            memcpy(data, content.constData() + offset, count);
            offset += count;
            return count;
        }
};

class RestrictiveNetworkAccessManager  :  public QNetworkAccessManager  {
    private:
        WebView *parent;
    public:
        RestrictiveNetworkAccessManager(WebView *_parent)
                : QNetworkAccessManager(_parent), parent(_parent)  {
        }
        virtual ~RestrictiveNetworkAccessManager()  {
            std::cout << "deleting RestrictiveNetworkAccessManager"<<std::endl;
        }
    protected:
        virtual QNetworkReply *createRequest(Operation op, const QNetworkRequest &request,
                                             QIODevice *outgoingData = nullptr)  override  {
            /// @todo create warning?

            if(parent->decideOverRequest(request))
                return QNetworkAccessManager::createRequest(op,request,outgoingData);
            else  {
                EmptyNetworkReply *reply = new EmptyNetworkReply(this);
                reply->setContent("You are not allowed to access this resource.");
                return reply;
            }
        }
};

class Label  :  public QLabel  {
    public:
        Label(const QString &text, QWidget *parent)
                : QLabel(text,parent)  {
        }
    public:
        virtual QSize minimumSizeHint()  const override  {
            QSize retval = sizeHint();
            return retval;
        }
};

/** @brief a @ QWebView that returns (mostly) correct values from @c QWidget::sizeHint() and
 *  @c QWidget::minimumSizeHint()
 *  @details The original @c QWebView class returns a minimum of 800x600 px which is ridiculously large for some
 *  pages. This widget does some dark magic to make some more reasonable size hints.
 */
class ResizingWebView  :  public QWebView  {
    public:
        ResizingWebView(QWidget *parent)  :  QWebView(parent)  { }
    protected:
        virtual QSize minimumSizeHint()  const override  {
            QSize retval;
            retval.setWidth(std::max(
                                page()->mainFrame()->contentsSize().width(),
                                page()->mainFrame()->evaluateJavaScript(
                                        "document.getElementsByTagName('body')[0].offsetWidth"
                                        ).toInt()
                            ));
            retval.setHeight(std::min(
                                page()->mainFrame()->contentsSize().height(),
                                page()->mainFrame()->evaluateJavaScript(
                                    "document.getElementsByTagName('body')[0].offsetHeight"
                                    ).toInt()
                            ));

            /// @todo the below code is buggy. Try putting the ResizingWebView into a QScrollArea instead
            /*// add some vertical space if the horizontal scrollbar is shown
            if(
                    page()->mainFrame()->evaluateJavaScript(
                        "document.getElementsByTagName('body')[0].scrollWidth"
                        ).toInt()
                    >
                    page()->mainFrame()->evaluateJavaScript(
                        "document.getElementsByTagName('body')[0].offsetWidth"
                        ).toInt()
            )  {
                //std::cerr << "scrollbar" <<std::endl;
                retval.setHeight(retval.height() + 10);  /// @todo magic number
                                                         /// @todo if this is larger than the scrollbar height, the widget height will run away
            }*/

            // prevent slowly drifting to smaller sizes (happens when network access is denied)
            if(retval.height() < height())
                retval.setHeight(std::min(100,retval.height()));   /// @todo magic number

            //std::cout << "hinting: ("<<retval.width()<<"|"<<retval.height()<<")"<<std::endl;
            return retval;
        }
        virtual QSize sizeHint()  const override  {
            return minimumSizeHint();
        }
};


/// @todo choose font
WebView::WebView(String content, WebDocumentType contentType, QWidget *parent, bool removeMargins)
        : QWidget(parent), networkAccessRestriction(DEFAULT)  {
    // set up GUI
    setContentsMargins(0,0,0,0);
    // root container
        rootContainer = new QBoxLayout(QBoxLayout::Direction::TopToBottom);
        rootContainer->setContentsMargins(0,0,0,0);
        rootContainer->setSpacing(0);
        setLayout(rootContainer);
            // network access restricted banner
            networkAccessRestrictedBanner = new QWidget(this);
            networkAccessRestrictedBanner->setObjectName("networkAccessRestrictedBanner");
            networkAccessRestrictedBanner->setStyleSheet("QWidget#networkAccessRestrictedBanner {"
                    "background-color: yellow;"
                    "}");
            networkAccessRestrictedBanner->setContentsMargins(8,8,8,8);  /// @todo magic numbers
            networkAccessRestrictedBanner->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Maximum);
            //networkAccessRestrictedBanner->setStyleSheet("margin:0px; padding:0px");
            networkAccessRestrictedBanner->hide();
            rootContainer->addWidget(networkAccessRestrictedBanner);
                // layout
                networkAccessRestrictedBannerLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);
                networkAccessRestrictedBannerLayout->setContentsMargins(0,0,0,0);
                networkAccessRestrictedBanner->setLayout(networkAccessRestrictedBannerLayout);
                    // label
                    networkAccessRestrictedBannerLabel = new Label(tr("This message wants to load external resources from the internet which is blocked by default for security reasons. Allow access to external resources?"),this);
                    networkAccessRestrictedBannerLabel->setWordWrap(true);
                    networkAccessRestrictedBannerLabel->setSizePolicy(QSizePolicy::Policy::MinimumExpanding,
                                                                      QSizePolicy::Policy::Preferred);
                    networkAccessRestrictedBannerLayout->addWidget(networkAccessRestrictedBannerLabel);
                    // buttons layout
                    networkAccessRestrictedBannerButtonsLayout = new QBoxLayout(QBoxLayout::Direction::TopToBottom,this);
                    networkAccessRestrictedBannerButtonsLayout->setContentsMargins(0,0,0,0);
                    networkAccessRestrictedBannerLayout->addLayout(networkAccessRestrictedBannerButtonsLayout);
                        // allow button
                        networkAccessRestrictedBannerAllowButton = new QPushButton(tr("Allow"),this);
                        networkAccessRestrictedBannerButtonsLayout->addWidget(networkAccessRestrictedBannerAllowButton);
                        connect(networkAccessRestrictedBannerAllowButton, &QPushButton::clicked,
                                this, &WebView::networkAccessRestrictedBannerAllowButtonClicked);
                        // deny button
                        networkAccessRestrictedBannerDenyButton = new QPushButton(tr("Deny"), this);
                        networkAccessRestrictedBannerButtonsLayout->addWidget(networkAccessRestrictedBannerDenyButton);
                        connect(networkAccessRestrictedBannerDenyButton, &QPushButton::clicked,
                                this, &WebView::networkAccessRestrictedBannerDenyButtonClicked);
                        /// @todo button for further information
            // QWebView
            qwebView = new ResizingWebView(this);
            qwebView->setContentsMargins(0,0,0,0);
            rootContainer->addWidget(qwebView);

    // fill QWebView with data
    if(contentType == PLAIN_TEXT)  {
        qwebView->setContent(QString::fromStdString(content).toUtf8(), "text/plain");
    }
    else {
        qwebView->settings()->setAttribute(QWebSettings::WebAttribute::JavascriptEnabled, false);
        qwebView->settings()->setAttribute(QWebSettings::WebAttribute::LocalContentCanAccessFileUrls, false);
        qwebView->settings()->setAttribute(QWebSettings::WebAttribute::LocalContentCanAccessRemoteUrls, false);
        //settings()->setUnknownUrlSchemePolicy(QWebSettings::UnknownUrlSchemePolicy::DisallowUnknownUrlSchemes);
        QWebPage *page = new QWebPage(this);
        page->setNetworkAccessManager(new RestrictiveNetworkAccessManager(this));
        if(contentType == HTML)
            ;  // no further preparation needed
        else if(contentType == MARKDOWN)
            content = Markdown::parseMarkdownToMarkup(content);
        else
            SHOULD_BE_UNREACHABLE("unknown WebDocumentType");
        this->content = content;
        connect(page, &QWebPage::loadFinished, this, &WebView::loadFinished);
        connect(page->mainFrame(), &QWebFrame::contentsSizeChanged, this, &WebView::contentsSizeChanged);
        page->mainFrame()->setHtml(QString::fromStdString(content));
        page->mainFrame()->setScrollBarPolicy(Qt::Orientation::Vertical, Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
        page->mainFrame()->setScrollBarPolicy(Qt::Orientation::Horizontal, Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
        qwebView->setPage(page);
        if(removeMargins)  {
            const String style_sheet = "body {margin:0px}";
            const String style_sheet_b64 = base64encode(std::vector<char>(style_sheet.begin(), style_sheet.end()));
            qwebView->settings()->setUserStyleSheetUrl(QUrl(QString::fromStdString(
                                        String("data:text/css;charset=utf-8;base64,"+style_sheet_b64)
            )));
        }
        //QTimer::singleShot(1000,this, &WebView::updateGeometry);
    }
}
WebView::~WebView()  {
    std::cout << "deleting WebView"<<std::endl;
}

void WebView::networkAccessRestrictedBannerAllowButtonClicked(bool checked)  {
    if(networkAccessRestriction != ALLOW)  {
        networkAccessRestriction = ALLOW;
        //qwebView->reload();  does not work when using setHtml() previously
        //qwebView->page()->triggerAction(QWebPage::WebAction::Reload);  does not work when using setHtml() previously
        loading = true;
        qwebView->page()->mainFrame()->setHtml(QString::fromStdString(content));
    }
    networkAccessRestrictedBanner->hide();
}
void WebView::networkAccessRestrictedBannerDenyButtonClicked(bool checked)  {
    if(networkAccessRestriction != DENY)  {
        networkAccessRestriction = DENY;
        loading = true;
        qwebView->page()->mainFrame()->setHtml(QString::fromStdString(content));
    }
    networkAccessRestrictedBanner->hide();
}
void WebView::loadFinished(bool ok)  {
    std::cout << "load finished" << std::endl;
    loading = false;
    (void)ok;
    qwebView->updateGeometry();
}

bool WebView::decideOverRequest(const QNetworkRequest &request)  {
    (void)request;
    switch(networkAccessRestriction)  {
        case ALLOW:
            return true;
        break;
        case DENY:
            return false;
        break;
        case DEFAULT:
        default:
            networkAccessRestrictedBanner->show();
            return false;  // block by default
        break;
    }
}

void WebView::contentsSizeChanged(const QSize &s)  {
    if(!loading)
        qwebView->updateGeometry();
}

