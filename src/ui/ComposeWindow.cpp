/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "ComposeWindow.hpp"

#include "src/Account.hpp"
#include "src/Contact.hpp"
#include "src/messages/EmailMessage.hpp"
#include "src/messages/TextInlineMessagePart.hpp"
#include "src/connections/Connection.hpp"

#include <QPushButton>
#include <QToolButton>
#include <QFrame>
#include <QFormLayout>
#include <QLabel>
#include "src/ui/AccountSelectorWidget.hpp"
#include "src/ui/ContactSelectorWidget.hpp"
#include <QLineEdit>
#include <QComboBox>
#include "src/ui/ComposeEdit.hpp"
#include "src/ui/MessageWidget.hpp"
#include "src/ui/WindowDocumentViewer.hpp"
#include <QResizeEvent>
#include <QMessageBox>

#include "src/util/Emoji.hpp"
#include "src/resources.hpp"

#include "src/util/tictoc.hpp"

extern QFont emojiFont;

ComposeWindow::ComposeWindow(Account *_account, Contact *_contact)
        : QMainWindow(nullptr)  {
    // preview message
    previewMessage = new EmailMessage(0,
                                      EmailMessage::FLAG_OUTGOING | EmailMessage::FLAG_SHADOW | EmailMessage::FLAG_SYNCHRONIZED,
                                      {new TextInlineMessagePart("dummy",TextInlineMessagePart::ContentType::PLAIN)},
                                      _contact);
    // ui
    setWindowTitle(tr("Compose message"));
    setAttribute(Qt::WA_DeleteOnClose);
    setContentsMargins(0,0,0,0);
    setCentralWidget(new QWidget(this));
    // vertical layout
    verticalLayout = new QBoxLayout(QBoxLayout::Direction::TopToBottom, this);
    verticalLayout->setContentsMargins(0,0,0,0);
    centralWidget()->setLayout(verticalLayout);
        // actions layout
        actionsLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight, this);
        actionsLayout->setAlignment(Qt::AlignmentFlag::AlignLeft);
        actionsLayout->setSizeConstraint(QBoxLayout::SizeConstraint::SetMaximumSize);
        actionsLayout->setContentsMargins(6,6,6,0);  ///< @todo magic numbers
        verticalLayout->addLayout(actionsLayout);
            // send button
            sendButton = new QPushButton(this);
            sendButton->setText(QString::fromStdString(Emoji::convertEmoji(":arrow_left:")+" "+tr("Send").toStdString()));
            sendButton->setFont(emojiFont);
            actionsLayout->addWidget(sendButton);
            connect(sendButton, &QPushButton::clicked, this, &ComposeWindow::sendButtonClicked);
            // spelling button
            spellingButton = new QToolButton(this);
            spellingButton->setText(tr("Spelling"));
            spellingButton->setEnabled(false);  /// @todo implement spell check
            actionsLayout->addWidget(spellingButton);
            // save draft button
            saveDraftButton = new QToolButton(this);
            saveDraftButton->setText(tr("Save"));  /// @todo implement saving drafts
            saveDraftButton->setEnabled(false);
            actionsLayout->addWidget(saveDraftButton);
        // form layout
        formLayout = new QFormLayout(this);
        formLayout->setContentsMargins(6,0,6,0);  ///< @todo magic numbers
        verticalLayout->addLayout(formLayout);
            // account
            accountSelectorWidget = new AccountSelectorWidget(this);
            accountSelectorWidget->selectAccount(_account);
            accountSelectorWidget->addObserver(this);
            formLayout->insertRow(0,tr("From:"),accountSelectorWidget);
            /// @todo observe changes in accountSelectorWidget
            // contact
            contactSelectorWidget = new ContactSelectorWidget(_account,this);
            contactSelectorWidget->setContact(_contact);
            formLayout->insertRow(1,tr("To:"), contactSelectorWidget);
            /// @todo optional subject
            /*subjectEdit = new QLineEdit(this);
            subjectEdit->setPlaceholderText("optional subject");
            formLayout->insertRow(2,tr("Subject:"),subjectEdit);*/
        // format layout
        formatLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight,this);
        formatLayout->setSizeConstraint(QBoxLayout::SizeConstraint::SetMaximumSize);
        formatLayout->setContentsMargins(6,0,6,0);  ///< @todo magic number
        verticalLayout->addLayout(formatLayout);
            // insert media button
            insertMediaButton = new QPushButton(this);
            insertMediaButton->setText(tr("insert media"));
                    //QString::fromStdString(Emoji::convertEmoji(":frame_with_picture:")));
            insertMediaButton->setEnabled(false);  ///< @todo allow to insert media
            formatLayout->addWidget(insertMediaButton);
            // format combo box
            formatComboBox = new QComboBox(this);
            formatComboBox->addItem(tr("plain text"),"text/plain");
            //formatComboBox->addItem(tr("Markdown"),"text/markdown");
            //formatComboBox->addItem(tr("HTML"),"text/html");
            formatComboBox->setEnabled(false);
            formatLayout->addWidget(formatComboBox);
            // format help label
            formatHelpLabel = new QLabel(this);
            formatHelpLabel->setText(QString::fromStdString("<a href='format-help'>?"//+Emoji::convertEmoji(":question:") +
                                                            "</a>"));
            formatHelpLabel->setFont(emojiFont);
            formatLayout->addWidget(formatHelpLabel);
            connect(formatHelpLabel, &QLabel::linkActivated, [](const QString&) {
                new WindowDocumentViewer(resMessageFormats,WebView::WebDocumentType::MARKDOWN,"Message Format Overview");
            });
            // spacer
            QLabel *spacer = new QLabel("",this);
            spacer->setSizePolicy(QSizePolicy::Policy::MinimumExpanding, QSizePolicy::Policy::Preferred);
            formatLayout->addWidget(spacer);
            // toogle preview button
            togglePreviewButton = new QPushButton(this);
            togglePreviewButton->setText(tr("Preview"));
            togglePreviewButton->setCheckable(true);
            togglePreviewButton->setChecked(true);
            formatLayout->addWidget(togglePreviewButton);
            connect(togglePreviewButton, &QPushButton::clicked, [this](bool checked)  {
                previewWidget->setVisible(checked);
            });
        // content layout
        contentLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight,this);
        contentLayout->setContentsMargins(0,0,0,0);
        contentLayout->setSpacing(6); ///< @todo magic number
        verticalLayout->addLayout(contentLayout);
            // compose edit
            composeEdit = new ComposeEdit(this,false);
            composeEdit->setText(QString::fromStdString("\n\n"+_account->getSignature()));
            composeEdit->setFrameShape(QFrame::Shape::NoFrame);
            contentLayout->addWidget(composeEdit);
            connect(composeEdit, &ComposeEdit::textChanged, [this]() {
                tictoc timer = tictoc::construct(__PRETTY_FUNCTION__,false);
                dynamic_cast<TextInlineMessagePart*>(previewMessage->getMessageParts()[0])
                        ->setText(composeEdit->document()->toPlainText().toStdString());
                previewMessage->setTimestampToNow();
            });
            // preview container
            previewWidget = new MessageWidget(previewMessage,this);
            previewWidget->setSizePolicy(QSizePolicy::Policy::MinimumExpanding, QSizePolicy::Policy::Maximum);
            contentLayout->addWidget(previewWidget);
            contentLayout->setAlignment(previewWidget, Qt::AlignmentFlag::AlignTop);

    // call event handlers once
    composeEdit->textChanged();

    /// @todo set tab order
    resize(800,600);  ///< @todo magic numbers
    show();
    composeEdit->setFocus();
}
ComposeWindow::~ComposeWindow()  {
    delete previewMessage;
}

void ComposeWindow::sendButtonClicked(bool checked)  {
    (void)checked;
    Account *account = accountSelectorWidget->getSelectedAccount();
    Contact *contact = contactSelectorWidget->getSelectedContact();
    if(account == nullptr)  {
        QMessageBox *box = new QMessageBox(QMessageBox::Icon::Critical,
                                           tr("no account selected"),
                                           tr("Please select an account to send the message from.")
                                           );
        box->setAttribute(Qt::WA_DeleteOnClose);
        box->show();
        return;
    }
    if(contact == nullptr)  {
        QMessageBox *box = new QMessageBox(QMessageBox::Icon::Critical,
                                           tr("no contact selected"),
                                           tr("Please select a contact to send to message to.")
                                           );
        box->setAttribute(Qt::WA_DeleteOnClose);
        box->show();
        return;
    }
    /// @todo warn if message is empty
    Connection *connection = account->getConnections()[0]; /// @todo allow to select connection
    Message *message = connection->composeMessage(composeEdit->document()->toPlainText().toStdString(), contact);

    setEnabled(false);
    try  {
        connection->sendMessage(message,true);
        close();
    }
    catch(std::exception &e)  {
        QMessageBox *box = new QMessageBox(QMessageBox::Icon::Critical,
                                           tr("message not sent"),
                                           tr("An error occured while sending the message:")+"\n" +
                                                QString::fromUtf8(e.what())
                                           );
        box->setAttribute(Qt::WA_DeleteOnClose);
        box->show();
    }
    setEnabled(true);
}

void ComposeWindow::accountSelectorWidgetEvent_accountSelected(AccountSelectorWidget *accountSelectorWidget,
                                                               Account *account)  {
    assert(accountSelectorWidget == this->accountSelectorWidget);
    contactSelectorWidget->setAccount(account);
}

void ComposeWindow::resizeEvent(QResizeEvent *event)  {
    previewWidget->setMaximumWidth(event->size().width()/2);
}
