/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "TextInlineMessagePartWidget.hpp"
#include <QBoxLayout>
#include "src/messages/TextInlineMessagePart.hpp"
#include "src/ui/PlainTextInlineMessagePartWidget.hpp"
#include "src/ui/HtmlTextInlineMessagePartWidget.hpp"
#include "src/util/exceptions/Exception.hpp"

TextInlineMessagePartWidget::TextInlineMessagePartWidget(TextInlineMessagePart *textMessagePart, QWidget *parent)
        : InlineMessagePartWidget(parent), plainTextMessagePartWidget(nullptr), htmlTextMessagePartWidget(nullptr)  {
    rootContainer = new QBoxLayout(QBoxLayout::Direction::TopToBottom, this);
    rootContainer->setContentsMargins(0,0,0,0);
    setLayout(rootContainer);

    switch(textMessagePart->getContentType())  {
        case TextInlineMessagePart::ContentType::PLAIN:
                plainTextMessagePartWidget = new PlainTextInlineMessagePartWidget(textMessagePart,this);
                rootContainer->addWidget(plainTextMessagePartWidget);
            break;
        case TextInlineMessagePart::ContentType::HTML:
        case TextInlineMessagePart::ContentType::WATCH_HTML:
                htmlTextMessagePartWidget = new HtmlTextInlineMessagePartWidget(textMessagePart, this);
                rootContainer->addWidget(htmlTextMessagePartWidget);
            break;
    }

    // DO NOT observe the messagePart, see class description
}
TextInlineMessagePartWidget::~TextInlineMessagePartWidget()  {
    if(plainTextMessagePartWidget != nullptr)
        delete plainTextMessagePartWidget;
    if(htmlTextMessagePartWidget != nullptr)
        delete htmlTextMessagePartWidget;
}

void TextInlineMessagePartWidget::messagePartEvent_dispositionChanged(MessagePart *messagePart)  {
    (void)messagePart;
    SHOULD_BE_UNREACHABLE("bridge class must handle");  /// @todo is this still up to date?
}
void TextInlineMessagePartWidget::messagePartEvent_contentChanged(MessagePart *messagePart)  {
    if(plainTextMessagePartWidget != nullptr)
        plainTextMessagePartWidget->messagePartEvent_contentChanged(messagePart);
    if(htmlTextMessagePartWidget != nullptr)
        ; /// @todo allow HtmlInlineTextMessagePartWidget to react to content changes
}
void TextInlineMessagePartWidget::messagePartEvent_deleted(MessagePart *messagePart)  {
    delete this;
}
