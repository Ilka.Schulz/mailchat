/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QWidget>
#include "src/messages/MessagePartObserver.hpp"

class QBoxLayout;
class MessagePart;
class InlineMessagePartWidget;
class AttachmentMessagePartWidget;

/** @brief
 *  @details This class follows the @c Bridge design pattern and allows to morph into either a @c InlineMessagePart or a
 *  @c AttachmentMessagePart. It is always a @c QWidget and behaves like one that can just change appearance.
 */
class MessagePartWidget  :  public QWidget, protected MessagePartObserver  {
    private:
        MessagePart *messagePart;  ///< the observed and displayed @c MessagePart
        QBoxLayout *rootContainer;
            InlineMessagePartWidget     *inlineMessagePartWidget;  /// @todo make this a @c InlineMessagePartWidget
            AttachmentMessagePartWidget *attachmentMessagePartWidget;
    public:
        MessagePartWidget(MessagePart *_messagePart, QWidget *_parent=nullptr);
        virtual ~MessagePartWidget();

    protected:
        // handlers to observe the @c MessagePart
        virtual void messagePartEvent_dispositionChanged(MessagePart *messagePart)  override;
        virtual void messagePartEvent_contentChanged(MessagePart *messagePart)  override;
        virtual void messagePartEvent_deleted(MessagePart *messagePart)  override;
};
