/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QWidget>
#include <QComboBox>

#include <vector>
#include "src/patterns/Observable.hpp"
#include <vector>
#include "src/AccountObserver.hpp"
#include "src/util/String.hpp"

class AccountSelectorWidget;
class Account;

#include "AccountSelectorWidgetObserver.hpp"

/** @brief a combo box widget that allows the user to select an @c Account
 *  @details Object of this class get updated automatically in case Accounts are constructed/destroyed/edited.
 */
class AccountSelectorWidget  :
                                public QComboBox,
                                public AccountObserver,
                                public Observable<AccountSelectorWidgetObserver>  {

    public:
        /** @brief creates a new @c AccountSelectorWidget and adds it to the container
         *  @param _parent  the parent container that shall hold this widget
         */
        AccountSelectorWidget(QWidget *_parent);
        ~AccountSelectorWidget();

    private:
        static QString accountToString(const Account &account);

    public:
        /** @brief gets the currently selected @c Account object
         *  @return the selected @c Account object or @c nullptr if no account is selected
         */
        Account *getSelectedAccount();

        /** @brief selects an account on the widget
         *  @details Calling this function emits an @c AccountSelectWidgetObserver::accountSelectorWidgetEvent_accountSelected
         *  event.
         *  @param account  the @c Account object to select
         */
        void selectAccount(Account *account);

    // event handlers
    protected:
        /** @brief implements @c AccountObserver::acountEvent_constructed
         *  @param account  the @c Account object to add
         */
        virtual void accountEvent_constructed(Account *account)  override;

        /** @brief implements @c AccountObserver::accountEvent_destructed
         *  @param account  the @c Account object to remove
         */
        virtual void accountEvent_destructed(Account *account)  override;

        /** @brief implements @c AccountObserver::accountEvent_nameChanged
         *  @param account  the observed @c Account object
         */
        virtual void accountEvent_nameChanged(Account *account)  override;
        virtual void accountEvent_emailChanged(Account *account)  override;
        virtual void accountEvent_pgpKeyFingerprintChanged(Account *account)  override;

    friend int src_ui_AccountSelectorWidget(int,char**);  // tests
};
