/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ContactWidgetObserver.hpp"
#include <algorithm>

/*std::vector<ContactWidgetObserver*> ContactWidgetObserver::allContactWidgetObservers;

ContactWidgetObserver::ContactWidgetObserver()  {
    allContactWidgetObservers.push_back(this);
}
ContactWidgetObserver::~ContactWidgetObserver()  {
    auto itr = std::find(allContactWidgetObservers.begin(), allContactWidgetObservers.end(), this);
    if(itr != allContactWidgetObservers.end())
        allContactWidgetObservers.erase(itr);
}

// do nothing by default
void ContactWidgetObserver::contactWidgetEvent_created(ContactWidget *contactWidget)  { }*/

// do nothing by default
void ContactWidgetObserver::contactWidgetEvent_clicked(ContactWidget *contactWidget)  { }
void ContactWidgetObserver::contactWidgetEvent_deleted(ContactWidget *contactWidget)  { }
