/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "AttachmentMessagePartWidget.hpp"
#include "src/messages/AttachmentMessagePart.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/settings.hpp"
#include <algorithm>
#include <QFileDialog>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <QMessageBox>

AttachmentMessagePartWidget::AttachmentMessagePartWidget(AttachmentMessagePart *messagePart, QWidget *parent)
        : QPushButton(parent), attachmentMessagePart(messagePart), fileDialog(nullptr)  {
    // set text
    setText(QString::fromStdString(attachmentMessagePart->getName().toStdString()));

    /// @todo set minimum height, because apparently that is asked to much from Qt...

    // adjust size
    /// @todo abbreviate file name
    setMaximumWidth(std::min(sizeHint().width() + 20, 300));  /// @todo magic numbers

    //
    connect(this, &QPushButton::clicked, this, &AttachmentMessagePartWidget::clickEvent);

    // DO NOT observe the messagePart, see class description
}

void AttachmentMessagePartWidget::messagePartEvent_dispositionChanged(MessagePart *messagePart)  {
    assert(messagePart == this->attachmentMessagePart);
    SHOULD_BE_UNREACHABLE("bridge class must handle");
}
void AttachmentMessagePartWidget::messagePartEvent_contentChanged(MessagePart *messagePart)  {
    /// @todo allow to change content, e.g. due to decryption
    NOT_YET_IMPLEMENTED();
}
void AttachmentMessagePartWidget::messagePartEvent_deleted(MessagePart *messagePart)  {
    assert(messagePart == this->attachmentMessagePart);
    delete this;
}

void AttachmentMessagePartWidget::clickEvent(bool checked)  {
    (void)checked;
    // skip if a dialog for this attachment is already open
    if(fileDialog != nullptr)
        return ;
    // create dialog
    fileDialog = new QFileDialog(this);
        fileDialog->setWindowTitle(tr("Save as..."));
        fileDialog->setAcceptMode(QFileDialog::AcceptMode::AcceptSave);
        fileDialog->setFileMode(QFileDialog::FileMode::AnyFile);
        //fileDialog->setNameFilter()
        fileDialog->setViewMode(QFileDialog::ViewMode::List);
        fileDialog->selectFile(QString::fromStdString(attachmentMessagePart->getName().toStdString()));
        connect(fileDialog, &QFileDialog::finished, this, &AttachmentMessagePartWidget::fileDialogFinished);
        //fileDialog->setOption(QFileDialog::Option::DontUseNativeDialog);
        fileDialog->open();
}
void AttachmentMessagePartWidget::fileDialogFinished(int result)  {
    assert(fileDialog != nullptr);
    if(result == QDialog::DialogCode::Accepted)  {
        QStringList files = fileDialog->selectedFiles();
        assert(files.size()==1);
        // save data to file
        if(Settings::dryRun)
            throw std::logic_error(String(__PRETTY_FUNCTION__)+" should not get called while Settings::dryRun is on.");
        std::ofstream f;
        f.exceptions(std::ios::failbit);
        try  {
            const String path = files[0].toStdString();
            f.open(path.c_str(), std::iostream::out | std::iostream::binary);
                f.write(&attachmentMessagePart->getData()[0], attachmentMessagePart->getData().size());
            f.close();
            //set modification time
            std::filesystem::last_write_time(path.toStdString(),
                                             std::filesystem::file_time_type(std::filesystem::file_time_type::duration(
                                                attachmentMessagePart->getModificationDate() != 0 ?
                                                    attachmentMessagePart->getModificationDate()  :
                                                    time(nullptr)
                                             )) );
            /// @todo set creation time?
        }
        catch(std::exception &e)  {
            QMessageBox box;
            box.setText(QString::fromStdString("cannot save file: "+String(e.what())));
            box.setIcon(QMessageBox::Icon::Critical);
            box.exec();  ///< @todo this will block execution
        }
    }
    delete fileDialog;
    fileDialog = nullptr;
}
