/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QBoxLayout>

class WindowAbout;

#include "src/ui/WindowGenericDialog.hpp"

class WindowAbout  : public QDialog  {
    private:
        QGridLayout *propertiesGrid;
            QLabel *icon;
            QLabel *nameLabel;
            QLabel *versionLabel;
            QLabel *messageLabel;
            QBoxLayout *legalBox;
                QLabel *licenseLabel;
                QLabel *privacyPolicyLabel;
    public:
        WindowAbout(QWidget *_parent);
        ~WindowAbout();

        void labelClickedEvent(QString uri);
};
