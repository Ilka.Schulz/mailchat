/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ContactWidget.hpp"
#include <iostream>
#include "src/util/exceptions/Exception.hpp"


ContactWidget::ContactWidget(ContactListWidget *_parent, Contact *_contact)
        : QPushButton(_parent), contact(_contact)  {
    if(contact->isConfirmed() == TristateBool::VAL_FALSE)
        throw Exception("trying to create a ContactWidget with a blocked Contact");
    setCheckable(true);
    setStyleSheet("QPushButton {border: none; background:transparent}"
                  "QPushButton:checked {background-color:#C0C0C0}");
    setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Preferred);
    QObject::connect(this, &QPushButton::clicked, this, &ContactWidget::clickedEvent);
        // horizontal box container
        horizontalBox = new QBoxLayout(QBoxLayout::LeftToRight, this);
        setLayout(horizontalBox);
            /// @todo image
            // labels
            QBoxLayout *labelContainer = new QBoxLayout(QBoxLayout::TopToBottom, this);
            horizontalBox->addLayout(labelContainer);
                // name
                labelName = new QLabel(this);
                labelName->setText("<name>");
                labelContainer->addWidget(labelName);
                // email
                labelEmail = new QLabel(this);
                labelEmail->setText("<email>");
                labelContainer->addWidget(labelEmail);
            /// @todo menu

    // call observer event handlers once to initialize GUI elements, etc.
    ContactWidget::contactEvent_nameChangedEvent(contact);
    ContactWidget::contactEvent_emailChangedEvent(contact);
    contact->addObserver(this);

    // adjust size
    setFixedHeight(55);  /// @todo make this desktop-independent
    //adjustSize();
}
ContactWidget::~ContactWidget()  {
    contact->removeObserver(this);
    for(ContactWidgetObserver *observer : observers)
        observer->contactWidgetEvent_deleted(this);
}

void ContactWidget::clickedEvent()  {
    /// @todo make ContactListWidget a ContactWidgetObserver
    for(ContactWidgetObserver *observer : observers)
        observer->contactWidgetEvent_clicked(this);
}

void ContactWidget::contactEvent_destroyedEvent(Contact *contact)  {
    delete this;
}
void ContactWidget::contactEvent_nameChangedEvent(Contact *contact)  {
    String s = contact->getName();
    if(contact->isConfirmed() == TristateBool::VAL_UNKNOWN)
        s = "("+s+")";  // contact is not yet confirmed or blocked
    else if(contact->isConfirmed() == TristateBool::VAL_FALSE)  {
        // DO NOT remove this Contact from the Account that owns it (then the info that this Contact is blocked would be
        // dropped...
        // Instead, just delete this widget.
        delete this;
        return ;
    }
    labelName->setText(QString::fromStdString(s));
}
void ContactWidget::contactEvent_emailChangedEvent(Contact *contact)  {
    labelEmail->setText(QString::fromStdString(contact->getEmail()));
}
void ContactWidget::contactEvent_confirmedChangedEvent(Contact *contact)  {
    contactEvent_nameChangedEvent(contact);  // use same handler...
}

Contact *ContactWidget::getAssociatedContact()  {
    return contact;
}
