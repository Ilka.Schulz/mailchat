/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <QMainWindow>
#include <QBoxLayout>
#include <QToolButton>

class MainWindow;
class ContactListWidget;
class ChatWidgetAllowingEmpty;
class AccountSelectorWidget;


#include "ContactListWidgetObserver.hpp"
#include "AccountSelectorWidgetObserver.hpp"
#include "src/processors/GpgEngineProcessorObserver.hpp"


/** @brief shows the main window
 *  @details This window shows a main menu, an @c AccountSelectorWidget, an @c ContactListWidget and a @c
 *  ChatWidgetAllowingEmpty
 *  @todo inherit from QMainWindow
 */
class MainWindow:
        public QMainWindow,
        protected ContactListWidgetObserver,
        protected AccountSelectorWidgetObserver,
        protected GpgEngineProcessorObserver
{

    private:
        QBoxLayout *container;  ///< horizontal box
            QWidget *leftVerticalWidget;  ///< container for @c MainWindow::leftVerticalBox
                QBoxLayout *leftVerticalBox;  ///< container holding the left GUI elements
                    QBoxLayout *accountBox;  ///< container for @c AccountSelectorWidget and Menu button
                        QToolButton *accountMenuButton;
                        /// @c Contact objects of this selected @c Account will be shown in @c contactListWidget
                        AccountSelectorWidget *accountSelectorWidget;
                    /// @c chatWidget will show chat with the @c Contact selected in this @c ContactListWidget
                    ContactListWidget *contactListWidget;
                /// shows chat with the selected @c Contact of the selected @c Account
                ChatWidgetAllowingEmpty *chatWidget;

        QAction *menuEditHelp;

    public:
        MainWindow();
        ~MainWindow();

    protected:
        virtual void closeEvent(QCloseEvent *event)  override;

        // event handlers for observed AccountSelectorWidget objects
        virtual void accountSelectorWidgetEvent_accountSelected(AccountSelectorWidget *accountSelectorWidget, Account *account)  override;

        // event handlers for observed ContactListWidget objects
        virtual void contactListWidgetEvent_contactWidgetSelected(ContactListWidget *contactListWidget, ContactWidget *contactWidget, bool active)  override;

        virtual void gpgEngineProcessorEvent_keyWillExpireSoon(Gpgme::Key &key)  override;
};
