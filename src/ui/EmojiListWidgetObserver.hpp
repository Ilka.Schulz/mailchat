/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/util/String.hpp"

class EmojiListWidget;

class  EmojiListWidgetObserver  {
    protected:
        /** @brief event when an emoji has been selected (clicked on by user)
         *  @param emojiListWidget  the observed object
         *  @param emoji  unicode string representing the emoji
         */
        virtual void emojiListWidgetEvent_emojiSelected(EmojiListWidget *emojiListWidget, String emoji);

    /// @c EmojiListWidget is the observed class so it needs to call the protected event handlers
    friend EmojiListWidget;
};
