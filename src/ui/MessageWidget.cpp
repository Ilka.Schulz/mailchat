/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/ui/MessageWidget.hpp"
#include "src/messages/Message.hpp"
#include "src/ui/ChatWidget.hpp"
#include "src/Account.hpp"
#include "src/messages/Message.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/time.hpp"
#include "src/messages/MessagePart.hpp"
#include "src/ui/MessagePartWidget.hpp"
#include "src/ui/AttachmentMessagePartWidget.hpp"
#include "src/util/Emoji.hpp"
#include <QAction>
#include "src/ui/WindowDocumentViewer.hpp"
#include <QMenu>

MessageWidget::MessageWidget(Message *_message, QWidget *_parent)
        : QWidget(_parent), ignoreLeaveEventCount(0), message(_message)  {
    setContentsMargins(0,0,0,0);
    setObjectName("MessageWidget");
    // horizontal box
    horizontalBox = new QBoxLayout(QBoxLayout::Direction::LeftToRight, this);
    horizontalBox->setContentsMargins(0,0,0,0);
    setLayout(horizontalBox);
        // message box widget
        messageBoxWidget = new QWidget(this);
        messageBoxWidget->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Preferred);
        messageBoxWidget->setObjectName("MessageBoxWidget");
        messageBoxWidget->setStyleSheet("QWidget#MessageBoxWidget {background-color:white; border-radius:15px}");//#E0FFE0}");
        horizontalBox->addWidget(messageBoxWidget);
            // message box
            messageBox = new QBoxLayout(QBoxLayout::TopToBottom, this);
            //messageBox->setContentsMargins(0,0,0,15);  /// @todo magic number
            messageBoxWidget->setLayout(messageBox);
                // sender label
                senderLabel = new QLabel(this);
                senderLabel->setObjectName("senderLabel");
                senderLabel->setTextFormat(Qt::PlainText);
                QFont font = senderLabel->font();
                font.setWeight(QFont::Weight::Medium);  // This may have no effect.
                senderLabel->setFont(font);
                //senderLabel->setStyleSheet("QLabel#senderLabel {color:#800000}");
                if(getAssociatedMessage()->hasFlag(Message::FLAG_OUTGOING))
                    senderLabel->hide();
                messageBox->addWidget(senderLabel);
                // content box
                contentBox = new QBoxLayout(QBoxLayout::TopToBottom);
                contentBox->setContentsMargins(0,0,0,0);
                messageBox->addLayout(contentBox);
                    // text content box
                    inlineContentBox = new QBoxLayout(QBoxLayout::TopToBottom);
                    inlineContentBox->setContentsMargins(0,0,0,0);
                    inlineContentBox->setSpacing(0);
                    contentBox->addLayout(inlineContentBox);
                    // attachment content box
                    attachmentContentBox = new QBoxLayout(QBoxLayout::LeftToRight);
                    attachmentContentBox->setContentsMargins(0,0,0,0);
                    attachmentContentBox->setSpacing(0);
                    contentBox->addLayout(attachmentContentBox);
                // time stamp label
                timeStampLabel = new QLabel(this);
                font = timeStampLabel->font();
                font.setPointSizeF(font.pointSize()*0.8);
                font.setItalic(true);
                timeStampLabel->setFont(font);
                messageBox->addWidget(timeStampLabel);
                if(getAssociatedMessage()->hasFlag(Message::FLAG_OUTGOING))
                    messageBox->setAlignment(timeStampLabel, Qt::AlignmentFlag::AlignRight);

            // manually trigger observer callbacks to fetch the data to display once
                // show the message content
                for( MessagePart *messagePart : message->getMessageParts() )
                    MessageWidget::messageEvent_messagePartAddedEvent(message, messagePart);

                // show the sender
                if( ! message->hasFlag(Message::FLAG_OUTGOING) )  {
                    if(message->getContact() != nullptr)
                        MessageWidget::contactEvent_nameChangedEvent(message->getContact());
                    else
                        senderLabel->hide();
                }

                // show time stamp
                MessageWidget::messageEvent_timeStampChangedEvent(message);


        // buttons widget
        buttonsWidget = new QWidget(this);
        buttonsWidget->setSizePolicy(QSizePolicy::Policy::Fixed, QSizePolicy::Policy::Preferred);
        {
            if(message->hasFlag(Message::FLAG_OUTGOING))
                horizontalBox->insertWidget(0, buttonsWidget);
            else
                horizontalBox->addWidget(buttonsWidget);
        }
            // buttons box
            buttonsBox = new QBoxLayout(QBoxLayout::Direction::LeftToRight, this);
            buttonsBox->setContentsMargins(0,0,0,0);
            buttonsWidget->setLayout(buttonsBox);
                // reply button
                replyButton = new QToolButton(this);
                replyButton->setText(QString::fromStdString(Emoji::convertEmoji(":arrow_right_hook:")));
                replyButton->setToolTip(tr("Reply"));
                replyButton->setEnabled(false);
                //replyButton->setMaximumSize(replyButton->font().pixelSize(), replyButton->font().pixelSize());
                buttonsBox->addWidget(replyButton);
                // options button
                optionsButton = new QToolButton(this);
                optionsButton->setText("\u2630");  /// @todo add this to emoji list);
                optionsButton->setToolTip(tr("Options"));
                optionsButton->setPopupMode(QToolButton::ToolButtonPopupMode::InstantPopup);
                optionsButton->setMenu(new QMenu(this));
                connect(optionsButton->menu(), &QMenu::aboutToShow, this, [this]()  {
                    ignoreLeaveEventCount = 1;
                });
                connect(optionsButton->menu(), &QMenu::aboutToHide, this, [this]()  {
                    leaveEvent(nullptr);
                });
                buttonsBox->addWidget(optionsButton);
                    // show raw full message
                    QAction *optionShowRawFullMessage = new QAction(tr("&Show source code"), this);
                    optionsButton->menu()->addAction(optionShowRawFullMessage);
                    connect(optionShowRawFullMessage, &QAction::triggered, this, [this](bool)  {
                        new WindowDocumentViewer(message->getRawFullMessage(),
                                                 WebView::WebDocumentType::PLAIN_TEXT,
                                                 "Message source code"
                                                );
                        leaveEvent(nullptr);
                    });

        buttonsWidget->setMinimumWidth(buttonsWidget->minimumSizeHint().width());

    // trigger events once
    MessageWidget::leaveEvent(nullptr);

    // register as observer
    message->addObserver(this);
    if(message->getContact() != nullptr)
        message->getContact()->addObserver(this); // e.g. senderLabel displays the sender's name
}
MessageWidget::~MessageWidget()  {
    message->removeObserver(this);
    if(message->getContact() != nullptr)
        message->getContact()->removeObserver(this);
    /// @todo delete row from parent container?
}

Message *MessageWidget::getAssociatedMessage()  {
    return message;
}

void MessageWidget::enterEvent(QEvent *event)  {
    (void)event;
    replyButton->setVisible(true);
    optionsButton->setVisible(true);
}
void MessageWidget::leaveEvent(QEvent *event)  {
    (void)event;
    if(ignoreLeaveEventCount != 0)  {
        ignoreLeaveEventCount--;
        return;
    }
    replyButton->setVisible(false);
    optionsButton->setVisible(false);
}

void MessageWidget::messageEvent_messagePartAddedEvent(Message *message, MessagePart *messagePart)  {
    assert(message == this->message);
    if(messagePartsAndWidgets.find(messagePart) == messagePartsAndWidgets.end())  {
        // new message part
        MessagePartWidget *messagePartWidget = new MessagePartWidget(messagePart);
        messagePartsAndWidgets[messagePart] = messagePartWidget;
        messagePartEvent_dispositionChanged(messagePart);
        messagePartWidget->show();
        // observe
        messagePart->addObserver(this);
    }
    else
        SHOULD_BE_UNREACHABLE("message part object is already registered");
}
void MessageWidget::messageEvent_timeStampChangedEvent(Message *message)  {
    assert(message == this->message);
    timeStampLabel->setText(QString::fromStdString(
                                Time::timeStampToText(message->getTimeStamp(), Time::widgetDateFormat)));
}
void MessageWidget::messageEvent_messageIdChangedEvent(Message *message)  {
    /// @todo implement
    NOT_YET_IMPLEMENTED();
}


void MessageWidget::messagePartEvent_dispositionChanged(MessagePart *messagePart)  {
    // find widget
    auto itr = messagePartsAndWidgets.find(messagePart);
    assert(itr != messagePartsAndWidgets.end());
    MessagePartWidget *messagePartWidget = itr->second;

    // remove widget from old container
    inlineContentBox->removeWidget(messagePartWidget);
    attachmentContentBox->removeWidget(messagePartWidget);

    // insert widget into new container
    if(messagePart->getDisposition() == MessagePart::INLINE)
        inlineContentBox->addWidget(messagePartWidget);
    else if(messagePart->getDisposition() == MessagePart::ATTACHMENT)
        attachmentContentBox->addWidget(messagePartWidget);
    else
        SHOULD_BE_UNREACHABLE("unknown message part type");
}
void MessageWidget::messagePartEvent_deleted(MessagePart *messagePart)  {
    // unregister
    messagePartsAndWidgets.erase(messagePartsAndWidgets.find(messagePart));
    // DO NOT delete the widget because the widget is already observing the messagePart and will destroy itself
}

void MessageWidget::contactEvent_nameChangedEvent(Contact *contact)  {
    assert(contact == message->getContact());
    // change sender label text if this widget shows an incoming message
    if( ! message->hasFlag(Message::FLAG_OUTGOING) )
        senderLabel->setText(QString::fromStdString(contact->getName().c_str()));
}
