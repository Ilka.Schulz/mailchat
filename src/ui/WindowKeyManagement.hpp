/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/ui/WindowGenericDialog.hpp"
#include "src/processors/GpgEngineProcessorObserver.hpp"

class QBoxLayout;
class QLineEdit;
class QTreeWidget;
class QToolButton;

class WindowKeyManagement  :  public WindowGenericDialog,
                              protected GpgEngineProcessorObserver  {
    private:
        QBoxLayout *verticalBox;
            QBoxLayout *headerBox;
                QLineEdit *searchEdit;
                QToolButton *menuButton;
            QTreeWidget *treeWidget;

    public:
        WindowKeyManagement();

    protected:
        void searchEditTextChangedEvent(const QString &text);
        void menuAddKeyGenerateNewTriggeredEvent(bool checked);

        void treeWidgetCustomContextMenuRequestedEvent(const QPoint &pos);
        void contextMenuTrustTrustTriggeredEvent(bool checked);
        void contextMenuTrustUntrustTriggeredEvent(bool checked);
        void contextMenuTrustCustomTriggeredEvent(bool checked);
        // Sign
        void contextMenuDisableTriggeredEvent(bool checked);
        void contextMenuEnableTriggeredEvent(bool checked);
        void contextMenuRevokeTriggeredEvent(bool checked);
        void contextMenuDeleteTriggeredEvent(bool checked);
        void contextMenuChangePassphraseTriggeredEvent(bool checked);

        void menuRefreshTriggeredEvent(bool);

        // event handlers
        virtual void gpgEngineProcessorEvent_keyAdded(Gpgme::Key &key)  override;
        virtual void gpgEngineProcessorEvent_keyRemoved(const Gpgme::Key &key)  override;

    public:
        void scanKeyRing();
};
