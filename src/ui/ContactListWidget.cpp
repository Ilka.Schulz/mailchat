/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ContactListWidget.hpp"

#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QScrollArea>
#include <QPushButton>

#include <iostream>
#include "src/ui/ContactWidget.hpp"
#include "src/ui/WindowNewContact.hpp"
#include "src/Account.hpp"
#include "src/util/exceptions/Exception.hpp"

ContactListWidget::ContactListWidget(QWidget *parent, Account *_account)
        : QWidget(parent), singleSelectionMode(false), lastSelected(nullptr), currentlyHandlingSelectEvent(false),
        noDeselectMode(false), account(_account)  {
    // vertical box
    verticalBox = new QBoxLayout(QBoxLayout::TopToBottom, this);
    verticalBox->setContentsMargins(0,0,0,0);
    setLayout(verticalBox);
        // caption label
        label = new QLabel(tr("Contacts"));
        verticalBox->addWidget(label);
        verticalBox->setAlignment(label, Qt::AlignTop);
        // scrolled Window
        scrolledWindow = new QScrollArea(this);
        scrolledWindow->setObjectName("scrolledWindow");
        scrolledWindow->setStyleSheet("#scrolledWindow {background:transparent}");
        scrolledWindow->setFrameShape(QFrame::NoFrame);
        scrolledWindow->setContentsMargins(0,0,0,0);
        verticalBox->addWidget(scrolledWindow);
            // scrolled window widget
            scrolledWindowWidget = new QWidget(this);
            scrolledWindowWidget->setObjectName("scrolledWindowWidget");
            scrolledWindowWidget->setStyleSheet("#scrolledWindowWidget {background:transparent}");
            scrolledWindow->setWidgetResizable(true);
            scrolledWindow->setWidget(scrolledWindowWidget);
            scrolledWindow->setMinimumWidth(250);
            scrolledWindow->setMinimumHeight(250);
                    // scrolled vertical box
                    scrolledVerticalBox = new QBoxLayout(QBoxLayout::TopToBottom, this);
                    scrolledVerticalBox->setContentsMargins(0,0,0,0);
                    scrolledVerticalBox->setAlignment(Qt::AlignTop);
                    scrolledWindowWidget->setLayout(scrolledVerticalBox);
                        // new contact button
                        newContactButton = new QPushButton();
                        newContactButton->setText(tr("add new contact"));
                        scrolledVerticalBox->addWidget(newContactButton);
                        connect(newContactButton, &QPushButton::clicked, this, &ContactListWidget::newContactButtonClickedEvent);
    // initially fetch all existing contacts from account
    // observe account to get updates about added or removed contacts
    setAccount(account);
}

void ContactListWidget::updateSelection()  {
    if( ! singleSelectionMode )
        return ;
    for(ContactWidget *child : children)
        if(lastSelected==nullptr || child != lastSelected)  {
            child->setChecked(false);
        }
}


void ContactListWidget::accountEvent_contactAdded(Account *account, Contact *contact)  {
    // ignore other accounts
    if(account != this->account)
        return ;
    // do not add widgets for blocked Contact objects – the ContactWidget constructor would throw an exception
    if(contact->isConfirmed() == TristateBool::VAL_FALSE)
        return ;
    // create ContactListWidget
    ContactWidget *contactWidget = new ContactWidget(this,contact);
    scrolledVerticalBox->insertWidget(children.size(), contactWidget);
    contactWidget->addObserver(this);
    children.push_back(contactWidget);
    // select the newly created child if this was previously empty
    if(children.size()==1)
        contactWidgetEvent_clicked(contactWidget);
}
void ContactListWidget::accountEvent_contactRemoved(Account *account, Contact *contact)  {
    if(account != this->account)
        return ;
    for(auto itr = children.begin(); itr != children.end(); itr++)  {
        if((*itr)->getAssociatedContact() == contact)  {
            delete *itr;
            children.erase(itr);
            return ;
        }
    }
}
void ContactListWidget::contactWidgetEvent_clicked(ContactWidget *contactWidget)  {
    contactWidget->setChecked(true);
    lastSelected = contactWidget;
    updateSelection();  /// @todo ouch, this looks procedural...
    for(ContactListWidgetObserver *observer : observers)
        observer->contactListWidgetEvent_contactWidgetSelected(this,contactWidget, true);
}
void ContactListWidget::contactWidgetEvent_deleted(ContactWidget *contactWidget)  {
    auto itr = std::find(children.begin(), children.end(), contactWidget);
    if(itr == children.end())
        throw Exception("received signal for unobserved child");
    children.erase(itr);
    updateSelection();
}

void ContactListWidget::setSingleSelectionMode(bool singleSelectionMode)  {
    this->singleSelectionMode = singleSelectionMode;
    updateSelection();
}
void ContactListWidget::setNoDeselectMode(bool noDeselectMode)  {
    this->noDeselectMode = noDeselectMode;
}

void ContactListWidget::setAccount(Account *newAccount)  {
    // stop observing old account
    if(account != nullptr)
        account->removeObserver(this);
    // start observing new account
    if(newAccount != nullptr)
        newAccount->addObserver(this);
    // update reference
    account = newAccount;
    // destroy currently shown children widgets
    for(ContactWidget *contactWidget : children)
        delete contactWidget;
    children.clear();
    std::cout<< "showing contacts of account "
             << (account==nullptr?"nullptr":account->getName().toStdString())
             << std::endl;
    // intially fetch all existing contacts from new account
    if(account != nullptr)
        for(Contact *contact : account->getContacts())
            ContactListWidget::accountEvent_contactAdded(account,contact);
}

QBoxLayout *ContactListWidget::getContainer()  {
    return scrolledVerticalBox;
}


void ContactListWidget::contactSelectEvent(ContactWidget *contactWidget, bool active)  {
    if(currentlyHandlingSelectEvent)
        return ;
    currentlyHandlingSelectEvent = true;
        // enforce noDeselectMode
        if( ! contactWidget->isChecked() && noDeselectMode )
            contactWidget->setChecked(true);
        else  {
            lastSelected = contactWidget;
            updateSelection();
        }
        // inform observers
        for( auto observer : observers )
            observer->contactListWidgetEvent_contactWidgetSelected(this, contactWidget, active);
    currentlyHandlingSelectEvent = false;
}
void ContactListWidget::newContactButtonClickedEvent(bool checked)  {
    (void)checked;
    new WindowNewContact(account);
}
