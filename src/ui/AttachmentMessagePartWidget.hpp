/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QPushButton>
#include "src/messages/MessagePartObserver.hpp"

class QFileDialog;

class MessagePart;
class AttachmentMessagePart;
class MessagePartWidget;

/** @brief
 *  @details I have a feeling that this class needs to follow the @c Proxy pattern as well...
 */
class AttachmentMessagePartWidget  :  public QPushButton, protected MessagePartObserver  {
    private:
        /// @todo store the file here in memory or sth. like that
        AttachmentMessagePart *attachmentMessagePart;

        /// @brief file dialog used to let the user save the content to disk
        /// @details This is a pointer in order to save memory. It is nullptr unless a dialog currently exists.
        QFileDialog *fileDialog;

    public:
        AttachmentMessagePartWidget(AttachmentMessagePart *messagePart, QWidget *parent=nullptr);
        virtual ~AttachmentMessagePartWidget() = default;

    protected:
        // event handlers for MessagPart
        virtual void messagePartEvent_dispositionChanged(MessagePart *messagePart)  override;
        virtual void messagePartEvent_contentChanged(MessagePart *messagePart)  override;
        virtual void messagePartEvent_deleted(MessagePart *messagePart)  override;

        // event handlers for QPushButton
        void clickEvent(bool checked);
        void fileDialogFinished(int result);

    friend MessagePartWidget;  ///< bridge of this class is allowed to call the event handlers
};
