/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "ContactSelectorWidget.hpp"
#include "src/Account.hpp"
#include "src/Contact.hpp"

#include "src/util/exceptions/Exception.hpp"
#include "src/settings.hpp"

Q_DECLARE_METATYPE(Contact*);  /// @todo is this really a good location for the macro call?

// do nothing by default
void ContactSelectorWidgetObserver::contactSelectorWidgetEvent_contactSelected(ContactSelectorWidget *, Contact *)  { }

ContactSelectorWidget::ContactSelectorWidget(Account *_account, QWidget *parent)
        : QComboBox(parent), account(nullptr)  {
    setAccount(_account);
    connect(this, &QComboBox::currentTextChanged, this, [this](const QString &)  {
        for(ContactSelectorWidgetObserver *observer : observers)
            observer->contactSelectorWidgetEvent_contactSelected(this, getSelectedContact());
    });
}
ContactSelectorWidget::~ContactSelectorWidget()  {
    // unregister as observer
    setAccount(nullptr);
}

String ContactSelectorWidget::contactToString(Contact *contact)  const  {
    return contact->getName() + " <"+contact->getEmail()+">";
}


void ContactSelectorWidget::setAccount(Account *account)  {
    // ignore if the account is still the same
    if(account == this->account)
        return ;
    // remove old items
    clear();
    // unregister as observer
    if(this->account != nullptr)  {
        this->account->removeObserver(this);
        for(Contact *contact : this->account->getContacts())
            contact->removeObserver(this);
    }
    // accept new account
    this->account = account;
    if(account != nullptr)  {
        // add and observe new items
        for(Contact *contact : account->getContacts())
            ContactSelectorWidget::accountEvent_contactAdded(account,contact);
        // register as observer
        account->addObserver(this);
    }
}
void ContactSelectorWidget::setContact(Contact *contact)  {
    if(contact == nullptr)
        setCurrentIndex(-1);
    assert(contact->getAccount() == this->account);
    QVariant userData;
    userData.setValue(contact);
    int idx = findData(userData);
    assert(idx != -1);
    setCurrentIndex(idx);
}
Account *ContactSelectorWidget::getAccount()  const  {
    return account;
}
Contact *ContactSelectorWidget::getSelectedContact() const  {
    QVariant userData = currentData();
    if(userData.type() == QVariant::Type::Invalid)
        return nullptr;  // nothing selected
    Contact *contact = userData.value<Contact*>();
    assert(dynamic_cast<Contact*>(contact) == contact);
    return contact;
}


void ContactSelectorWidget::accountEvent_contactAdded(Account *account, Contact *contact)  {
    assert(account == this->account);
    if(contact->isConfirmed() == TristateBool::VAL_FALSE)
        return ;
    QVariant userData;
    userData.setValue(contact);
    contact->addObserver(this);
    addItem(QString::fromStdString(contactToString(contact)), userData);
}
void ContactSelectorWidget::accountEvent_contactRemoved(Account *account, Contact *contact)  {
    assert(account == this->account);
    QVariant userData;
    userData.setValue(contact);
    int idx = findData(userData);
    if(idx == -1)
        return ;
    removeItem(idx);
    contact->removeObserver(this);
}
void ContactSelectorWidget::accountEvent_destructed(Account *account)  {
    assert(account == this->account);
    // unregister as observer
    account->removeObserver(this);
    for(Contact *contact : account->getContacts())
        contact->removeObserver(this);
    // select new account
    if(Account::getAllAccounts().empty())
        setAccount(nullptr);
    else
        setAccount(Account::getAllAccounts().at(0));
}

void ContactSelectorWidget::contactEvent_destroyedEvent(Contact *contact)  {
    return ;
    // do nothing here becasue accountEvent_contactRemoved already does everything
}
void ContactSelectorWidget::contactEvent_nameChangedEvent(Contact *contact)  {
    assert(contact->getAccount() == this->account);

    QVariant userData;
    userData.setValue(contact);
    int idx = findData(userData);
    assert(idx != -1);

    setItemText(idx, QString::fromStdString(contactToString(contact)));
}
void ContactSelectorWidget::contactEvent_emailChangedEvent(Contact *contact)  {
    return contactEvent_nameChangedEvent(contact);
}


// tests
#include <iostream>
#include <QApplication>
#undef NDEBUG
#include <assert.h>
int src_ui_ContactSelectorWidget(int argc, char** argv)  {
    Settings::dryRun = true;
    // manage creation and deletion of objects
    {
        // pre-existing data
        Account *a1 = new Account(0,"a1","a1@example.com","FPR","foo",false);
            Contact *c11 = new Contact(0, a1, "c11");

        // create test object
        QApplication app(argc, argv);
        ContactSelectorWidget widget(a1, nullptr);
        assert(widget.count()==1);
        assert(widget.getSelectedContact() == c11);

        // new contact
        Contact *c12 = new Contact(0, a1, "c12");
        assert(widget.count()==2);
        assert(widget.getSelectedContact() == c11);

        // new blocked contact
        Contact *c13 = new Contact(0, a1, "c13", "c13@example.com", TristateBool::VAL_FALSE);
        assert(widget.count()==2);

        // new account
        Account *a2 = new Account(0, "a2", "a2@example.com", "FPR", "bar", false);
        assert(widget.count()==2);
        assert(widget.getSelectedContact() == c11);

        // set empty account
        widget.setAccount(a2);
        assert(widget.count()==0);
        assert(widget.getSelectedContact() == nullptr);

        // new contact to empty account
        Contact *c21 = new Contact(0, a2, "c21");
        assert(widget.count()==1);
        assert(widget.getSelectedContact() == c21);

        // delete only contact
        delete c21; c21=nullptr;
        assert(widget.count()==0);
        assert(widget.getSelectedContact() == nullptr);

        // delete account
        delete a2; a2=nullptr;
        assert(widget.getAccount() == a1);
        assert(widget.count()==2);
        assert(widget.getSelectedContact() == c11);

        // set previous account
        widget.setAccount(a1);
        assert(widget.count()==2);
        assert(widget.getSelectedContact() == c11);

        // delete only account
        delete a1; a1=nullptr;
        assert(widget.getAccount()==nullptr);
        assert(widget.count()==0);
        assert(widget.getSelectedContact() == nullptr);
    }

    assert(Account::getAllAccounts().empty());
    assert(Contact::getAllContacts().empty());

    // react to changes
    {
        Account a1(0, "a1", "a1@example.com", "FPR", "foo", false);
            Contact c11(0, &a1, "c11", "c11@example.com");
            Contact c12(0, &a1, "c12", "c12@example.com");

        QApplication app(argc,argv);
        ContactSelectorWidget widget(&a1,nullptr);

        // static name
        assert(widget.currentText() == "c11 <c11@example.com>");

        // change name
        c11.setName("foo");
        assert(widget.currentText() == "foo <c11@example.com>");

        // change email
        c11.setEmail("foo@example.com");
        assert(widget.currentText() == "foo <foo@example.com>");
    }

    /// @todo test setContact
    /// @todo test Contact::setConfirmed
    return 0;
}
