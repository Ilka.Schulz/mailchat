/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QLabel>
#include "src/messages/MessagePartObserver.hpp"
#include "src/ui/InlineMessagePartWidget.hpp"

class QBoxLayout;

class MessagePart;
class TextInlineMessagePart;

class MessagePartWidget;
class PlainTextInlineMessagePartWidget;
class HtmlTextInlineMessagePartWidget;


/** @brief
 *  @details This class will NOT DIRECTLY observe @c MessagePart because its Bridge parent (v.s.) will call the event
 *  handlers. This class follows the bridge pattern to morph into a @c PlainTextMessagePartWidget or a @c
 *  HtmlTextMessagePartWidget. @todo update this text
 */
class TextInlineMessagePartWidget  :  public InlineMessagePartWidget  {
    private:
        QBoxLayout *rootContainer;
        PlainTextInlineMessagePartWidget *plainTextMessagePartWidget;
        HtmlTextInlineMessagePartWidget *htmlTextMessagePartWidget;

    public:
        TextInlineMessagePartWidget(TextInlineMessagePart *textMessagePart, QWidget *parent=nullptr);
        virtual ~TextInlineMessagePartWidget();

    protected:
        virtual void messagePartEvent_dispositionChanged(MessagePart *messagePart)  override;
        virtual void messagePartEvent_contentChanged(MessagePart *messagePart)  override;
        virtual void messagePartEvent_deleted(MessagePart *messagePart)  override;

    friend MessagePartWidget;  ///< bridge of this class is allowed to call the event handlers
};
