/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "WindowGenericDialog.hpp"
#include "src/processors/GpgEngineProcessorObserver.hpp"
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTreeWidget>

class WindowNewAccount;


/** @brief window allowing the user to add an @c Account
 *  @details This dialog allows to add an existing email account (or similar) to the application.
 *  It does not allow the user to open an email account (or similar) with an email service provider.
 */
class WindowNewAccount  :  public WindowGenericDialog,
                           protected GpgEngineProcessorObserver  {
    private:
        /**
         * @brief if true and the wizard fails to create a new account: the application will exit
         * @details Useful if the application can not continue to run without an account created.
         * If the window is successful (in creating an account) and can therefore safely be destroyed without the
         * application depending on it anymore this member needs to be set to false (usually internally).
         */
        bool required;
        QGridLayout *propertiesGrid;   ///< grid  layout holding the labels and input elements
            QLabel    *nameLabel;      ///< label for @c Account::name
            QLineEdit *nameInput;      ///< input element for @c Account::name
            QLabel    *emailLabel;     ///< label for @c Account::email and @c EmailConnection::host and @c EmailConnection::username
            QLineEdit *emailInput;     ///< input element for @c Account::email and @c EmailConnection::host and @c EmailConnection::username
            QLabel    *passwordLabel;  ///< label for @c EmailConnection::password
            QLineEdit *passwordInput;  ///< input element for @c EmailConnection::password
            QLabel    *pgpKeyFingerprintLabel;            ///< label for @c Account::pgpKeyFingerprint
            QTreeWidget *pgpKeyFingerprintTreeWidget;     ///< input element to choose an PGP keypair for @c Account::pgpKeyFingerprint
            QLabel    *pgpKeyCreateLabel;                 ///< label showing hyperlink to create a @c WindowCreateKeypair

    public:
        /**
         * creates a new standalone window which guides the user through the process of adding an account
         * @param _required  initializes required
         */
        WindowNewAccount(bool _required=false);

        /// terminates the application if(required==true)
        virtual ~WindowNewAccount();

    protected:

        /// @brief handles user input into the email entry GUI element
        /// @details The handler updates the PGP key fingerpint list.
        void emailInputChangedEvent();

        /// @brief handles click on create keypair label GUI element
        /// @details This handler creates a @c WindowCreateKeyPair
        void keyCreateLabelClickedEvent();

        /**
         * @brief handles click on OK button
         * @details The handler closes the window and creates a persitent @c Account object
         * @todo implement check whether the email is already used by another account
         */
        virtual void okButtonClickedEvent() override;


        // event handlers
        virtual void gpgEngineProcessorEvent_keyAdded(Gpgme::Key &key)  override;
        virtual void gpgEngineProcessorEvent_keyRemoved(const Gpgme::Key &key)  override;
};
