/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "ImageInlineMessagePartWidget.hpp"
#include "src/messages/ImageInlineMessagePart.hpp"
#include <QBoxLayout>
#include <QLabel>


ImageInlineMessagePartWidget::ImageInlineMessagePartWidget(ImageInlineMessagePart *messagePart, QWidget *parent)
        : InlineMessagePartWidget(parent)  {
    // layout
    container = new QBoxLayout(QBoxLayout::Direction::LeftToRight, this);
    setLayout(container);
        // label
        label = new QLabel(this);
        container->addWidget(label);

    // call event handlers once
    ImageInlineMessagePartWidget::messagePartEvent_contentChanged(messagePart);
}

void ImageInlineMessagePartWidget::messagePartEvent_contentChanged(MessagePart *messagePart)  {
    ImageInlineMessagePart *imageMessagePart = dynamic_cast<ImageInlineMessagePart*>(messagePart);
    assert(imageMessagePart != nullptr);
    // set pixmap
    const std::vector<char> &data = imageMessagePart->getData();
    QPixmap pixmap;
    if( data.empty()  ||  ! pixmap.loadFromData((unsigned char*)&data[0], data.size()) )
        label->setText(QString::fromStdString(imageMessagePart->getAltText().value_or(
                                                tr("could not load image and not alternative text available").toStdString()
                       )));
    else
        label->setPixmap(pixmap);
}
