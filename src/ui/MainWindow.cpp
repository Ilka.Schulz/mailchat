/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/ui/MainWindow.hpp"
#include "src/ui/ContactWidget.hpp"
#include "src/ui/ContactListWidget.hpp"
#include "src/ui/ChatWidgetAllowingEmpty.hpp"
#include "src/ui/AccountSelectorWidget.hpp"
#include "src/ui/WindowAbout.hpp"
#include "src/ui/WindowNewAccount.hpp"
#include "src/ui/WindowCreateKeypair.hpp"
#include "src/ui/WindowNewContact.hpp"
#include "src/Application.hpp"
#include "src/Account.hpp"
#include "src/connections/Connection.hpp"
#include "src/settings.hpp"
#include <QMenuBar>
#include "src/ui/WindowDocumentViewer.hpp"
#include "src/resources.hpp"
#include "src/ui/ComposeWindow.hpp"
#include "src/ui/WindowAccountSettings.hpp"
#include "src/ui/WindowKeyManagement.hpp"
#include <QMessageBox>
#include "src/util/time.hpp"


extern QIcon applicationIcon;

MainWindow::MainWindow()
    : QMainWindow()  {
    // window
    resize(800,600);
    setWindowTitle(QString::fromStdString(Settings::applicationName));
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowIcon(applicationIcon);
        // central widget
        setCentralWidget(new QWidget(this));
        centralWidget()->setContentsMargins(0,0,0,0);
        //QPushButton *button = new QPushButton(this);
        //button->setText("test");
        // horizontal container
        container = new QBoxLayout(QBoxLayout::LeftToRight, this);
        container->setContentsMargins(0,0,0,0);
        container->setObjectName("container");
        centralWidget()->setLayout(container);
            // left vertical widget
            leftVerticalWidget = new QWidget(this);
            leftVerticalWidget->setSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::MinimumExpanding);
            leftVerticalWidget->setObjectName("leftVerticalWidget");
            leftVerticalWidget->setStyleSheet("#leftVerticalWidget {background-color:#E0E0E0}");
            leftVerticalWidget->setMaximumWidth(300);  /// @todo magic number
            container->addWidget(leftVerticalWidget);
                // left vertical container
                leftVerticalBox = new QBoxLayout(QBoxLayout::TopToBottom);
                leftVerticalBox->setContentsMargins(0,0,0,0);
                leftVerticalBox->setObjectName("leftVerticalBox");
                leftVerticalWidget->setLayout(leftVerticalBox);
                    // account box
                    accountBox = new QBoxLayout(QBoxLayout::LeftToRight, this);
                    leftVerticalBox->addLayout(accountBox);
                        // account selector
                        accountSelectorWidget = new AccountSelectorWidget(this);
                        accountSelectorWidget->setObjectName("accountSelectorWidget");
                        accountSelectorWidget->addObserver(this);
                        accountBox->addWidget(accountSelectorWidget);
                        // account menu button
                        accountMenuButton = new QToolButton(this);
                        accountMenuButton->setText("\u2630");  /// @todo add this to emoji list
                        accountBox->addWidget(accountMenuButton);
                        QObject::connect(accountMenuButton, &QToolButton::clicked, this, [this](bool)  {
                            /// @todo disable button when no account is selected
                            new WindowAccountSettings(accountSelectorWidget->getSelectedAccount());
                        });
                    // contact selector
                    contactListWidget = new ContactListWidget(this,nullptr);
                    contactListWidget->setObjectName("contactListWidget");
                    contactListWidget->setSingleSelectionMode(true);
                    contactListWidget->setNoDeselectMode(true);
                    contactListWidget->addObserver(this);
                    leftVerticalBox->addWidget(contactListWidget);
                    //leftVerticalBox->setAlignment(contactListWidget, Qt::AlignmentFlag::AlignTop);
                // chat widget
                chatWidget = new ChatWidgetAllowingEmpty(this, nullptr);
                container->addWidget(chatWidget);

        // main menu bar
        {
        QMenu *menuFile = menuBar()->addMenu(tr("&File"));
            // New
            QMenu *menuFileNew = menuFile->addMenu(tr("New"));
                // Message
                QAction *menuFileNewMessage = menuFileNew->addAction(tr("&Message"));
                menuFileNewMessage->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_N));
                connect(menuFileNewMessage, &QAction::triggered, this, [this](bool)  {
                    Contact *contact = chatWidget->getContact();
                    Account *account = (contact==nullptr?nullptr:contact->getAccount());
                    new ComposeWindow(account, contact);
                });
                // separator
                menuFileNew->addSeparator();
                // Account
                QAction *menuFileNewAccount = menuFileNew->addAction(tr("&Account"));
                QObject::connect(menuFileNewAccount, &QAction::triggered, this, [this](bool)  {
                    (void)this;
                    new WindowNewAccount();
                });
                // Contact
                QAction *menuFileNewContact = menuFileNew->addAction(tr("&Contact"));
                QObject::connect(menuFileNewContact, &QAction::triggered, this, [this](bool)  {
                    new WindowNewContact(accountSelectorWidget->getSelectedAccount());
                });
                // Keypair
                QAction *menuFileNewKeypair = menuFileNew->addAction(tr("&Keypair"));
                QObject::connect(menuFileNewKeypair, &QAction::triggered, this, [this](bool)  {
                    (void)this;
                    new WindowCreateKeypair();
                });

            // Fetch message
            QAction *menuFileFetchMessages = menuFile->addAction(tr("Fetch Messages"));
            menuFileFetchMessages->setShortcut(QKeySequence(Qt::Key_F5));
            QObject::connect(menuFileFetchMessages, &QAction::triggered, this, [this](bool)  {
                Account *account = accountSelectorWidget->getSelectedAccount();
                if(account != nullptr)
                    for(Connection *connection : account->getConnections())
                        connection->fetchIncomingMessages(false);
            });

            // Quit
            QAction *menuFileQuit = menuFile->addAction(tr("Quit"));
            menuFileQuit->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q));
            QObject::connect(menuFileQuit, &QAction::triggered, this, [this](bool)  {
                close();
            });

        // Edit
        QMenu *menuEdit = menuBar()->addMenu(tr("&Edit"));
            // Account settings
            menuEditHelp = menuEdit->addAction(tr("&Account settings"));
            QObject::connect(menuEditHelp, &QAction::triggered, this, [this](bool)  {
                new WindowAccountSettings(accountSelectorWidget->getSelectedAccount());
            });
            // Key Management
            QAction *menuEditKeyMangement = menuEdit->addAction(tr("&Key Management"));
            QObject::connect(menuEditKeyMangement, &QAction::triggered, this, [this](bool)  {
                (void)this;
                new WindowKeyManagement();
            });
        // Help
        QMenu *menuHelp = menuBar()->addMenu(tr("&Help"));
            // Privacy Policy
            QAction *menuHelpPrivacyPolicy = menuHelp->addAction(tr("&Privacy Policy"));
            QObject::connect(menuHelpPrivacyPolicy, &QAction::triggered, this, [this](bool)  {
                (void)this;
                /// @todo @c WindowDocumentViewer should get Caption from Markdown
                new WindowDocumentViewer(resPrivacyPolicy,
                                         WebView::WebDocumentType::MARKDOWN,
                                         Settings::applicationName+" Privacy Policy");
            });
            // License
            QAction *menuHelpLicense = menuHelp->addAction(tr("&License"));
            QObject::connect(menuHelpLicense, &QAction::triggered, this, [this](bool)  {
                (void)this;
                new WindowDocumentViewer(resCopying,
                                         WebView::WebDocumentType::PLAIN_TEXT,
                                         Settings::applicationName+" License");
            });
            // Changelog
            QAction *menuHelpChangelog = menuHelp->addAction(tr("&What's new?"));
            QObject::connect(menuHelpChangelog, &QAction::triggered, this, [this](bool)  {
                (void)this;
                new WindowDocumentViewer(resChangelog,
                                         WebView::WebDocumentType::MARKDOWN,
                                         Settings::applicationName+" Changelog");
            });
            // About
            QAction *menuHelpAbout = menuHelp->addAction(tr("&About"));
            QObject::connect(menuHelpAbout, &QAction::triggered, this, [this](bool)  {
                (void)this;
                new WindowAbout(nullptr);
            });
        }
    show();

    // manually call event handler once to prepare the UI
    MainWindow::accountSelectorWidgetEvent_accountSelected(accountSelectorWidget, accountSelectorWidget->getSelectedAccount());
}
MainWindow::~MainWindow()  {
    accountSelectorWidget->removeObserver(this);
    contactListWidget->removeObserver(this);
}

void MainWindow::closeEvent(QCloseEvent *event)  {
    (void) event;
    Application::getInstance()->exit();
}

void MainWindow::accountSelectorWidgetEvent_accountSelected(AccountSelectorWidget *accountSelectorWidget,
                                                            Account *account)  {
    assert(accountSelectorWidget == this->accountSelectorWidget);
    this->contactListWidget->setAccount(account);
    bool enabled = account!=nullptr;  // whether the account settings shall be enabled
    accountMenuButton->setEnabled(enabled);
}

void MainWindow::contactListWidgetEvent_contactWidgetSelected(ContactListWidget *contactListWidget,
                                                              ContactWidget *contactWidget,
                                                              bool active)  {
    assert(contactListWidget == this->contactListWidget);
    if(active)  {
        this->chatWidget->setContact(contactWidget->getAssociatedContact());
    }
}

void MainWindow::gpgEngineProcessorEvent_keyWillExpireSoon(Gpgme::Key &key)  {
    if(key.isSecret())  {
        // find affected account
        const String email = key.getUserIds()->email;
        Account *account = nullptr;
        for(Account *a : Account::getAllAccounts())
            if(a->getEmail().equals(email,CASE_INSENSITIVE))  {
                account = a;
                break;
            }
        if(account != nullptr)  {
            // show warning
            QMessageBox *box = new QMessageBox;
            box->setWindowTitle(tr("Kill will expire"));
            box->setText(tr("Your key with the fingerprint") + " " +
                         QString::fromStdString(key.getFingerprint()) + " " +
                         tr("for your account") + " " +
                         QString::fromStdString(account->getNameAndEmail()) + " " +
                         tr("will expire soon on ") + " " +
                         QString::fromStdString(Time::timeStampToText(key.getExpiry(), Time::widgetDateFormat)) + tr(".")
                        );
            box->setIcon(QMessageBox::Icon::Warning);
            box->show();
        }
    }
}
