/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

class ContactListWidgetObserver;
class ContactListWidget;
class ContactWidget;

/** @brief observer of @c ContactListWidget
 *  @details design pattern @c Observer
 */
class ContactListWidgetObserver  {
    protected:
        /** @brief a @c ContactWidget has been selected by the user (or by the application)
         *  @param contactListWidget  the observed @c ContactListWidget object
         *  @param contactWidget      the selected @c ContactWidget object
         *  @param active             whether the @c ContactWidget has been activated (true) or deactivated (false)
         */
        virtual void contactListWidgetEvent_contactWidgetSelected(ContactListWidget *contactListWidget, ContactWidget *contactWidget, bool active);

    /// @c ContactListWidget is the observed class so it needs to call the protected event handlers
    friend ContactListWidget;
};
