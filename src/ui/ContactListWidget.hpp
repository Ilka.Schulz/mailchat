/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
class QBoxLayout;
class QLabel;
class QScrollArea;
class QPushButton;

class ContactListWidget;
class Account;
class ContactWidget;

#include <QWidget>
#include "src/patterns/Observable.hpp"
#include "src/ui/ContactListWidgetObserver.hpp"
#include "src/ui/ContactWidgetObserver.hpp"
#include "src/AccountObserver.hpp"


/** @brief a widget that lists available @c ContactWidget objects representing the @c Contact objects of an
 *  @c Account
 *  @todo sort methods
 */
class ContactListWidget  :
        public QWidget,
        public Observable<ContactListWidgetObserver>,
        public AccountObserver,
        public ContactWidgetObserver  {
    private:
        QBoxLayout *verticalBox;                  ///< root widget
            QLabel *label;                        ///< caption label, e.g. saying "Contacts"
            QScrollArea *scrolledWindow;          ///< contains box with list of contacts
                QWidget *scrolledWindowWidget;
                    QBoxLayout *scrolledVerticalBox;  ///< container for @c ContactWidget objects
                    QPushButton *newContactButton;    ///< button to let the user create new @c Contact objects
        bool singleSelectionMode;                 ///< if true: only one contact can be selected at a time
        ContactWidget *lastSelected;              ///< the @c ContactWidget that has been selected last
        bool currentlyHandlingSelectEvent;        ///< if true: contactSelectEvent has been called and has not returned yet
        bool noDeselectMode;                      ///< if true: user only select items but can not deselect an item by clicking on it
        Account *account;                         ///< observed account whiches contacts are being displayed by this object
        std::vector<ContactWidget*> children;     ///< list of all @c ContactWidget objects shown as children of this object  @todo sort widgets by name or so  @todo allow to filter by search string or so

    protected:
        /// enforces selection policies, e.g. @c ContactListWidget::updateSelection
        /// if singleSelectionMode is true: deselectes all previous selections
        void updateSelection();

    // event handlers called by observed Account class
    protected:
        virtual void accountEvent_contactAdded(Account *account, Contact *contact)  override;
        virtual void accountEvent_contactRemoved(Account *account, Contact *contact)  override;
        virtual void contactWidgetEvent_clicked(ContactWidget *contactWidget)  override;
        virtual void contactWidgetEvent_deleted(ContactWidget *contactWidget)  override;

    public:
        /** @brief constructs a widget that lists all contacts of a specific account
         *  @param parent  the parent widget that contains this widget
         *  @param _account  the account whiches contacts are being displayed by the constructed object (set to null to show no contacts)
         */
        ContactListWidget(QWidget *parent, Account *_account);

        /// sets @c ContactListWidget::singleSelectionMode  @param singleSelectionMode  the value to set
        void setSingleSelectionMode(bool singleSelectionMode);
        /// sets @c ContactListWidget::noDeselectMode  @param noDeselectMode  the value to set
        void setNoDeselectMode(bool noDeselectMode);
        /// sets @c ContactListWidget::account and shows the new account's contacts  @param newAccount  the new account
        /// to set
        void setAccount(Account *newAccount);

        /** @brief gets @c ContactListWidget::scrolledHorizontalBox so that children can add their widgets to it
         *  @returns @c ContactListWidget::scrolledHorizontalBox
         */
        QBoxLayout *getContainer();

        /** @brief event handler called by children that get selected / deselected
         *  @param contactWidget  the calling @c ContactWidget object
         *  @param active  whether the caller has been actived or deactivated
         *  @todo consider making this class an observer of ContactWidget
         */
        void contactSelectEvent(ContactWidget *contactWidget, bool active);

        void newContactButtonClickedEvent(bool checked);
};
