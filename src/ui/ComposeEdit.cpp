/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "ComposeEdit.hpp"
#include <QKeyEvent>
#include <QAbstractItemView>
#include <QScrollBar>
#include <QTreeView>
#include "src/util/Emoji.hpp"
#include <QStandardItemModel>
#include <algorithm>
#include "src/util/tictoc.hpp"

#include <iostream>

extern QFont emojiFont;

ComposeEdit::ComposeEdit(QWidget *parent, bool _autoResize)
    : QTextEdit(parent), fontMetrics(emojiFont), autoResize(_autoResize)  {
    int emoji_width = fontMetrics.horizontalAdvance(QString::fromStdString(Emoji::emojis.at(":cat:")));
    setPlaceholderText(tr("Send a message..."));
    setFont(emojiFont);
    connect(this, &QTextEdit::textChanged, this, &ComposeEdit::textChangedEvent);
    // completer
    QCompleter *completer = new QCompleter(this);
        // popup
        QTreeView *popup = new QTreeView(this);
        popup->setRootIsDecorated(false);
        popup->setHeaderHidden(true);
        completer->setPopup(popup);
            // model
            QStandardItemModel *model = new QStandardItemModel(0,2);
            for(const auto &pair : Emoji::ordered_emojis)  {
                QStandardItem *first = new QStandardItem(0,0);
                first->setText(QString::fromStdString(pair.second));
                first->setFont(emojiFont);
                QStandardItem *second = new QStandardItem(0,0);
                second->setText(QString::fromStdString(pair.first));
                model->appendRow(QList<QStandardItem*>{first,second});
            }
            completer->setModelSorting(QCompleter::ModelSorting::CaseSensitivelySortedModel);
            completer->setModel(model);
        popup->setColumnWidth(0, emoji_width);
    completer->setCompletionColumn(1);
    completer->setFilterMode(Qt::MatchContains);
    completer->setCompletionMode(QCompleter::CompletionMode::PopupCompletion);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setWidget(this);
    setCompleter(completer);
}
ComposeEdit::~ComposeEdit()  {
}

void ComposeEdit::setCompleter(QCompleter *completer)  {
    if(this->completer != nullptr)
        completer->disconnect(this);
    this->completer = completer;
    if(completer == nullptr)
        return ;
    completer->setWidget(this);
    //completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    QObject::connect(completer, QOverload<const QString &>::of(&QCompleter::activated), this, &ComposeEdit::insertCompletion);
}
QCompleter *ComposeEdit::getCompleter() const  {
    return completer;
}

void ComposeEdit::focusInEvent(QFocusEvent *e)  {
    if (completer)
        completer->setWidget(this);
    QTextEdit::focusInEvent(e);
}
void ComposeEdit::keyPressEvent(QKeyEvent *e)  {
    tictoc timer = tictoc::construct(__PRETTY_FUNCTION__, false);
        if (completer && completer->popup()->isVisible()) {
            // The following keys are forwarded by the completer to the widget
            switch (e->key()) {
                case Qt::Key_Enter:
                case Qt::Key_Return:
                case Qt::Key_Escape:
                case Qt::Key_Tab:
                case Qt::Key_Backtab:
                    e->ignore();  // let the completer do default behavior
                    return;
                default:
                    break;
            }
        }
        else  {
            if(e->key() == Qt::Key_Tab && e->modifiers() == Qt::NoModifier)  {
                return ;  // ignore tab
                /// @todo advance focus onto next GUI element (like normal tab key behavior)
            }

        }

        // do not process the shortcut when we have a completer
        const bool isShortcut = (e->modifiers().testFlag(Qt::ControlModifier) && e->key() == Qt::Key_E); // CTRL+E
        if (!completer || !isShortcut)
            QTextEdit::keyPressEvent(e);

        if(completer!=nullptr)  {
            // ignore modifiers
            const bool ctrlOrShift = e->modifiers().testFlag(Qt::ControlModifier) ||
                                     e->modifiers().testFlag(Qt::ShiftModifier);
            if (ctrlOrShift && e->text().isEmpty())
                return;

            static QString eow("~!@#$%^&*()+{}|:\"<>?,./;'[]\\-= \t\n");  // end of word
            const bool hasModifier = (e->modifiers() != Qt::NoModifier) && !ctrlOrShift;
            QString completionPrefix = wordUnderCursor();
            //std::cout << ">" << completionPrefix.toStdString() << "<"<<std::endl;

            QString text = e->text();
            if (!isShortcut && (  hasModifier ||
                                  completionPrefix.isEmpty() ||
                                  completionPrefix[0]!=':' ||
                                  completionPrefix.size()<3 ||
                                  ( ! e->text().isEmpty() && eow.contains(e->text().right(1)) )
                                )
               ) {
                completer->popup()->hide();
                return;
            }
            // remove starting colon
            completionPrefix.remove(0,1);
            // show popup widget
            if (completionPrefix != completer->completionPrefix()) {
                completer->setCompletionPrefix(completionPrefix);
                completer->popup()->setCurrentIndex(completer->completionModel()->index(0, 0));
            }
            QRect cr = cursorRect();
            cr.setWidth(completer->popup()->sizeHintForColumn(0) +
                        completer->popup()->sizeHintForColumn(1) +
                        completer->popup()->verticalScrollBar()->sizeHint().width());
            completer->complete(cr); // popup it up!
        }
}
void ComposeEdit::resizeEvent(QResizeEvent *e)  {
    QTextEdit::resizeEvent(e);
    adjustHeight();
}
QSize ComposeEdit::minimumSizeHint()  const  {
    if(autoResize)
        return minSizeHint;
    else
        return QTextEdit::minimumSizeHint();
}
QSize ComposeEdit::sizeHint() const  {
    if(autoResize)
        return minSizeHint;
    else
        return QTextEdit::sizeHint();
}

void ComposeEdit::insertCompletion(const QString &completion)  {
    if(completer==nullptr)
        return ;
    QTextCursor tc = textCursor();
    tc.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor, completer->completionPrefix().size()+1);
    tc.removeSelectedText();
    tc.insertText(QString::fromStdString( Emoji::convertEmoji(completion.toStdString()) ));
    setTextCursor(tc);
}
void ComposeEdit::textChangedEvent()  {
    adjustHeight();
}


QString ComposeEdit::wordUnderCursor() const  {
    // select line under cursor
    QTextCursor tc = textCursor();
    QString plainText = toPlainText().left(tc.position());
    String ret = plainText.toStdString();
    // select last word
    size_t idx = ret.findLast({' ', '\t','\n'},PLAIN);
    ret.erase(0,idx+1);
    return QString::fromStdString(ret);
}

void ComposeEdit::adjustHeight()  {
    if(autoResize)  {
        int line_count = toPlainText().count('\n')+1;
        int h = std::min<int>(document()->size().height() + frameWidth()*2,
                         7*(fontMetrics.lineSpacing() + frameWidth()*2));
        minSizeHint = QSize(100,h);
        updateGeometry();
    }
}
