/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "HtmlTextInlineMessagePartWidget.hpp"
#include "src/messages/TextInlineMessagePart.hpp"
#include <assert.h>


#include <QtWebKitWidgets/QWebView>
#include <QWebFrame>
#include <iostream>

HtmlTextInlineMessagePartWidget::HtmlTextInlineMessagePartWidget(TextInlineMessagePart *textMessagePart, QWidget *parent)
    : WebView(textMessagePart->getText(), WebView::WebDocumentType::HTML, parent)  {
    assert(textMessagePart->getContentType() == TextInlineMessagePart::ContentType::HTML  ||
           textMessagePart->getContentType() == TextInlineMessagePart::ContentType::WATCH_HTML);

    //std::cout << qwebView->page()->mainFrame()-> ->preferredContentsSize().height() << std::endl;
    //setMinimumHeight();
}
