/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QWidget>
#include <QLineEdit>
#include <QScrollArea>
#include <QGridLayout>
#include <QGraphicsView>

#include <unordered_map>
#include <string>

class GraphicsEmojiItem;

#include "src/ui/EmojiListWidgetObserver.hpp"
#include "src/patterns/Observable.hpp"

class EmojiListWidget  :  public QWidget, public Observable<EmojiListWidgetObserver>  {
    private:
        const static size_t emoji_size;
        const static size_t n_columns;
        static QGraphicsScene *graphicsScene;
        static std::unordered_map<String, GraphicsEmojiItem*> graphicsItems;  /// @todo do not use pointers
        QBoxLayout *container;  ///< vertical container holding the different parts of the widget
            QLineEdit *searchInput;  ///< input to search for emojis
            QGraphicsView *graphicsView;

    public:
        EmojiListWidget(QWidget *parent=nullptr);
    private:
        void searchInputEditedEvent(const QString &text);
        void graphicsEmojiItemClickedEvent(const GraphicsEmojiItem *item);
    public:
        void setPartial(String partial);

    friend GraphicsEmojiItem;  ///< children need to call event handlers
};
