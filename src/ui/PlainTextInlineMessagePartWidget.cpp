/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "PlainTextInlineMessagePartWidget.hpp"
#include "src/messages/TextInlineMessagePart.hpp"
#include <assert.h>

extern const QFont emojiFont;

PlainTextInlineMessagePartWidget::PlainTextInlineMessagePartWidget(TextInlineMessagePart *textMessagePart, QWidget *parent)
    : QLabel(parent)  {
    assert(textMessagePart->getContentType() == TextInlineMessagePart::PLAIN);
    setTextInteractionFlags(Qt::TextBrowserInteraction | Qt::TextSelectableByKeyboard);
    setTextFormat(Qt::TextFormat::PlainText);
    setWordWrap(true);  /// @todo QLabel::sizeHint is buggy when QLabel::wordWrap is true
    setFont(emojiFont);
    /// @todo set maximum height and make scrollable

    // DO NOT register as observer, see class description

    // call event handlers once
    PlainTextInlineMessagePartWidget::messagePartEvent_contentChanged(textMessagePart);
}

void PlainTextInlineMessagePartWidget::messagePartEvent_contentChanged(MessagePart *messagePart)  {
    TextInlineMessagePart *textMessagePart = dynamic_cast<TextInlineMessagePart*>(messagePart);
    assert(textMessagePart != nullptr);
    setText(QString::fromStdString(textMessagePart->getText()));
}
