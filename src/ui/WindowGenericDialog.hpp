/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QDialog>
#include <QBoxLayout>
#include <QPushButton>
#include <QFrame>
#include "src/util/String.hpp"
#include <QLabel>
#include <QStatusBar>

class WindowGenericDialog;

/** @brief a window with basic dialog elements
 *  @details This class is supposed to be derived for specific dialogs. This base class provides an OK and a Cancel
 *  button.
 */
class WindowGenericDialog  :  public QDialog  {
    private:
        QBoxLayout *verticalBox;            ///< root container
            QLabel *captionLabel;
            QFrame *captionLine;

            QWidget *container;          ///< subclasses can insert their widgets into this container
            QFrame *buttonsLine;                   ///< line that separates the content from the button box
            QBoxLayout *buttonsBox;         ///< a box containing an OK and a Cancel button
                QPushButton *cancelButton;  ///< button to cancel the dialog without success
                QPushButton *okButton;      ///< button to confirm the dialog and successfully exit it

            QStatusBar *statusBar;

    public:
        /**
         * constructs and shows a generic dialog with functional OK and Cancel button
         * @param title   window title
         * @param width   window width (set to zero to use default size)
         * @param height  window hight (set to zero to use default size)
         */
        WindowGenericDialog(String title, int width=0, int height=0);

    // slots:
    private:
        void cancelButtonClickedSlot(bool checked);
        void okButtonClickedSlot(bool checked);

    protected:
        /// @returns container into which subclasses can insert their widgets
        QWidget *getContainer();
        QStatusBar *getStatusBar();
        void disableDialogButtons();
        void enableDialogButtons();

        /**
         * handles click on cancel button
         */
        virtual void cancelButtonClickedEvent();
        /**
         * handles click on OK button
         * closes the window and creates persitent Account object
         * @todo implement check whether the email is already used by another account
         */
        virtual void okButtonClickedEvent();
};
