/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QWidget>
#include <QLayout>
#include <QGridLayout>
#include <QScrollArea>
#include <QPushButton>
#include <QLineEdit>
#include <QToolButton>

#include <vector>
#include <list>

class ChatWidget;
class Contact;

#include "MainWindow.hpp"
#include "MessageWidget.hpp"
#include "src/messages/MessageCache.hpp"
#include "src/messages/MessageCacheObserver.hpp"
#include "src/ContactObserver.hpp"
#include "src/ui/EmojiListWidgetObserver.hpp"
#include "src/ui/ComposeEdit.hpp"
#include "src/processors/GpgEngineProcessorObserver.hpp"


/** @brief widget showing messages and compose line
 */
class ChatWidget  :  public QWidget,
                     protected MessageCacheObserver,
                     protected ContactObserver,
                     protected EmojiListWidgetObserver,
                     protected GpgEngineProcessorObserver
{
    private:
        QBoxLayout *verticalBox;                     ///< root container holding messages container and compose area
            QScrollArea *messageScrolledWindow;      ///< scrolled window holding @c messageContainer
            QWidget *messageScrolledWindowWidget;
                QBoxLayout *messageContainer;        ///< container for @c MessageWidget objects
                QBoxLayout *confirmButtonContainer;  ///< button box to confirm or block a contact
                    QPushButton *confirmContactButton;  ///< button to confirm the @c ChatWidget::contact
                    QPushButton *blockContactButton; ///< button to block the @c ChatWidget::contact
            QGridLayout *composeGridLayout;          ///< container for compose area
                QLabel *composePgpWarningLabel;      ///< warning if the contact's PGP pubkey is not available
                ComposeEdit *composeInput;           ///< input element to compose a message
                QPushButton *composeWindowButton;    ///< button pop up new @c ComposeWindow
                QToolButton *composeEmoticonButton;  ///< button to add emoticons to the composed message
                    EmojiListWidget *composeEmoticonButtonEmojiListWidget;
                QToolButton *composeSendButton;      ///< button to send the composed message

        /// cache holding all currently displayed messages
        MessageCache messageCache;
        /// register of all existing @c MessageWidget object
        std::list<MessageWidget*> messageWidgets;
        /// widget shows chat with this @c Contact
        Contact *contact;

    public:
        /** @brief constructor
         *  @details The widget will be added the the specified parent container. It will show the conversation with
         *  the specified contact.
         *  @param _parentContainer  the parent widget
         *  @param _contact          show the conversation with this contact
         */
        ChatWidget(QWidget *_parentContainer, Contact *_contact);
        virtual ~ChatWidget();

    private:
        // event handlers for GUI

        virtual void resizeEvent(QResizeEvent *event)  override;

        /// handler for clicked event on @c ChatWidget::confirmContactButton
        void confirmContactButtonClickedEvent();

        /// handler for clicked event on @c ChatWidget::blockContactButton
        void blockContactButtonClickedEvent();

        void composePgpWarningLabelLinkActivatedEvent(const QString &link);

        /// handler for clicked event on @c ChatWidget::composeSendButton
        void composeSendButtonClickedEvent();

    protected:
        // event handlers for observed objects
        /** @brief handle @c Message added to @c messageCache and create according @c MessageWidget object
         *  @param messageCache  should be equal to this object's @c messageCache
         *  @param pos           position of @c Message object to be added
         *  @param message       the new @c Message
         */
        virtual void messageCacheEvent_messageAdded(MessageCache *messageCache, int pos, Message *message)  override;

        /** @brief handle @c Message removed from @c messageCache and remove according @c MessageWidget object
         *  @param messageCache  should be equal to this object's @c ChatWidget::messageCache
         *  @param message       the new @c Message to delete
         */
        virtual void messageCacheEvent_messageRemoved(MessageCache *messageCache, Message *message)  override;

        /** @brief handle @c Contact changed it's confirmed status
         *  @param contact  should be equal to this object's @c ChatWidget::contact
         */
        virtual void contactEvent_confirmedChangedEvent(Contact *contact)  override;

        virtual void emojiListWidgetEvent_emojiSelected(EmojiListWidget *emojiListWidget, String emoji)  override;

        virtual void gpgEngineProcessorEvent_keyAdded(Gpgme::Key &key)  override;
        virtual void gpgEngineProcessorEvent_keyRemoved(const Gpgme::Key &key)  override;

    public:

        /** @returns @c ChatWidget::contact
         *  @details @c MessageWidget needs to access @c contact in order to observe its parent @c Account object.s
         */
        Contact *getContact();
};
