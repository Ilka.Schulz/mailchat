/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "WindowGenericDialog.hpp"
#include <iostream>
#include <QFrame>

WindowGenericDialog::WindowGenericDialog(String title, int width, int height)
        : QDialog(nullptr)  {
    // window
    if( width!=0 && height!=0 )
        resize(width, height);
    setWindowTitle(QString::fromStdString(title));
    setContentsMargins(0,0,0,0);
        // vertical box
        verticalBox = new QBoxLayout(QBoxLayout::TopToBottom);
        verticalBox->setContentsMargins(0,0,0,0);
        verticalBox->setSpacing(0);
        setLayout(verticalBox);
            // caption label
            captionLabel = new QLabel(this);
            captionLabel->setText(QString::fromStdString(title));
            captionLabel->setStyleSheet("QLabel { background-color:white; color:black; font-size:20pt; padding-top:0.5em; padding-bottom:0.5em}");
            captionLabel->setBackgroundRole(QPalette::ColorRole::WindowText);
            captionLabel->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
            verticalBox->addWidget(captionLabel);
            // caption line
            captionLine = new QFrame(this);
            captionLine->setFrameShape(QFrame::HLine);
            captionLine->setFrameShadow(QFrame::Sunken);
            captionLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
            verticalBox->addWidget(captionLine);
            // container
            container = new QWidget(this);
            verticalBox->addWidget(container);
            // horizontal line
            buttonsLine = new QFrame(this);
            buttonsLine->setFrameShape(QFrame::HLine);
            buttonsLine->setFrameShadow(QFrame::Sunken);
            verticalBox->addWidget(buttonsLine);
            // buttons box
            buttonsBox = new QBoxLayout(QBoxLayout::LeftToRight, this);
            buttonsBox->setContentsMargins(8,8,8,8); /// @todo magic number
            buttonsBox->setSpacing(8); /// @todo magic number
            buttonsBox->setAlignment(Qt::AlignRight);
            verticalBox->addLayout(buttonsBox);
                // cancel button
                cancelButton = new QPushButton(this);
                cancelButton->setText(tr("Cancel"));
                buttonsBox->addWidget(cancelButton);
                QObject::connect(cancelButton, &QPushButton::clicked, this, &WindowGenericDialog::cancelButtonClickedSlot);
                // OK button
                okButton = new QPushButton(this);
                okButton->setText(tr("OK"));
                buttonsBox->addWidget(okButton);
                QObject::connect(okButton, &QPushButton::clicked, this, &WindowGenericDialog::okButtonClickedSlot);

        statusBar = new QStatusBar(this);
        statusBar->hide();
        verticalBox->addWidget(statusBar);

    okButton->setDefault(true);
    // show();  this method must be called at the end of the constructors of derived classes. Qt wants it that way,
    // otherwise, setFocus() won't work properly...
}

// slots
void WindowGenericDialog::cancelButtonClickedSlot(bool checked)  {
    (void) checked;
    cancelButtonClickedEvent();  // call the virtual method
}
void WindowGenericDialog::okButtonClickedSlot(bool checked)  {
    (void) checked;
    okButtonClickedEvent();  // call the virtual method
}

// protected methods
QWidget *WindowGenericDialog::getContainer()  {
    return container;
}
QStatusBar *WindowGenericDialog::getStatusBar()  {
    return statusBar;
}
void WindowGenericDialog::disableDialogButtons()  {
    cancelButton->setEnabled(false);
    okButton->setEnabled(false);
}
void WindowGenericDialog::enableDialogButtons()  {
    cancelButton->setEnabled(true);
    okButton->setEnabled(true);
}

void WindowGenericDialog::cancelButtonClickedEvent()  {
    reject();
}
void WindowGenericDialog::okButtonClickedEvent()  {
    accept();
}
