/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "WindowDetectPgpPubkeys.hpp"
#include <QTreeWidget>
#include <QHeaderView>
#include "src/settings.hpp"
#include "src/connections/CurlJob.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/exceptions/macros.hpp"
#include <QMessageBox>
#include <deque>
#include "src/util/time.hpp"
#include <QStatusBar>
#include "src/util/gpgme.hpp"

WindowDetectPgpPubkeysCurlJob::WindowDetectPgpPubkeysCurlJob(WindowDetectPgpPubkeys *_window,
                                                             CURL *_curl,
                                                             bool _deleteWhenFinished)
        : CurlJob(_curl, _deleteWhenFinished), window(_window)  {
}
WindowDetectPgpPubkeys *WindowDetectPgpPubkeysCurlJob::getWindow() const  {
    return window;
}

WindowDetectPgpPubkeys::WindowDetectPgpPubkeys(const String &_email)
        : WindowGenericDialog("search for public keys",600,400),  /// @todo magic numbers
          email(_email)  {
    if(email.size() < 5)
        ERROR(std::runtime_error, "email is unreasonably short");

    // grid layout
    gridLayout = new QGridLayout(this);
    getContainer()->setLayout(gridLayout);
        // tree widget
        treeWidget = new QTreeWidget(nullptr);
        treeWidget->setEnabled(false);
        treeWidget->setColumnCount(3);  /// @todo magic number
        treeWidget->setHeaderLabels(QStringList{tr("Name"), tr("Key ID"), tr("Expires")});
        treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeMode::Interactive);
        treeWidget->setRootIsDecorated(false);
        //treeWidget->setSelectionMode(QTreeWidget::SelectionMode::ExtendedSelection);  constraint in okButtonClickedEvent
        treeWidget->setSelectionMode(QTreeWidget::SelectionMode::SingleSelection);
        gridLayout->addWidget(treeWidget);

    // status bar
    getStatusBar()->show();
    getStatusBar()->showMessage("fetching overview for keyserver...");

    disableDialogButtons();

    // create curl request to list all keys for email
    CURL *curl = curl_easy_init();
    if(curl==nullptr)
        ERROR(std::runtime_error, "cannot initialize libcurl");
    String encodedEmail;  {
        const char *c_str = curl_easy_escape(curl,email.c_str(), email.size());
        if(c_str == nullptr)
            ERROR(std::runtime_error, "curl_easy_escape returned nullptr");
        encodedEmail = c_str;
        delete[] c_str;
        if(encodedEmail.empty())
            ERROR(std::runtime_error, "encodedEmail is empty");
    }
    curl_easy_setopt(curl, CURLOPT_USERAGENT    , Settings::httpsUserAgent.c_str());
    curl_easy_setopt(curl, CURLOPT_USE_SSL      , (long)CURLUSESSL_ALL);
    curl_easy_setopt(curl, CURLOPT_VERBOSE      , Settings::imapCurlVerbose);
    curl_easy_setopt(curl, CURLOPT_URL          , ("https://"+Settings::pgpKeyServer+
                                                   "/pks/lookup?op=index&options=mr&search="+encodedEmail
                                                   ).c_str());
    WindowDetectPgpPubkeysCurlJob *curlJob = new WindowDetectPgpPubkeysCurlJob(this,curl,true);
    curlJob->callbackOnFinish = WindowDetectPgpPubkeys::staticIndexFinishedCallback;
    curlJob->start(false);

    show();

}

void WindowDetectPgpPubkeys::staticIndexFinishedCallback(CurlJob *curlJob)  {
    // cast opaque data
    WindowDetectPgpPubkeysCurlJob *job = dynamic_cast<WindowDetectPgpPubkeysCurlJob*>(curlJob);
    if(job == nullptr)
        ERROR(std::logic_error, "dynamic_cast failed");  // make this into its own exception type
    WindowDetectPgpPubkeys *this_ = job->getWindow();

    this_->enableDialogButtons();

    // check error code
    if(job->result != CURLE_OK)  {
        QMessageBox *box = new QMessageBox;
        box->setWindowTitle("Lookup failed");
        box->setText(tr("Could not look up pubkeys because the keyserver is not reachable.") + "\n\n" +
                     QString::fromLatin1(curl_easy_strerror(job->result)) + " "
                     "(" + tr("error code") + " " + QString::number(job->result) + ")"
                    );
        box->setIcon(QMessageBox::Icon::Critical);
        box->show();
        delete this_;
        return ;
    }

    // parse result
    // specification: https://datatracker.ietf.org/doc/html/draft-shaw-openpgp-hkp-00
    long responseCode;
    if( curl_easy_getinfo(job->curl, CURLINFO_RESPONSE_CODE, &responseCode) != CURLE_OK)
        ERROR(std::runtime_error, "cannot get HTTP response code");

    if(responseCode == 404)  {
        // key not found
        QMessageBox *box = new QMessageBox;
        box->setWindowTitle("Key not found");
        box->setText(tr("The keyserver does not now any pubkeys for the selected email address"));
        box->setIcon(QMessageBox::Icon::Critical);
        box->show();
        delete this_;
        return ;
    }
    else if(responseCode == 200)  {
        std::deque<String> lines;  {
            std::vector<String> vec_lines = job->incomingData.splitIntoLines(SKIP_ALL_EMPTY);
            std::copy(vec_lines.begin(), vec_lines.end(), std::inserter(lines, lines.end()));
        }
        if(lines.empty())
            ERROR(std::runtime_error, "keyserver responded with empty list");
        std::optional<size_t> count;
        std::vector<String> firstLineParts = lines.front().split(':',PLAIN);
        if(firstLineParts.size()==3 && firstLineParts[0].equals("info",CASE_INSENSITIVE))  {
            lines.pop_front();
            // read optional info
            if(firstLineParts[1].unequal("1",CASE_INSENSITIVE))
                ERROR(std::runtime_error, "keyserver uses unknown HKP version");
            count = firstLineParts[2].toInt();
        }

        while( ! lines.empty() )  {
            // parse "pub" line
            std::vector<String> parts = lines.front().split(':',PLAIN);
            lines.pop_front();
            if(parts.size() != 7)
                ERROR(std::runtime_error, "keyserver replied invalid HKP");
            if(parts[0].unequal("pub",CASE_INSENSITIVE))
                ERROR(std::runtime_error, "keyserver replied no pub line");
            const String keyId     = parts[1],
                         algorithm = parts[2],
                         length    = parts[3],
                         created   = parts[4],
                         expires   = parts[5],
                         flags     = parts[6];

            QTreeWidgetItem *item = new QTreeWidgetItem(this_->treeWidget);

            // parse "uid" lines
            if(lines.empty())
                ERROR(std::runtime_error, "keyserver replied no uid line after pub line");
            parts = lines.front().split(':',PLAIN);
            lines.pop_front();
            if(parts.size() != 5)
                ERROR(std::runtime_error, "keyserver replied invalid HKP");
            const String escapedUid = parts[1],
                         uidCreated = parts[2],
                         uidExpires = parts[3],
                         uidFlags   = parts[4];

            // skip additional "uid" lines
            while( ! lines.empty() && lines.front().split(':',PLAIN,2)[0].equals("uid",CASE_INSENSITIVE))
                lines.pop_front();

            const String uid = curl_easy_unescape(job->curl, escapedUid.c_str(), escapedUid.size(), nullptr);


            item->setText(0, QString::fromStdString(uid));
            item->setText(1, QString::fromStdString(keyId));
            // expiration
            String expirationString;
            if(expires.empty())
                expirationString = "unknown or never";
            else  {
                Time::timeStampToText(expires.toInt(), Time::pgpExpirationDateFormat);
            }
            item->setText(2, QString::fromStdString(expirationString));

            /// @todo mark if already on keyring
            /// @todo allow to download
        }
        // release GUI
        this_->getStatusBar()->showMessage(tr("Please select a keypair."));
        this_->treeWidget->clearSelection();
        this_->treeWidget->topLevelItem(0)->setSelected(true);
        for(int i=0; i<this_->treeWidget->columnCount(); i++)
            this_->treeWidget->resizeColumnToContents(i);
        this_->treeWidget->setEnabled(true);
    }
    else  {
        // bad response code
        QMessageBox *box = new QMessageBox;
        box->setWindowTitle("Bad server response");
        box->setText(tr("The keyserver responded with a bad status code:") + " " +
                     QString::number(responseCode)
                    );
        box->setIcon(QMessageBox::Critical);
        box->show();
        delete this_;
        return ;
    }

    if(Settings::debug)
        std::cout << "received answer from keyserver: >" << job->incomingData << "<" << std::endl;
}

void WindowDetectPgpPubkeys::okButtonClickedEvent()  {
    // constraints
    if(treeWidget->selectedItems().size() > 1)
        ERROR(std::logic_error, "multiselection should be disabled");
    if(treeWidget->selectedItems().size() < 1)  {
        QMessageBox *box = new QMessageBox;
        box->setWindowTitle(tr("No key selected"));
        box->setText(tr("Please select a key to import."));
        box->setIcon(QMessageBox::Icon::Information);
        box->show();
        return;
    }

    const String fingerprint = treeWidget->selectedItems().at(0)->text(1).toStdString();  /// @todo magic string

    getStatusBar()->showMessage(tr("fetching key") + " " +
                                QString::fromStdString(fingerprint) + " " +
                                tr("from keyserver")
                               );
    disableDialogButtons();
    treeWidget->setEnabled(false);


    // create curl request to fetch specific key
    CURL *curl = curl_easy_init();
    if(curl==nullptr)
        ERROR(std::runtime_error, "cannot initialize libcurl");
    curl_easy_setopt(curl, CURLOPT_USERAGENT    , Settings::httpsUserAgent.c_str());
    curl_easy_setopt(curl, CURLOPT_USE_SSL      , (long)CURLUSESSL_ALL);
    curl_easy_setopt(curl, CURLOPT_VERBOSE      , Settings::imapCurlVerbose);
    curl_easy_setopt(curl, CURLOPT_URL          , ("https://"+Settings::pgpKeyServer+
                                                   "/pks/lookup?op=get&options=mr&search="+fingerprint
                                                   ).c_str());
    WindowDetectPgpPubkeysCurlJob *curlJob = new WindowDetectPgpPubkeysCurlJob(this, curl, true);
    curlJob->callbackOnFinish = staticGetFinishedCallback;
    curlJob->start(false);
}

void WindowDetectPgpPubkeys::staticGetFinishedCallback(CurlJob *curlJob)  {
    // cast opaque data
    WindowDetectPgpPubkeysCurlJob *job = dynamic_cast<WindowDetectPgpPubkeysCurlJob*>(curlJob);
    if(job == nullptr)
        ERROR(std::logic_error, "dynamic_cast failed");  // make this into its own exception type
    WindowDetectPgpPubkeys *this_ = job->getWindow();

    this_->getStatusBar()->showMessage(tr("Please select a keypair."));  // not really, but necessary if the fetch failes
    this_->treeWidget->setEnabled(true);
    this_->enableDialogButtons();

    // check error code
    if(job->result != CURLE_OK)  {
        QMessageBox *box = new QMessageBox;
        box->setWindowTitle(tr("Lookup failed"));
        box->setText(tr("Could not fetch pubkey because the keyserver is not reachable.") + "\n\n" +
                     QString::fromLatin1(curl_easy_strerror(job->result)) + " "
                     "(" + tr("error code") + " " + QString::number(job->result) + ")"
                    );
        box->setIcon(QMessageBox::Icon::Critical);
        box->show();
        delete this_;
        return ;
    }

    // parse result
    long responseCode;
    if( curl_easy_getinfo(job->curl, CURLINFO_RESPONSE_CODE, &responseCode) != CURLE_OK)
        ERROR(std::runtime_error, "cannot get HTTP response code");

    if(responseCode == 200)  {
        try  {
            Gpgme::Key::import(job->incomingData);
        }
        catch(Gpgme::GpgmeImportingKeyFailedException &e)  {
            QMessageBox *box;
            box->setWindowTitle(tr("Import failed"));
            box->setText(tr("Could not import the key:") + "\n\n" +
                         QString::fromStdString(e.what())
                        );
            box->setIcon(QMessageBox::Icon::Critical);
            box->show();
            return ;
        }
    }
    else  {
        // bad response code
        QMessageBox *box = new QMessageBox;
        box->setWindowTitle("Bad server response");
        box->setText(tr("The keyserver responded with a bad status code:") + " " +
                     QString::number(responseCode)
                    );
        box->setIcon(QMessageBox::Critical);
        box->show();
        delete this_;
        return ;
    }

    // show success message
    QMessageBox *box = new QMessageBox;
    box->setWindowTitle(tr("Import success"));
    box->setText(tr("The pubkey has been succuessfully imported."));
    box->setIcon(QMessageBox::Icon::Information);
    box->show();


    // finish GUI dialog
    this_->WindowGenericDialog::okButtonClickedEvent();
}
