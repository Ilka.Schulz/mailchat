/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QMainWindow>

#include "src/ui/AccountSelectorWidgetObserver.hpp"

class Account;
class Contact;

class QBoxLayout;
class QPushButton;
class QToolButton;
class QFrame;
class QFormLayout;
class QLabel;
class AccountSelectorWidget;
class ContactSelectorWidget;
class QLineEdit;
class QComboBox;
class ComposeEdit;
class MessageWidget;
class EmailMessage;

/// @todo must observe account and contact in order to handle destruction
class ComposeWindow  :  public QMainWindow,  protected AccountSelectorWidgetObserver  {
    private:
        EmailMessage *previewMessage;
        QBoxLayout *verticalLayout;
            QBoxLayout *actionsLayout;
                QPushButton           *sendButton;
                QToolButton           *spellingButton;
                QToolButton           *saveDraftButton;
            QFormLayout *formLayout;
                AccountSelectorWidget *accountSelectorWidget;
                ContactSelectorWidget *contactSelectorWidget;
                QLineEdit             *subjectEdit;
            QBoxLayout *formatLayout;
                QPushButton           *insertMediaButton;
                QComboBox             *formatComboBox;
                QLabel                *formatHelpLabel;
                QPushButton           *togglePreviewButton;
            QBoxLayout *contentLayout;
                ComposeEdit           *composeEdit;
                MessageWidget         *previewWidget;

    public:
        ComposeWindow(Account *_account, Contact *_contact);
        ~ComposeWindow();

    protected:
        void sendButtonClicked(bool checked);

        virtual void accountSelectorWidgetEvent_accountSelected(AccountSelectorWidget* accountSelectorWidget,
                                                                Account *account);

        virtual void resizeEvent(QResizeEvent *event);
};
