/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <vector>

class ContactWidget;

class ContactWidgetObserver  {
    /*private:
        static std::vector<ContactWidgetObserver*> allContactWidgetObservers;
    public:
        ContactWidgetObserver();
        ~ContactWidgetObserver();
        static std::vector<ContactWidgetObserver*> getAllContactWidgetObservers();
    protected:
        virtual void contactWidgetEvent_created(ContactWidget *contactWidget);*/
    protected:
        virtual void contactWidgetEvent_clicked(ContactWidget *contactWidget);
        virtual void contactWidgetEvent_deleted(ContactWidget *contactWidget);

    /// @c ContactWidget is the observed class so it needs to call the protected event handlers
    friend ContactWidget;
};
