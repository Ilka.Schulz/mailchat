/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QTextEdit>
#include <QCompleter>

class ComposeEdit  :  public QTextEdit  {
    private:
        QCompleter *completer=nullptr;
        QFontMetrics fontMetrics;
        /// whether the widget automatically resizes its height according to the content's number of lines
        bool autoResize;
        QSize minSizeHint;

    public:
        ComposeEdit(QWidget *parent=nullptr, bool _autoResize=true);
        ~ComposeEdit();

    public:
        void setCompleter(QCompleter *completer);
        QCompleter *getCompleter()  const;

    protected:
        virtual void keyPressEvent(QKeyEvent *event)  override;
        virtual void focusInEvent(QFocusEvent *event)  override;
        virtual void resizeEvent(QResizeEvent *e)  override;
        virtual QSize minimumSizeHint()  const override;
        virtual QSize sizeHint()  const override;

    protected slots:
        void insertCompletion(const QString &completion);
        void textChangedEvent();

    private:
        QString wordUnderCursor()  const;
        void adjustHeight();

};
