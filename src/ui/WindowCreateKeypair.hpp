/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QComboBox>
#include <string>
#include "WindowGenericDialog.hpp"

class WindowCreateKeypair;
class AccountSelectorWidget;
class Account;

/** @brief A window that allows the user to create a PGP keypair and add it to GnuPG's keyring
 *  @todo send signal to all instances of @c WindowNewAccount (and others?) when keypair is created
 *  @todo allow to select the key type and key length
 */
class WindowCreateKeypair  :  public WindowGenericDialog  {
    private:
        QGridLayout *propertiesGrid=nullptr;  ///< grid layout holding the labels and input elements

            // manual account info entry
            QLabel *nameLabel=nullptr;        ///< label for the keypair's name
            QLineEdit *nameInput=nullptr;     ///< input element for the keypair's name
            QLabel *emailLabel=nullptr;       ///< label for the keypair's email
            QLineEdit *emailInput=nullptr;    ///< input element for the keypair's email

            // account selection
            QLabel *accountLabel=nullptr;
            AccountSelectorWidget *accountSelectorWidget=nullptr;
            QLabel *accountSwitchToManualLabel=nullptr;

            QLabel *expiryLabel=nullptr;      ///< label for the keypair's expiry
            QBoxLayout *expiryInputBox=nullptr;       ///< container for input elements for keypair's expiry
                QSpinBox *expiryNumberInput=nullptr;  ///< input element for the keypair's expiry (number, not the unit)
                QComboBox *expiryUnitInput=nullptr;   ///< input element for the keypair's expiry (unit, not the number)

    public:
        /** @brief constructor (account selection mode)
         *  @param _account  initial value for @c WindowCreateKeypair::accountSelectorWidget
         */
        WindowCreateKeypair(Account *_account=nullptr);

        /** @brief constructor (manual entry mode)
         *  @param name  initial value for @c WindowCreateKeypair::nameInput
         *  @param email initial value for @c WindowCreateKeypair::emailInput
         */
        WindowCreateKeypair(String name, String email);

    protected:
        void accountSwitchToManualLabelLinkActivatedEvent(const QString &link);

    protected:
        /// @brief event handler when the dialog's OK button gets clicked
        /// @details This function tries to create a keypair and will add it to the user's GnuPG keyring.
        virtual void okButtonClickedEvent() override;
};
