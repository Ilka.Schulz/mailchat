/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "WindowGenericDialog.hpp"
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>

class WindowNewContact;
class Account;
class AccountSelectorWidget;

/** @brief A window that allows the user to create a new @c Contact object and add it to their @c Account object
 *  @todo add GUI elements to create a connection associated with that account: individual host, port, username,
 *  password for @c ImapConnection and @c SmtpConnection
 */
class WindowNewContact  :  protected WindowGenericDialog  {
    private:
        QGridLayout *propertiesGrid; ///< grid container holding
            QLabel *accountLabel;    ///< label for @c Contact::account
            AccountSelectorWidget *accountSelectorWidget;  ///< @c input element for @c Contact::account
            QLabel    *nameLabel;    ///< label for @c Contact::name
            QLineEdit *nameInput;    ///< input element for @c Contact::name
            QLabel    *emailLabel;   ///< label for @c Contact::email
            QLineEdit *emailInput;   ///< input element for @c Contact::email

    public:
        /**
         * constructs a window that lets the user add a contact to their contact list
         * @param account  preselect this account to add the contact to (ignored if null)
         */
        WindowNewContact(Account *account=nullptr);

    protected:
        /**
         * handles click on OK button
         * closes the window and creates persitent Contact object
         * @todo implement check whether this contact already exists
         */
        virtual void okButtonClickedEvent() override;
};
