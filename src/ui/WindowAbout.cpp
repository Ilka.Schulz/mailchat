/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "WindowAbout.hpp"
#include "src/settings.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/ui/WindowDocumentViewer.hpp"
#include "src/resources.hpp"
#include <QPicture>


extern QIcon applicationIcon;

/// @todo make this window class a singleton
WindowAbout::WindowAbout(QWidget *_parent)
    : QDialog(_parent)  {
    setWindowTitle(QString::fromStdString("About "+Settings::applicationName));
    /// @todo delete on close?
    // grid
    propertiesGrid = new QGridLayout(this);
    propertiesGrid->layout()->setSpacing(8); /// @todo magic number
    propertiesGrid->setMargin(10); /// @todo magic number
    setLayout(propertiesGrid);
        // icon
        icon = new QLabel(this);
        icon->setPixmap(applicationIcon.pixmap(128,128));
        propertiesGrid->addWidget(icon, 0,0,3,1);
        // name
        nameLabel = new QLabel(this);
        nameLabel->setText(QString::fromStdString(Settings::applicationName));
        propertiesGrid->addWidget(nameLabel, 0,1, 1,2);
        // version
        versionLabel = new QLabel(this);
        versionLabel->setText(QString::fromStdString(
            (Settings::applicationVersion.getString()+" ("+Settings::buildSpecification+")  <a href='changelog'>Changelog</a>")
            ));
        propertiesGrid->addWidget(versionLabel, 1,1, 1,2);
        QObject::connect(versionLabel, &QLabel::linkActivated, this, &WindowAbout::labelClickedEvent);
        // message
        messageLabel = new QLabel(this);
        messageLabel->setText(QString::fromStdString( String(
            "Mailchat is designed to bring feedom to the users that we\n"
            "do not get from the messed up instant messenging industry."
            )));
        propertiesGrid->addWidget(messageLabel, 2,1, 1,2);
        /// @todo support/donation hint?
        // legal box
        legalBox = new QBoxLayout(QBoxLayout::LeftToRight);
        propertiesGrid->addLayout(legalBox, 3,0, 1,3);
            // license
            licenseLabel = new QLabel(this);
            licenseLabel->setText("<a href='license'>Licensing Information</a>");
            legalBox->addWidget(licenseLabel);
            legalBox->setAlignment(licenseLabel, Qt::AlignHCenter);
            QObject::connect(licenseLabel, &QLabel::linkActivated, this, &WindowAbout::labelClickedEvent);
            // privacy policy
            privacyPolicyLabel = new QLabel(this);
            privacyPolicyLabel->setText("<a href='privacy policy'>Privacy Policy</a>");
            legalBox->addWidget(privacyPolicyLabel);
            legalBox->setAlignment(privacyPolicyLabel, Qt::AlignHCenter);
            QObject::connect(privacyPolicyLabel, &QLabel::linkActivated, this, &WindowAbout::labelClickedEvent);

    show();
}

WindowAbout::~WindowAbout()  {

}

void WindowAbout::labelClickedEvent(QString uri)  {
    if(uri == "changelog")
        new WindowDocumentViewer(resChangelog,
                                 WebView::WebDocumentType::MARKDOWN,
                                 Settings::applicationName+" Changelog");
    else if(uri == "license")
        new WindowDocumentViewer(resCopying,
                                 WebView::WebDocumentType::PLAIN_TEXT,
                                 Settings::applicationName+" License");
    else if(uri == "privacy policy")
        new WindowDocumentViewer(resPrivacyPolicy,
                                 WebView::WebDocumentType::MARKDOWN,
                                 Settings::applicationName+" Privacy Policy");
    else
        throw Exception("unknown uri: "+uri.toStdString());
}
