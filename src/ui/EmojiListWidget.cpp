/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "EmojiListWidget.hpp"
#include <QLabel>
#include <QToolButton>
#include <QScrollBar>
#include "src/util/Emoji.hpp"
#include <QIcon>
#include <QStyle>
#include <iostream>
#include <QAction>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneHoverEvent>
#include "src/util/tictoc.hpp"

extern QIcon applicationIcon;
extern QFont emojiFont;

const size_t EmojiListWidget::emoji_size = 35;
const size_t EmojiListWidget::n_columns = 10;
QGraphicsScene *EmojiListWidget::graphicsScene=nullptr;
std::unordered_map<String, GraphicsEmojiItem*> EmojiListWidget::graphicsItems{};


class GraphicsEmojiItem  :  public QGraphicsItem  {
    private:
        EmojiListWidget *parent;
        String emoji;
        bool highlighted;
    public:
        GraphicsEmojiItem(String _emoji, EmojiListWidget *_parent)
            : QGraphicsItem(nullptr),
              parent(_parent),
              emoji(_emoji),
              highlighted(false)  {
            setAcceptHoverEvents(true);
        }
    public:
        virtual void mousePressEvent(QGraphicsSceneMouseEvent *event)  override  {
            parent->graphicsEmojiItemClickedEvent(this);
            event->accept();
        }
        virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event)  override  {
            highlighted = true;
            update();
            event->accept();
        }
        virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event)  override  {
            highlighted = false;
            update();
            event->accept();
        }
        virtual QRectF boundingRect()  const override  {
            return QRectF(0,0,EmojiListWidget::emoji_size, EmojiListWidget::emoji_size);
        }
        virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)  override {
            if(highlighted)  {
                painter->setPen(Qt::lightGray);
                painter->setBrush(Qt::lightGray);
                painter->drawRoundedRect(boundingRect(),20,20,Qt::SizeMode::RelativeSize);
            }
            QFont font = emojiFont;
            font.setPixelSize(EmojiListWidget::emoji_size*0.65);  /// @todo magic number
            painter->setFont(font);
            painter->drawText(boundingRect(), Qt::AlignCenter, QString::fromStdString(emoji));
        }

        String getEmoji()  const  {
            return emoji;
        }
};


EmojiListWidget::EmojiListWidget(QWidget *parent)
    : QWidget(parent)  {
    setContentsMargins(0,0,0,0);
        // container
        container = new QBoxLayout(QBoxLayout::TopToBottom, this);
        container->setContentsMargins(0,0,0,0);
        container->setSpacing(0);
        setLayout(container);
            // search input
            searchInput = new QLineEdit(this);
            searchInput->setPlaceholderText(tr("Search"));
            searchInput->setClearButtonEnabled(true);
            /// @todo search icon
            //searchInput->addAction(QIcon::fromTheme("system-search"), QLineEdit::LeadingPosition);
            //searchInput->addAction(searchInput->style()->standardIcon(QStyle::)))
            QObject::connect(searchInput, &QLineEdit::textChanged, this, &EmojiListWidget::searchInputEditedEvent);
            container->addWidget(searchInput);
            // graphics scene
            if(graphicsScene == nullptr)
                graphicsScene = new QGraphicsScene(0,0,n_columns*emoji_size,
                                                   emoji_size * (Emoji::emojis.size()+n_columns-1)/n_columns
                                                   );  /// @todo magic numbers
            // graphics view
            graphicsView = new QGraphicsView();
            graphicsView->setScene(graphicsScene);
            graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
            graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOn);
            int naturalWidth = graphicsView->sizeHint().width() + graphicsView->verticalScrollBar()->sizeHint().width();
            graphicsView->setMinimumSize(naturalWidth, naturalWidth);  /// @todo respect n_rows
            graphicsView->setMaximumSize(naturalWidth, naturalWidth);
            graphicsView->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

            graphicsView->setMouseTracking(true);

            container->addWidget(graphicsView);

    // call event handlers once to update UI
    searchInputEditedEvent("");
    /// @todo this does not work during the constructor but in searchInputEditEvent: graphicsView->verticalScrollBar()->setValue(0);
}

/// @todo bad performance when showing *all* emojis
void EmojiListWidget::searchInputEditedEvent(const QString &text)  {
    tictoc timer = tictoc::construct(__PRETTY_FUNCTION__,false);
        // clear existing scene
        //graphicsScene->clear();  DO NOT call QGraphicsScene::clear because that destroyes the children items
        for( auto &itr : graphicsItems)
            itr.second->hide();

        // get suggested emojis
        String partial = text.toStdString();
        std::unordered_map<std::string,std::string> emojis = Emoji::getSuggestions(partial);  /// @todo use @c String
        /// @todo sort emojis

        // show emojis
        int i=0;
        for(const auto &pair : emojis)  {
            const String &emoji = pair.second;
            int x = (i%n_columns) * emoji_size;  /// @todo magic numbers
            int y = (i/n_columns) * emoji_size;
            // fetch or create GraphicsSimpleTextItem
            auto itr = graphicsItems.find(emoji);
            if( itr == graphicsItems.end() )  {
                GraphicsEmojiItem *item = new GraphicsEmojiItem(emoji, this);
                graphicsScene->addItem(item);
                graphicsItems.insert(
                            std::pair(emoji,
                                      item
                            ));
                itr = graphicsItems.find(emoji);
                assert(itr != graphicsItems.end());
            }
            std::cout.flush();
            GraphicsEmojiItem *item = itr->second;
            // set graphic item's position
            item->setPos(x,y);
            item->show();
            i++;
        }

        // scroll to top
        graphicsView->setSceneRect(0,0, n_columns*emoji_size, emoji_size*((i+n_columns-1)/n_columns));
        graphicsView->setAlignment(Qt::AlignTop);
        graphicsView->verticalScrollBar()->setValue(0);
}
void EmojiListWidget::graphicsEmojiItemClickedEvent(const GraphicsEmojiItem *item)  {
    for(EmojiListWidgetObserver *observer : observers)
        observer->emojiListWidgetEvent_emojiSelected(this, item->getEmoji());
    QWidget *parent = parentWidget();
    assert(parent);
    parent->close();
}
