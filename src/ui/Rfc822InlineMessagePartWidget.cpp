/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "Rfc822InlineMessagePartWidget.hpp"
#include "src/messages/Rfc822InlineMessagePart.hpp"
#include "src/messages/EmailMessage.hpp"
#include <QBoxLayout>
#include <QFrame>
#include <QLabel>
#include <src/ui/MessageWidget.hpp>

#include "src/util/exceptions/Exception.hpp"

/// @todo The @c QFrame objects have a lot of copy-pasted code.
Rfc822InlineMessagePartWidget::Rfc822InlineMessagePartWidget(Rfc822InlineMessagePart *messagePart,
                                                             QWidget *parent)
        : InlineMessagePartWidget(parent)  {
    // container
    container = new QBoxLayout(QBoxLayout::Direction::TopToBottom, this);
    container->setContentsMargins(0,0,0,0);
    setLayout(container);
        // description box
        descriptionBox = new QBoxLayout(QBoxLayout::Direction::LeftToRight, this);
        container->addLayout(descriptionBox);
            // description left frame
            descriptionLeftFrame = new QFrame(this);
            descriptionLeftFrame->setFrameShape(QFrame::Shape::HLine);
            descriptionLeftFrame->setStyleSheet("color:#909090");
            descriptionLeftFrame->setMinimumWidth(10);  /// @todo magic number
            descriptionBox->addWidget(descriptionLeftFrame);
            // description label
            descriptionLabel = new QLabel(this);
            descriptionLabel->setStyleSheet("color:#909090");
            descriptionLabel->setTextFormat(Qt::TextFormat::RichText);
            String description = messagePart->getDescription().value_or("unnamed forwarded message").preview(100);
            descriptionLabel->setTextFormat(Qt::TextFormat::PlainText);
            descriptionLabel->setText(QString::fromStdString(description));
            descriptionBox->addWidget(descriptionLabel);
            // description right frame
            descriptionRightFrame = new QFrame(this);
            descriptionRightFrame->setFrameShape(QFrame::Shape::HLine);
            descriptionRightFrame->setStyleSheet("color:#909090");
            descriptionRightFrame->setMinimumWidth(10);  /// @todo magic number
            descriptionBox->addWidget(descriptionRightFrame);
        // message widget
        messageWidget = new MessageWidget(messagePart->getMessage(), this);
        messageWidget->layout()->setContentsMargins(0,0,0,0);
        container->addWidget(messageWidget);
        // bottom frame
        bottomFrame = new QFrame(this);
        bottomFrame->setFrameShape(QFrame::Shape::HLine);
        bottomFrame->setStyleSheet("color:#909090");
        bottomFrame->setMinimumWidth(10);  /// @todo magic number
        container->addWidget(bottomFrame);

}

void Rfc822InlineMessagePartWidget::messagePartEvent_contentChanged(MessagePart *messagePart)  {
    /// @todo implement
    NOT_YET_IMPLEMENTED();
}
