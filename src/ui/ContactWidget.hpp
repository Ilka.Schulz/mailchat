/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QWidget>
#include <QPushButton>
#include <QBoxLayout>
#include <QLabel>

#include <string>

#include "src/ui/ContactListWidget.hpp"
#include "src/ui/ContactWidgetObserver.hpp"
#include "src/patterns/Observable.hpp"
#include "src/Contact.hpp"
#include "src/ContactObserver.hpp"

/** @brief implements a GUI element to show a contact
 *  @details One @c Contact object can be shown in several ContactWidget instances.
 *  @todo The setName, setEmail, etc. functions are called directly by the Contact class. – make this a ContactObserver!
 */
class ContactWidget  :  public  QPushButton, protected ContactObserver, public Observable<ContactWidgetObserver>  {
    private:
            QBoxLayout *horizontalBox;   ///< container for picture (left), name+email (center) and options (right)
            QLabel *labelName;           ///< name label
            QLabel *labelEmail;          ///< email label
        /// the @c Contact object that is represented by this widget
        Contact *contact;

    protected:
        /** @brief event  handler for click on button
         */
        void clickedEvent();

    public:
        /** @brief constructor
         *  @param _parent  initializes @c ContactWidget::parent and is the widget to which this one is added
         *  @param _contact initializes @c ContactWidget::contact
         */
        ContactWidget(ContactListWidget *_parent, Contact *_contact);
        virtual ~ContactWidget();

    public:
        // event handlers of ContactObserver
        virtual void contactEvent_destroyedEvent(Contact *contact)  override;
        virtual void contactEvent_nameChangedEvent(Contact *contact)  override;
        virtual void contactEvent_emailChangedEvent(Contact *contact)  override;
        /// @details Calls to this method may delete the object! (if contact->isConfirmed==TristateBool::VAL_FALSE)
        virtual void contactEvent_confirmedChangedEvent(Contact *contact)  override;

        /// @returns @c ContactWidget::contact
        Contact *getAssociatedContact();
};
