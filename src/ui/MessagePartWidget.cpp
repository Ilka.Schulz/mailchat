/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "MessagePartWidget.hpp"
#include "src/util/exceptions/Exception.hpp"

#include "src/messages/MessagePart.hpp"
#include "src/messages/AttachmentMessagePart.hpp"
#include "src/messages/InlineMessagePart.hpp"
#include "src/messages/TextInlineMessagePart.hpp"
#include "src/messages/ImageInlineMessagePart.hpp"
#include "src/messages/Rfc822InlineMessagePart.hpp"
#include "src/messages/UnsupportedInlineMessagePart.hpp"

#include "src/ui/AttachmentMessagePartWidget.hpp"
#include "src/ui/ImageInlineMessagePartWidget.hpp"
#include "src/ui/TextInlineMessagePartWidget.hpp"
#include "src/ui/Rfc822InlineMessagePartWidget.hpp"
#include "src/ui/UnsupportedInlineMessagePartWidget.hpp"

#include <QBoxLayout>


MessagePartWidget::MessagePartWidget(MessagePart *_messagePart, QWidget *_parent)
        : QWidget(_parent),
          messagePart(_messagePart),
          rootContainer(new QBoxLayout(QBoxLayout::TopToBottom, this)),
          inlineMessagePartWidget(nullptr),
          attachmentMessagePartWidget(nullptr)  {
    // prepare root container
    setLayout(rootContainer);
    rootContainer->setContentsMargins(0,0,0,0);
    // manually trigger observer callback once to fetch the data to display once
    MessagePartWidget::messagePartEvent_dispositionChanged(messagePart);
    // observe displayed messagePart
    messagePart->addObserver(this);
}
MessagePartWidget::~MessagePartWidget()  {
    messagePart->removeObserver(this);
}

void MessagePartWidget::messagePartEvent_dispositionChanged(MessagePart *messagePart)  {
    // delete old widgets
    if(inlineMessagePartWidget != nullptr)  {
        delete inlineMessagePartWidget;
        inlineMessagePartWidget = nullptr;
    }
    if(attachmentMessagePartWidget != nullptr)  {
        delete attachmentMessagePartWidget;
        attachmentMessagePartWidget = nullptr;
    }
    // construct new widget
    if(TextInlineMessagePart *textInlineMessagePart = dynamic_cast<TextInlineMessagePart*>(messagePart))  {
        inlineMessagePartWidget = new TextInlineMessagePartWidget(textInlineMessagePart, this);
        rootContainer->addWidget(inlineMessagePartWidget);
    }
    else if(ImageInlineMessagePart *imageInlineMessagePart = dynamic_cast<ImageInlineMessagePart*>(messagePart))  {
        inlineMessagePartWidget = new ImageInlineMessagePartWidget(imageInlineMessagePart, this);
        rootContainer->addWidget(inlineMessagePartWidget);
    }
    else if(Rfc822InlineMessagePart *rfc822InlineMessagePart = dynamic_cast<Rfc822InlineMessagePart*>(messagePart))  {
        inlineMessagePartWidget = new Rfc822InlineMessagePartWidget(rfc822InlineMessagePart,this);
        rootContainer->addWidget(inlineMessagePartWidget);
    }
    else if(UnsupportedInlineMessagePart *unsupportedInlineMessagePart = dynamic_cast<UnsupportedInlineMessagePart*>(messagePart))  {
        inlineMessagePartWidget = new UnsupportedInlineMessagePartWidget(unsupportedInlineMessagePart, this);
        rootContainer->addWidget(inlineMessagePartWidget);
    }
    else if(AttachmentMessagePart *attachmentMessagePart = dynamic_cast<AttachmentMessagePart*>(messagePart))  {
        attachmentMessagePartWidget = new AttachmentMessagePartWidget(attachmentMessagePart);
        rootContainer->addWidget(attachmentMessagePartWidget);
    }
    else
        SHOULD_BE_UNREACHABLE("unknown message part class");
}
void MessagePartWidget::messagePartEvent_contentChanged(MessagePart *messagePart)  {
    if(inlineMessagePartWidget != nullptr)
        inlineMessagePartWidget->messagePartEvent_contentChanged(messagePart);
    if(attachmentMessagePartWidget != nullptr)
        attachmentMessagePartWidget->messagePartEvent_contentChanged(messagePart);
}
void MessagePartWidget::messagePartEvent_deleted(MessagePart *messagePart)  {
    assert(messagePart == this->messagePart);
    delete this;
}
