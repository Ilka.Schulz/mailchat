/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "ChatWidget.hpp"
#include "src/Contact.hpp"
#include <iostream>
#include "src/util/exceptions/Exception.hpp"
#include "src/util/exceptions/macros.hpp"
#include "src/messages/Message.hpp"
#include "src/Account.hpp"
#include "src/connections/Connection.hpp"
#include "src/util/Emoji.hpp"
#include "src/ui/EmojiListWidget.hpp"
#include <QWidgetAction>
#include <QCompleter>
#include <QStringListModel>
#include <QStandardItemModel>
#include <QTreeView>
#include <QFontDatabase>
#include <QMessageBox>
#include <QScrollBar>
#include <QResizeEvent>
#include "src/util/exceptions/SmtpSendCurlJobFailedException.hpp"
#include "src/util/exceptions/ImapSendCurlJobFailedException.hpp"
#include "src/ui/ComposeWindow.hpp"
#include "src/util/gpgme.hpp"
#include "src/ui/WindowDocumentViewer.hpp"
#include "src/resources.hpp"
#include "src/ui/WindowDetectPgpPubkeys.hpp"

extern QIcon applicationIcon;
extern QFont emojiFont;

ChatWidget::ChatWidget(QWidget *_parentContainer, Contact *_contact)
        : QWidget(_parentContainer), messageCache(_contact), contact(_contact)  {
    setContentsMargins(0,0,0,0);
    // vertical box
    verticalBox = new QBoxLayout(QBoxLayout::TopToBottom, this);
    verticalBox->setContentsMargins(0,0,0,0);
    verticalBox->setSpacing(0);
    setLayout(verticalBox);
        /// @todo header info: picture, settings, action buttons
        // message scrolled window
        messageScrolledWindow = new QScrollArea(this);
        messageScrolledWindow->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
        messageScrolledWindow->setMinimumWidth(250);  /// @todo magic number
        messageScrolledWindow->setFrameShape(QFrame::NoFrame);
        messageScrolledWindow->setWidgetResizable(true);
        messageScrolledWindow->setContentsMargins(0,0,0,0);
        messageScrolledWindowWidget = new QWidget(this);
        messageScrolledWindowWidget->setContentsMargins(0,0,0,0);
        messageScrolledWindow->setWidget(messageScrolledWindowWidget);
        verticalBox->addWidget(messageScrolledWindow);
            // message container
            messageContainer = new QBoxLayout(QBoxLayout::TopToBottom, this);
            //messageContainer->setContentsMargins(0,0,0,0);
            messageContainer->setSpacing(20);  /// @todo magic number
            messageContainer->setAlignment(Qt::AlignTop);
            messageScrolledWindowWidget->setLayout(messageContainer);
                // confirm button container
                if(contact->isConfirmed())
                    confirmButtonContainer = nullptr;
                else  {
                    confirmButtonContainer = new QBoxLayout(QBoxLayout::LeftToRight, this);
                    confirmButtonContainer->setAlignment(Qt::AlignHCenter);
                    messageContainer->addLayout(confirmButtonContainer);
                        // confirm button
                        confirmContactButton = new QPushButton(this);
                        confirmContactButton->setText(tr("Confirm"));
                        QObject::connect(confirmContactButton, &QPushButton::clicked,
                                         this, &ChatWidget::confirmContactButtonClickedEvent);
                        confirmButtonContainer->addWidget(confirmContactButton);
                        // block button
                        blockContactButton = new QPushButton(this);
                        blockContactButton->setText(tr("Block"));
                        connect(blockContactButton, &QPushButton::clicked, this, &ChatWidget::blockContactButtonClickedEvent);
                        confirmButtonContainer->addWidget(blockContactButton);
                }
        // compose tools container
        composeGridLayout = new QGridLayout(this);
        composeGridLayout->setContentsMargins(8,8,8,8);  /// @todo magic numbers
        composeGridLayout->setHorizontalSpacing(8);  /// @todo magic number
        composeGridLayout->setVerticalSpacing(0);    /// @todo magic number
        verticalBox->addLayout(composeGridLayout);
            QFont font = emojiFont;
            font.setPointSize(15);  /// @todo magic number

            // compose PGP warning Label
            /// @todo also add such a label to @c ComposeWindow
            composePgpWarningLabel = new QLabel(this);
            composePgpWarningLabel->setTextFormat(Qt::TextFormat::RichText);
            composePgpWarningLabel->setText(QString::fromStdString(Emoji::convertEmoji(":warning:")) + " " +
                                            tr("This contact does not support end-to-end encryption.") + " " +
                                            "<a href='detect-pubkey'>search for pubkey online</a>" + " - " +
                                            "<a href='help'>help</a>"
                                           );
            composePgpWarningLabel->setWordWrap(true);  /// @todo QLabel::setWordWrap has issues with sizeHint ?
            composePgpWarningLabel->setStyleSheet("QLabel {background:yellow; padding:10px}");  /// @todo magic number
            try {
                for(const Gpgme::Key &key : Gpgme::Key::getByEmail(contact->getEmail()))
                    if(key.isHealthy())  {
                        composePgpWarningLabel->setVisible(false);
                        break;
                    }
            }
            catch(Gpgme::GpgmeKeyNotFoundException&)  {
            }
            connect(composePgpWarningLabel, &QLabel::linkActivated,
                    this, &ChatWidget::composePgpWarningLabelLinkActivatedEvent);
            composeGridLayout->addWidget(composePgpWarningLabel,0,0,1,4);

            // compose input
            composeInput = new ComposeEdit(this);
            composeInput->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
            composeGridLayout->addWidget(composeInput,1,0);

            // compose window button
            composeWindowButton = new QPushButton(this);
            composeWindowButton->setText(QString::fromStdString(Emoji::convertEmoji(":scroll:")));

            composeWindowButton->setFont(font);
            int iconButtonWidth = composeWindowButton->minimumSizeHint().height() * 1.2;  /// @todo magic number

            connect(composeWindowButton, &QPushButton::clicked, this, [this](bool){
                new ComposeWindow(contact->getAccount(), contact);
            });
            composeGridLayout->addWidget(composeWindowButton, 1,1);

            // compose emoticon button
            composeEmoticonButton = new QToolButton(this);
            composeEmoticonButton->setText(QString::fromStdString(Emoji::convertEmoji(":slightly_smiling_face:")));
            composeEmoticonButton->setPopupMode(QToolButton::InstantPopup);
                QWidgetAction *composeEmoticonButtonWidgetAction = new QWidgetAction(this);
                    composeEmoticonButtonEmojiListWidget = new EmojiListWidget(this);
                    composeEmoticonButtonEmojiListWidget->addObserver(this);
                    composeEmoticonButtonWidgetAction->setDefaultWidget(composeEmoticonButtonEmojiListWidget);
                composeEmoticonButton->addAction(composeEmoticonButtonWidgetAction);
            composeGridLayout->addWidget(composeEmoticonButton, 1,2);

            // compose send button
            composeSendButton = new QToolButton(this);
            composeSendButton->setText(QString::fromStdString(Emoji::convertEmoji(":arrow_right:")));
            composeGridLayout->addWidget(composeSendButton, 1,3);
            QObject::connect(composeSendButton, &QPushButton::clicked, this, &ChatWidget::composeSendButtonClickedEvent);

            /// @todo sort this code

            composeEmoticonButton->setFont(font);
            composeEmoticonButton->setMinimumSize(iconButtonWidth, iconButtonWidth);
            composeEmoticonButton->setMaximumSize(iconButtonWidth, iconButtonWidth);
            //composeEmoticonButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

            composeSendButton->setFont(font);
            composeSendButton->setMinimumSize(iconButtonWidth, iconButtonWidth);
            composeSendButton->setMaximumSize(iconButtonWidth, iconButtonWidth);

            composeWindowButton->setFont(font);
            composeWindowButton->setMinimumSize(iconButtonWidth, iconButtonWidth);
            composeWindowButton->setMaximumSize(iconButtonWidth, iconButtonWidth);

            //composeInput->setMaximumHeight(iconButtonWidth);

    // get messages from message cache
    int pos=0;
    for(Message *message : messageCache.getMessages())  {
        ChatWidget::messageCacheEvent_messageAdded(&messageCache, pos, message);
        pos++;
    }
    messageCache.addObserver(this);

    // observe contact
    //ChatWidget::contactEvent_confirmedChangedEvent(contact);  ///< not needed because the above code already checks
    // the condition
    contact->addObserver(this);



    /// @todo get this to work
    /*messageScrolledWindow->verticalScrollBar()->setValue(
                //100//messageScrolledWindowWidget->sizeHint().height()
                messageScrolledWindow->verticalScrollBar()->maximum()
                );
    repaint();*/
}
ChatWidget::~ChatWidget()  {
    messageCache.removeObserver(this);
    contact->removeObserver(this);
    composeEmoticonButtonEmojiListWidget->removeObserver(this);
    for(MessageWidget *messageWidget : messageWidgets)
        delete messageWidget;
}


void ChatWidget::resizeEvent(QResizeEvent *event)  {
    // resize width of message widgets
    for(MessageWidget *messageWidget : messageWidgets)  {
        messageWidget->setMaximumWidth(
                    ( contentsRect().width()
                    -  messageScrolledWindow->verticalScrollBar()->size().width())  /// @todo do not substract if invisible
                    * 80/100  /// @todo magic number
                    );
        /// @todo stupid @c QWidget will always take minimumWidget if its contents is only labels with wordWrap==true (plain text message), also see PlainTextInlineMessagePartWidget
        messageWidget->setMinimumWidth(
                    std::min(
                        messageWidget->maximumWidth(),
                        std::max(
                            250,  /// @todo magic number
                            messageWidget->maximumWidth()*1/2  /// @todo magic number
                        )
                    )
        );
        messageContainer->setAlignment(messageWidget,
                                       messageWidget->getAssociatedMessage()->hasFlag(Message::FLAG_OUTGOING) ?
                                       Qt::AlignmentFlag::AlignRight :
                                       Qt::AlignmentFlag::AlignLeft
                                      );
    }
}


void ChatWidget::confirmContactButtonClickedEvent()  {
    contact->setConfirmed(true);
}
void ChatWidget::blockContactButtonClickedEvent()  {
    contact->setConfirmed(false);
}

void ChatWidget::composePgpWarningLabelLinkActivatedEvent(const QString &link)  {
    if(link == "detect-pubkey")  {
        new WindowDetectPgpPubkeys(contact->getEmail());
    }
    else if(link == "help")  {
        new WindowDocumentViewer(resPgpHelp, WebView::WebDocumentType::MARKDOWN, "PGP Help");
    }
    else
        ERROR(std::logic_error, QString("unknown link: >"+link+"<").toStdString());
}

void ChatWidget::composeSendButtonClickedEvent()  {
    if(contact == nullptr)
        throw Exception("no contact selected");

    std::vector<Connection*> connections = contact->getAccount()->getConnections();
    if(connections.empty())
        throw Exception("account has no connections set up");  /// @todo allow user to create a connection

    Message *message = connections[0]->composeMessage(
                /// @todo maybe use toHtml()
                composeInput->toPlainText().toStdString(),
                contact
                );  ///< @todo correct time zone

    bool clearInput=true;  // whether to clean the ComposeEdit after sending the message
    composeSendButton->setEnabled(false);
    composeInput->setEnabled(false);
    try {
        connections[0]->sendMessage(message,true);  /// @todo see #20: smarter selection which connection to use
    }
    catch(SmtpSendCurlJobFailedException &e)  {
        // If this happens, the message has not been sent over SMTP nor has it been stored to the IMAP "Sent" folder.
        // Just delete the message object, keep the compose edit and warn the user.
        delete message;  /// @todo somehow, this creates two new message widgets after restarting the application – look into how this is written to disk...
        QMessageBox *box = new QMessageBox();
        box->setText(tr("The message could not be sent. Retry or check your internet connection.")+"\n\n"+
                     tr("Further information:")+"\n"+e.what());
        box->setIcon(QMessageBox::Warning);
        box->setAttribute(Qt::WA_DeleteOnClose);
        box->open();
        clearInput = false;
    }
    catch(ImapSendCurlJobFailedException &e)  {
        // If this happens, the message has been sent over SMTP but could be saved to the "Sent" IMAP folder.
        // The message will stay existing (and be stored to disk!) but the Message::FLAG_SYNCHRONIZED will not be set
        /// @todo impelement processor to retry storing unsynchronized mails to IMAP "Sent" folder
        /// @todo maybe inform the user?
    }
    catch (Exception &e) {
        QMessageBox *box = new QMessageBox();
        box->setText(QString::fromStdString("could not send message:\n\n"+String(e.what())));
        box->setIcon(QMessageBox::Critical);
        box->setAttribute(Qt::WA_DeleteOnClose);
        box->open();
        clearInput = false;
    }
    if(clearInput)
        composeInput->clear();
    composeSendButton->setEnabled(true);
    composeInput->setEnabled(true);
}

void ChatWidget::messageCacheEvent_messageAdded(MessageCache *messageCache, int pos, Message *message)  {
    assert(messageCache == &this->messageCache);

    // create widget
    MessageWidget *messageWidget = new MessageWidget(message, this);

    // add to container
    //bool scroll = true; /// @todo
    messageContainer->insertWidget(pos,messageWidget);
    messageScrolledWindow->verticalScrollBar()->setMinimum(0);
    messageScrolledWindow->verticalScrollBar()->setMaximum(100);
    /*if(scroll)
        messageScrolledWindow->verticalScrollBar()->setValue(
                    //100//messageScrolledWindowWidget->sizeHint().height()
                    messageScrolledWindow->verticalScrollBar()->maximum()
                    );*/
    //messageScrolledWindow->ensureVisible(messageWidget->pos().x(), messageWidget->pos().y());
    /*messageWidget->ensurePolished();
    messageScrolledWindow->ensurePolished();
    messageScrolledWindow->ensureWidgetVisible(messageWidget);*/

    // register
    auto itr = messageWidgets.begin();
    std::advance(itr, pos);
    messageWidgets.insert(itr, messageWidget);
}
void ChatWidget::messageCacheEvent_messageRemoved(MessageCache *messageCache, Message *message)  {
    if(messageCache != &this->messageCache)
        throw Exception("received event from unobserved message cache object");
    for(MessageWidget *messageWidget : messageWidgets)
        if(messageWidget->getAssociatedMessage() == message)
            delete messageWidget;
}

void ChatWidget::contactEvent_confirmedChangedEvent(Contact *contact)  {
    if(contact != this->contact)
        throw Exception("received event from unobserved contact object");
    if(contact->isConfirmed() != TristateBool::VAL_UNKNOWN)
        if(confirmButtonContainer != nullptr)  {
            delete confirmButtonContainer;
            confirmButtonContainer = nullptr;
        }
}

void ChatWidget::emojiListWidgetEvent_emojiSelected(EmojiListWidget *emojiListWidget, String emoji)  {
    if(emojiListWidget != composeEmoticonButtonEmojiListWidget)
        throw Exception("received event from object that was not observed");
    composeInput->insertPlainText(QString::fromStdString(emoji));
}

void ChatWidget::gpgEngineProcessorEvent_keyAdded(Gpgme::Key &key)  {
    if(String(key.getUserIds()->email).equals(contact->getEmail(),CASE_INSENSITIVE))
        composePgpWarningLabel->setVisible(false);
}
void ChatWidget::gpgEngineProcessorEvent_keyRemoved(const Gpgme::Key &key)  {
    if(String(key.getUserIds()->email).equals(contact->getEmail(),CASE_INSENSITIVE))
        composePgpWarningLabel->setVisible(true);
}

Contact *ChatWidget::getContact()  {
    return contact;
}
