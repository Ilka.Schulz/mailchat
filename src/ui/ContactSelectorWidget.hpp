/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <QComboBox>
#include "src/patterns/Observable.hpp"
#include "src/AccountObserver.hpp"
#include "src/ContactObserver.hpp"
#include "src/util/String.hpp"

class Account;
class Contact;
class ContactSelectorWidget;

class ContactSelectorWidgetObserver  {
    protected:
        virtual void contactSelectorWidgetEvent_contactSelected(ContactSelectorWidget *widget, Contact *contact);

    friend class ContactSelectorWidget;  // observed class muss call protected handler
};

class ContactSelectorWidget  :  public QComboBox,
                                public Observable<ContactSelectorWidgetObserver>,
                                protected AccountObserver,
                                protected ContactObserver  {
    private:
        Account *account;
    public:
        ContactSelectorWidget(Account *_account, QWidget *parent=nullptr);
        ~ContactSelectorWidget();

    private:
        String contactToString(Contact *contact)  const;

    public:
        void setAccount(Account *account);
        void setContact(Contact *contact);

        Account *getAccount()  const;
        Contact *getSelectedContact()  const;

    // event handlers
    protected:
        // Account
        virtual void accountEvent_contactAdded(Account *account, Contact *contact)  override;
        virtual void accountEvent_contactRemoved(Account *account, Contact *contact)  override;
        virtual void accountEvent_destructed(Account *account)  override;

        // Contact
        virtual void contactEvent_destroyedEvent(Contact *contact)  override;
        virtual void contactEvent_nameChangedEvent(Contact *contact)  override;
        virtual void contactEvent_emailChangedEvent(Contact *contact)  override;
        /// @todo confirmed changed

    friend int src_ui_ContactSelectorWidget(int,char**);  // tests
};
