/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/connections/CurlJob.hpp"
#include "src/ui/WindowGenericDialog.hpp"
#include "src/util/String.hpp"

class QTreeWidget;
class WindowDetectPgpPubkeys;


class WindowDetectPgpPubkeysCurlJob  :  public CurlJob  {
    private:
        WindowDetectPgpPubkeys *window;
    public:
        WindowDetectPgpPubkeysCurlJob(WindowDetectPgpPubkeys *_window, CURL *_curl, bool _deleteWhenFinished);
        WindowDetectPgpPubkeys *getWindow()  const;
};


class WindowDetectPgpPubkeys  :  public WindowGenericDialog  {
    private:
        QGridLayout *gridLayout;
            QTreeWidget *treeWidget;

        const String email;
    public:
        WindowDetectPgpPubkeys(const String &_email);

        static void staticIndexFinishedCallback(CurlJob *curlJob);
        static void staticGetFinishedCallback(CurlJob *curlJob);

    protected:
        virtual void okButtonClickedEvent()  override;
};
