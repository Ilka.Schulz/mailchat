/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QWidget>
#include <QBoxLayout>
#include <QToolButton>
#include <QLabel>
#include <string>

class MessageWidget;
class Message;
class ChatWidget;
class Contact;
class MessagePart;
class MessagePartWidget;

#include "src/messages/MessageObserver.hpp"
#include "src/ContactObserver.hpp"
#include "src/messages/MessagePartObserver.hpp"

#include <map>

/** @brief a widget that shows the content and meta data of a single @c Message
 */
class MessageWidget  :  public QWidget,
                        public MessageObserver,
                        public ContactObserver,
                        public MessagePartObserver  {
    private:
        size_t ignoreLeaveEventCount;  ///< number of QWidget::leaveEvent calls that shall be ignored in the future

        QBoxLayout *horizontalBox;   ///< container for message bubble and buttons
            QWidget *messageBoxWidget;
                QBoxLayout *messageBox;      ///< root container for message bubble
                    QLabel *senderLabel;     ///< label showing sender name
                    QBoxLayout *contentBox;   ///< box for text and attachments (and possibly other content)
                        QBoxLayout *inlineContentBox;
                        QBoxLayout *attachmentContentBox;
                        std::map<MessagePart*,MessagePartWidget*> messagePartsAndWidgets;  ///< widgets showing all parts of the @c Message
                //QLabel *messageLabel;    ///< label showing the message content
                QLabel *timeStampLabel;  ///< label showing the time stamp (human-readable, of course)
            QWidget *buttonsWidget;
                QBoxLayout *buttonsBox;      ///< container for buttons
                    QToolButton *replyButton;
                    QToolButton *optionsButton;

        Message *message;  ///< the @c Message object which's content is displayed in this widget object

    public:
        /** @brief constructor
         * @param _message initializes @c MessageWidget::message
         * @param _parent  initializes @c MessageWidget::parent
         */
        MessageWidget(Message *_message, QWidget *_parent=nullptr);
        ~MessageWidget();

        /// @returns @c MessageWidget::message
        Message *getAssociatedMessage();


    protected:
        // GUI event handlers
        virtual void enterEvent(QEvent *event)  override;
        virtual void leaveEvent(QEvent *event)  override;


        // event handlers of MessageObserver
        // btw: constructing and deleting widgets is done by @c ChatWidget class
        virtual void messageEvent_messagePartAddedEvent(Message *message, MessagePart *messagePart)  override;
        virtual void messageEvent_timeStampChangedEvent(Message *message)  override;
        virtual void messageEvent_messageIdChangedEvent(Message *message)  override;

        // event handlers of MessagePart
        virtual void messagePartEvent_dispositionChanged(MessagePart *messagePart)  override;
        // no need to observe contentChanged() because MessagePartWidget does that already
        virtual void messagePartEvent_deleted(MessagePart *messagePart)  override;

        // event handlers of ContactObserver
        virtual void contactEvent_nameChangedEvent(Contact *contact) override;
};
