/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "UnsupportedInlineMessagePartWidget.hpp"
#include <QBoxLayout>
#include <QLabel>
#include "src/settings.hpp"
#include "src/util/String.hpp"
#include "src/util/exceptions/Exception.hpp"

UnsupportedInlineMessagePartWidget::UnsupportedInlineMessagePartWidget(UnsupportedInlineMessagePart *messagePart,
                                                                       QWidget *parent)
        : InlineMessagePartWidget(parent)  {
    // container
    container = new QBoxLayout(QBoxLayout::Direction::TopToBottom, this);
    container->setContentsMargins(0,0,0,0);
    setLayout(container);
        // label
        label = new QLabel(this);
        QFont font = label->font();
        font.setItalic(true);
        label->setFont(font);
        label->setWordWrap(true);  /// @todo label's sizeHint() function is broken
        // this is a workaround for the size hint
        label->setMinimumSize(500,100);
        switch(messagePart->getCause())  {
            case UnsupportedInlineMessagePart::Cause::CONTENT_TYPE:
                    label->setText(QString::fromStdString(
                            "This message part is not understood by "+Settings::applicationName+". " +
                            "Maybe it is some proprietory format (\"Content-Type\" specification is \"" +
                            String(messagePart->getContentType()).preview(50,"...").toStdString()+"\")."
                    ));
                break;
            case UnsupportedInlineMessagePart::Cause::EXCEPTION:
                    label->setText(QString::fromStdString(
                            "This message part is not understood by "+Settings::applicationName+". " +
                            "An exception with the following message has been raised: >" +
                            messagePart->getExceptionWhat()+"<"
                    ));
                break;
            case UnsupportedInlineMessagePart::Cause::BAD_MULTIPART_ALTERNATIVE:
                    label->setText(QString::fromStdString(
                            "This message part is not understood by "+Settings::applicationName+". " +
                            "It is a \"multipart/alternative\" type which has not a single properly formed child."
                    ));
                break;
            case UnsupportedInlineMessagePart::Cause::SECURITY:
                    label->setText(QString::fromStdString(
                            "This message part is intentionally not displayed for security reasons."
                    ));
                break;

        }
        container->addWidget(label);
}

void UnsupportedInlineMessagePartWidget::messagePartEvent_contentChanged(MessagePart *messagePart)  {
    // do nothing
}
