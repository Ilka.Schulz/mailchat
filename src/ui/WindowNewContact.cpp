/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/ui/WindowNewContact.hpp"
#include "src/Contact.hpp"
#include "src/ui/AccountSelectorWidget.hpp"

WindowNewContact::WindowNewContact(Account *account)
        : WindowGenericDialog("New Contact", 600, 400)  {
    // properties grid
    propertiesGrid = new QGridLayout(this);
    propertiesGrid->layout()->setSpacing(4);  /// @todo magic number
    getContainer()->setLayout(propertiesGrid);
    propertiesGrid->setAlignment(Qt::AlignTop);
        // account
            // label
            accountLabel = new QLabel(this);
            accountLabel->setText(tr("Account:"));
            propertiesGrid->addWidget(accountLabel, 0,0, 1,1);
            // account selector widget
            accountSelectorWidget = new AccountSelectorWidget(this);
            if(account!=nullptr)
                accountSelectorWidget->selectAccount(account);
            propertiesGrid->addWidget(accountSelectorWidget, 0,1, 1,1);
        // name
            // label
            nameLabel = new QLabel(this);
            nameLabel->setText(tr("Name:"));
            propertiesGrid->addWidget(nameLabel, 1,0, 1,1);
            // input
            nameInput = new QLineEdit(this);
            propertiesGrid->addWidget(nameInput, 1,1, 1,1);
        // email
            // label
            emailLabel = new QLabel(this);
            emailLabel->setText(tr("Email address:"));
            propertiesGrid->addWidget(emailLabel, 2,0, 1,1);
            // input
            emailInput = new QLineEdit(this);
            propertiesGrid->addWidget(emailInput, 2,1, 1,1);
    setTabOrder(accountSelectorWidget, nameInput);
    setTabOrder(nameInput, emailInput);
    show();
    nameInput->setFocus();
}

void WindowNewContact::okButtonClickedEvent()  {
    new Contact(0,
                accountSelectorWidget->getSelectedAccount(),
                nameInput->text().toStdString(),
                emailInput->text().toStdString(),
                TristateBool::VAL_TRUE);  // if the user explicitly creates this contact, it is
                                          // confirmed already
    delete this;
}
