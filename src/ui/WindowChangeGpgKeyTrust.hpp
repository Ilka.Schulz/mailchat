/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

class QFormLayout;
class QLabel;
class QComboBox;


#include "src/ui/WindowGenericDialog.hpp"
#include "src/util/gpgme.hpp"
#include <map>

class WindowChangeGpgKeyTrust  :  public WindowGenericDialog  {
    private:
        QFormLayout *formLayout;
            QLabel *keyLabel;  ///< @todo key selection combobox
            QLabel *currentTrustLabel;
            QLabel *trustLabel;
            QComboBox *trustInput;
    public:
        WindowChangeGpgKeyTrust(const Gpgme::Key &key);

    protected:
        // event handlers
        virtual void okButtonClickedEvent()  override;
};
