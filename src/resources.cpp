/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "resources.hpp"

const String resCopying((const char*)&_binary_COPYING_start,
                        &_binary_COPYING_end  - &_binary_COPYING_start);
const String resChangelog((const char*)&_binary_CHANGELOG_md_start,
                          &_binary_CHANGELOG_md_end - &_binary_CHANGELOG_md_start);
const String resPrivacyPolicy((const char*)&_binary_PRIVACY_POLICY_md_start,
                              &_binary_PRIVACY_POLICY_md_end - &_binary_PRIVACY_POLICY_md_start);

const String resMessageFormats((const char*)&_binary_doc_message_formats_md_start,
                               &_binary_doc_message_formats_md_end - &_binary_doc_message_formats_md_start);

const String resPgpHelp((const char*)&_binary_doc_pgp_help_md_start,
                        &_binary_doc_pgp_help_md_end - &_binary_doc_pgp_help_md_start);
