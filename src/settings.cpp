/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "settings.hpp"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "src/util/exceptions/MissingPropertyException.hpp"

#include <QIcon>
#include <QFont>
QIcon applicationIcon;  /// @todo move to reasonable location
QFont emojiFont;  /// @todo move to reasonable location

bool Settings::dryRun=false;

Version Settings::applicationVersion(0);

const String Settings::getBaseDirectory()  {
    #if defined(__linux__)
        const char *homedir;
        if ((homedir = getenv("HOME")) == NULL)
            homedir = getpwuid(getuid())->pw_dir;
        String retval = homedir;
        if(retval[retval.size()-1] != directorySeparator)
            retval += directorySeparator;
        return retval + ".mailchat"+directorySeparator;
    #elif defined(_WIN32)
        const String key = "APPDATA";
        String retval = getenv(key.c_str());
        if(retval.empty())
            throw MissingPropertyException(key);
        if(retval[retval.size()-1] != directorySeparator)
            retval += directorySeparator;
        return retval + "mailchat"+directorySeparator;
    #elif defined(__APPLE__)
        #error "not yet defined"
    #elif defined(BSD)
        #error "not yet defined"
    #elif defined(__QNX__)
        #error "not yet defined"
    #else
        #error "unknown operating system"
    #endif
}
