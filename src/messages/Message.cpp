/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Message.hpp"
#include "src/Account.hpp"
#include "src/Contact.hpp"
#include "src/messages/MessageCache.hpp"
#include "src/settings.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/time.hpp"
#include "src/messages/MessagePart.hpp"

#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <cassert>


const String Message::propertyKeyFlags        = "Message::flags";
const String Message::propertyKeyId           = "Message::id";
const String Message::propertyKeyMessageId    = "Message::messageId";
const String Message::propertyKeyTimeStamp    = "Message::timeStamp";
const String Message::propertyKeyContactEmail = "Message::contactEmail";

const Message::MessageFlags Message::FLAG_OUTGOING     = 0b0001;
const Message::MessageFlags Message::FLAG_SYNCHRONIZED = 0x0002;
const Message::MessageFlags Message::FLAG_SHALLOW      = 0x0004;
const Message::MessageFlags Message::FLAG_SHADOW       = 0x0008;
std::vector<Message*> Message::allMessages;

Message::Message(MessageId _id, MessageFlags _flags, String _rawFullMessage, Contact *_contact,
                 std::time_t _timeStamp, String _messageId)
        :  id(_id==0 ? generateNewMessageId() : _id), flags(_flags), rawFullMessage(_rawFullMessage), contact(_contact),
           timeStamp(_timeStamp), messageId(_messageId)  {
    if(_id == 0)
        addToIndex();
    // save
    saveToFile();  /// @todo call saveToFile in all necessary locations
    // register
    allMessages.push_back(this);
}
Message::~Message()  {
    // delete message parts
    for(MessagePart *messagePart : messageParts)
        delete messagePart;
    messageParts.clear();

    // unregister
    auto itr = std::find(allMessages.begin(), allMessages.end(), this);
    if(itr != allMessages.end())
        allMessages.erase(itr);
    // inform observers
    for(MessageObserver *observer : observers)//MessageObserver::getAllMessageObservers())
        observer->messageEvent_destructionStartedEvent(this);
}

std::vector<Message*> Message::getAllMessages()  {
    return allMessages;
}

Message::MessageId Message::generateNewMessageId()  {
    MessageId max=0;
    /// @todo sort @c Messages::allMessages by @c MessageId
    for(Message *message : allMessages)
        if(message->getId() > max)
            max = message->getId();
    return max+1;
}
Message *Message::getById(MessageId id)  {
    for(Message *message : allMessages)   ///< @todo sort @c Message::allMessages
        if(message->getId() == id)
            return message;
    throw Exception("no message with id "+std::to_string(id)+" found");
}

Message::MessageId Message::getId() const  {
    return id;
}
bool Message::hasFlag(MessageFlags flag) const  {
    return this->flags & flag;
}
String Message::getRawFullMessage()  {
    if(hasFlag(FLAG_SHALLOW))  {
        String fn = Settings::diskMessagesDirectory +
                         getContact()->getAccount()->getId() +
                         "/" + std::to_string(getId()) +
                         ".raw_full";
        rawFullMessage = readFromTextFile(fn);
        flags &= ~FLAG_SHALLOW;
        reparseRawFullMessage();
    }
    return rawFullMessage;
}
std::vector<MessagePart*> Message::getMessageParts()  {
    if(hasFlag(FLAG_SHALLOW))
        getRawFullMessage();
    return messageParts;
}
std::time_t Message::getTimeStamp()  const  {
    return timeStamp;
}
String Message::getMessageId() const  {
    return messageId;
}
Contact *Message::getContact()  const  {
    return contact;
}

void Message::addToIndex()  {
    if(Settings::dryRun)
        return ;
    if(hasFlag(FLAG_SHADOW))
        return ;
    // create directory
    String dir = Settings::diskMessagesDirectory + getContact()->getAccount()->getId();
    std::filesystem::create_directories(dir.toStdString());
    // save to index file
    std::fstream f_index((dir+"/index").c_str(),  std::ios_base::out | std::ios_base::binary | std::ios_base::app);
        // see Disk::loadAllFrom Disk for reading correspondant
        f_index.seekp(std::ios_base::end);
        f_index.write((char*)&id, sizeof(id));
        f_index.write((char*)&flags, sizeof(flags));
        f_index.write((char*)&timeStamp, sizeof(timeStamp));
        Contact::ContactId contactId = contact->getId();
        f_index.write((char*)&contactId, sizeof(contactId));
    f_index.close();
}
void Message::saveToFile()  {
    if(Settings::dryRun)
        return ;
    if(hasFlag(FLAG_SHALLOW))  // this message has not even been loaded from disk, so it cannot be written back to disk
        return ;
    if(hasFlag(FLAG_SHADOW))
        return ;
    /// @todo store information about subclasses (e.g. EmailMessage)
    /// @todo store information about which Connection object "owns" this Message object
    // create directory
    String dir = Settings::diskMessagesDirectory + getContact()->getAccount()->getId();

    /* // first, save the bridge file
    Properties properties;
    properties[propertyKeyFlags]        = std::to_string(flags);
    properties[propertyKeyId]           = std::to_string(id);
    properties[propertyKeyMessageId]    = getMessageId();
    properties[propertyKeyTimeStamp]    = timeStampToText(getTimeStamp(), fileDateFormat);
    properties[propertyKeyContactEmail] = getContact()->getEmail();
    std::ofstream f_bridge((dir+"/"+std::to_string(id)+".bridge").c_str());
        writePropertiesToStream(f_bridge, properties);
    f_bridge.close();*/

    // raw content file
    std::ofstream f_raw_full((dir+"/"+std::to_string(id)+".raw_full").c_str());
        f_raw_full.write(getRawFullMessage().c_str(), getRawFullMessage().size());
    f_raw_full.close();

}
void Message::setMessageFlags(MessageFlags flags)  {
    this->flags = flags;
    saveToFile();
    /// @todo save changed flags to index file
}
void Message::addMessageFlags(MessageFlags flags)  {
    this->flags |= flags;
    saveToFile();
    /// @todo save changed flags to index file
}
void Message::setTimeStamp(std::time_t timeStamp)  {
    this->timeStamp = timeStamp;
    for(MessageObserver *observer : observers)
        observer->messageEvent_timeStampChangedEvent(this);
    saveToFile();
}
void Message::setTimestampToNow()  {
    setTimeStamp(std::time(nullptr));
    saveToFile();
}
void Message::setMessageId(String messageId)  {
    /// @todo move to EmailMessage
    /// @todo check for FLAG_SHALLOW
    this->messageId = messageId;
    for(MessageObserver *observer : MessageObserver::getAllMessageObservers())
        observer->messageEvent_messageIdChangedEvent(this);
    saveToFile();
}
