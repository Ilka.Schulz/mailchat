/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <string>
#include <vector>
#include <ctime>  /// @todo maybe switch to C++ chrono library?

class Message;
class Contact;
class MessageCache;
class MessagePart;

#include "src/ui/MessageWidget.hpp"
#include "src/patterns/Observable.hpp"
#include "MessageObserver.hpp"
#include "src/util/Properties.hpp"

/** @brief a message with raw content, timestamp, id, etc.
 *  @details design pattern @c Proxy: objects of this class can get really big (up to several Megabytes per object!) and
 *  reading all of them from Disk can potentially take minutes or be even impossible due to limited memory. This is
 *  inacceptable and therefore it is possible to create "shallow" @c Message objects with the @c Message::FLAG_SHALLOW
 *  set that has only the most important values set and not the @c Message::rawFullMessage which is large and has a
 *  dynamic size. Methods working on these proxied members must check if the shallow flag is set and may need to load
 *  the proxied data from disk first. The original @c Proxy design pattern uses a separate class (e.g. @c MessageProxy)
 *  to clearly distinguish between the bare and the proxy class. This is not implemented here because that would require
 *  virtual methods for all methods that work on the proxied data but using virtual methods can get extremely expense:
 *  it takes 64 bits of extra memory per virtual method per object which would significantly increase memory footprint.
 *  Using the shallow flag needs 0..1 extra bytes per object. Handling extra memory could also significantly increase
 *  CPU usage but I am not sure about that.
 *  @todo every set* method must call saveToFile()
 *  @todo consider writing a separate @c MessageBridge class – however, that would need virtual methods which would
 *  drastically cost performance
 *  @todo maybe use std::optional for timeStamp and messageId
 */
class Message  :  public Observable<MessageObserver>  {
    static const String propertyKeyFlags;         ///< property key for @c Message::flags
    static const String propertyKeyId;            ///< property key for @c Message::id
    static const String propertyKeyMessageId;     ///< property key for @c Message::type
    static const String propertyKeyTimeStamp;     ///< property key for @c Message::timeStamp
    static const String propertyKeyContactEmail;  ///< property key for @c Message::contactEmail

    public:
        /// message flags type, see @c Message::FLAG* for valid flags
        typedef uint16_t MessageFlags;
        /// if set, the message is actuall composed by the user and sent to a contact, not received by the user from the contact
        static const MessageFlags FLAG_OUTGOING;
        /// if set, the message also exists on the remove server, not just on this client application.
        static const MessageFlags FLAG_SYNCHRONIZED;
        /// if set, this object has not been fully loaded from file (design pattern @c Bridge, see class documentation)
        static const MessageFlags FLAG_SHALLOW;
        /// if set, this object will not be saved to disk – useful for @c Rfc822InlineMessagePart and tests
        static const MessageFlags FLAG_SHADOW;

        /// unique ID of @c Message objects
        /// @todo generate IDs for each account @c Account object separately
        typedef uint32_t MessageId;

    protected:
        /// unique ID of @c Message objects
        MessageId id;

        /// message type, see @c Message::MessageType
        MessageFlags flags;

        /// the raw full message including all email headers, full body and any legal or illegal bytes
        /// @todo maybe use @c std::vector<char> instead?
        String rawFullMessage;

        /// all message parts (might change due to translation / decryption / etc.)
        /// note: these are pointers because @c MessagePart is polymorphic
        std::vector<MessagePart*> messageParts;

        /// sender field in incoming messages; recipient field in outgoing messages (see @c Message::type)
        Contact *contact;

        /// time stamp when the message was sent
        std::time_t timeStamp;

        /// @brief email message id, e.g. "4c61ca2a-f19b-33b5-c677-476490a6ebad@example.org"
        /// @todo define properly whether the "<" and ">" are included
        /// @todo move to @c EmailMessage
        String messageId;

        /// static register of all existing objects
        static std::vector<Message*> allMessages;

    public:
        /** @brief constructor
         *  @param _id             initializes @c Message::id  (can be zero to let the constructor generate a new unique ID)
         *  @param _flags          initializes @c Message::flags
         *  @param _rawFullMessage initializes @c Message::rawFullContent
         *  @param _contact        initializes @c Message::contact
         *  @param _timeStamp      initializes @c Message::timeStamp
         *  @param _messageId      initializes @c Message::messageId
         */
        Message(MessageId _id, MessageFlags _flags, String _rawFullMessage, Contact *_contact,
                std::time_t _timeStamp=0, String _messageId="");
        virtual ~Message();

    protected:
        /** @brief gets called every time the @c Message::rawFullMessage changed
         *  @details This method must be implemeted by derived classes and their implementations need to do the
         *  following:
         *   - parse the @c Message::rawFullMessage and update the @c Message::messageParts vector
         *   - inform all observers about newly created @c MessagePart objects (changes and deletions do not need to be sent to observers; the @c MessagePart class will take care of that)
         */
        virtual void reparseRawFullMessage()=0;

    public:
        /// @returns @c Message::allMessages
        static std::vector<Message*> getAllMessages();

        /// @returns a newly geneated unique id
        static MessageId generateNewMessageId();
        /** @param id  the id to search for in @c Message::allMessages
         *  @returns the @c Message object with the specified id
         */
        static Message *getById(MessageId id);

        /// @returns @c Message::id;
        MessageId getId()  const;

        /// @param flag  the flag to check
        /// @returns whether the specified flag is included in @c Message::flags
        bool hasFlag(MessageFlags flag)  const;

        /// @returns @c Message::rawFullContent
        String getRawFullMessage();

        /// @returns @c Message::messageParts
        std::vector<MessagePart*> getMessageParts();

        /// @todo implement getTeaser() in order to show desktop notifications

        /// @returns @c Message::timeStamp
        std::time_t getTimeStamp()  const;

        /// @returns @c Message::messageId;
        String getMessageId()  const;

        /// @returns @c Message::contact
        Contact *getContact()  const;

        /// adds this object to the account's message index on disk
        /// @todo prevent it from being saved more than once
        void addToIndex();
        /// saves this object to disk
        void saveToFile();



        /** @brief sets @c Message::flags (and drops old flags)
         *  @todo emit signal
         *  @param flags  the new flags to set
         */
        void setMessageFlags(MessageFlags flags);

        /** @brief add @c flags to @c Message::flags (and keeps old flags)
         *  @todo emit signal
         *  @param flags  the new flags to add
         */
        void addMessageFlags(MessageFlags flags);


        /** @brief sets @c Message::timeStamp
         *  @details emits @c MessageObserver::messageEvent_timeStampChangedEvent
         *  @param timeStamp  the new timeStamp to set
         */
        void setTimeStamp(std::time_t timeStamp);

        /** @brief sets @c Message::timeStamp to the current local time noting the time zone
         *  @todo does not note time zone
         *  @details emits @c MessageObserver::messageEvent_timeStampChangedEvent
         */
        void setTimestampToNow();

        /** @brief sets @c Message::messageId
         *  @details emits @c MessageObserver::messageEvent_messageIdChangedEvent
         *  @param messageId  the new messageId to set
         */
        void setMessageId(String messageId);

};
