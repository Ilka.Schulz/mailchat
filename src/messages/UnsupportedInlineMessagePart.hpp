/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/messages/InlineMessagePart.hpp"
#include "src/util/String.hpp"
#include <vector>
#include <exception>

class UnsupportedInlineMessagePart  :  public InlineMessagePart  {
    public:
        enum Cause {EXCEPTION, CONTENT_TYPE, BAD_MULTIPART_ALTERNATIVE, SECURITY};
    private:
        Cause cause;
        String exceptionWhat;
        String contentType;
        std::vector<char> data;
    public:
        UnsupportedInlineMessagePart(const std::exception *_exception);
        UnsupportedInlineMessagePart(const String &_contentType, const std::vector<char> &_data);
        UnsupportedInlineMessagePart(const Cause _cause);

        const Cause &getCause()  const;
        const String &getExceptionWhat()  const;
        const String &getContentType()  const;
        const std::vector<char> &getData()  const;
};
