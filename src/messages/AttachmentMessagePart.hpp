/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "src/messages/MessagePart.hpp"

#include <vector>
#include "src/util/String.hpp"

class AttachmentMessagePart  :  public MessagePart  {
    private:
        String name;
        std::vector<char> data;
        time_t creation_date;
        time_t modification_date;

    public:
        AttachmentMessagePart(const std::vector<char> &_data,
                              const String &_name,
                              const time_t _creation_date,
                              const time_t _modification_date
                              );

        const String &getName()  const;
        const std::vector<char> &getData()  const;
        time_t getCreationDate()  const;
        time_t getModificationDate()  const;

        virtual Disposition getDisposition()  const override;
};
