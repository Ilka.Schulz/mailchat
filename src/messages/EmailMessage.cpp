/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "EmailMessage.hpp"
#include "src/Account.hpp"
#include "src/Contact.hpp"
#include "src/util/time.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/misc.hpp"
#include "src/messages/AttachmentMessagePart.hpp"
#include "src/messages/ImageInlineMessagePart.hpp"
#include "src/messages/Rfc822InlineMessagePart.hpp"
#include "src/messages/TextInlineMessagePart.hpp"
#include "src/messages/UnsupportedInlineMessagePart.hpp"
#include "src/util/String.hpp"
#include "src/util/iostream.hpp"
#include "src/streams/base64.hpp"
#include "src/streams/quotedprintable.hpp"
#include "src/util/exceptions/macros.hpp"
#include "src/util/time.hpp"

#include "submodules/iconvpp/iconv.hpp"

#include <map>
#include <vector>


// will be constexpr and static class member in C++ 20, see https://gitlab.com/Ilka.Schulz/mailchat/-/issues/52
const String
HEAD_KEY_VALUE_SEPARATOR = ": ",
HEAD_VALUE_VALUE_SEPARATOR = ";",
MIME_VERSION     = "Mime-Version",
MIME_VERSION_1_0 = "1.0",
EXTENSION_PREFIX = "X-",
FROM         = "From",
TO           = "To",
SUBJECT      = "Subject",
DATE         = "Date",
MESSAGE_ID   = "Message-ID",

CHARSET_UTF8 = "utf-8",

CONTENT_TRANSFER_ENCODING                  = "Content-Transfer-Encoding",
CONTENT_TRANSFER_ENCODING_7BIT             = "7bit",
CONTENT_TRANSFER_ENCODING_8BIT             = "8bit",
CONTENT_TRANSFER_ENCODING_BASE64           = "base64",
CONTENT_TRANSFER_ENCODING_BINARY           = "binary",
CONTENT_TRANSFER_ENCODING_QUOTED_PRINTABLE = "quoted-printable",

CONTENT_DISPOSITION                   = "Content-Disposition",
CONTENT_DISPOSITION_INLINE            = "inline",
CONTENT_DISPOSITION_ATTACHMENT        = "attachment",
CONTENT_DISPOSITION_FILENAME          = "filename",
CONTENT_DISPOSITION_CREATION_DATE     = "creation-date",
CONTENT_DISPOSITION_MODIFICATION_DATE = "modification-date",
CONTENT_DISPOSITION_READ_DATE         = "read-date",
CONTENT_DISPOSITION_SIZE              = "size",


CONTENT_TYPE = "Content-Type",
CONTENT_TYPE_NAME = "name",

CONTENT_TYPE_APPLICATION_SLASH         = "application/",
CONTENT_TYPE_APPLICATION_OCTET_STREAM  = "application/octet-stream",
CONTENT_TYPE_APPLICATION_POSTSCRIPT    = "application/postscript",
CONTENT_TYPE_APPLICATION_PGP_SIGNATURE = "application/pgp-signature",
CONTENT_TYPE_APPLICATION_PGP_ENCRYPTED = "application/pgp-encrypted",
CONTENT_TYPE_APPLICATION_PGP_KEYS      = "application/pgp-keys",


CONTENT_TYPE_AUDIO_SLASH               = "audio/",
CONTENT_TYPE_IMAGE_SLASH               = "image/",

CONTENT_TYPE_MESSAGE_SLASH             = "message/",
CONTENT_TYPE_MESSAGE_RFC822            = "message/rfc822",
CONTENT_TYPE_MESSAGE_PARTIAL           = "message/partial",
CONTENT_TYPE_MESSAGE_EXTERNAL_BODY     = "message/external-body",

CONTENT_TYPE_MULTIPART_SLASH           = "multipart/",
CONTENT_TYPE_MULTIPART_MIXED           = "multipart/mixed",
CONTENT_TYPE_MULTIPART_ALTERNATIVE     = "multipart/alternative",
CONTENT_TYPE_MULTIPART_DIGEST          = "multipart/digest",
CONTENT_TYPE_MULTIPART_PARALLEL        = "multipart/parallel",
CONTENT_TYPE_MULTIPART_ECNRYPTED       = "multipart/encrypted",
CONTENT_TYPE_MULTIPART_BOUNDARY        = "boundary",

CONTENT_TYPE_TEXT_SLASH                = "text/",
CONTENT_TYPE_TEXT_PLAIN                = "text/plain",
CONTENT_TYPE_TEXT_RICHTEXT             = "text/richtext",
CONTENT_TYPE_TEXT_HTML                 = "text/html",
CONTENT_TYPE_TEXT_WATCH_HTML           = "text/watch-html",
CONTENT_TYPE_TEXT_CHARSET              = "charset",

CONTENT_TYPE_VIDEO_SLASH               = "video/";


EmailMessage::EmailMessage(MessageId _id, MessageFlags _flags, String _rawFullMessage, Contact *_contact,
                           std::time_t _timeStamp, String _messageId)
        : Message(_id, _flags, _rawFullMessage, _contact, _timeStamp, _messageId)  {
    saveToFile();
    // inform ALL observers that a (non-pure virtual!) object has been created
    if( ! hasFlag(FLAG_SHADOW) )
        for( MessageObserver *observer : MessageObserver::getAllMessageObservers() )
            observer->messageEvent_constructedEvent(this);
    // parse content (if available) and inform observers
    if( ! hasFlag(FLAG_SHALLOW) )
        EmailMessage::reparseRawFullMessage();
}
EmailMessage::EmailMessage(MessageId _id, MessageFlags _flags, std::vector<MessagePart*> _messageParts, Contact *_contact,
             std::time_t _timeStamp, String _messageId)
        : Message(_id, _flags, "", _contact, _timeStamp, _messageId) {
    saveToFile();
    // inform ALL observers that a (non-pure virtual!) object has been created
    if( ! hasFlag(FLAG_SHADOW) )
        for( MessageObserver *observer : MessageObserver::getAllMessageObservers() )
            observer->messageEvent_constructedEvent(this);
    // set message parts
    messageParts = _messageParts;
}

EmailMessage *EmailMessage::createFromRawFullMessage(String _rawFullMessage, Account *account, bool shadow)  {
    // parse response
    String head,body;
    std::tie(head,body) = splitIntoHeadAndBody(_rawFullMessage);
    const auto tags = parseHeadIntoTags(head);

    Message::MessageFlags flags = 0;
    if(shadow)
        flags |= EmailMessage::FLAG_SHADOW;

    // search for from,to,date,message-id
    const auto itrFrom = tags.equal_range(FROM.toLower());
    const auto itrTo   = tags.equal_range(TO.toLower());
    const auto itrDate = tags.equal_range(DATE.toLower());
    const auto itrMId  = tags.equal_range(MESSAGE_ID.toLower());

    std::optional<String> from;
    std::optional<String> to;
    std::optional<String> date;
    std::optional<String> mId;
    if(itrFrom.first != itrFrom.second)
        from = itrFrom.first->second;
    if(itrTo.first != itrTo.second)
        to = itrTo.first->second;
    if(itrDate.first != itrDate.second)
        date = itrDate.first->second;
    if(itrMId.first != itrMId.second)
        mId = itrMId.first->second;

        /*
        ERROR(ParseException, ": no From field");
        if(itrTo.first   == itrTo.second)
            ERROR(ParseException, ": no To field");
        if(itrDate.first == itrDate.second)
            ERROR(ParseException, ": no Date field");
        if(itrMId.first  == itrMId.second) /// @todo decide on proper handling for messages without message-ids, see https://gitlab.com/Ilka.Schulz/mailchat/-/issues/39
            ERROR(ParseException, ": no Message-ID field");
        */

    /// @todo check if this message already exists in memory. However, this should already be done by
    /// ImapFetchJob::fetchHeadersDoneCallback IF the message is constructed from fetched server content. It may also
    /// be fetched from disk...

    // find contact
    Contact *contact = nullptr;
    if(from.has_value() && to.has_value() && account!=nullptr)  {
        // split From field into fromName and fromEmail
        std::vector<AddressInfo> fromInfo = addressLineToInfo(from.value());
        std::vector<AddressInfo> toInfo   = addressLineToInfo(to.value());
        // determine which addressInfo to search for
        std::vector<AddressInfo> addressInfo;
        if(fromInfo.size()==1 && fromInfo[0].email == account->getEmail()  // outgoing message
        /// @todo        || this_->imapFolder->isSent
        )  {
            addressInfo = toInfo;
            flags |= Message::FLAG_OUTGOING | Message::FLAG_SYNCHRONIZED;
        }
        else  {  // incoming message
            addressInfo = fromInfo;
            flags &= ~Message::FLAG_OUTGOING;
        }
        contact = Contact::getEmailContact(account, addressInfo);
    }

    EmailMessage *message = new EmailMessage(0,
                                             flags,
                                             _rawFullMessage,
                                             contact,
                                             date.has_value() ?
                                                 Time::textToTimeStamp(date.value(), Time::emailDateFormat) :
                                                 0,
                                             mId.value_or("")
                                             );
    /// @todo create shallow @c Message object even if no new @c OutgoingMessage or @c IncomingMessage objects are
    /// created
    return message;
}


// ========== parsing stuff ==========
std::tuple<String,String>
EmailMessage::splitIntoHeadAndBody(String full)  {
    std::size_t pos = full.findFirst({"\n\n", "\r\n\r\n"},PLAIN);
    if(pos == String::npos)
        return std::tuple(full,"");
    String body = full.substr(pos);
    String head = full.substr(0, pos);
    head.trim();
    body.trim();
    return std::tuple(head,body);
}

std::multimap<String, String>
EmailMessage::parseHeadIntoTags(String head)  {
    std::multimap<String, String> retval;
    String lastKey="";
    for(const auto &line : head.splitIntoLines(PLAIN))  {
        if(line.empty())  // empty lines separate head and body, so the had does not have empty lines
            ERROR(std::logic_error, ": empty line");
        // read key and content
        String key,content;
        if(std::isspace(line[0]))  {
            key = lastKey;
            content = line;
        }
        else  {
            const auto parts = line.split(HEAD_KEY_VALUE_SEPARATOR,CASE_INSENSITIVE,2);  /// @todo respect quotes
            lastKey = key = parts[0];
            if(parts.size()==2)
                content = parts[1];
        }
        // split content into parts and add each to the retval multimap
        const auto parts = content.split(HEAD_VALUE_VALUE_SEPARATOR,CASE_INSENSITIVE);  /// @todo this splits some user-agent values, e.g. Thunderbird @todo respect quotes
        for(String part : parts)  {
            part.trim();
            part.trimMatchingQuotes();
            if(part.empty())
                continue;
            retval.insert(std::pair(key.toLower(),part));  /// @todo really use toLower()
        }
    }
    return retval;
}


/// @todo this really needs documentation!
void EmailMessage::parseHeadTag(const std::multimap<String,String> &headerTags,
                                const String key,
                                std::optional<String> &primaryValue,
                                std::map<String, std::optional<String>&> otherValues,
                                ReactionOptionToUnknownKey reactionToUnknownKey
                                )  {
    auto elements = headerTags.equal_range(key.toLower());
    for(auto itr=elements.first; itr!=elements.second; itr++)  {
        auto pair = itr->second.split('=',PLAIN,2);  /// @todo respect quotes?
        for(String &value : pair)
            value.trim();
        if(pair.size()==1)
            primaryValue = pair[0];
        else  {
            bool found=false;
            for(auto &itr : otherValues)  {
                if(pair[0].toLower() == itr.first.toLower())  {  /// @todo inefficient
                    String &value = pair[1];
                    // remove quotes
                    value.trimMatchingQuotes();
                    itr.second = value;
                    found=true;
                    break;
                }
            }
            if( ! found )
                switch(reactionToUnknownKey)  {
                    case THROW:
                        throw ParseException(String(__PRETTY_FUNCTION__) +
                                             ": unknown subkey >"+pair[0]+"< in header key >"+key+"<");
                        break;
                    case IGNORE:
                        break;
                }
        }
    }
}

/// @todo look up MIME standard and apply to this method (see https://gitlab.com/Ilka.Schulz/mailchat/-/issues/48)
std::vector<MessagePart *> EmailMessage::parseMessagePart(const std::multimap<String,String> &headerTags, String encodedBody)  {
    // check MIME version (RFC 1521, sec. 3)
    std::optional<String> mime_version;  {
        parseHeadTag(headerTags,
                     MIME_VERSION.toLower(),
                     mime_version,
                     {}
                     );
        if( ! mime_version.has_value() )
            mime_version = MIME_VERSION_1_0;
        if(mime_version.value().split(' ',PLAIN)[0].unequal(MIME_VERSION_1_0,CASE_INSENSITIVE))  /// @todo respect quotes?
            ERROR(ParseException, "unsupported MIME version: >"+mime_version.value()+"<");
    }

    /// @todo Content-Description (RFC ???)

    // decode content (Content-Transfer-Encoding, RFC 1521 sec. 5) (incomplete)
    std::vector<char> body;  {
        // read tag
        std::optional<String> content_transfer_encoding;
        parseHeadTag(headerTags,
                     CONTENT_TRANSFER_ENCODING,
                     content_transfer_encoding,
                     {} );
        // 7bit, 8bit
        if( (! content_transfer_encoding.has_value()) ||
            (content_transfer_encoding.value().equals(CONTENT_TRANSFER_ENCODING_7BIT,CASE_INSENSITIVE))  ||
            (content_transfer_encoding.value().equals(CONTENT_TRANSFER_ENCODING_8BIT,CASE_INSENSITIVE)) )  {
                body = std::vector<char>(encodedBody.begin(), encodedBody.end());
        }
        // base64
        else if(content_transfer_encoding.value().equals(CONTENT_TRANSFER_ENCODING_BASE64,CASE_INSENSITIVE))
            body = base64decode(encodedBody,true);
        // quoted-printable
        else if(content_transfer_encoding.value().equals(CONTENT_TRANSFER_ENCODING_QUOTED_PRINTABLE,CASE_INSENSITIVE))
            body = quotedprintabledecode(encodedBody);
        /// @todo binary
        else
            throw ParseException(String(__PRETTY_FUNCTION__) +
                                 ": unsupported Content-Transfer-Encoding: >"+content_transfer_encoding.value()+"<");
    }

    /// @todo consider reading Content-ID (RFC 1521 sec. 6.1) – however, this is only needed for "message/external-body"
    /// which is suppressed for security reasons
    /// @todo consider reading Content-Description (RFC 1521 sec. 6.2) – but for what?

    // interprete disposition (Content-Disposition, RFC 2183 sec. 2)
    std::optional<String> content_disposition;
    std::optional<String> content_disposition_filename;
    std::optional<String> content_disposition_creation_date;
    std::optional<String> content_disposition_modification_date;
    std::optional<String> content_disposition_read_date;
    std::optional<String> content_disposition_size;  {
        // read tags
        parseHeadTag(headerTags,
                     CONTENT_DISPOSITION,
                     content_disposition,
                     {
                         {CONTENT_DISPOSITION_FILENAME         , content_disposition_filename},
                         {CONTENT_DISPOSITION_CREATION_DATE    , content_disposition_creation_date},
                         {CONTENT_DISPOSITION_MODIFICATION_DATE, content_disposition_modification_date},
                         {CONTENT_DISPOSITION_READ_DATE        , content_disposition_read_date},
                         {CONTENT_DISPOSITION_SIZE             , content_disposition_size}
                     } );
        reinterprete_disposition:  // jump back here if content_disposition[_*] has been changed intentionally
        // process creation/modification date
        if(content_disposition_creation_date.has_value() && ! content_disposition_modification_date.has_value())
            content_disposition_modification_date = content_disposition_creation_date;
        if( ! content_disposition_creation_date.has_value() && content_disposition_modification_date.has_value())
            content_disposition_creation_date = content_disposition_modification_date;

        // inline
        if( ! content_disposition.has_value() ||
              content_disposition.value().equals(CONTENT_DISPOSITION_INLINE,CASE_INSENSITIVE))  {
            // read Content-Type tag (RFC 1521 sec. 4;7)
            std::optional<String> content_type;  {
                // read tag
                parseHeadTag(headerTags,
                             CONTENT_TYPE,
                             content_type,
                             {},  // parse parameters later based on primary key (v.i.)
                             IGNORE);
                // set default value
                if( ! content_type.has_value() )
                    content_type = CONTENT_TYPE_TEXT_PLAIN;
                // application/* (RFC 1521 sec. 7.4)
                if(content_type.value().beginsWith(CONTENT_TYPE_APPLICATION_SLASH   , CASE_INSENSITIVE))  {
                    if(content_type.value().equals(CONTENT_TYPE_APPLICATION_OCTET_STREAM,CASE_INSENSITIVE))  {
                        // offer as attachment - this could be anything
                        // - value of key "type" will not be read as this is never used in the wild
                        // - value of key "padding" will not be read as this is never used in the wild
                        // - value of key "name" will be read in the disposition==attachment code section
                        content_disposition = CONTENT_DISPOSITION_ATTACHMENT;
                        goto reinterprete_disposition;
                    }
                    else if(content_type.value().equals(CONTENT_TYPE_APPLICATION_POSTSCRIPT,CASE_INSENSITIVE))  {
                        // This will not be supported as I was not able to observe it even a single time in the wild.
                        // Also: "The execution of general-purpose PostScript interpreters entails serious security
                        //        risks, and implementors are discouraged from simply sending PostScript email bodies to
                        //        "off-the-shelf" interpreters." [RFC 1521 sec. 7.4.2]
                        // Also: "PostScript is a registered trademark of Adobe Systems, Inc." [RFC 1521 sec. 7.4.2]
                        return {new UnsupportedInlineMessagePart(UnsupportedInlineMessagePart::Cause::SECURITY)};
                    }
                    else if(content_type.value().equals(CONTENT_TYPE_APPLICATION_PGP_SIGNATURE,CASE_INSENSITIVE) ||
                       content_type.value().equals(CONTENT_TYPE_APPLICATION_PGP_ENCRYPTED,CASE_INSENSITIVE) ||
                       content_type.value().equals(CONTENT_TYPE_APPLICATION_PGP_KEYS     ,CASE_INSENSITIVE)
                    )  {
                        /// @todo implement PGP
                        NOT_YET_IMPLEMENTED();// return {new UnsupportedInlineMessagePart(content_type.value(), body)};
                    }
                    else  {
                        // offer as attachment - people send the weirdest shit as this type,
                        // e.g. "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        content_disposition = CONTENT_DISPOSITION_ATTACHMENT;
                        goto reinterprete_disposition;
                    }
                }
                // audio/*  (RFC 1521 sec. 7.6)
                else if(content_type.value().beginsWith(CONTENT_TYPE_AUDIO_SLASH    , CASE_INSENSITIVE))  {
                    /// @todo implement audio player – this might be useful for voice messages
                    // offer as attachment – this is a mail client and not an audio player (yet)!
                    content_disposition = CONTENT_DISPOSITION_ATTACHMENT;
                    goto reinterprete_disposition;
                }
                // image/*  (RFC 1521 sec. 7.5)
                else if(content_type.value().beginsWith(CONTENT_TYPE_IMAGE_SLASH    , CASE_INSENSITIVE))  {
                    std::optional<String> altText;  /// @todo get this working
                    /// @todo use image format hint
                    return {new ImageInlineMessagePart(body, altText)};
                }
                // message/* (RFC 1521 sec. 7.3)
                else if(content_type.value().beginsWith(CONTENT_TYPE_MESSAGE_SLASH  , CASE_INSENSITIVE))  {
                    // message/rfc822 (RFC 1521 sec. 7.3.1)
                    if(content_type.value().equals(CONTENT_TYPE_MESSAGE_RFC822,CASE_INSENSITIVE))  {
                        std::optional<String> content_type_dummy;  // do not read content_type a second time
                        std::optional<String> content_type_name;
                        parseHeadTag(headerTags,
                                     CONTENT_TYPE,
                                     content_type_dummy,
                                     {
                                         {CONTENT_TYPE_NAME, content_type_name}
                                     },
                                     ReactionOptionToUnknownKey::IGNORE  /// @todo is this wise?
                        );
                        /// @todo identify account (this will also make it possible to identify the contact)
                        return {new Rfc822InlineMessagePart(body,nullptr,content_type_name)};
                    }
                    // message/partial (RFC 1521 sec. 7.3.2)
                    else if(content_type.value().equals(CONTENT_TYPE_MESSAGE_PARTIAL,CASE_INSENSITIVE))  {
                        // absolutely not a priority as I have not observed this type even a single time in my own inbox
                        NOT_YET_IMPLEMENTED();
                    }
                    // message/external-body (RFC 1521 sec. 7.3.3)
                    else if(content_type.value().equals(CONTENT_TYPE_MESSAGE_EXTERNAL_BODY,CASE_INSENSITIVE))  {
                        // Loading external bodies really shows how mindless people were about security back in the 90s.
                        // However, I have not observed this type even a single time in my own inbox.
                        return {new UnsupportedInlineMessagePart(UnsupportedInlineMessagePart::Cause::SECURITY)};
                    }
                    else
                        return {new UnsupportedInlineMessagePart(content_type.value(),body)};
                }
                // multipart/*  (RFC 1521 sec. 7.2)
                else if(content_type.value().beginsWith(CONTENT_TYPE_MULTIPART_SLASH, CASE_INSENSITIVE))  {
                    // identify exact multipart type
                    bool multipart_alternative=false,
                         multipart_digest=false;  {
                    if(content_type.value().equals(CONTENT_TYPE_MULTIPART_MIXED,CASE_INSENSITIVE))
                        ;  // nothing to do
                    else if(content_type.value().equals(CONTENT_TYPE_MULTIPART_ALTERNATIVE,CASE_INSENSITIVE))
                        multipart_alternative = true;
                    else if(content_type.value().equals(CONTENT_TYPE_MULTIPART_DIGEST,CASE_INSENSITIVE))
                        multipart_digest = true;
                    else if(content_type.value().equals(CONTENT_TYPE_MULTIPART_PARALLEL,CASE_INSENSITIVE))
                        ;  // Mailchat displays all message parts simultaneously anyways...
                    else if(content_type.value().equals(CONTENT_TYPE_APPLICATION_PGP_ENCRYPTED,CASE_INSENSITIVE))
                        NOT_YET_IMPLEMENTED();  /// @todo implement PGP
                    else
                        return {new UnsupportedInlineMessagePart(content_type.value(),body)};
                    }
                    // parse boundary
                    std::optional<String> content_type_boundary;  {
                        // read tag
                        std::optional<String> content_type_dummy;  // do not read content_type a second time
                        parseHeadTag(headerTags,
                                     CONTENT_TYPE.toLower(),
                                     content_type_dummy,
                                     {
                                         {CONTENT_TYPE_MULTIPART_BOUNDARY,content_type_boundary}
                                     },
                                     ReactionOptionToUnknownKey::IGNORE
                                     );

                        // sanity check
                        if( ! content_type_boundary.has_value() )
                            ERROR(ParseException, CONTENT_TYPE+" is multipart but has not boundary tag");
                        if(content_type_boundary.value().size()<5)
                            ERROR(ParseException, CONTENT_TYPE+" is multipart but boundary tag is very short: >" +
                                                  content_type_boundary.value()+"<");
                    }
                    // parse all parts
                    std::vector<MessagePart*> retval;
                    for(String &part : String(vectorToString(body))
                                                   // discard everything after final boundary tag
                                                   .split("--"+content_type_boundary.value()+"--",PLAIN)[0]
                                                   // split along boundary
                                                   .split("--"+content_type_boundary.value(),PLAIN))  {
                        // parse
                        part.trim();
                        if(part.empty())
                            continue;
                        String head,body;
                        std::tie(head,body) = splitIntoHeadAndBody(part);
                        if(body.empty())  // "multipart/*" messages often begin with an illegal "This is a multi-part
                                          // message in MIME format." string with is not a valid message part.
                            continue;
                        auto headerTags = parseHeadIntoTags(head);
                        // use "message/rfc822" as default Content-Type if the multipart/* type is "digest"
                        if(multipart_digest)  {
                            const auto itrContentType = headerTags.equal_range(CONTENT_TYPE.toLower());
                            if(itrContentType.first == itrContentType.second)
                                headerTags.insert(std::pair(CONTENT_TYPE.toLower(),CONTENT_TYPE_MESSAGE_RFC822));
                        }
                        // parse children and add to return value
                        try  {
                            std::vector<MessagePart*> children = parseMessagePart(headerTags, body);
                            retval.insert(retval.end(), children.begin(), children.end());
                        }
                        catch(std::exception &e)  {
                            retval.push_back(new UnsupportedInlineMessagePart(&e));
                        }
                    }
                    // remove low tier alternatives if the multipart/* type is "alternative"
                    if(multipart_alternative)  {
                        MessagePart *chosenMessagePart=nullptr;
                        // try to find best supported part
                        if(chosenMessagePart==nullptr)
                            for(MessagePart *messagePart : retval)
                                if(dynamic_cast<UnsupportedInlineMessagePart*>(messagePart) == nullptr)
                                    chosenMessagePart = messagePart;
                        // try to find best unsupported part
                        if(chosenMessagePart == nullptr)
                            if( ! retval.empty() )
                                chosenMessagePart = retval.back();
                        // default to new unsupported part
                        if(chosenMessagePart == nullptr)
                            retval.push_back(
                                        chosenMessagePart =
                                        new UnsupportedInlineMessagePart(
                                                UnsupportedInlineMessagePart::Cause::BAD_MULTIPART_ALTERNATIVE
                                        )
                            );
                        // set return value to chosen part
                        for(size_t i=0; i<retval.size(); i++)  {
                            if(retval[i] != chosenMessagePart)  {
                                delete retval[i];
                                retval.erase(retval.begin()+i);
                                i--;
                            }
                        }
                    }
                    return retval;
                }
                // text/* (RFC 1521 sec 7.1)
                else if(content_type.value().beginsWith(CONTENT_TYPE_TEXT_SLASH     , CASE_INSENSITIVE))  {
                    // read tags
                    std::optional<String> content_type_text_charset;  {
                        std::optional<String> primaryValueDummy;
                        parseHeadTag(headerTags,
                                     CONTENT_TYPE.toLower(),
                                     primaryValueDummy,
                                     {
                                         {CONTENT_TYPE_TEXT_CHARSET, content_type_text_charset}
                                     },
                                     ReactionOptionToUnknownKey::IGNORE  ///< @todo is this wise?
                                     );
                    }
                    // convert charset
                    std::string text;  {
                        if(content_type_text_charset.has_value() &&
                           content_type_text_charset.value().unequal(CHARSET_UTF8,CASE_INSENSITIVE))  {
                            // convert to utf-8
                            std::string in(body.begin(), body.end());  /// @todo performance
                            iconvpp::converter *converter=nullptr;
                            try {
                                converter = new iconvpp::converter(CHARSET_UTF8.toLower(),
                                                                   content_type_text_charset.value().toLower(),
                                                                   false,  // do not ignore errors
                                                                   4096    // output buffer size
                                                                  );
                            }
                            catch(std::runtime_error &e)  {
                                ERROR(ParseException, "unsupported charset: >"+content_type_text_charset.value()+"<");
                            }
                            try  {
                                converter->convert(in,text);
                            }
                            catch(std::runtime_error &e)  {
                                delete converter;
                                ERROR(ParseException, "could not convert charset: >"+content_type_text_charset.value()+"<");
                            }
                            delete converter;
                        }
                        else
                            // keep data
                            text = vectorToString(body);
                    }
                    // parse primary key
                    TextInlineMessagePart::ContentType contentType = TextInlineMessagePart::ContentType::PLAIN;
                    if( ! content_type.has_value() )
                        contentType = TextInlineMessagePart::ContentType::PLAIN;
                    else if(content_type.value().equals(CONTENT_TYPE_TEXT_PLAIN     , CASE_INSENSITIVE))
                        contentType = TextInlineMessagePart::ContentType::PLAIN;
                    else if(content_type.value().equals(CONTENT_TYPE_TEXT_RICHTEXT  , CASE_INSENSITIVE))  {
                        // This was defined in the obsolete RFC 1341 sec. 7.1.3 I but could not observe it in the wild.
                        /// @todo pass text instead of body?
                        return {new UnsupportedInlineMessagePart(content_type.value(),body)};
                    }
                    else if(content_type.value().equals(CONTENT_TYPE_TEXT_HTML      , CASE_INSENSITIVE))
                        contentType = TextInlineMessagePart::ContentType::HTML;
                    else if(content_type.value().equals(CONTENT_TYPE_TEXT_WATCH_HTML, CASE_INSENSITIVE))
                        contentType = TextInlineMessagePart::ContentType::WATCH_HTML;
                    else if(content_type.value().beginsWith(
                                                        CONTENT_TYPE_TEXT_SLASH+EXTENSION_PREFIX, CASE_INSENSITIVE))  {
                        // offer as attachment - this could be anything...
                        /// @todo support most important "text/x-*" types, e.g. "text/x-vcard"
                        content_disposition = CONTENT_DISPOSITION_ATTACHMENT;
                        goto reinterprete_disposition;
                    }
                    else
                        /// @todo pass text instead of body?
                        return {new UnsupportedInlineMessagePart(content_type.value(), body)};
                    return {new TextInlineMessagePart(text, contentType)};
                }
                // video/*
                else if(content_type.value().beginsWith(CONTENT_TYPE_VIDEO_SLASH    , CASE_INSENSITIVE))  {
                    /// @todo implement RFC 1521 sec. 7.7
                    // offer as attachment – this is a mail client and not a video player!
                    content_disposition = CONTENT_DISPOSITION_ATTACHMENT;
                    goto reinterprete_disposition;
                }
                // x-*  (RFC 1521 sec. 7.8)
                else if(content_type.value().beginsWith(EXTENSION_PREFIX            , CASE_INSENSITIVE))
                    return {new UnsupportedInlineMessagePart(content_type.value(), body)};
                else
                    return {new UnsupportedInlineMessagePart(content_type.value(), body)};
            }  //
        }
        // attachment
        else if(content_disposition.value().equals(CONTENT_DISPOSITION_ATTACHMENT,CASE_INSENSITIVE))    {
            /// @todo allow multipart attachments (see RFC 2183 sec. 3)
            return {new AttachmentMessagePart(
                           body,
                           content_disposition_filename.value_or("unnamed file"),  /// @todo sanitze?
                           content_disposition_creation_date.has_value() ?
                                Time::textToTimeStamp(content_disposition_creation_date.value(), Time::emailDateFormat) :
                                0,
                           content_disposition_modification_date.has_value() ?
                                Time::textToTimeStamp(content_disposition_modification_date.value(), Time::emailDateFormat) :
                                0
                       )};
        }
        else
            ERROR(ParseException, "invalid Content-Disposition: >"+content_disposition.value()+"<");
    }
}


/// @todo merge ImapFetchJob::fetchHeadersDoneCallback, EmailMessage::createFromRawFullMessage and EmailMessage::reparseRawFullMessage into one handy function
void EmailMessage::reparseRawFullMessage()  {
    // basic parsing
    String head,body;
    std::tie(head,body) = splitIntoHeadAndBody(getRawFullMessage());
    std::multimap<String,String> headerTags = parseHeadIntoTags(head);

    // delete old message parts
    for(MessagePart *messagePart : messageParts)
        delete messagePart;
    messageParts.clear();

    // parse new message parts
    try  {
        messageParts = parseMessagePart(headerTags,body);
    }
    catch(std::exception &e)  {
        messageParts = {new UnsupportedInlineMessagePart(&e)};
        /// @todo log exception
    }

    // inform observers
    for(MessageObserver *observer : observers)
        for(MessagePart *messagePart : messageParts)
            observer->messageEvent_messagePartAddedEvent(this, messagePart);
}


// tests
#include <iostream>
#undef NDEBUG
#include <assert.h>
#include "src/settings.hpp"
int src_messages_EmailMessage(int,char**)  {
    Settings::dryRun = true;

    const String head = String({
            "Return-Path: <bob@schulz.com.de>\n"
            "Delivered-To: alice@schulz.com.de\n"
            "Received: from schulz.com.de\n"
            "    by schulz.com.de with LMTP\n"
            "    id XTfPKXJv5GEZUwAAinrl1Q\n"
            "    (envelope-from <bob@schulz.com.de>)\n"
            "    for <alice@schulz.com.de>; Sun, 16 Jan 2022 20:18:10 +0100\n"
            "Received: by schulz.com.de (from unknown)\n"
            "From: Bob <bob@schulz.com.de>\n"
            "To: Alice <alice@schulz.com.de>\n"
            "Subject: something in HTML!\n"
            "Message-ID: <6e81ef1b-3155-0eec-db00-48ba71eb173f@schulz.com.de>\n"
            "Date: Sun, 16 Jan 2022 20:18:06 +0100\n"
            "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101\n"
            " Thunderbird/68.5.0\n"
            "MIME-Version: 1.0\n"
            "Content-Type: multipart/alternative;\n"
            " boundary=\"------------FA76900A96BC675E2A527C2F\"\n"
            "Content-Language: de-DE"});
    const String body = String({
            "This is a multi-part message in MIME format.\n"
            "--------------FA76900A96BC675E2A527C2F\n"
            "Content-Type: text/plain; charset=utf-8\n"
            "Content-Transfer-Encoding: 7bit\n"
            "\n"
            "Hello Alice,\n"
            "\n"
            "this is in HTML! :-)\n"
            "\n"
            "some image\n"
            "\n"
            "Best,\n"
            "\n"
            "Bob\n"
            "\n"
            "\n"
            "--------------FA76900A96BC675E2A527C2F\n"
            "Content-Type: text/html; charset=utf-8\n"
            "Content-Transfer-Encoding: 7bit\n"
            "\n"
            "<html>\n"
            "  <head>\n"
            "    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\n"
            "  </head>\n"
            "  <body>\n"
            "    <p>Hello Alice,</p>\n"
            "    <p><font size=\"+2\">this is in HTML!</font> <span\n"
            "        class=\"moz-smiley-s1\"><span>:-)</span></span></p>\n"
            "    <p><img moz-do-not-send=\"true\"\n"
            "        src=\"https://maxcdn.icons8.com/Share/icon/Logos/duckduckgo1600.png\"\n"
            "        alt=\"some image\" width=\"219\" height=\"219\"></p>\n"
            "    <p>Best,</p>\n"
            "    <p><font color=\"#3d48ae\">Bob</font><br>\n"
            "    </p>\n"
            "  </body>\n"
            "</html>\n"
            "\n"
            "--------------FA76900A96BC675E2A527C2F--\""});
    const String full = head+"\n\n"+body+"\n";

    // splitIntoHeadAndBody
    {
        String h,b;
        std::tie(h,b) = EmailMessage::splitIntoHeadAndBody(full);
        assert(h == head);
        assert(b == body);
    }

    // parseHeadIntoTags
    {
        const auto tags = EmailMessage::parseHeadIntoTags(head);
        std::multimap<String,String> correct = std::multimap<String,String>(
        {
                {"return-path" , "bob@schulz.com.de"},
                {"delivered-to", "alice@schulz.com.de"},
                {"received"    , "from schulz.com.de" },
                {"received"    , "by schulz.com.de with LMTP"},
                {"received"    , "id XTfPKXJv5GEZUwAAinrl1Q"},
                {"received"    , "(envelope-from <bob@schulz.com.de>)"},
                {"received"    , "for <alice@schulz.com.de>"},
                {"received"    , "Sun, 16 Jan 2022 20:18:10 +0100"},
                {"received"    , "by schulz.com.de (from unknown)"},
                {"from"        , "Bob <bob@schulz.com.de>"},
                {"to"          , "Alice <alice@schulz.com.de>"},
                {"subject"     , "something in HTML!"},
                {"message-id"  , "6e81ef1b-3155-0eec-db00-48ba71eb173f@schulz.com.de"},
                {"date"        , "Sun, 16 Jan 2022 20:18:06 +0100"},
                {"user-agent"  , "Mozilla/5.0 (X11"},  /// @todo this should probably be one line – look up standard
                {"user-agent"  , "Linux x86_64"},
                {"user-agent"  , "rv:68.0) Gecko/20100101"},
                {"user-agent"  , "Thunderbird/68.5.0"},
                {"mime-version", "1.0"},
                {"content-type", "multipart/alternative"},
                {"content-type", "boundary=\"------------FA76900A96BC675E2A527C2F\""},
                {"content-language", "de-DE"}
        });
        assert(tags == correct);

    }

    // parseHeadTag
    {
        const auto tags = EmailMessage::parseHeadIntoTags(head);

        std::optional<String> content_type, content_type_boundary, content_type_invalid;
        EmailMessage::parseHeadTag(tags, CONTENT_TYPE.toLower(), content_type,{
                                       {"boundary", content_type_boundary},
                                       {"invalid" , content_type_invalid}
                                    }
                                   );
        assert(content_type.has_value());
        assert(content_type_boundary.has_value());
        assert( ! content_type_invalid.has_value() );
        assert(content_type.value() == "multipart/alternative");
        assert(content_type_boundary == "------------FA76900A96BC675E2A527C2F");
    }

    // parseMessagePart
    std::optional<std::vector<MessagePart*> > correctMessageParts;  // for use in later tests
    {
        // parse above example message
        {
            EmailMessage *message = new EmailMessage(0,0,full,nullptr); // cause a memory leak here (it is just a test) in
            assert(message->getMessageParts().size()==1);               // order to keep pointers in correctMessageParts valid
            TextInlineMessagePart *textMessagePart = dynamic_cast<TextInlineMessagePart*>(message->getMessageParts().at(0));
            assert(textMessagePart != nullptr);
            assert(textMessagePart->getDisposition() == MessagePart::Disposition::INLINE);
            assert(textMessagePart->getContentType() == TextInlineMessagePart::ContentType::HTML);
            /// @todo checktextMessagePart->getText()
            correctMessageParts = message->getMessageParts();
        }

        // parse nested multipart message
        {
            EmailMessage *message = new EmailMessage(0,0,
                                                     "Content-Type: multipart/mixed; boundary=\"outer\"\n"
                                                     "\n"
                                                     "--outer\n"
                                                     "  Content-Type: multipart/alternative; boundary=\"inner\"\n"
                                                     "\n"  /// @todo allow whitespace between these two line breaks
                                                     "  --inner\n"
                                                     "    Content-Type: text/plain\n\nfoo bar\n\n"
                                                     "  --inner\n"
                                                     "    Content-Type: text/html\n\n<html><body><h1>foo bar</h1></body></html>\n\n"
                                                     "  --inner--\n"
                                                     "  some garbage...\n"
                                                     "--outer\n"
                                                     "  Content-Type: image/just-for-testing\n\ndummy data\n\n"
                                                     "--outer--\n"
                                                     ,
                                                     nullptr);
            assert(message->getMessageParts().size()==2);
            TextInlineMessagePart *part0 = dynamic_cast<TextInlineMessagePart*>(message->getMessageParts()[0]);
            ImageInlineMessagePart *part1 = dynamic_cast<ImageInlineMessagePart*>(message->getMessageParts()[1]);
            assert(part0 != nullptr);
            assert(part1 != nullptr);
            assert(part0->getDisposition() == MessagePart::Disposition::INLINE);
            assert(part1->getDisposition() == MessagePart::Disposition::INLINE);
            assert(part0->getContentType() == TextInlineMessagePart::ContentType::HTML);
        }
    }

    // createFromRawFullMessage
    {
        // incoming message
        {
            Account account;
            account.setName("Alice");
            account.setEmail("alice@schulz.com.de");
            Contact contact(0, &account, "Bob", "bob@schulz.com.de");

            EmailMessage *message = EmailMessage::createFromRawFullMessage(full, &account);
            assert(message->getContact() == &contact);
            assert(message->getMessageId() == "6e81ef1b-3155-0eec-db00-48ba71eb173f@schulz.com.de");
            assert(message->getTimeStamp() ==
                   Time::textToTimeStamp("Sun, 16 Jan 2022 20:18:06 +0100", Time::emailDateFormat));
            assert(message->getRawFullMessage() == full);
            assert(message->getMessageParts().size() == correctMessageParts.value().size());
            //for(int i=0; i<correctMessageParts.value().size(); i++)
            //    assert(*message->getMessageParts().at(i) == *correctMessageParts.value().at(i));

        }
        // outgoing message
        {
            Account account;
            account.setName("Bob");
            account.setEmail("bob@schulz.com.de");

            EmailMessage *message = EmailMessage::createFromRawFullMessage(full, &account);
            Contact *contact = message->getContact();
            assert(contact->getAccount() == &account);
            assert(contact->getName() == "Alice");
            assert(contact->getEmail() == "alice@schulz.com.de");
            assert(message->getMessageId() == "6e81ef1b-3155-0eec-db00-48ba71eb173f@schulz.com.de");
            assert(message->getTimeStamp() ==
                   Time::textToTimeStamp("Sun, 16 Jan 2022 20:18:06 +0100", Time::emailDateFormat));
            assert(message->getRawFullMessage() == full);
            assert(message->getMessageParts().size() == correctMessageParts.value().size());
            //for(int i=0; i<correctMessageParts.value().size(); i++)
            //    assert(*message->getMessageParts().at(i) == *correctMessageParts.value().at(i));
        }
    }

    // no tests for constructor because it is mostly trivial

    return 0;
}
