/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/messages/InlineMessagePart.hpp"
#include "src/util/String.hpp"
#include <vector>

class EmailMessage;
class Account;

class Rfc822InlineMessagePart  :  public InlineMessagePart  {
    private:
        EmailMessage *message;
        std::optional<String> description;
    public:
        Rfc822InlineMessagePart(const std::vector<char> &data,
                                Account *account,
                                std::optional<String> _description);
        virtual ~Rfc822InlineMessagePart();

    public:
        EmailMessage *getMessage()  const;
        const std::optional<String> &getDescription()  const;
};
