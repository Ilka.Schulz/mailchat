/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <string>
#include <vector>
#include <list>
#include <algorithm>

class MessageCache;
class MessageCacheObserver;

#include "src/Contact.hpp"
#include "src/messages/Message.hpp"
#include "src/patterns/Observable.hpp"


/**
 * @brief The MessageCache class caches references to @c Message objects and keeps a sorted list of these references.
 * @details It only caches messages that meet its filter (e.g., see @c contact).
 * @todo implement limit (e.g. 100 messages for caches used by @c ChatWidget)
 */
class MessageCache  :  public Observable<MessageCacheObserver>, public MessageObserver  {
    private:
        /// static register of all existing objects of this class
        static std::vector<MessageCache*> allMessageCaches;
        /// sorted list of cached messages
        std::list<Message*> messages;

        // filters
        /// filter: Message::getContact() must equal this field
        Contact *contact;

    public:
        /** @brief constructor
         *  @param _contact  initializes @c MessageCache::contact
         */
        MessageCache(Contact *_contact);
        ~MessageCache();

        /// @returns @c MessageCache::contact
        Contact *getContact();
        /// @returns @c MessageCache::messages
        std::list<Message*> getMessages();

    public:
        /** @brief add Message to @c messages
         * @details the @c message is inserted into @c messages at the correct position
         * @param message  the @c Message to add to @c messages
         */
        virtual void messageEvent_constructedEvent(Message *message)  override;
        virtual void messageEvent_destructionStartedEvent(Message *message)  override;
        /// @todo handlers for contact changed (and maybe account changed)
};
