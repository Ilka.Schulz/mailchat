/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include "src/messages/MessagePart.hpp"

#include "src/util/String.hpp"


/// @todo TextMessagePartObserver needs to implement textMessagePartEvent_contentTypeChanged
class TextInlineMessagePart  :  public MessagePart  {
    public:
        enum ContentType  {
            PLAIN=1,
            HTML=2,
            WATCH_HTML=3
        };

    private:
        String text;
        ContentType contentType;

    public:
        TextInlineMessagePart(String _text, ContentType _contentType=PLAIN);

        void setText(const String &text);

        String getText()  const;
        ContentType getContentType()  const;

        virtual Disposition getDisposition()  const override;
        // NOTE: if implementing setText or setContentType, call the specific handlers
};
