/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <string>

class EmailMessage;
class String;
class Account;

#include "src/messages/Message.hpp"

/** @brief a @c Message sent over IMAP/SMTP
 *  @details This class is not derived from @c MessagePartObserver due to performace reasons (see @c Message
 *  documentation for further information)
 */
class EmailMessage  :  public Message  {
    public:
        /** @brief constructor
         *  @param _id              passed to @c Message
         *  @param _flags           passed to @c Message
         *  @param _rawFullMessage  passed to @c Message
         *  @param _contact         passed to @c Message
         *  @param _timeStamp       passed to @c Message
         *  @param _messageId       passed to @c Message
         */
        EmailMessage(MessageId _id, MessageFlags _flags, String _rawFullMessage, Contact *_contact,
                     std::time_t _timeStamp=0, String _messageId="");

        /** @brief constructs an object with the specified message parts
         */
        EmailMessage(MessageId _id, MessageFlags _flags, std::vector<MessagePart*> _messageParts, Contact *_contact,
                     std::time_t _timeStamp=0, String _messageId="");

    protected:
        // auxiliary stuff for email MIME parsing
        enum ReactionOptionToUnknownKey {THROW=1,IGNORE=2};  /// @todo option WARN

        static std::tuple<String,String> splitIntoHeadAndBody(String full);
        static std::multimap<String, String>  parseHeadIntoTags(String head);
        static void parseHeadTag(const std::multimap<String,String> &headerTags,
                                 const String key,
                                 std::optional<String> &mainValue,
                                 std::map<String, std::optional<String>&> otherValues,
                                 ReactionOptionToUnknownKey reactionToUnknownKey=THROW
                                 );
        static std::vector<MessagePart*>
        parseMessagePart(const std::multimap<String,String> &headerTags, String encodedBody);

    public:
        /** @brief pseudo-constructor
         *  @details This function constructs a @c EmailMessage object from a raw full message
         *  @param _rawFullMessage  email message source code (header and body)
         *  @param account          passed to @c Message::Message
         *  @returns the constructed object
         */
        static EmailMessage *createFromRawFullMessage(String _rawFullMessage, Account *account, bool shadow=false);

        /**
         *  @todo addons need to manipulate the content
         *  @todo support decryption
         */
        virtual void reparseRawFullMessage()  override;

    friend int src_messages_EmailMessage(int,char**);  // unit tests function
};
