/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

class MessageCacheObserver;
class MessageCache;
class Message;

/** @brief observer of @c MessageCache
 *  @details design pattern @c Observer
 */
class MessageCacheObserver  {
    protected:
        /** @brief a @c Message has been added to the observed @c MessageCache object
         *  @param messageCache  the observed @c MessageCache object
         *  @param pos           the position at which the event has been added (starting from zero)
         *  @param message       the @c Message object that has been added
         */
        virtual void messageCacheEvent_messageAdded(MessageCache *messageCache, int pos, Message *message);

        /** @brief a @c Message has been removed from the observed @c MessageCache object
         *  @param messageCache  the observed @c MessageCache object
         *  @param message       the removed @c Message object
         *  @todo parameter to indicate position
         */
        virtual void messageCacheEvent_messageRemoved(MessageCache *messageCache, Message *message);

    /// @c MessageCache is the observed class so it needs to call the protected event handlers
    friend MessageCache; // must call protected event handlers
};
