/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "AttachmentMessagePart.hpp"

AttachmentMessagePart::AttachmentMessagePart(const std::vector<char> &_data,
                                             const String &_name,
                                             const time_t _creation_date,
                                             const time_t _modification_date
                                             )
        : name(_name),
          data(_data),
          creation_date(_creation_date),
          modification_date(_modification_date)  {
    name.trimMatchingQuotes();
}

const String &AttachmentMessagePart::getName()  const  {
    return name;
}
const std::vector<char> &AttachmentMessagePart::getData() const  {
    return data;
}
time_t AttachmentMessagePart::getCreationDate() const  {
    return creation_date;
}
time_t AttachmentMessagePart::getModificationDate() const  {
    return modification_date;
}
AttachmentMessagePart::Disposition AttachmentMessagePart::getDisposition() const  {
    return Disposition::ATTACHMENT;
}
