/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "UnsupportedInlineMessagePart.hpp"
#include <assert.h>

UnsupportedInlineMessagePart::UnsupportedInlineMessagePart(const std::exception *_exception)
        : cause(EXCEPTION), exceptionWhat(_exception->what())  {
}
UnsupportedInlineMessagePart::UnsupportedInlineMessagePart(const String &_contentType,
                                                           const std::vector<char> &_data)
        : cause(CONTENT_TYPE), contentType(_contentType), data(_data)  {
}
UnsupportedInlineMessagePart::UnsupportedInlineMessagePart(const Cause _cause)
        : cause(_cause)  {
}


const UnsupportedInlineMessagePart::Cause &UnsupportedInlineMessagePart::getCause()  const  {
    return cause;
}
const String &UnsupportedInlineMessagePart::getExceptionWhat() const  {
    assert(cause == EXCEPTION);
    return exceptionWhat;
}
const String &UnsupportedInlineMessagePart::getContentType() const  {
    assert(cause == CONTENT_TYPE);
    return contentType;
}
const std::vector<char> &UnsupportedInlineMessagePart::getData() const  {
    assert(cause == CONTENT_TYPE);
    return data;
}
