/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/messages/MessageCache.hpp"
#include "src/messages/MessageCacheObserver.hpp"
#include "src/messages/Message.hpp"
#include "src/util/exceptions/Exception.hpp"


std::vector<MessageCache*> MessageCache::allMessageCaches;

MessageCache::MessageCache(Contact *_contact)
        : contact(_contact)  {
    // fill cache with existing messages
    for(Message *message : Message::getAllMessages())  {  /// @todo @c Message::getAllMessages is inefficient
        messageEvent_constructedEvent(message);
    }

    // register
    allMessageCaches.push_back(this);
}
MessageCache::~MessageCache()  {
    // unregister
    auto itr = std::find(allMessageCaches.begin(), allMessageCaches.end(), this);
    if(itr != allMessageCaches.end())
        allMessageCaches.erase(itr);
}

Contact *MessageCache::getContact()  {
    return contact;
}
std::list<Message*> MessageCache::getMessages()  {
    return messages;
}

void MessageCache::messageEvent_constructedEvent(Message *message)  {
    if(message->hasFlag(Message::FLAG_SHADOW))
        return ;
    if(message->getContact() == contact)  {
        int pos=0;
        std::list<Message*>::iterator itr = messages.begin();
        while(itr != messages.end())  {
            if((*itr)->getTimeStamp() > message->getTimeStamp())
                break;
            itr++;
            pos++;
        }
        messages.insert(itr, message);
        message->addObserver(this);
        for(MessageCacheObserver *observer : observers)
            observer->messageCacheEvent_messageAdded(this, pos, message);
    }
}
void MessageCache::messageEvent_destructionStartedEvent(Message *message)  {
    /// @todo
    auto itr = std::find(messages.begin(), messages.end(), message);
    if(itr != messages.end())  {
        messages.erase(itr);
        message->removeObserver(this);
        for(MessageCacheObserver *observer : observers)
            observer->messageCacheEvent_messageRemoved(this, message);
    }
    else
        throw Exception("trying to remove message from message cache that was never added");
}
