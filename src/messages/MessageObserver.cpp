/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MessageObserver.hpp"
#include <algorithm>


std::vector<MessageObserver*> MessageObserver::allMessageObservers;

MessageObserver::MessageObserver()  {
    allMessageObservers.push_back(this);
}
MessageObserver::~MessageObserver()  {
    auto itr = std::find(allMessageObservers.begin(), allMessageObservers.end(), this);
    if(itr != allMessageObservers.end())
        allMessageObservers.erase(itr);
}

std::vector<MessageObserver*> MessageObserver::getAllMessageObservers()  {
    return allMessageObservers;
}

// do nothing by default
void MessageObserver::messageEvent_constructedEvent(Message *)  { }

void MessageObserver::messageEvent_destructionStartedEvent(Message *)  { }
void MessageObserver::messageEvent_messagePartAddedEvent(Message *, MessagePart *)  { }
void MessageObserver::messageEvent_timeStampChangedEvent(Message *)  { }
void MessageObserver::messageEvent_messageIdChangedEvent(Message *)  { }
