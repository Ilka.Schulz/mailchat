/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>

class MessageObserver;
class Message;
class IncomingMessage;
class OutgoingMessage;
class MessagePart;

/** @brief observer of @c Message
 *  @details design pattern @c Observer
 */
class MessageObserver  {
    private:
        /** @brief static register of all existing @c MessageObserver objects
         *  @details This list is necessary for messageEvent_constructedEvent and messageEvent_destructionStartedEvent
         *  because they are called on all existing observers (that did not have the chance yet to register as observer
         *  for the newly created @c Message.
         */
        static std::vector<MessageObserver*> allMessageObservers;

    protected:
        /// default constructor (registers object)
        MessageObserver();

        /// destructor (deregisters object)
        virtual ~MessageObserver();

    public:
        /// @brief returns allMessageObservers @return copy of allMessageObservers
        static std::vector<MessageObserver*> getAllMessageObservers();
    public:

        /** @brief new @c Message has been constructed
         *  @details This handler is called by the derived classes' constructors on ALL existing @c MessageObserver
         *  objects because they did not have any chance yet to register with the @c Observable. It is not called by @c
         *  Message::Message().
         *  @param message  the ~~observed~~ newly created object of a class derived by @c Message
         */
        virtual void messageEvent_constructedEvent(Message *message);

        /** @brief the observed @c Message object is about to be destructed
         *  @param message the observed @c Message object
         */
        virtual void messageEvent_destructionStartedEvent(Message *message);

        /** @brief a message parts has been added
         *  @details This event can happen quite often on the same object, e.g. by content processors that translate /
         *  decrypt some message parts
         *  @param message      the observed @c Message object
         *  @param messagePart  the newly added @c MessagePart object
         */
        virtual void messageEvent_messagePartAddedEvent(Message *message, MessagePart *messagePart);

        /** @brief the time stamp has been changed
         *  @param message  the observed @c Message object
         */
        virtual void messageEvent_timeStampChangedEvent(Message *message);

        /** @brief the message id has changed
         *  @todo This should never been called and observers should not care – maybe remove it?
         *  @param message  the observed @c Message object
         */
        virtual void messageEvent_messageIdChangedEvent(Message *message);
};
