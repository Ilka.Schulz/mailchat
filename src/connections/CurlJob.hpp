/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <curl/curl.h>
#include <vector>
#include "src/util/String.hpp"

class CurlJob;
class Connection;
class Exception;

/** @brief throws an @c Exception if the specified CURL (easy) result code is not healthy
 *  @param code  the result code to check
 *  @todo choose name without "assert"
 */
#define assertCurlOK(code)  { \
    if(code != CURLE_OK) \
        throw Exception(String(curl_easy_strerror(code))+" (error code "+std::to_string(code)+") occured in "+String(__PRETTY_FUNCTION__)); \
}

/// handler called by Application::run()'s main loop  (just a wrapper for the static method CurlJob::performAsync())
void processCurlJobs();

/// @todo consider deriving classes from this to add additional job-specific data (e.g. the name of a folder for a folder-scanning job type)
/// @todo implement static function that tells the separate thread in performLoop() no to accept new jobs and tries to joing the threads with a certain timeout.
/// @todo if the threads can not be joined within the timeout, write jobs to be performed to disk to continue them later
/// @todo restrict number of concurrent jobs
class CurlJob  {
    private:
        /// curl multi handle for all instances of CurlJob
        static CURLM *mcurl;
        /// list of all existing instances
        static std::vector<CurlJob*> allCurlJobs;

    public:
        // data to be set/read by users
        /// @todo write get/set functions

        /// handle to already initialized libcurl easy(!) handle
        CURL *curl;
        /// raw data received by the remote server
        String incomingData;

        /// number of bytes read from @c CurlJob::outgoingData
        size_t bytes_read;
        /// raw data to sent to the remote server
        /// if the calling code wants to upload data, it must also set CURLOPT_UPLOAD
        String outgoingData;
        /// whether the job is done (but the class will be destructed immediately when deleteWhenFinished option is true)
        bool done;
        /// result of the curl perform (may have any invalid value as long as the job is not done)
        CURLcode result;
        /**
         * callback function to call by the job scheduler as soon as job is done
         * set to nullptr to call no callback function on job finish
         * @param data  a pointer to an object of this class.
         */
        void (*callbackOnFinish)(CurlJob *CurlJob);

        /// if true: this objects will automatically be destroyed as soon as it has finished (after
        /// @c CurlJob::calbackOnFinish has been called)
        bool deleteWhenFinished;

    public:
        /** @brief completely initializes a job but does not perform it.
         *  @details The calling code can still make changes to the libcurl (easy) handle and add the job to the job
         *  queue with start().
         *  @param _curl  CURL (easy) handle
         *  @param _deleteWhenFinished  if true: the scheduler will destroy this object after it is done
         */
        CurlJob(CURL *_curl, bool _deleteWhenFinished);

    protected:
        /** @brief private destructor because event handlers (may) destroy the element and users should only construct
         *  it dynamically
         *  @todo It is possible that objects exist that have never been started and are dangling in the allCurlJobs
         *  vector...
         *  @todo It is also possible that finished objects do not destroyed.
         */
        virtual ~CurlJob();

    private:
        /** @brief callback function to handle data received from remote (writes them to CurlJob::incomingData)
         *  @details This method should not be called directly but only by libcurl as a callback function. \n
         *  documentation: https://curl.se/libcurl/c/CURLOPT_WRITEFUNCTION.html
         *  @param buffer         buffer holding data read by libcurl
         *  @param size           no meaning
         *  @param nitems         number of bytes read by libcurl
         *  @param user_data_ptr  this pointer
         *  @returns number of read bytes
         */
        static size_t libCurlWriteCallback(char *buffer, size_t size, size_t nitems, void* user_data_ptr);

        /** @brief callback function to send data to libcurl
         *  @param buffer         buffer to write new data to send to
         *  @param size           size of one block to write to buffer
         *  @param nitems         number of block to write to buffer
         *  @param user_data_ptr  this pointer
         *  @returns number of bytes written to buffer
         */
        static size_t libcurlReadCallback(char *buffer, size_t size, size_t nitems, void *user_data_ptr);

        /// feeds libcurl in multi-mode asynchronously and return soon. Needs to be called repeatedly, preferably at least once per second.
        static void performAsync();

    public:
        /// @returns @c CurlJob::allCurlJobs
        static std::vector<CurlJob*> getAllCurlJobs();

        /// runs the job  @param block wether to block
        virtual void start(bool block=false);

    /// @c ::processCurlJobs needs to call @c CurlJob::performAsync()
    friend void processCurlJobs();
};
