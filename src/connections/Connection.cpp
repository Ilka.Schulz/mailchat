/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Connection.hpp"
#include <vector>
#include <algorithm>
#include "src/util/exceptions/Exception.hpp"
#include "src/Account.hpp"
#include "src/messages/Message.hpp"
#include <filesystem>
#include <iostream>
#include "src/util/Properties.hpp"

#include "SmtpConnection.hpp"
#include "ImapConnection.hpp"
#include "EmailConnection.hpp"

const String Connection::propertyKeyShadow = "Connection::shadow";
const String Connection::propertyKeyType   = "Connection::type";
std::vector<Connection*> Connection::allConnections;

Connection::Connection(Account *_account, Properties properties)
        : account(_account)  {
    // register
    if( ! properties.has(propertyKeyShadow) ||
          properties.get(propertyKeyShadow).unequal(TristateBool(true).toString(),PLAIN) )
        allConnections.push_back(this);
    // read data from properties
    Connection::setProperties(properties);
}
Connection::Connection(Account *_account, bool shadow)
        : account(_account)  {
    // register
    if(!shadow)
        allConnections.push_back(this);
}
Connection::~Connection()  {
    auto itr = std::find(allConnections.begin(), allConnections.end(), this);
    if( itr != allConnections.end() )
        allConnections.erase(itr);
}


std::vector<Connection*> Connection::getAllConnections()  {
    return allConnections;
}


String Connection::getType()  const  {
    return getStaticType();
}
String Connection::getStaticType()  {
    return "Connection";
}


Account *Connection::getAccount()  {
    return account;
}
void Connection::setProperties(Properties properties)  {
}
// only relevant for derived classes that have additional properties
Properties Connection::getProperties()  {
    Properties properties;
    properties.set(propertyKeyType, getType());
    return properties;
}

void Connection::sendMessage(Message *message, bool block)  {
    if(message->hasFlag(Message::FLAG_SYNCHRONIZED))
        throw Exception("trying to send a message that is already marked synchronized");
    if(account!=nullptr)  {
        std::vector<Contact*> contactList = account->getContacts();
        if( std::find(contactList.begin(), contactList.end(), message->getContact()) == contactList.end() )
            throw Exception("cannot send message: recipient is not in account's contact list");
    }
}
void Connection::fetchIncomingMessages(bool block)  {
}
void Connection::test(bool block)  {
    // this class does not implement any functionaly so there is none that can not be supported
    return /* without an exception */;
}
