/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

#include <string>
#include "src/util/TristateBool.hpp"
#include <map>
#include "src/connections/ImapUid.hpp"
#include "src/util/Properties.hpp"
#include "src/messages/Message.hpp"

/** @brief structure holding information about an IMAP folder such as name or number of unread messages
 *  @todo define proper class with get and set methods, etc.
 *  @todo keep list of fetched UIDs
 *  @todo property NoSelect (or so)
 *  @todo property NoInferiors (or so)
 *  @todo property Referral (or so)
 */
struct ImapFolder  {
    static const String propertyKeyName;            ///< property key for @c ImapFolder::name
    static const String propertyKeyHasChildren;     ///< property key for @c ImapFolder::hasChildren
    static const String propertyKeyIsSent;          ///< property key for @c ImapFolder::isSent
    static const String propertyKeyIsTrash;         ///< property key for @c ImapFolder::isTrash
    static const String propertyKeyMarked;          ///< property key for @c ImapFolder::marked
    static const String propertyKeyNRemoteMessages; ///< property key for @c ImapFolder::nRemoteMessages
    static const String propertyKeyNRemoteRecent;   ///< property key for @c ImapFolder::nRemoteRecent
    static const String propertyKeyNRemoteUnseen;   ///< property key for @c ImapFolder::nRemoteUnseen
    static const String propertyKeyUidStates;       ///< property key for @c ImapFolder::uidStates


    /// name and identifier of the folder
    String name;
    /// if true: this folder has other folders as children
    TristateBool hasChildren;
    /// if true: this folder is the "sent" folder where to move sent messages
    TristateBool isSent;
    /// if true: this folder is the "trash" folder where to move deleted messages
    TristateBool isTrash;
    /// if true: server has told us that the directory has unread messages
    TristateBool marked;
    /// number of messages in the REMOTE folder
    int nRemoteMessages;
    /// number of recent messages in the REMOTE folder
    int nRemoteRecent;
    /// number of unseen messages in the REMOTE folder
    int nRemoteUnseen;
    /// register of which @c ImapUids has been downloaded yet and which has not
    std::map<ImapUid, Message::MessageId> uidStates;



    ImapFolder();
    /// constructor
    /// @param properties  the @c Properties to set (see static @c propertyKey* members for valid keys)
    ImapFolder(Properties properties);

    /** @brief get information about a @c ImapUid
     *  @param uid  the @c ImapUid in question (should be a valid UID of this @c ImapFolder)
     *  @returns whether the message with this UID has been fetched
     */
    bool isUidFetched(ImapUid uid);

    /** @brief assignment operator
     *  @param source  source to copy
     *  @returns this object
     */
    ImapFolder &operator=(ImapFolder &source);

    /// @returns a @c Properties list (see static @c propertyKey* members for valid keys)
    Properties getProperties();
    /// adjust object according to specified properties
    /// @param properties  the @c Properties to set (see static @c propertyKey* members for valid keys)
    void setProperties(Properties properties);
};
