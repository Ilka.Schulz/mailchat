/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "GenericConnection.hpp"

#include "src/util/exceptions/Exception.hpp"

#include "Connection.hpp"
#include "SmtpConnection.hpp"
#include "ImapConnection.hpp"
#include "EmailConnection.hpp"


GenericConnection::GenericConnection(Account *account, Properties properties)
        :  ConnectionInterface()  {
    // create impl object
    String type = properties.get(Connection::propertyKeyType);
    if( type.equals(SmtpConnection::getStaticType(),PLAIN) )
        impl = new SmtpConnection(account, properties);
    else if( type.equals(ImapConnection::getStaticType(),PLAIN) )
        impl = new ImapConnection(account, properties);
    else if( type.equals("EmailConnection",PLAIN) )
        impl = new EmailConnection(account, properties);
    else
        throw Exception("unknown Connection type: \""+type+"\"");
}
GenericConnection::~GenericConnection()  {
    delete impl;
}
Message *GenericConnection::composeMessage(String content, Contact *contact)  {
    return impl->composeMessage(content, contact);
}

Properties GenericConnection::getProperties()  {
    return impl->getProperties();
}
void GenericConnection::setProperties(Properties properties)  {
    return impl->setProperties(properties);
}
String GenericConnection::getType() const  {
    return impl->getType();
}
void GenericConnection::sendMessage(Message *message, bool block)  {
    return impl->sendMessage(message, block);
}
void GenericConnection::fetchIncomingMessages(bool block)  {
    return impl->fetchIncomingMessages(block);
}
void GenericConnection::test(bool block)  {
    return impl->test(block);
}
