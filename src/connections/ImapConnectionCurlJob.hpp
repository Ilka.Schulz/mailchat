/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

class FolderSpecificImapConnectionCurlJob;
class SendMessageImapConnectionCurlJob;
class ImapFolder;
class Message;

#include "src/connections/ConnectionCurlJob.hpp"

/// @c ImapConnectionCurlJob is a @c ConnectionCurlJob for @c ImapConnection objects
typedef ConnectionCurlJob ImapConnectionCurlJob;


/** @brief an @c ImapConnectionCurlJob that is folder-specific, e.g. fetching attributes of a specific folder
 */
class FolderSpecificImapConnectionCurlJob  :  public ImapConnectionCurlJob  {
    private:
        /**
         * @brief the name of the folder which is passed to the folder-specific command
         * @details This is required because the callback function handling the result would not know the folder name
         * otherwise.
         * @todo Technically, it could read it from the server's answer but I can implement that another time.
         */
        String folderName;

    public:
        /** @brief constructor
         *  @param _curl                passed to @c ImapConnectionCurlJob
         *  @param _deleteWhenFinished  passed to @c ImapConnectionCurlJob
         *  @param _connection          passed to @c ImapConnectionCurlJob
         *  @param _folderName          name of the associated folder – initializes
         *      @c FolderSpecificImapConnectionCurlJob::folderName
         */
        FolderSpecificImapConnectionCurlJob(CURL *_curl, bool _deleteWhenFinished, Connection *_connection, String _folderName);

    protected:
        virtual ~FolderSpecificImapConnectionCurlJob();

    public:
        /// @returns @c FolderSpecificImapConnectionCurlJob::folderName
        String getFolderName();
};

/** @brief a @c ImapConnectionCurlJob to put a @c Message object into a folder (typically "Sent" folder)
 */
class SendMessageImapConnectionCurlJob  :  public ImapConnectionCurlJob  {
    private:
        /// the @c Imapfolder to put the @c Message into
        ImapFolder *folder;
        /// the @c Message object to upload to the IMAP server
        Message *message;

    public:
        /** @brief constructor
         *  @param _curl                passed to @c ImapConnectionCurlJob::ImapConnectionCurlJob
         *  @param _deleteWhenFinished  passed to @c ImapConnectionCurlJob::ImapConnectionCurlJob
         *  @param _connection          passed to @c ImapConnectionCurlJob::ImapConnectionCurlJob
         *  @param _folder              initializes @c SendMessageImapConnectionCurlJob::folder
         *  @param _message             initializes @c SendMessageImapConnectionCurlJob::message
         */
        SendMessageImapConnectionCurlJob(CURL *_curl, bool _deleteWhenFinished, Connection *_connection,
                                         ImapFolder *_folder, Message *_message);

    public:
        /// @returns @c SendMessageImapConnectionCurlJob::folder
        ImapFolder *getFolder();
        /// @returns @c SendMessageImapConnectionCurlJob::message
        Message *getMessage();
};
