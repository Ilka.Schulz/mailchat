/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include "src/util/TristateBool.hpp"
#include <curl/curl.h>

class ImapConnection;

#include "src/connections/Connection.hpp"
#include "src/connections/ImapFolder.hpp"
#include "src/connections/ImapConnectionCurlJob.hpp"


/** @brief a @c Connection that uses the IMAP protocol
 *  @details useful overview over the most important IMAP commands: https://busylog.net/telnet-imap-commands-note/ \n
 *  IMAP URL scheme: https://datatracker.ietf.org/doc/html/rfc5092 and https://stackoverflow.com/questions/27115807/c-libcurl-imap-fetch-header-fields \n
 *  @todo implement some method (maybe sendMessage(OutgoingMessage *message)) to put sent messages in the Sent folder
 */
class ImapConnection  :  public Connection  {
        static const String propertyKeyHost;      ///< property key for @c ImapConnection::host
        static const String propertyKeyUsername;  ///< property key for @c ImapConnection::username
        static const String propertyKeyPassword;  ///< property key for @c ImapConnection::password
        static const String propertyKeyFolders;   ///< property key for @c ImapConnection::folders

    private:
        // persistent  (saveable to host)
        /// resolvable host running the IMAP server
        String host;
        /// username for IMAP authentication
        String username;
        /// password for IMAP authentication
        String password;

        // non-persistent (not saveable to host)
        /// list of IMAP folders existing on the remote server
        std::vector<ImapFolder*> folders;  // should be no pointer but I cannot get to solve this bug: https://stackoverflow.com/questions/64758775/result-type-must-be-constructible-from-value-type-of-input-range-when-trying-t

    public:
        /** @brief constructor
         *  @param account     passed to @c Connection::Connection
         *  @param properties  the @c Properties to assign to this object (see @c ImapConnection::setProperties)
         */
        ImapConnection(Account *account, Properties properties=Properties());
        ImapConnection(Account *account, bool shadow, String host, String username, String password);
        ~ImapConnection();

        virtual Message *composeMessage(String content, Contact *contact);

    public:
        /** @brief constructs new CURL (easy) handle that contains commonly used standard settings (e.g. host,username,
         *  password)
         *  @details This method is used public because other schedulers like ImapFetchJob may need to create a handle.
         *  @returns the constructed handle
         */
        CURL *newCurl();

    public:
        /// set @c ImapConnection::host  @param host  the hostname to set
        void setHost(String host);
        /// set @c ImapConnection::username  @param username  the username to set
        void setUsername(String username);
        /// set @c ImapConnection::password  @param password  the password to set
        void setPassword(String password);
        /// @returns @c ImapConnection::host
        String getHost();
        /// @returns @c ImapConnection::username
        String getUsername();
        /// @returns @c ImapConnection::password
        String getPassword();

    public:
        // implements interface of Connection base class
        /// @returns the @c Properties describing this object (see @c ImapConnection::setProperties for valid keys)
        virtual Properties getProperties()  override;

        /** @brief changes this object according to specified properties
         *  @param properties  the properties to set
         *  @details allowed properties: \n
         *      - ImapConnection::host      value of @c ImapConnection::host \n
         *      - ImapConnection::username  value of @c ImapConnection::username \n
         *      - ImapConnection::password  value of @c ImapConnection::password \n
         *      - all properties of base classes
         */
        virtual void setProperties(Properties properties)  override;

        /// wrapper for @c ImapConnection::getStaticType  @returns return value of wrapped function
        virtual String getType()  const override;
        /// @returns @c "ImapConnection"
        static String getStaticType();

    private:
        /** @brief runs a LIST command to fetch the latest folder list
         *  @param block  whether to block
         */
        void updateFolderList(bool block);

        /** @brief callback function when the @c ImapCurlJob constructed in @c ImapConnection::updateFolderList is done
         *  @param curlJob  the @c ImapCurlJob that has finished
         */
        static void updateFolderListDoneCallback(CurlJob *curlJob);

        /** @brief runs a STATUS command for each IMAP folder to get basic information about messages stored in those
         *  folders (but not heir UIDs, use fetchUIDLists() for that)
         *  @param block  whether to block
         */
        void statusJobFolders(bool block);
        /** @brief callback function when the @c ImapCurlJob constructed in @c ImapConnection::statusJobFolders is done
         *  @param curlJob  the @c ImapCurlJob that has finished
         */
        static void statusJobDoneCallback(CurlJob *curlJob);

        /** @brief runs a UID SEARCH command for each folder to get a complete lists of existing messages
         *  @param block  whether to block
         */
        void fetchUIDLists(bool block);
        /** @brief callback function when the @c ImapCurlJob constructed in @c ImapConnection::fetchUIDLists is done
         *  @param curlJob  the @c ImapCurlJob that has finished
         */
        static void fetchUIDListsDoneCallback(CurlJob *curlJob);

        /** @brief callback function when the @c SendMessageImapConnectionCurlJob constructed in
         *  @c ImapConnection::sendMessage is done
         *  @param curlJob  the @c SendMessageImapConnectionCurlJob that has finished
         */
        static void examineDoneCallback(CurlJob *curlJob);
        /** @brief callback function when the @c SendMessageImapConnectionCurlJob constructed in
         *  @c ImapConnection::examineDoneCallback is done
         *  @param curlJob  the @c SendMessageImapConnectionCurlJob that has finished
         */
        static void appendDoneCallback(CurlJob *curlJob);

        static void testDoneCallback(CurlJob *curlJob);

    public:
        // implements interface of Connection base class

        /// @details This method's callback function is @c ImapConnection::examineDoneCallback
        virtual void sendMessage(Message *message, bool block)  override;

        /// implements @c Connection::fetchIncomingMessages  @param block  whether to block
        virtual void fetchIncomingMessages(bool block)  override;
        virtual void test(bool block) override;

};
