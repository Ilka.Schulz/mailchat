/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/connections/ImapFetchJob.hpp"
#include <algorithm>
#include "src/connections/ImapConnection.hpp"
#include "src/connections/ImapFolder.hpp"
#include "src/Application.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/exceptions/macros.hpp"
#include <boost/algorithm/string.hpp>
#include <sstream>
#include "src/messages/Message.hpp"
#include "src/messages/Message.hpp"
#include "src/Account.hpp"
#include "src/Contact.hpp"
#include "src/connections/EmailConnection.hpp"
#include "src/settings.hpp"
#include <iostream>
#include "src/messages/EmailMessage.hpp"


bool ImapFetchJob::schedulerRegistered=false;
std::vector<ImapFetchJob*> ImapFetchJob::allImapFetchJobs;
std::vector<ImapFetchJob*> ImapFetchJob::runningImapFetchJobs;


void processImapFetchJobs()  {
    /*if( ! ImapFetchJob::allImapFetchJobs.empty() &&
        ImapFetchJob::runningImapFetchJobs.empty() )
        ImapFetchJob::allImapFetchJobs[0]->start(false);*/

    while(ImapFetchJob::runningImapFetchJobs.size() < Settings::concurrentImapFetchJobs
    && ImapFetchJob::allImapFetchJobs.size() > ImapFetchJob::runningImapFetchJobs.size())  {
        for( ImapFetchJob *job : ImapFetchJob::allImapFetchJobs )  {
            bool running=false;
            for(ImapFetchJob *runningJob : ImapFetchJob::runningImapFetchJobs)
                if( job == runningJob )  {
                    running = true;
                    break;
                }
            if(!running)  {
                job->start();
                break;
            }
        }
    }
}


// ======================================== ImapFetchCurlJob ========================================
ImapFetchCurlJob::ImapFetchCurlJob(CURL *_curl, bool _deleteWhenFinished, ImapFetchJob *_imapFetchJob)
        :  CurlJob(_curl, _deleteWhenFinished), imapFetchJob(_imapFetchJob)  {
}
ImapFetchJob *ImapFetchCurlJob::getImapFetchJob()  {
    return imapFetchJob;
}


// ======================================== ImapFetchBodyCurlJob ========================================
ImapFetchBodyCurlJob::ImapFetchBodyCurlJob(CURL *_curl, bool _deleteWhenFinished, ImapFetchJob *_imapFetchJob)
        : ImapFetchCurlJob(_curl, _deleteWhenFinished, _imapFetchJob)  {

}

// ======================================== ImapFetchJob ========================================
ImapFetchJob::ImapFetchJob(ImapConnection *_imapConnection, ImapFolder *_imapFolder, ImapUid _uid)
        :  imapConnection(_imapConnection), imapFolder(_imapFolder), uid(_uid), curlJob(nullptr)  {
    allImapFetchJobs.push_back(this);
    if(!schedulerRegistered)  {
        Application::getInstance()->addHandler(processImapFetchJobs);
        schedulerRegistered = true;
    }
}
ImapFetchJob::~ImapFetchJob()  {
    /// @todo for all classes that register pointers in a global vector: throw exception if(itr==allImapFetchJobs.end())
    if(curlJob != nullptr)
        delete curlJob;
    auto itr = std::find(allImapFetchJobs.begin(), allImapFetchJobs.end(), this);
    if(itr != allImapFetchJobs.end())
        allImapFetchJobs.erase(itr);
    itr = std::find(runningImapFetchJobs.begin(), runningImapFetchJobs.end(), this);
    if(itr != runningImapFetchJobs.end())
        runningImapFetchJobs.erase(itr);
}

std::vector<ImapFetchJob*> ImapFetchJob::getAllImapFetchJobs()  {
    return allImapFetchJobs;
}

void ImapFetchJob::start()  {
    // check if already running
    if(std::find(runningImapFetchJobs.begin(), runningImapFetchJobs.end(), this) != runningImapFetchJobs.end())
        throw Exception("cannot start ImapFetchJob: already running");
    // register
    runningImapFetchJobs.push_back(this);
    // create curl job
    curlJob = new ImapFetchCurlJob(imapConnection->newCurl(), false, this);  // do not automatically delete because destructor must delete finished AND dangling ImapFetchCurlJob instances

    char *encodedFolderName_cstr = curl_easy_escape(curlJob->curl, imapFolder->name.c_str(), imapFolder->name.size());  /// @todo this same monstrum is in ImapConnection
    String encodedFolderName = encodedFolderName_cstr;
    delete[] encodedFolderName_cstr;

    String url = "imaps://"+imapConnection->getHost()+
                      ":"+std::to_string(Settings::imapPort)+
                      "/"+encodedFolderName+
                      "/;UID="+std::to_string(uid)+
                      "/;SECTION=HEADER.FIELDS%20(From%20Message-ID%20To%20Date)";
    curl_easy_setopt(curlJob->curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curlJob->curl, CURLOPT_VERBOSE, Settings::imapCurlVerbose);
    curlJob->callbackOnFinish = fetchHeadersDoneCallback;
    // start
    curlJob->start(false);  // this class does not block
}


/// @todo so many magic strings here...
void ImapFetchJob::fetchHeadersDoneCallback(CurlJob *curlJob)  {
    ImapFetchCurlJob *job = dynamic_cast<ImapFetchCurlJob*>(curlJob);  // this object will be probably be deleted later in this function
    if(job == nullptr)
        throw std::bad_cast();
    ImapFetchJob *this_ = job->imapFetchJob;  // practically this pointer
    assertCurlOK(job->result);

    /// @todo decide on proper handling for messages without message-ids instead of ignoring them, see https://gitlab.com/Ilka.Schulz/mailchat/-/issues/39
    /*// parse response
    /// @todo use code in EmailMessage::createFromRawFullMessage
    std::map<String, String> fields;
    for(const String &line : job->incomingData.splitIntoLines(SKIP_EMPTY_LAST))  {
        const std::vector<String> parts = line.split(':',PLAIN);  /// @todo respect quotes?
        if(parts.size()!=2)
            ERROR(ParseException,"invalid email header line: >"+line+"< (no colon)");
        String key = parts.at(0),
               val = parts.at(1);
        fields[key.toLower()] = val;
    }
    // sanity check on parsed headers
    if( fields.find("message-id") == fields.end()  ||  fields["message-id"].empty() )  {
        //throw Exception("invalid email header: >"+job->incomingData+"< (no Message-ID field)");
        // just ignore messages without message-id and do not continue to fetch body
        this_->curlJob->deleteWhenFinished = true;
        this_->curlJob = nullptr;
        delete this_;
        return ;
    }
    // removed quotes, etc. from headers
    fields["message-id"].trimMatchingQuotes();*/

    /// @todo check if this message is already downloaded
    bool exists=false;

    //std::cout << "fetched headers for "<<fields["message-id"]<<" " <<
    //             (exists?"(exists already)":"(new message)")<<std::endl;

    if( ! exists )  {
        // remove reference to old CurlJob
        this_->curlJob->deleteWhenFinished=true;  // equals `job` // do not delete directly because this would make the scheduler of CurlJob read a trash value for deleteWhenFinished and attempt to delete it again
        this_->curlJob = nullptr;

        // create new CurlJob to fetch body
        this_->curlJob = new ImapFetchBodyCurlJob(this_->imapConnection->newCurl(), false, this_);

        char *encodedFolderName_cstr = curl_easy_escape(curlJob->curl, this_->imapFolder->name.c_str(), this_->imapFolder->name.size());  /// @todo this same monstrum is in ImapConnection
        String encodedFolderName = encodedFolderName_cstr;
        delete[] encodedFolderName_cstr;

        curl_easy_setopt(this_->curlJob->curl, CURLOPT_URL, ("imaps://"+this_->imapConnection->getHost()+
                                                             ":"+std::to_string(Settings::imapPort)+
                                                             "/"+encodedFolderName+
                                                             "/;UID="+std::to_string(this_->uid)
                                                             ).c_str());

        this_->curlJob->callbackOnFinish = fetchBodyDoneCallback;

        this_->curlJob->start(false);
    }
    else  {
        this_->curlJob->deleteWhenFinished = true;
        this_->curlJob=nullptr;
        delete this_;
    }
}

void ImapFetchJob::fetchBodyDoneCallback(CurlJob *curlJob)  {
    ImapFetchBodyCurlJob *job = dynamic_cast<ImapFetchBodyCurlJob*>(curlJob);  // this object will probably be deleted later in this function
    if(job == nullptr)
        throw std::bad_cast();
    ImapFetchJob *this_ = job->imapFetchJob;  // practically this pointer
    assertCurlOK(curlJob->result);

    // generate Message object
    Message *message = EmailMessage::createFromRawFullMessage(job->incomingData, this_->imapConnection->getAccount());

    // register this UID as read
    this_->imapFolder->uidStates[this_->uid] = message->getId();

    // destruct
    curlJob->deleteWhenFinished = true;
    this_->curlJob = nullptr;
    delete this_;
}
