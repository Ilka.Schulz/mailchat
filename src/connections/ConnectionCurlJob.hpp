/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "CurlJob.hpp"

class ConnectionCurlJob;
class Connection;


/** @brief a @c CurlJob that is associated with a @c Connection object
 *  @details This class basically does nothing more than its base class than holding a reference to the @c Connection
 *  object that created it / is associated with it.
 */
class ConnectionCurlJob  :  public CurlJob  {
    public:
        /// pointer to the Connection instance that constructed this job
        /// @todo make private
        Connection *connection;

    public:
        /** @brief constructor
         *  @param _curl                passed to @c CurlJob
         *  @param _deleteWhenFinished  passed to @c CurlJob
         *  @param _connection          the @c Connection associated with this object
         */
        ConnectionCurlJob(CURL *_curl, bool _deleteWhenFinished, Connection *_connection);

        /// @return @c ConnectionCurlJob::connection field
        Connection *getConnection()  const;
};
