/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file
#pragma once

#include <vector>

class ImapFetchJob;
class ImapConnection;
class ImapFolder;
class CurlJob;

#include "src/connections/ImapUid.hpp"
#include "src/connections/CurlJob.hpp"

/** @brief scheduler for picking up queued @c ImapFetchJob objects and starting them.
 *  @details This schedular must be registered to @c Application's handler list but it is not by default. The
 *  constructor of @c ImapFetchJob registers it if it has not been registered. This way, it is only used if the
 *  application actually has (or had during runtime) at least one @c ImapFetchJob object.
 */
void processImapFetchJobs();

/** @brief a @c CurlJob that calls the callback handlers of @c ImapFetchJob
 *  Design pattern @c Command: inherited from @c CurlJob \n
 *  Design pattern @c Proxy: This is the "real subject" of the "proxy" class @c ImapFetchJob.
 */
class ImapFetchCurlJob  :  public  CurlJob  {
    private:
        /// @brief reference to the "proxy" ImapFetchJob
        /// @details This reference is needed in order to let @c ImapFetchJob's handler know on which object to operate
        ImapFetchJob *imapFetchJob;

    protected:
        /** @brief constructs a new @c CurlJob
         *  @details This constructor does *not* add special behavior to the base class' constructor's behavior, e.g. it
         *  does not immediately start the job or anything like that. It is private because this class is only supposed
         *  to be used by @c ImapFetchJob
         *  @param _curl  passed to base class
         *  @param _deleteWhenFinished passed to base class
         *  @param _imapFetchJob initializes @c imapFetchJob;
         */
        ImapFetchCurlJob(CURL *_curl, bool _deleteWhenFinished, ImapFetchJob *_imapFetchJob);

    public:
        /// @return unaltered @c imapFetchJob
        ImapFetchJob *getImapFetchJob();

    friend ImapFetchJob;  ///< This class is only supposed to be used by ImapFetchJob (therefore the private constructor)
};

/** @brief an @c ImapFetchCurlJob to fetch a message *body* when the headers are already fetched
 *  @details It is useful to split fetching a message into fetching headers and fetching the body in order to manage
 *  (network) ressources.
 *  @todo maybe remove this class, also see @c ImapFetchJob::fetchBodyDoneCallback
 */
class ImapFetchBodyCurlJob  :  public  ImapFetchCurlJob  {
    protected:
        /** @brief constructor
         *  @param _curl                passed to @c ImapFetchCurlJob
         *  @param _deleteWhenFinished  passed to @c ImapFetchCurlJob
         *  @param _imapFetchJob        passed to @c ImapFetchCurlJob
         *  @param _from                initializes @c ImapFetchBodyCurlJob::from
         *  @param _to                  initializes @c ImapFetchBodyCurlJob::to
         *  @param _messageId           initializes @c ImapFetchBodyCurlJob::messageId
         *  @param _date                initializes @c ImapFetchBodyCurlJob::date
         */
        ImapFetchBodyCurlJob(CURL *_curl, bool _deleteWhenFinished, ImapFetchJob *_imapFetchJob);

    /// This class is only supposed to be used by @c ImapFetchJob, therefore the private constructor and the friend
    /// @c ImapFetchJob.
    friend ImapFetchJob;
};


/**
 * @brief fetches one entire mail (head and content) from a remote IMAP server (design patterns: @c Command, @c Proxy)
 * @details This class takes care of sending the appropriate IMAP commands via a @c CurlJob object to the specified
 * remote server to fetch the messages with the specified @c ImapUid from the specified @c ImapFolder. Constructing an
 * object does not start network communication. Instead, the schedular @c ::processImapFetchJobs picks objects up and
 * starts them at a (possibly) later point.\n
 *
 * Design pattern @c Command: This class aggregates (a reference to) a @c CurlJob which is of a Command itself. If this
 * class was not a @c Proxy it would directly inherit from @c CurlJob.\n
 *
 * Design pattern @c Proxy: This class does not inherit from CurlJob but rather aggregates a single instance of it
 * (dynamically allocated) because this class could potentially be instanciated several million times and @c CurlJob
 * objects are huge! @c CurlJob is the "real subject" and this class is the "proxy".
 */
class ImapFetchJob  {
    private:
        ImapConnection *imapConnection;  ///< reference to the @c Connection holding host, username, password, etc.
        ImapFolder *imapFolder;          ///< reference to the @c ImapFolder that holds the message to fetch
        ImapUid uid;                     ///< @c ImapUid of the message to fetch
        ImapFetchCurlJob *curlJob;       ///< dynamically constructed and deleted @c ImapFetchCurlJob (real subject indesign pattern @c Proxy)

        /// whether the scheduler (@c ::processImapFetchJobs) has been registered to @c Application's handlers
        /// The scheduler is a functional @c Singleton which is created by this class' constructor. That is why it needs
        /// this static variable whether it has been registered yet.
        static bool schedulerRegistered;
        static std::vector<ImapFetchJob*> allImapFetchJobs;  ///< static register of *all existing* instances – includes running and not running jobs
        static std::vector<ImapFetchJob*> runningImapFetchJobs;  ///< static register of all *running* instances

    public:
        /** @brief constructs and object and adds it the queue for the scheduler to pick up
         *  @details The constructor also takes care of registering the scheduler, see @c ::processImapFetchJobs.
         *  @param _imapConnection  the @c ImapConnection to construct @c CURL objects which the correct host, username, password
         *  @param _imapFolder      the @c ImapFolder that holds the message to fetch
         *  @param _uid             the @c ImapUid of the message to fetch
         */
        ImapFetchJob(ImapConnection *_imapConnection, ImapFolder *_imapFolder, ImapUid _uid);
        virtual ~ImapFetchJob();

        /// @returns @c ImapFetchJob::allImapFetchJobs
        static std::vector<ImapFetchJob*> getAllImapFetchJobs();

        /** @brief starts the job immediately (should usually only be called by scheduler)
         *  @details This function does not block.
         */
        void start();

    private:
        /// @brief callback function for @c CurlJob when headers have been fetched
        /// @param curlJob  the @c ImapFetchCurlJob that is done
        static void fetchHeadersDoneCallback(CurlJob *curlJob);

        /// @brief callback function for @c CurlJob when the entire message has been fetched
        /// @param curlJob  the @c ImapFetchCurlJob that is done
        /// @todo move this to @c ImapFetchBodyCurlJob?
        static void fetchBodyDoneCallback(CurlJob *curlJob);

    friend void processImapFetchJobs();
};
