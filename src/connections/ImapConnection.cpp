/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/connections/ImapConnection.hpp"
#include <curl/curl.h>
#include "src/util/exceptions/Exception.hpp"
#include <algorithm>
#include <sstream>
#include "src/connections/ImapFetchJob.hpp"
#include "src/streams/base64.hpp"
#include "src/settings.hpp"
#include <iostream>
#include "src/messages/Message.hpp"
#include "src/util/exceptions/ImapSendCurlJobFailedException.hpp"

const String ImapConnection::propertyKeyHost     = "ImapConnection::host";
const String ImapConnection::propertyKeyUsername = "ImapConnection::username";
const String ImapConnection::propertyKeyPassword = "ImapConnection::password";
const String ImapConnection::propertyKeyFolders  = "ImapConnection::folders";


ImapConnection::ImapConnection(Account *account, Properties properties)
        : Connection(account, properties)  {
    ImapConnection::setProperties(properties);
}
ImapConnection::ImapConnection(Account *account, bool shadow, String host, String username, String password)
        : Connection(account, shadow)  {
    setHost(host);
    setUsername(username);
    setPassword(password);
}
ImapConnection::~ImapConnection()  {
}

Message *ImapConnection::composeMessage(String content, Contact *contact)  {
    throw Exception("not yet implemented");
    /// @todo implement
}

CURL *ImapConnection::newCurl()  {
    CURL *curl = curl_easy_init();
    if(curl==nullptr)
        throw Exception("cannot initialize libcurl");
    curl_easy_setopt(curl, CURLOPT_USERNAME     , username.c_str());
    curl_easy_setopt(curl, CURLOPT_PASSWORD     , password.c_str());
    curl_easy_setopt(curl, CURLOPT_USERAGENT    , Settings::imapUserAgent.c_str());
    curl_easy_setopt(curl, CURLOPT_USE_SSL      , (long)CURLUSESSL_ALL);
    curl_easy_setopt(curl, CURLOPT_VERBOSE      , Settings::imapCurlVerbose);
    curl_easy_setopt(curl, CURLOPT_URL          , ("imaps://"+host+":"+std::to_string(Settings::imapPort)).c_str());
    return curl;
}


void ImapConnection::setHost(String host)  {
    this->host = host;
}
void ImapConnection::setUsername(String username)  {
    this->username = username;
}
void ImapConnection::setPassword(String password)  {
    this->password = password;
}
String ImapConnection::getHost()  {
    return host;
}
String ImapConnection::getUsername()  {
    return username;
}
String ImapConnection::getPassword()  {
    return password;
}


Properties ImapConnection::getProperties()  {
    Properties properties = Connection::getProperties();  // fetch properties of base class
    properties.set(propertyKeyHost    , host);
    properties.set(propertyKeyUsername, username);
    properties.set(propertyKeyPassword, password);

    // folders
    std::ostringstream foldersStream;
        base64encodebuf b64buf(foldersStream.rdbuf());
            std::ostream os(&b64buf);
                for(auto itr : folders)  {
                    writePropertiesToStream(os, (*itr).getProperties());
                    //os.flush();
                    b64buf.sync();
                    foldersStream << ";";
                }
    foldersStream.flush();

    properties.set(propertyKeyFolders, foldersStream.str());
    return properties;
}
void ImapConnection::setProperties(Properties properties)  {
    Connection::setProperties(properties);  // inform base class first
    host     = properties.get(propertyKeyHost    );
    username = properties.get(propertyKeyUsername);
    password = properties.get(propertyKeyPassword);

    for(String folderString : properties.get(propertyKeyFolders).split(';',SKIP_EMPTY_LAST))  {
        std::stringstream folderStream(folderString.toStdString());
            base64decodebuf b64buf(folderStream.rdbuf());
                std::istream is(&b64buf);
                    folders.push_back(new ImapFolder(readPropertiesFromStream(is)));
    }
}
String ImapConnection::getType()  const  {
    return getStaticType();
}
String ImapConnection::getStaticType()  {
    return "ImapConnection";
}


void ImapConnection::updateFolderListDoneCallback(CurlJob *curlJob)  {
    std::cout << "update folder list via IMAP done"<<std::endl;
    ImapConnectionCurlJob *job = dynamic_cast<ImapConnectionCurlJob*>(curlJob);
    if(job == nullptr)
        throw std::bad_cast();
    ImapConnection *this_ = dynamic_cast<ImapConnection*>(job->connection);
    if(this_ == nullptr)
        throw std::bad_cast();
    assertCurlOK(job->result);

    /// @todo rewrite this to use String parsing features
    for(String line : job->incomingData.splitIntoLines(SKIP_EMPTY_LAST))  {
        String orig_line = line;
        line.trim();

        // read line header
        static const String starting_string = "* LIST ";
        if( ! line.beginsWith(starting_string,CASE_INSENSITIVE) )  {
            std::cerr << "invalid IMAP line (does not start with \"* LIST\"): >"<<orig_line.toStdString()<<"<" <<
                         std::endl;
            continue;
        }
        line = line.erase(0, starting_string.size());
        line.trim();

        // read tags
        std::map<String,bool> tags;
        if( !line.empty() && line[0]=='(' )  {
            size_t pos = line.find(')',PLAIN);  /// @todo respect quotes?
            if(pos == String::npos)  {
                std::cerr << "invalid IMAP line (opens bracket but does not close it): >" <<
                             orig_line.toStdString()<<"<"<<std::endl;
                continue;
            }
            String tags_string = line.substr(1,pos-1);
            line.erase(0,pos+1);

            for(String tag : tags_string.split(' ',SKIP_ALL_EMPTY))  {  /// @todo respect quotes?
                tag.trim();
                tags[tag] = true;
            }
        }
        line.trim();

        // read location
        if( line.empty() || (line[0]!='\"' && line[0]!='.' && line[0]!='/') )  {
            std::cerr << "invalid IMAP line (does not contain valid location): >"<<orig_line.toStdString()<<"<" <<
                         std::endl;
            continue;
        }
        size_t pos;  /// @todo use respect quotes flag here
        if(line[0]=='\"')
            pos = line.substr(1,String::npos).find('\"',PLAIN) + 1;
        else
            pos = line.find(' ',PLAIN);
        String location = line.substr(0,pos+1);
        line.erase(0, pos+1);
        line.trim();

        // read folder name
        if(line.empty())  {
            std::cerr << "invalid IMAP line (empty folder name): >"<<orig_line.toStdString()<<"<"<<std::endl;
            continue;
        }
        String name = line;
        if(name[0] == '\"')
            name = name.substr(1,name.size()-2);

        // find existing ImapFolder object or generate new one
        ImapFolder *imapFolder=nullptr;
        for(ImapFolder *f : this_->folders)
            if(f->name == name)  {
                imapFolder = f;
                break;
            }
        if(imapFolder == nullptr)  {
            imapFolder = new ImapFolder();
            imapFolder->name = name;
            this_->folders.push_back(imapFolder);
        }

        // set already parsed tags
        imapFolder->isTrash = tags["\\Trash"]; // this tag does not have a counterpart "\\NoTrash", so it is a bool, not a tristate
        imapFolder->isSent = tags["\\Sent"];   // this tag does not have a counterpart "\\NoTrash", so it is a bool, not a tristate

        if(tags["\\HasChildren"])
            imapFolder->hasChildren = true;
        else if(tags["\\HasNoChildren"])
            imapFolder->hasChildren = false;
        else
            imapFolder->hasChildren = TristateBool::VAL_UNKNOWN;

        if(tags["\\Marked"])  /// @todo I am not sure whether this Marked tag exists but UnMarked does
            imapFolder->marked = true;
        else if(tags["\\UnMarked"])
            imapFolder->marked = false;
        else
            imapFolder->marked = TristateBool::VAL_UNKNOWN;
    }
}
void ImapConnection::updateFolderList(bool block)  {
    std::cout << "updating folder list via IMAP from "<<host<<std::endl;
    CURL *curl = newCurl();
    curl_easy_setopt(curl, CURLOPT_VERBOSE, Settings::imapCurlVerbose);
    ImapConnectionCurlJob *job = new ImapConnectionCurlJob(curl,true, this);
        job->callbackOnFinish = &ImapConnection::updateFolderListDoneCallback;
        job->start(block);
}

void ImapConnection::statusJobDoneCallback(CurlJob *curlJob)  {
    FolderSpecificImapConnectionCurlJob *job = dynamic_cast<FolderSpecificImapConnectionCurlJob*>(curlJob);
    if(job == nullptr)
        throw std::bad_cast();
    ImapConnection *imapConnection = dynamic_cast<ImapConnection*>(job->connection);
    if(imapConnection == nullptr)
        throw std::bad_cast();
    std::cout << "status folder \""+job->getFolderName()+"\" via IMAP done"<<std::endl;
    assertCurlOK(job->result);

    // get a reference to the folder
    std:: vector<ImapFolder*> imapConnectionFolders = imapConnection->folders;
    ImapFolder *imapFolder=nullptr;
    for( ImapFolder *f : imapConnectionFolders)
        if(f->name == job->getFolderName())
            imapFolder = f;
    if(imapFolder == nullptr)
        throw Exception("received status of a folder that is not registered ("+job->getFolderName()+")");

    // get part between braces
    String line = job->incomingData;
    const String orig_line = line;
    line.trim();
    size_t pos = line.find("(", PLAIN);  /// @todo respect quotes?
    if(pos == String::npos)  {
        std::cerr << "invalid IMAP line (does not contain braces): >"<<orig_line<<"<"<<std::endl;
        return;
    }
    line = line.substr(pos+1, String::npos);
    line = line.erase(line.size()-1,1);
    line.trim();

    // split into words
    std::vector<String> words;
    line.replaceAll("  "," ",PLAIN);
    for(const String &word : line.split(' ',PLAIN))  /// @todo respect quotes? @todo skip empty?
        words.push_back(word);

    // parse words
    if(words.size() % 2 != 0)  {
        std::cerr << "invalid IMAP line (uneven amount of words): >"<<orig_line<<"<"<<std::endl;
        return;
    }
    while(words.size())  {
        String val = words.back();
        words.pop_back();
        String key = words.back();
        words.pop_back();
        if(key == "MESSAGES")
            imapFolder->nRemoteMessages = val.toInt();
        else if(key=="RECENT")
            imapFolder->nRemoteRecent = val.toInt();
        else if(key=="UNSEEN")
            imapFolder->nRemoteUnseen = val.toInt();
        else  {
            std::cerr << "invalid IMAP line (unknown word \""+key+"\"): >"<<orig_line<<"<"<<std::endl;
            return;
        }
    }
}
void ImapConnection::statusJobFolders(bool block)  {
    Connection::fetchIncomingMessages(block);

    std::vector<FolderSpecificImapConnectionCurlJob*> jobs;
    for(ImapFolder *folder : folders)  {
        std::cout << "getting status of folder "<<folder->name<<"..."<<std::endl;
        FolderSpecificImapConnectionCurlJob *job = new FolderSpecificImapConnectionCurlJob(
                newCurl(),
                ! block,         // automatically delete jobs after done if non-blocking behavior
                this,
                folder->name);
        curl_easy_setopt(job->curl, CURLOPT_CUSTOMREQUEST, ("STATUS \""+folder->name+"\" (MESSAGES RECENT UNSEEN)").c_str());
        curl_easy_setopt(job->curl, CURLOPT_VERBOSE, Settings::imapCurlVerbose);
        job->callbackOnFinish = &ImapConnection::statusJobDoneCallback;
        jobs.push_back(job);
        job->start(block);
    }
    if(block)  {
        /// @todo join every single job
    }
}

void ImapConnection::fetchUIDListsDoneCallback(CurlJob *curlJob)  {
    FolderSpecificImapConnectionCurlJob *job = dynamic_cast<FolderSpecificImapConnectionCurlJob*>(curlJob);
    if(job == nullptr)
        throw std::bad_cast();
    ImapConnection *imapConnection = dynamic_cast<ImapConnection*>(job->connection);
    if(imapConnection == nullptr)
        throw std::bad_cast();
    std::cout<<"fetching UID list from \""<<job->getFolderName()<<"\" done"<<std::endl;
    assertCurlOK(job->result);

    // get a reference to the folder
    /// @todo consider putting this into a method of ImapFolderSpecificJob
    std:: vector<ImapFolder*> imapConnectionFolders = imapConnection->folders;
    ImapFolder *imapFolder=nullptr;
    for( ImapFolder *f : imapConnectionFolders)
        if(f->name == job->getFolderName())
            imapFolder = f;
    if(imapFolder == nullptr)
        throw Exception("received status of a folder that is not registered ("+job->getFolderName()+")");

    String line = job->incomingData;
    String orig_line = line;
    line.trim();

    // check command
    static const String cmdString = "* SEARCH";
    if( ! line.beginsWith(cmdString,CASE_INSENSITIVE) )  {
        std::cerr << "invalid IMAP line (does start with \""+cmdString+"\"): >"<<orig_line<<"<"<<std::endl;
        return;
    }
    line = line.erase(0, cmdString.size());
    line.trim();

    // get list of UIDs
    std::istringstream lineStream(line);
    String uidString;
    for(String uidString : line.split(' ',SKIP_ALL_EMPTY))  {  /// @todo respect quotes?
        uidString.trim();
        // check if UID is already fetched
        ImapUid uid = uidString.toInt();
        if( ! imapFolder->isUidFetched(uid) )  {  ///< @todo this check is bad if a message file is removed from disk because this check will prevent the application from fetching the mail with that UID again
            imapFolder->uidStates[uid] = 0;  // use invalid message id 0 to mark that a download job is going on but not finished
            new ImapFetchJob(imapConnection, imapFolder, uid);

            /// @todo make concept to avoid duplicates (when a message gets moved to a new folder it gets a new UID but keeps it Message-ID)
            /// @todo make class that accepts incomplete data:
            // - Message (implementes an interface to read properties like content, account, metadata and contact)
            //    - IncompleteMessage (just a typedef of Message for now but abstraction layer might come in handy)
            //      - IncompleteMessageFetchableViaImap (stores unique reference to ImapConnection object, folder name and UID)
            //      - IncompleteMessageFetchableFromDisk  (stores unique reference to file descriptor)
            //    - CompleteMessage  (has all data in memory)
        }
    }
}
void ImapConnection::fetchUIDLists(bool block)  {
    std::cout << "fetching all UID lists..."<<std::endl;
    for(ImapFolder *folder : folders)  {
        // do not read trash folder
        if(folder->isTrash)
            continue;
        /// @todo check if this is junk folder

        std::cout << "fetching UID list for folder \""<<folder->name<<"\"..."<<std::endl;
        ImapConnectionCurlJob *imapJob = new FolderSpecificImapConnectionCurlJob(newCurl(), true, this, folder->name);

        char *encodedFolderName_cstr = curl_easy_escape(imapJob->curl, folder->name.c_str(), folder->name.size());
        String encodedFolderName = encodedFolderName_cstr;
        delete[] encodedFolderName_cstr;

        curl_easy_setopt(imapJob->curl, CURLOPT_URL, ("imaps://"+host+":993/"+encodedFolderName).c_str());
        curl_easy_setopt(imapJob->curl, CURLOPT_CUSTOMREQUEST, "UID SEARCH ALL");
        curl_easy_setopt(imapJob->curl, CURLOPT_VERBOSE, Settings::imapCurlVerbose);
        imapJob->callbackOnFinish = fetchUIDListsDoneCallback;
        imapJob->start(block);
    }
}

void ImapConnection::examineDoneCallback(CurlJob *curlJob)  {
    SendMessageImapConnectionCurlJob *job = dynamic_cast<SendMessageImapConnectionCurlJob*>(curlJob);
    if(job == nullptr)
        throw std::bad_cast();
    ImapConnection *this_ = dynamic_cast<ImapConnection*>(job->connection);
    if(this_ == nullptr)
        throw std::bad_cast();
    if( job->result != CURLE_OK)
        throw ImapSendCurlJobFailedException(job);

    String txt = job->incomingData;
    size_t pos = txt.find("uidnext", CASE_INSENSITIVE);
    if(pos == String::npos)
        throw Exception("could not retrieve next IMAP UID");
    txt = txt.substr(pos+String("uidnext").size(), String::npos);
    txt.trim();

    ImapUid uid = -1;  /// @todo use proper converting function
    std::stringstream stxt(txt);
    stxt >> uid;
    std::cout << "next UID: "<<uid<<std::endl;

    /// @todo ensure that no other job uses up this UID

    // create new job
    SendMessageImapConnectionCurlJob *newJob = new SendMessageImapConnectionCurlJob(this_->newCurl(), true, this_,
                                                                                    job->getFolder(), job->getMessage());
    newJob->outgoingData = newJob->getMessage()->getRawFullMessage();
    String url = "imaps://"+
                      this_->getHost()+
                      ":"+std::to_string(Settings::imapPort)+
                      "/"+newJob->getFolder()->name+
                      "/;UID="+std::to_string(uid);
    curl_easy_setopt(newJob->curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(newJob->curl, CURLOPT_UPLOAD, 1L);
    curl_easy_setopt(newJob->curl, CURLOPT_INFILESIZE, newJob->outgoingData.size());
    newJob->callbackOnFinish =  appendDoneCallback;
    newJob->start(false);  ///< @todo implement blocking behavior
}
void ImapConnection::appendDoneCallback(CurlJob *curlJob)  {
    SendMessageImapConnectionCurlJob *job = dynamic_cast<SendMessageImapConnectionCurlJob*>(curlJob);
    if(job == nullptr)
        throw std::bad_cast();
    ImapConnection *this_ = dynamic_cast<ImapConnection*>(job->connection);
    if(this_ == nullptr)
        throw std::bad_cast();
    if( job->result != CURLE_OK)
        throw ImapSendCurlJobFailedException(job);
    String txt = job->incomingData;
    std::cout << "append done: >"<<txt<<"<"<<std::endl;
}

void ImapConnection::sendMessage(Message *message, bool block)  {
    if(message->hasFlag(Message::FLAG_OUTGOING))  {
        //Connection::sendMessage(message, block); do not call because message is already marked sent
        if(folders.empty())
            updateFolderList(true);  // find solution for non-blocking behavior

        ImapFolder *sentFolder = nullptr;
        for(ImapFolder *folder : folders)
            if(folder->isSent)  {
                sentFolder = folder;
                break;
            }
        if(sentFolder == nullptr)  {
            /// @todo create "Sent" folder
            throw Exception("IMAP account does not have a \"Sent\" folder");
        }

        SendMessageImapConnectionCurlJob *job = new SendMessageImapConnectionCurlJob(newCurl(), true, this,
                                                                                     sentFolder, message);
            curl_easy_setopt(job->curl, CURLOPT_CUSTOMREQUEST, ("EXAMINE "+sentFolder->name).c_str());
            job->callbackOnFinish = examineDoneCallback;
        job->start(block);
    }
    else
        throw Exception("not yet implemented");
}

/// @todo check that all jobs are destructed properly
void ImapConnection::fetchIncomingMessages(bool block)  {
    Connection::fetchIncomingMessages(block);
    updateFolderList(folders.empty() || block);
    //statusJobFolders(block);
    fetchUIDLists(block);
}


void ImapConnection::test(bool block)  {
    /// @todo implement blocking behavior
    std::cout << std::endl << "testing imap connection to "<<host<<std::endl;
    Connection::test(block);
    CURL *curl = curl_easy_init();
    if(curl==nullptr)
        throw Exception("cannot initialize libcurl");

    curl_easy_setopt(curl, CURLOPT_USERNAME     , username.c_str());
    curl_easy_setopt(curl, CURLOPT_PASSWORD     , password.c_str());
    curl_easy_setopt(curl, CURLOPT_USERAGENT    , Settings::imapUserAgent.c_str());
    curl_easy_setopt(curl, CURLOPT_USE_SSL      , (long)CURLUSESSL_ALL);
    curl_easy_setopt(curl, CURLOPT_VERBOSE      , Settings::imapCurlVerbose);

    curl_easy_setopt(curl, CURLOPT_URL          , ("imaps://"+host+":"+std::to_string(Settings::imapPort)).c_str());
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "NOOP");

    CURLcode result = curl_easy_perform(curl);

    curl_easy_cleanup(curl);
    std::cout << std::endl;

    if(result != CURLE_OK)
        throw Exception("could not connect to "+host+" via IAMP:\n\n"+String(curl_easy_strerror(result)));
}
