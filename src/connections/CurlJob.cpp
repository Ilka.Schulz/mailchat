/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "CurlJob.hpp"
#include "src/util/exceptions/Exception.hpp"
#include <algorithm>
#include <memory.h>
#include <iostream>
#include "src/Application.hpp"
#include <unistd.h>

CURLM *CurlJob::mcurl=nullptr;
std::vector<CurlJob*> CurlJob::allCurlJobs;

/// @todo the event scheduler needs to be more sophisticated because it needs to get the progrees with this: https://curl.se/libcurl/c/CURLOPT_XFERINFOFUNCTION.html
CurlJob::CurlJob(CURL *_curl, bool _deleteWhenFinished)
    : curl(_curl), deleteWhenFinished(_deleteWhenFinished), incomingData(""), bytes_read(0), outgoingData(""), done(false), result(CURLE_BAD_FUNCTION_ARGUMENT), callbackOnFinish{nullptr}  {
    // tell CURL (easy) handle to use this classes callback functions with this object as user_data parameter
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, libCurlWriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA    , this);
    curl_easy_setopt(curl, CURLOPT_READFUNCTION , libcurlReadCallback);
    curl_easy_setopt(curl, CURLOPT_READDATA     , this);
    // @todo: progress callback
    // @todo: other callback

    // register job in CurlJob::allCurlJobs
    allCurlJobs.push_back(this);

    // generate curl multi handle and start thread if not already existing
    if(mcurl == nullptr)  {
        CURLM *temp = curl_multi_init();
        if(temp==nullptr)
            throw Exception("could not create libcurl multi handle");
        mcurl = temp;

        Application::getInstance()->addHandler(processCurlJobs);
    }
    // DO NOT register job in CURL multi – that is done in method start()
}
CurlJob::~CurlJob()  {
    auto itr = std::find(allCurlJobs.begin(), allCurlJobs.end(), this);
    if(itr != allCurlJobs.end())
        allCurlJobs.erase(itr);
    curl_multi_remove_handle(mcurl, curl);
    curl_easy_cleanup(curl);
}

size_t CurlJob::libCurlWriteCallback(char *buffer, size_t size, size_t nitems, void *user_data_ptr)  {
    if(size != 1)
        throw Exception("size!=1, see https://curl.se/libcurl/c/CURLOPT_WRITEFUNCTION.html");

    char *nullTerminatedString = new char[nitems+1];
    memcpy(nullTerminatedString, buffer, nitems);
    nullTerminatedString[nitems] = '\0';
    static_cast<CurlJob*>(user_data_ptr)->incomingData += nullTerminatedString;
    delete[] nullTerminatedString;

    //static_cast<CurlJob*>(user_data_ptr)->incomingData.write(reinterpret_cast<const char*>(buffer), nitems);
    return nitems;
}

/// provides all necessary data to the callback function which provides data to libcurl
struct LibcurlReadCallbackUserdata {
    /// pointer to the null-terminated string that is supposed to be sent
    const char *data;
    /// bytes that have been read so far
    size_t bytes_read;
};

/// callback function to send data to libcurl
size_t CurlJob::libcurlReadCallback(char *buffer, size_t size, size_t nitems, void *user_data_ptr)  {
    CurlJob *this_ = static_cast<CurlJob*>(user_data_ptr);
    size_t room = size * nitems;  // maximum amount of byes that the caller can handle

    if((size == 0) || (nitems == 0) || ((size*nitems) < 1))  // caller does not accept a single byte
        return 0;

    const char *data  =  this_->outgoingData.c_str() + this_->bytes_read;

    if(data) {
        size_t len = this_->outgoingData.size() - this_->bytes_read;
        if(room < len)
          len = room;
        memcpy(buffer, data, len);
        this_->bytes_read += len;

        return len;
    }
    return 0;  // tell libcurl that we reached EOF
}

void CurlJob::performAsync()  {
    // read / write data
    int handlesRunning;
    CURLMcode curlMultiCode;
    curlMultiCode = curl_multi_perform(mcurl, &handlesRunning);

    // fetch messages
    int messagesInQueue;
    CURLMsg *message = curl_multi_info_read(mcurl, &messagesInQueue);
    if(message!=nullptr)
        ;//std::cout<<std::endl<<"[libcurl: "<<messagesInQueue+1<<" messages in queue]"<<std::endl;
    while(message!=nullptr)  {
        // find according CurlJob
        CurlJob *connectionCurlJob=nullptr;
        for(CurlJob *job : allCurlJobs)
            if(job->curl == message->easy_handle)  {
                connectionCurlJob = job;
                break;
            }
        if(connectionCurlJob == nullptr)
            throw Exception("libcurl multi says it has a job but the job is not registered in the job list");
        // handle message
        if(message->msg == CURLMSG_DONE)  {
            //std::cout<<"[message: job is done]"<<std::endl;
            connectionCurlJob->done = true;
            connectionCurlJob->result = message->data.result;
            if(connectionCurlJob->callbackOnFinish != nullptr)
                (*connectionCurlJob->callbackOnFinish)(connectionCurlJob);
                //(xonnectionCurlJob->connection->*(connectionCurlJob->callbackOnFinish))();
            if(connectionCurlJob->deleteWhenFinished)
                delete connectionCurlJob;  // no use for this job to be kept after finished and finish-callback handler called. That destructor also frees the CURL (easy) handle.
        }
        // fetch more messages
        message = curl_multi_info_read(mcurl, &messagesInQueue);
    }

    // yield
    if(handlesRunning != 0)
        curlMultiCode = curl_multi_poll(mcurl, NULL, 0, 50, NULL);  // wait for activity, timeout or "nothing"

    if(curlMultiCode != CURLM_OK)
        throw Exception("libcurl multi threw error:\n\n"+String(curl_multi_strerror(curlMultiCode))+" (error code "+std::to_string(curlMultiCode)+")");
}

std::vector<CurlJob*> CurlJob::getAllCurlJobs()  {
    return allCurlJobs;
}
void CurlJob::start(bool block)  {
    // do not delete during this function call
    bool originalDeleteWhenFinished = deleteWhenFinished;
    if(block)
        deleteWhenFinished=false;
    // perform
    curl_multi_add_handle(mcurl, curl);

    // block if desired
    if(block)  {
        while(!done)
            Application::getInstance()->refresh(false);  // do not let the application freeze ;)
                                                         // note: setting parameter handleExceptions to false in order
                                                         // to propagate exceptions to the calling code
        // delete object now if originally desired
        deleteWhenFinished = originalDeleteWhenFinished;
        if(deleteWhenFinished)
            delete this;
    }
}

void processCurlJobs()  {
    CurlJob::performAsync();
}
