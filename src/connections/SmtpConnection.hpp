/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <curl/curl.h>

class SmtpConnection;
class Contact;
class OutgoingMessage;
class EmailMessage;
class CurlJob;

#include "src/connections/Connection.hpp"

/** @brief a @c Connection that uses the SMTP protocol
 */
class SmtpConnection  :  public Connection  {
    public:
        static const String propertyKeyHost;      ///< property key for @c SmtpConnection::host
        static const String propertyKeyUsername;  ///< property key for @c SmtpConnection::username
        static const String propertyKeyPassword;  ///< property key for @c SmtpConnection::password

    private:
        /// resolvable host running the SMTP server
        String host;
        /// username for SMTP authentication
        String username;
        /// password for SMTP authentication
        String password;

    public:
        /** @brief constructs a new objects and associates it with the specified account
         *  @param account     passed to @c Connection::Connection
         *  @param properties  the @c Properties to assign to this object (see @c SmtpConnection::setProperties)
         */
        SmtpConnection(Account *account, Properties properties=Properties());
        SmtpConnection(Account *account, bool shadow, String _host, String _username, String _password);

        Message *composeMessage(String content, Contact *contact);

    private:
        /** @brief fetch a new valid and unique message ID from the remote server
         *  @param block  whether to block execution
         *  @returns an new message ID
         */
        String newMessageId();

        /** @brief constructs new CURL (easy) handle that contains commonly used standard settings (e.g. host,username,
         *  password)
         *  @returns the constructed handle
         */
        CURL *newCurl();

    public:
        // implementing interface of Connection class
        /** @brief sends a @c OutgoingMessage via SMTP (implements @c Connection::sendMessage)
         *  @param message  the @c OutgoingMessage to send (@c OutgoingMessage::sent should be @c false)
         *  @param block    whether to block
         */
        virtual void sendMessage(Message *message, bool block)  override;

        /** @brief test wether a connection to the SMTP server can be established (also login)
         *  @details This method will throw an @c ConnectionTestFailedException if block==true, otherwise that exception will
         *  be thrown directly into the event loop.
         *  @param block  whether to block
         */
        virtual void test(bool block)  override;
    private:
        static void sendMessageDoneCallback(CurlJob *curlJob);
        static void testDoneCallback(CurlJob *curlJob);

    public:
        // implementing load/save properties of Connection class
        virtual Properties getProperties()  override;

        /** @brief changes this object according to specified properties
         *  @param properties  the properties to set
         *  @details allowed properties: \n
         *      - SmtpConnection::host      value of @c SmtpConnection::host \n
         *      - SmtpConnection::username  value of @c SmtpConnection::username \n
         *      - SmtpConnection::password  value of @c SmtpConnection::password \n
         *      - all properties of base classes
         */
        virtual void setProperties(Properties properties)  override;
        /// wrapper for @c SmtConnection::getStaticType  @returns  return value of the wrapped function
        virtual String getType()  const override;
        /// @returns "SmtpConnection"
        static String getStaticType();

    public:
        /// sets @c SmtpConnection::host  @param host  the new host to set
        void setHost(String host);
        /// sets @c SmtpConnection::username  @param username  the new username to set
        void setUsername(String username);
        /// sets @c SmtpConnection::password  @param password  the new password to set
        void setPassword(String password);

        /// @returns @c SmtpConnection::host
        String getHost();
        /// @returns @c SmtpConnection::username
        String getUsername();
        /// @returns @c SmtpConnection::password
        String getPassword();
};
