/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>

class EmailConnection;
class SmtpConnection;
class ImapConnection;

#include "Connection.hpp"
#include "SmtpConnection.hpp"
#include "ImapConnection.hpp"


/** @brief a @c Connection that aggregates an @c SmtpConnection and an @c ImapConnection
 *  @details This class allows to send and fetch messages from an email account.
 */
class EmailConnection  :  public Connection  {
    private:
        /// used to connect to the mail service provider's SMTP server
        SmtpConnection smtpConnection;
        /// used to connect to the mail service provider's IMAP server
        ImapConnection imapConnection;

    public:
        /** @brief constructor
         *  @param _account    passed to @c Connection::Connection
         *  @param properties  passed to @c Connection::Connection and also parsed like in @c EmailConnection::getProperties
         */
        EmailConnection(Account *_account, Properties properties);
        EmailConnection(Account *_account, bool shadow=false,
                        String smtpHost="", String stmpUsername="", String smtpPassword="",
                        String imapHost="", String imapUsername="", String imapPassword="");
        ~EmailConnection();

        Message *composeMessage(String content, Contact *contact);

    public:
        // methods to manage SmtpConnection and ImapConnection
        /// calls @c SmtpConnection::setHost and @c ImapConnection::setHost  @param host  the host to set
        void setHost(String host);
        /// calls @c SmtpConnection::setUsername and @c ImapConnection::setUsername  @param username  the username to set
        void setUsername(String username);
        /// calls @c SmtpConnection::setPassword and @c IampConnection::setPassword  @param password  the password to set
        void setPassword(String password);

    public:
        // interface of ConnectionInterface
        /// @returns get a complete list of defining properties, including @c EmailConnection::smtpConnection and
        /// @c EmailConnection::imapConnection
        virtual Properties getProperties()  override;

        /** @brief calls @c Connection::setProperties, @c SmtpConnection::setProperties and
         *      @c ImapConnection::setProperties
         *  @param properties  the properties to set
         *  @details allowed properties: all of the classes whose "getProperties" gets called and none additional
         */
        virtual void setProperties(Properties properties)  override;

        /// @returns wrapper for @c EmailConnection::getStaticType
        virtual String getType()  const  override;
        /// @returns "EmailConnection"
        static String getStaticType();

        virtual void sendMessage(Message *message, bool block)  override;
        virtual void fetchIncomingMessages(bool block)  override;
        virtual void test(bool block)  override;
};
