/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <vector>
#include <map>

class Connection;
class Account;
class Contact;
class Message;

#include "src/util/Properties.hpp"

/** @brief interface of @c Connection
 *  @details design pattern @c Abstract @c Factory: see method @c ConnectionInterface::composeMessage
 */
class ConnectionInterface  {
    public:
        ConnectionInterface() = default;
    public:
        // virtual methods to be implemented by subclasses

        /// @todo write guide how to use properties in derived classes properly

        /// @brief should be implemented by subclasses so their properties get saved to file
        /// @returns a @c Properties list describing the object
        virtual Properties getProperties()=0;

        /// @brief should be implemented by subclasses so they can get their properties read from file
        /// @param properties  @c Properties list that the object shall accept
        virtual void setProperties(Properties properties)=0;

        /// @brief should be implemented by subclasses indicating their class, e.g. "SmtpConnection"
        /// @returns a unique string for each derived class
        virtual String getType()  const =0;

        /** @brief sends a message to a contact
         *  @param message  will be send to the recipient
         *  @param block  whether the method should block execution
         */
        virtual void sendMessage(Message *message, bool block)=0;

        /** @brief pseudo-constructor for @c Message
         *  @details Most classes derived from @c Connection will probably be able to construct @c Message objects that
         *  have unique properties due to it being "owned" by the @c Connection object. E.g. an @c SmtpConnection will
         *  be able to construct an @c EmailMessage object whose @c Message::rawFullMessage has an email message header
         *  and body while other classes derived from @c Connection may construct @c Message objects that have a
         *  completely different syntax for @c Message::rawFullMessage. \n
         *  This method will not actually send the constructed @c Message object. Use
         *  @c ConnectionInterface::sendMessage for that.
         *  @param content  the (formatted, human-readable) content of the message (what is returned by get by @c Message::getContent
         *  @param contact  the @c Contact object that respresents the recipient of the composed message
         *  @returns the constructed @c Message object
         */
        virtual Message *composeMessage(String content, Contact *contact)=0;

        /** @brief pulls all messages from remote
         *  @details Ideally, subclasses also implement some push mechanism to receive messages.
         *  @param block  whether the method should block execution
         */
        virtual void fetchIncomingMessages(bool block)=0;

        /** @brief tests whether client can connect to server, authenticate and has the necessary permissions for all
         *  actions supported by the class
         *  @details A subclass that only implements @c Connection::sendMessage() only needs to check whether sending
         *  messages is supported and subclasses that only implements @c Connection::fetchIncomingMessages() only needs
         *  to check whether fetching messages is supported. This function throws an exception if not successful.
         *  @param block  whether the method should block execution
         */
        virtual void test(bool block)=0;
};

/** @brief implements an interface to send and receive messages
 *  @details An object of this class belongs to one account but it can send/receive message to/from all contacts of that
 *  account.
 *  @todo periodically call fetchIncomingMessages();
 */
class Connection  :  public ConnectionInterface  {
    public:
        /// property key @details if true: this object is not registered with @c Account::allConnections
        static const String propertyKeyShadow;
        /// property key for @c Connection::getType @details This is parsed by @c GenericConnection.
        static const String propertyKeyType;

    private:

        /// @c Account objects with which this @c Connection object is associated
        Account *account;

        /// static register of all existing objects
        static std::vector<Connection*> allConnections;

    public:
        /** @brief constructor
         *  @param _account    the @c Account that owns this object
         *  @param properties  @c Properties to initialize this object (see @c Connection::setProperties for valid keys)
         */
        Connection(Account *_account, Properties properties=Properties());
        Connection(Account *_account, bool shadow);
        virtual ~Connection();

    public:
        /// @returns Connection::allConnections
        static std::vector<Connection*> getAllConnections();

        /// @returns Connection::account
        Account *getAccount();

    public:
        // interface of ConnectionInterface
        /// @returns wrapper for @c Connection::getStaticType
        virtual String getType()  const override;
        /// @returns "Connection"
        static String getStaticType();

        /** @brief changes this object according to specified properties
         *  @param properties  the properties to set
         *  @details allowed properties: \n
         *      - Connection::shadow  if true: object is not registered in @c Connection::allConnections \n
         */
        virtual void setProperties(Properties properties)  override;

        /** @returns complete list of properties of this object (see @c Connection::setProperties for valid keys)
         */
        virtual Properties getProperties()  override;

        /** @brief empty implementation of @c ConnectionInterface::sendMessage
         *  @param message  see @c ConnectionInterface::sendMessage
         *  @param block    see @c ConnectionInterface::sendMessage
         */
        virtual void sendMessage(Message* message, bool block)  override;

        /** @brief empty implementation of @c ConnectionInterface::fetchIncomingMessages
         *  @param block  see ConnectionInterface::fetchIncomingMessages
         */
        virtual void fetchIncomingMessages(bool block)  override;

        /** @brief empty implementation of @c ConnectionInterface::test
         *  @param block  see @c ConnectionInterface::test
         */
        virtual void test(bool block)  override;
};
