/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "src/connections/ImapFolder.hpp"
#include <sstream>
#include "src/streams/base64.hpp"
#include "src/util/exceptions/Exception.hpp"


const String ImapFolder::propertyKeyName             = "ImapFolder::name";
const String ImapFolder::propertyKeyHasChildren      = "ImapFolder::hasChildren";
const String ImapFolder::propertyKeyIsSent           = "ImapFolder::isSent";
const String ImapFolder::propertyKeyIsTrash          = "ImapFolder::isTrash";
const String ImapFolder::propertyKeyMarked           = "ImapFolder::marked";
const String ImapFolder::propertyKeyNRemoteMessages  = "ImapFolder::nRemoteMessages";
const String ImapFolder::propertyKeyNRemoteRecent    = "ImapFolder::nRemoteRecent";
const String ImapFolder::propertyKeyNRemoteUnseen    = "ImapFolder::nRemoteUnseen";
const String ImapFolder::propertyKeyUidStates        = "ImapFolder::uidStates";


ImapFolder::ImapFolder()
        : name(""), hasChildren(TristateBool::VAL_UNKNOWN), isTrash(false), marked(TristateBool::VAL_UNKNOWN),
          nRemoteMessages(0), nRemoteRecent(0), nRemoteUnseen(0)  {
}
ImapFolder::ImapFolder(Properties properties)
        : ImapFolder()  {
    setProperties(properties);
}

bool ImapFolder::isUidFetched(ImapUid uid)  {
    auto itr = uidStates.find(uid);
    if(itr == uidStates.end())
        return false;
    Message::MessageId id = itr->second;
    try  {
        Message::getById(id);
        return true;
    }
    catch(...)  {
        return false;
    }
}

ImapFolder &ImapFolder::operator=(ImapFolder &source)  {
    name        = source.name;
    hasChildren = source.hasChildren;
    isSent      = source.isSent;
    isTrash     = source.isTrash;
    marked      = source.marked;
    nRemoteMessages = source.nRemoteMessages;
    nRemoteRecent   = source.nRemoteRecent;
    nRemoteUnseen   = source.nRemoteUnseen;
    return *this;
}

Properties ImapFolder::getProperties()  {
    Properties properties;
    properties.set(propertyKeyName,            name);
    properties.set(propertyKeyHasChildren,     hasChildren.toString());
    properties.set(propertyKeyIsSent,          isSent.toString());
    properties.set(propertyKeyIsTrash,         isTrash.toString());
    properties.set(propertyKeyMarked,          marked.toString());
    properties.set(propertyKeyNRemoteMessages, std::to_string(nRemoteMessages));
    properties.set(propertyKeyNRemoteRecent,   std::to_string(nRemoteRecent));
    properties.set(propertyKeyNRemoteUnseen,   std::to_string(nRemoteUnseen));

    // UID states
    Properties uidStatesProperties;
    for(auto itr : uidStates)
        uidStatesProperties.set( std::to_string(itr.first), std::to_string(itr.second) );
    std::ostringstream uidStatesStream;
        base64encodebuf b64buf(uidStatesStream.rdbuf());
            std::ostream os(&b64buf);
                writePropertiesToStream(os, uidStatesProperties);
            //os.flush();
        b64buf.sync();
    uidStatesStream.flush();
    properties.set(propertyKeyUidStates, uidStatesStream.str());

    return properties;
}

void ImapFolder::setProperties(Properties properties)  {
    name            = properties.get(propertyKeyName);
    hasChildren     = properties.get(propertyKeyHasChildren);
    isSent          = properties.get(propertyKeyIsSent);
    isTrash         = properties.get(propertyKeyIsTrash);
    marked          = properties.get(propertyKeyMarked);
    nRemoteMessages = properties.get(propertyKeyNRemoteMessages).toInt();
    nRemoteRecent   = properties.get(propertyKeyNRemoteRecent).toInt();
    nRemoteUnseen   = properties.get(propertyKeyNRemoteUnseen).toInt();

    std::stringstream uidStatesStream(properties.get(propertyKeyUidStates));
        base64decodebuf b64buf(uidStatesStream.rdbuf());
            std::istream is(&b64buf);
                Properties uidStateProperties = readPropertiesFromStream(is);
    for(auto itr : uidStateProperties)  {
        Message::MessageId id = itr.second.toInt();
        if(id != 0)  // uid 0 is invalid and only used during runtime to prevent parallel fetch jobs
            uidStates[itr.first.toInt()] = id;
    }
}
