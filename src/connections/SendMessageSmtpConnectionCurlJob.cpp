/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "SendMessageSmtpConnectionCurlJob.hpp"
#include "src/messages/Message.hpp"
#include "src/Contact.hpp"
#include "src/connections/SmtpConnection.hpp"
#include "src/Account.hpp"


SendMessageSmtpConnectionCurlJob::SendMessageSmtpConnectionCurlJob(CURL *_curl, bool _deleteWhenFinished, SmtpConnection *_connection, Message *_message)
        : ConnectionCurlJob(_curl, _deleteWhenFinished, _connection), message(_message)  {
    // set sender
    curl_easy_setopt(curl, CURLOPT_MAIL_FROM , connection->getAccount()->getEmail().c_str());   ///< @todo Theoretically, this could differ from the message header "From:" field.

    // set recipients
    curl_slist *recipients = nullptr;  // btw, this gets deleted in @c SendMessageSmtpConnectionCurlJob::~SendMessageSmtpConnectionCurlJob()
        recipients = curl_slist_append(recipients, message->getContact()->getEmail().c_str());
    curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);

    // set payload
    outgoingData = message->getRawFullMessage();
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
}
SendMessageSmtpConnectionCurlJob::~SendMessageSmtpConnectionCurlJob()  {
    /// @todo find out where `recipients` gets freed - apparently before this destructor...
    /// curl_slist_free_all(recipients);
}

Message *SendMessageSmtpConnectionCurlJob::getMessage() const  {
    return message;
}
SmtpConnection *SendMessageSmtpConnectionCurlJob::getConnection() const  {
    SmtpConnection *smtpConnection = dynamic_cast<SmtpConnection*>(ConnectionCurlJob::getConnection());
    assert(smtpConnection != nullptr);
    return smtpConnection;
}
