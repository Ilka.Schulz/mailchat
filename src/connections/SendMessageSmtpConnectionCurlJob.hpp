/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/// @file

#include "ConnectionCurlJob.hpp"

class Message;
class SmtpConnection;

/** @brief a @c CurlJob that sends a message via an @c SmtpConnection
 *  @details
 */
class SendMessageSmtpConnectionCurlJob  :  public ConnectionCurlJob  {
    private:
        Message *message;
        curl_slist *recipients; ///< list of recipients. only stored here because it needs to be freed at the end
    public:
        SendMessageSmtpConnectionCurlJob(CURL *_curl, bool _deleteWhenFinished, SmtpConnection *_connection, Message *_message);
        virtual ~SendMessageSmtpConnectionCurlJob();

        Message *getMessage()  const;
        SmtpConnection *getConnection()  const;
};
