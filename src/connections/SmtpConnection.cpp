/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/connections/SmtpConnection.hpp"
#include "src/Contact.hpp"
#include "src/messages/EmailMessage.hpp"
#include "src/util/exceptions/Exception.hpp"
#include <curl/curl.h>
#include <iostream>
#include "src/Account.hpp"
#include "src/util/Properties.hpp"
#include "src/settings.hpp"
#include "src/settings.hpp"
#include "src/util/time.hpp"
#include <openssl/sha.h>
#include <random>
#include "src/connections/SendMessageSmtpConnectionCurlJob.hpp"
#include "src/util/exceptions/SmtpSendCurlJobFailedException.hpp"
#include "src/util/exceptions/ConnectionTestFailedException.hpp"

const String SmtpConnection::propertyKeyHost     = "SmtpConnection::host";
const String SmtpConnection::propertyKeyUsername = "SmtpConnection::username";
const String SmtpConnection::propertyKeyPassword = "SmtpConnection::password";


SmtpConnection::SmtpConnection(Account *account, Properties properties)
        : Connection(account, properties)  {
    setProperties(properties);
}
SmtpConnection::SmtpConnection(Account *account, bool shadow, String _host, String _username, String _password)
        : Connection(account, shadow)  {
    setHost(_host);
    setUsername(_username);
    setPassword(_password);
}

/// @todo line width is restricted
Message *SmtpConnection::composeMessage(String content, Contact *contact)  {
    // read current time
    /// @todo this might leak information, see comment in @c SmtpConnection::newMessageId
    std::time_t timeStamp = std::time(nullptr);

    // generate message body to be sent over SMTP
    String messageBody="";  // the new value to set to Message::rawFullMessage
        // header
        std::vector<String> headerLines;
            headerLines.push_back("To: "+contact->getName()+" <"+contact->getEmail()+">");
            headerLines.push_back("From: "+getAccount()->getName()+" <"+getAccount()->getEmail()+">");
            headerLines.push_back("Date: "+Time::timeStampToText(timeStamp, Time::emailDateFormat));
            headerLines.push_back("Subject: chat");  /// @todo decide on mail subject
            headerLines.push_back("Message-ID: " + newMessageId() );
            headerLines.push_back("Content-Type: text/plain; charset=utf-8");
            headerLines.push_back("Content-Transfer-Encoding: 8bit");
            headerLines.push_back("User-Agent: "+Settings::smtpUserAgent);

            /** @todo implement all these header infos
            Content-Language: de-DE
            */

        for(String line : headerLines)
            messageBody += line+"\r\n";
    messageBody += "\r\n";  // empty line to divide headers from body, see RFC5322
    messageBody += content;  /// @todo encode
    messageBody += "\r\n\r\n";

    return new EmailMessage(
                    0,
                    Message::FLAG_OUTGOING,
                    messageBody,
                    contact,
                    timeStamp,
                    ""   ///< @todo get message id
                );
}

String SmtpConnection::newMessageId()  {
    // use current time
    const time_t timepoint = std::time(nullptr);

    // use random number
    std::random_device generator;
    std::uniform_int_distribution<int64_t> distribution(INT64_MIN, INT64_MAX);
    const int64_t random_number = distribution(generator);

    // combine data
    unsigned char data[sizeof(timepoint) + sizeof(random_number)];
    memcpy(data, &timepoint, sizeof(timepoint));
    memcpy(data+sizeof(timepoint), &random_number, sizeof(random_number));

    // calculate hash value
    // This is important because we do not want to leak information about the system time. The MTA, MDA or recipient may
    // be able to reconstruct the user's time zone, system clock offset, network latency or maybe even system clock
    // drift from the system clock time stamp.
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256(data, sizeof(timepoint)+sizeof(random_number), hash);

    // encode as hexadecimal string
    char hex_hash[SHA256_DIGEST_LENGTH * 2];
    const String charset = "0123456789ABCDEF";
    for(int i=0; i<SHA256_DIGEST_LENGTH; i++)  {
        hex_hash[i*2]   = charset[hash[i]/16];
        hex_hash[i*2+1] = charset[hash[i]%16];
    }

    // return
    String retval = String((char*)hex_hash, SHA256_DIGEST_LENGTH*2) + "@" + getHost();
    return retval;
}
CURL *SmtpConnection::newCurl()  {
    CURL *curl = curl_easy_init();
    if(curl==nullptr)
        throw Exception("could not initate curl easy interface");
    curl_easy_setopt(curl, CURLOPT_USERNAME  , getUsername().c_str());
    curl_easy_setopt(curl, CURLOPT_PASSWORD  , getPassword().c_str());
    curl_easy_setopt(curl, CURLOPT_USERAGENT , Settings::smtpUserAgent.c_str());
    curl_easy_setopt(curl, CURLOPT_USE_SSL   , (long)CURLUSESSL_ALL);
    curl_easy_setopt(curl, CURLOPT_VERBOSE   , Settings::smtpCurlVerbose);
    curl_easy_setopt(curl, CURLOPT_URL       , ("smtp://"+host+":"+std::to_string(Settings::smtpPort)).c_str());  ///< @todo this is STARTTLS. support TLS.
    /**
     * @todo allow to ignore certificate validity. excerpt from example at https://curl.se/libcurl/c/smtp-tls.html :
     *
     * If your server doesn't have a valid certificate, then you can disable
     * part of the Transport Layer Security protection by setting the
     * CURLOPT_SSL_VERIFYPEER and CURLOPT_SSL_VERIFYHOST options to 0 (false).
     *   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
     *   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
     * That is, in general, a bad idea. It is still better than sending your
     * authentication details in plain text though.  Instead, you should get
     * the issuer certificate (or the host certificate if the certificate is
     * self-signed) and add it to the set of certificates that are known to
     * libcurl using CURLOPT_CAINFO and/or CURLOPT_CAPATH. See docs/SSLCERTS
     * for more information.
     *
     * curl_easy_setopt(curl, CURLOPT_CAINFO, "/path/to/certificate.pem");
     */
    return curl;
}
/// @todo implement blocking behavior
/// @todo use @c CurlJob
void SmtpConnection::sendMessage(Message *message, bool block)  {
    // call base class
    Connection::sendMessage(message, block);

    // sanity checks
    if(dynamic_cast<EmailMessage*>(message) == nullptr)
        throw Exception("trying to send a message over SMTP that is not an email message");
    if(message->hasFlag(Message::FLAG_SYNCHRONIZED))
        throw Exception("trying to send a message that has already been sent");

    /// @todo verify email address before manipulating data

    SendMessageSmtpConnectionCurlJob *curlJob = new SendMessageSmtpConnectionCurlJob(newCurl(), true, this, message);  // constructor takes care of
                                                                                     // filling CURL handle
    curlJob->callbackOnFinish = SmtpConnection::sendMessageDoneCallback;
    curlJob->start(block);
}
/// @todo implement blocking behavior
/// @todo use @c CurlJob
void SmtpConnection::test(bool block)  {
    // call base class
    Connection::test(block);
    // try to log in and quit immediately
    CurlJob *curlJob = new CurlJob(newCurl(), true);
    curl_easy_setopt(curlJob->curl, CURLOPT_CUSTOMREQUEST, "QUIT");  // This is the command executed directly after successful login.
    curlJob->callbackOnFinish = SmtpConnection::testDoneCallback;
    curlJob->start(block);
}

void SmtpConnection::sendMessageDoneCallback(CurlJob *curlJob)  {
    SendMessageSmtpConnectionCurlJob *job = dynamic_cast<SendMessageSmtpConnectionCurlJob*>(curlJob);
    assert(job != nullptr);
    if(curlJob->result != CURLE_OK)
        throw SmtpSendCurlJobFailedException(job);
    job->getMessage()->addMessageFlags(Message::FLAG_SYNCHRONIZED);
}
void SmtpConnection::testDoneCallback(CurlJob *curlJob)  {
    /// @todo add info about the @c SmtpConnection object that failed to the exception object
    if(curlJob->result != CURLE_OK)  // e.g. CURLE_LOGIN_DENIED
        throw ConnectionTestFailedException();
    //String(curl_easy_strerror(result))+" (error code "+std::to_string(result)+");
}

Properties SmtpConnection::getProperties()  {
    Properties properties = Connection::getProperties();  // get properties from base class
    properties.set(propertyKeyHost    , host);
    properties.set(propertyKeyUsername, username);
    properties.set(propertyKeyPassword, password);
    return properties;
}
void SmtpConnection::setProperties(Properties properties)  {
    Connection::setProperties(properties);  // set properties of base class
    host     = properties.get(propertyKeyHost);
    username = properties.get(propertyKeyUsername);
    password = properties.get(propertyKeyPassword);
}
String SmtpConnection::getType()  const  {
    return getStaticType();
}
String SmtpConnection::getStaticType()  {
    return "SmtpConnection";
}


String SmtpConnection::getHost()  {
    return host;
}
String SmtpConnection::getUsername()  {
    return username;
}
String SmtpConnection::getPassword()  {
    return password;
}

void SmtpConnection::setHost(String host)  {
    this->host = host;
}
void SmtpConnection::setUsername(String username)  {
    this->username = username;
}
void SmtpConnection::setPassword(String password)  {
    this->password = password;
}
