/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "EmailConnection.hpp"
#include "src/Account.hpp"
#include "src/Contact.hpp"
#include "src/messages/Message.hpp"
#include "src/util/exceptions/Exception.hpp"
#include "src/util/misc.hpp"
#include "src/util/Properties.hpp"
#include "src/util/time.hpp"

#include <boost/algorithm/string.hpp>
#include <sstream>
//#include <QMessageBox>  /// @todo this is GUI stuff in a non-GUI class!


EmailConnection::EmailConnection(Account *_account, Properties properties)
    :  Connection(_account, properties),
      smtpConnection(getAccount(), mergeProperties(properties, Properties{{Connection::propertyKeyShadow,"true"}}, true)),
      imapConnection(getAccount(), mergeProperties(properties, Properties{{Connection::propertyKeyShadow,"true"}}, true))
{
}
EmailConnection::EmailConnection(Account *_account, bool shadow,
                                 String smtpHost, String smtpUsername, String smtpPassword,
                                 String imapHost, String imapUsername, String imapPassword)
    : Connection(_account, shadow),
      smtpConnection(_account, true, smtpHost, smtpUsername, smtpPassword),
      imapConnection(_account, true, imapHost, imapUsername, imapPassword)
{
}
EmailConnection::~EmailConnection()  {
}

Message *EmailConnection::composeMessage(String content, Contact *contact)  {
    return smtpConnection.composeMessage(content, contact);
}


void EmailConnection::setHost(String host)  {
    smtpConnection.setHost(host);
    imapConnection.setHost(host);
}
void EmailConnection::setUsername(String username)  {
    smtpConnection.setUsername(username);
    imapConnection.setUsername(username);
}
void EmailConnection::setPassword(String password)  {
    smtpConnection.setPassword(password);
    imapConnection.setPassword(password);
}


Properties EmailConnection::getProperties()  {
    /// @todo maybe implement a cleaner method to merge Properties objects...
    Properties smtpProperties = smtpConnection.getProperties();
    smtpProperties.remove(Connection::propertyKeyType);
    Properties imapProperties = imapConnection.getProperties();
    imapProperties.remove(Connection::propertyKeyType);

    Properties properties = mergeProperties(Connection::getProperties(), smtpProperties);
    properties = mergeProperties(properties, imapProperties);
    return properties;
}
void EmailConnection::setProperties(Properties properties)  {
    Connection::setProperties(properties);
    smtpConnection.setProperties(properties);
    imapConnection.setProperties(properties);
}
String EmailConnection::getType() const  {
    return getStaticType();
}
String EmailConnection::getStaticType()  {
    return "EmailConnection";
}
void EmailConnection::sendMessage(Message *message, bool block)  {
    /*
    try {
        smtpConnection.sendMessage(message, block);
    }  catch (SmtpSendCurlJobFailedException &e) {
        TODO: retry a couple times but not too often
    }
    do {
        bool retry = false;
        try {
            imapConnection.sendMessage(message, block);
        }  catch (ImapSendCurlJobFailedException &e) {
            QMessageBox box;
            box.setText(tr("The message could not be saved to the \"Sent\" folder.\nDo you want to retry (recommended: yes)?"));
            box.setIcon(QMessageBox::Warning);
            box.addButton(QMessageBox::Yes);
            box.addButton(QMessageBox::No);
            int result = box.exec();
            if(result==QMessageBox::Yes)
                retry = true;
        }
    while(retry);*/
    smtpConnection.sendMessage(message, block);
    imapConnection.sendMessage(message, block);
}
void EmailConnection::fetchIncomingMessages(bool block)  {
    imapConnection.fetchIncomingMessages(block);
}
void EmailConnection::test(bool block)  {
    smtpConnection.test(block);
    imapConnection.test(block);
}

