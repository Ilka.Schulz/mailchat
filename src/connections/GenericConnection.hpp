/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

class GenericConnection;

#include "Connection.hpp"

/** @brief bridge of class @c Connection
 *  @details This class is useful because @c GenericConnection objects need to be constructed from @c ::Properties lists
 *  read from disk. \n
 *  design pattern @c Bridge
 */
class GenericConnection  :  public ConnectionInterface  {
    private:
        /// design pattern Bridge -> Implementation class
        Connection *impl;

    public:
        /** @brief constructor
         *  @details This constructor reads the value to key @c "Connection::type" from properties and decides which
         *  constructor to call, e.g. @c SmtpConnection::SmtpConnection, @c ImapConnection::ImapConnection or
         *  @c EmailConnection::EmailConnection.
         *  @param account     passed to the according constructor
         *  @param properties  passed to the according constructor and key @c "Connection::type" is used to decide which
         *  constructor to call
         */
        GenericConnection(Account *account, Properties properties);
        ~GenericConnection();

        virtual Message *composeMessage(String content, Contact *contact)  override;

        // interface of Connection
        /// @returns @c GenericConnection::impl->getProperties
        virtual Properties getProperties()  override;

        /// wrapper for @c GenericConnection::impl->setProperties  @param properties  passed to wrapped function
        virtual void setProperties(Properties properties)  override;

        /// @returns @c GenericConnection::impl->getType
        virtual String getType()  const override;

        /// wrapper for @c GenericConnection::impl->sendMessage
        /// @param message  passed to wrapped function
        /// @param block    passed to wrapped function
        virtual void sendMessage(Message *message, bool block)  override;

        /// wrapper for @c GenericConnection::impl->fetchIncomingMessages
        /// @param block  passed to wrapped function
        virtual void fetchIncomingMessages(bool block)  override;

        /// wrapper for @c GenericConnection::impl->test
        /// @param block  passed to wrapped function
        virtual void test(bool block)  override;
};
