# Mailchat

a chat application that communicates over email.

To seek, receive and impart information and ideas through any media and regardless of frontiers is a human right (yes, it is, look it up: [Art. 19 UDHR](https://www.un.org/en/about-us/universal-declaration-of-human-rights)). There should be not frontiers due to incompatible instant messengers, censoring governments and spying criminals.

**Modern instant messengers (IM)**

- spy on their users (e.g. WhatsApp, Facebook Messenger)
- use unsecure channels (e.g. Telegram)
- are full of bugs (e.g. Signal, Element)
- are not compatible with each other (all IM apps on the market)

Ironically, there is an IM protocol that is fast, widely adopted, mature, secure and decentralized: email. Of course, that is not as lucrative for IM producers...

**Mailchat** does not add yet another stupid IM protocol and it is 100 % compatible with all the other email clients that already exist because it just uses email (IMAP, SMTP).


### VFAQ

Is this application fast enough for IM chat?
> Yes, messages will be delivered in well under one second. If you experience slower delivery, check your internet connection, ask you internet service provider or your mail service provider.

Will I see my mailbox flooded with messages as soon as I use a regular mail client again?
> No, Mailchat keeps tidiness by sorting your chat messages into extra mail folders.
> This feature will be implemented before the first full release of the application, see #16.

Is chatting over this application secure?
> Yes, all messages are end-to-end encrypted. If your chat partner does not support end-to-end encryption (e.g. they do not use Mailchat but a conventional mail client where they forgot to turn on encryption), you will see a warning when sending a message to them.

Do my chat partners need to install Mailchat as well?
> No, they can continue to use the mail client they are currently using. Mailchat uses regular email, so that is all compatible.

### Installation

TODO: add download links here as soon as CI works

### Build from source

You can easily build the application from source.

Tested on Debian 11:

```sh
# clone project
git clone https://gitlab.com/Ilka.Schulz/mailchat.git
cd mailchat

# install dependencies
git submodule update --init
./configure

# build
cmake .
cmake --build . --target all --parallel 4

# pack
upx mailchat
```

You can then run it:

```sh
./mailchat
```

There is no additional configuration or anything needed.

### Copyright

Mailchat Copyright (C) 2021 Ilka Schulz<br>
This program comes with ABSOLUTELY NO WARRANTY; for details, see the [License file](https://gitlab.com/Ilka.Schulz/mailchat/-/blob/main/COPYING).<br>
This is free software, and you are welcome to redistribute it under certain conditions; see the [License file](https://gitlab.com/Ilka.Schulz/mailchat/-/blob/main/COPYING) for details.


### Contact

Please add **bugs** and **feature requests** to the [issue tracker](https://gitlab.com/Ilka.Schulz/mailchat/-/issues).

There is currently no forum for **questions**.

You can contact the author under mailchat@schulz.com.de.
