# Markdown sample

This is a markdown test.
This sentence is on the same paragraph as the previous one.

This sentence is on a new paragraph.

## Quotes

> some quote
> over two lines
and some additional text

and

> some quote
>> some indented quote
> some quote again
>> some indented quote
> > in two lines
> some quote again


## Links

[This link](https://www.duckduckgo.com) goes to DuckDuckGo and [this link](https://example.com) goes to example.com.

This <a href='https://www.google.com'>other link</a> goes to Google.


[some
link to Duck<br>
Duck
Go](https:
//
duckduckgo.com)

## Emphasis

Some **bold**, more __bold__, some *italic*, more _italic_, some ~~striketrough~~.

Not modified: * a*, __ b__, ~~c ~~.

## Lists

- root
	1. first
	100. second
	1. This is one
	item.
		- further indented
		- bla
	1. fourth
		- This is one
		  item.


not a list:
- bla


## Pictures

See this ![picture](file:///home/user/mailchat/res/icon/icon64.png).

## Code

```
#!/bin/bash
some plain text
```

```text
#!/bin/bash
some plain text
```

bla

```bash
#!/bin/bash
some plain text
```

```python
#!/bin/bash
some plain text
```

## References

See [Links](#links) and [Pictures](#pictures).

## Crashes
some text
### some caption
some more text and now:
```code```text`code`text
