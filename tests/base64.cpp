/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

int main(int argc, char **argv)
{
    const std::string s = "Hallo Welt!";
    {
        // input stream
        std::cout<<">";
            std::stringstream ss(s);
                base64encodebuf encbuf(ss.rdbuf());
                    base64decodebuf decbuf(&encbuf);
                        std::istream i(&decbuf);
                        while(i)  {
                            char c;
                            i.read(&c, 1);
                            if(i)
                                std::cout << c;
                        }
        std::cout<<"<"<<std::endl;
    }

    {
        // output stream
        std::cout<<">";
            base64decodebuf decbuf(std::cout.rdbuf());
                base64encodebuf encbuf(&decbuf);
                    std::ostream os(&encbuf);
                    os.write(s.c_str(), s.size());
                encbuf.sync();
            decbuf.sync();
        std::cout<<"<"<<std::endl;
    }
    return 0;
}
