/*
 * Mailchat - a chat application that communicates over email
 * Copyright (C) 2021 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */



void smtpTestConnection()  {
    Account account(true);
    account.setEmail("sender@senderhost");

    SmtpConnection smtpConnection(&account);
    smtpConnection.setHost("senderhost");
    smtpConnection.setUsername("senderusername");
    smtpConnection.setPassword("senderpassword");

    smtpConnection.test();
}

void smtpSendMail()  {
    Account *account = new Account();
        account->setName("sender name");
        account->setEmail("sender@senderhost");
    SmtpConnection *connection = new SmtpConnection(account);
        connection->setHost("senderhost");
        connection->setUsername("senderusername");
        connection->setPassword("senderpassword");
    Contact *contact = new Contact();
        contact->setAccount(account);
        contact->setName("recipient name");
        contact->setEmail("recipient@recipienthost");
    OutgoingMessage *message = new OutgoingMessage();
        message->setContent("Hello World!");

    connection->sendMessage(contact, message);
}
