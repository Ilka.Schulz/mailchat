# Message Format Overview

Mailchat supports the following message formats:

- [plain text](#plain-text) (recommended): simple text message, supporting emojis and light formatting
- [Markdown](#markdown): formatted text message including pictures
- [HTML](#html): heavily formatted message with all kinds of media supported – basically an entire web page


## Plain Text

TODO: documentation


## Markdown

Markdown is not yet supported.



## HTML

HTML is not yet supported.
